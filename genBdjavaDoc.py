
# -*- coding: utf-8 -*-
# -*- encoding: utf-8 -*-

import re
import sys
import json

methods = []

def gen_snippets(java_file):

    global methods        

    if '\\' in java_file:
        work_class = java_file.split('\\')[-1].split('.')[0]
    else:
        work_class = java_file.split('.')[0]

    try:
        with open(java_file,'rb') as f:
            data = f.read().decode('utf-8')
        f.close()
    except Exception as e:
        data = f.read().decode('cp1252').encode('utf-8')
        print(s)
        print(java_file)
    f.close()

    add = False

    pattern = re.compile("()")
    
    abstrais = []
    abstral = []
    comentario = []
    aux_linha = ""
    eh_assinatura = False
    level = []
    for linha in data.split("\n"):
        linha_striped = linha.strip()

        if eh_assinatura:
            linha_striped = aux_linha + " " + linha_striped

        if len(linha_striped) == 0:
            continue

        if linha_striped.startswith("/*"):
            comentario.append(linha_striped)
            add = True
            continue

        if linha_striped.endswith("*/"):
            comentario.append(linha_striped)
            add = False
            level = []
            continue

        eh_assinatura = False

        if linha_striped.startswith('public static void') or linha_striped.startswith('public static boolean') or linha_striped.startswith('public static Bds'):
            eh_assinatura = True

        if linha_striped.startswith('static public void') or linha_striped.startswith('static public boolean') or linha_striped.startswith('static public Bds'):
            eh_assinatura = True

        if eh_assinatura:
            
            if not linha_striped.endswith(';') and linha_striped.endswith('{'):
                abstrais.append({"comentario": "\n".join(comentario), "assinatura": linha_striped})
                comentario = []
                add = False
                aux_linha = ""
                continue

            aux_linha = linha_striped

        if add:
            if linha_striped.endswith("}") and len(level) > 0:
                level.pop()
            comentario.append(''.join(level) + linha_striped)
            if linha_striped.endswith("{"):
                level.append('¨')
        else:
            comentario = []


    return {'classe': work_class, 'abstrais': abstrais}

import glob, os

dirs = ["D:\\eclipse-workspace\\selenium\\src\\br\\com\\epsoft\\selenium","d:\\eclipse-workspace\\bdjava\\src\\main\\java\\br\\com\\epsoft\\bdjava"]

all_completions = []
for d in dirs:
    #print(d)
    os.chdir(d)
    tree = []
    for file in glob.glob("*.java"):
        #print(file)
        ret = gen_snippets(file)
        if ret == 0:
            continue
        all_completions.append(ret)
    for classe in all_completions:
        rst_data = []
        rst_data.append('\n')
        rst_data.append(classe['classe'])
        rst_data.append('\n')
        equals = ''

        for e in range(len(classe['classe'])):
            equals+= "="
        
        rst_data.append(equals)
        rst_data.append('\n')
        tem_funcao = False
        for data in classe['abstrais']:

            metodo = data['assinatura'].split()[3]

            if 'Bds' in data['assinatura']:
                retype = 'Bds'

            if 'void' in data['assinatura']:
                retype = 'void'

            if 'boolean' in data['assinatura']:
                retype = 'boolean'


            if '(' in metodo:
                metodo = metodo.split('(')[0]

            if metodo == 'main':
                continue

            if len(data['assinatura'].split("(")) < 2:
                continue

            lista_argumentos = data['assinatura'].split("(")[1]

            lista_argumentos = lista_argumentos.split(")")[0].split(',')
            args = []
            invalido = False

            for item_argumento in lista_argumentos:
                if len(item_argumento) == 0:
                    continue
                argumentos = item_argumento.strip().split(' ')
                if len(argumentos) < 2:
                    invalido = True
                    continue
                if 'Bds' in argumentos[0]  or 'String' in argumentos[0]:
                    args.append(argumentos[1])
                else:
                    invalido = True
                    break

            if invalido:
                continue

            args_rst = ["\n\t:param " + a.strip() +":" for a in args]

            descricao = data['comentario'].replace("/*","").replace("*/","").replace("*","")

            rst_data.append('.. function:: ' + classe['classe'] + "." + metodo + " " + " ".join(args) + "\n")
            tem_funcao = True
            rst_data.append('\n')
            tem_param = False

            if len(descricao) > 0:
                if '@param' in descricao:
                    descricao = descricao.replace("@param","\n\t:param ")
                    tem_param = True
                
                descricao = descricao.replace("@return","\n\n\t:rtype: " + retype + " ")
                descricao = descricao.replace("@author","\n\n\t:author: ")
                descricao = descricao.replace("@see","\n\n\t:see: ")
                descricao = descricao.replace("@throws","\n\n\t:throws: ")
                descricao = descricao.replace("void  void","void")
                descricao = descricao.replace("boolean  boolean","boolean")
                descricao = descricao.replace("Bds  Bds","Bds")
                
            descricao_rst = []

            for linha in descricao.split("\n"):
                linha_striped = linha.strip()
                linha_striped.replace('¨', '\t')
                if ':param' in linha_striped:
                    splited = linha_striped.split(" ")
                    linha_striped = "\n\n\t" + " ".join(splited[0:3]) + ": " + " ".join(splited[3::])

                if ':retype' in linha_striped:
                    splited = linha_striped.split(" ")
                    linha_striped = "\n\t" + " ".join(splited[0:2])

                if linha_striped.startswith("-"):
                    linha_striped = linha_striped[1::]

                descricao_rst.append(linha_striped)

            descricao_rst_txt = "\n\t:comentário: " + "\n\t".join(descricao_rst).replace("::",":")

            rst_data.append(descricao_rst_txt)
            
            if not tem_param:
                rst_data.append("\n\t".join(args_rst))

            rst_data.append('\n')
            rst_data.append('\n')

        if not tem_funcao:
            continue

        rst_file_path = 'D:\\repositorio\\superclient-docs\\source\\sintax\\' + classe['classe'] +'.rst'
        tree.append(classe['classe'])
        output_file = open(rst_file_path,'w',99999,"utf-8")

        try:
            str_rst_data = re.sub(r"(:\n\t)(\w)", r": \2", "".join(rst_data))
            str_rst_data = re.sub(r"(: \n\t|:\n\t])(\w)", r": \2", str_rst_data)
            str_rst_data = re.sub(r"(:param  [\w]*: )(\-)", r"\1", str_rst_data)
            str_rst_data = re.sub(r"\t ", r"\t", str_rst_data)
            str_rst_data = re.sub(r"(\t)([^:])", r"\t\t\2", str_rst_data)
            str_rst_data = re.sub(r"(:\n\t+|: \n\t+)(\w)", r": \2", str_rst_data)

            output_file.write(str_rst_data)
            output_file.close()
        except Exception as e:
            print(e)
            print("deu ruim")
            print(classe['classe'])

    



print("\t" + "\n\t".join(tree))