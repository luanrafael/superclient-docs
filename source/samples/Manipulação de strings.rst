Manipulação de strings e listas
===============================


troca
-----

.. function:: troca nomeDaVariavel '-' ';'


:Comentário:
	Este comando, quando executado, troca os caracteres indicados de uma string pelo segundo caracter dado como argumento. Neste caso ele trocará os '-' por ';'.


maiuscula
---------

.. function:: maiuscula nomeDaVariavel


:Comentário:
	Este comando, quando executado, torna o texto da variável em caixa alta.


minuscula
---------

.. function:: minuscula nomeDaVariavel


:Comentário:
	Este comando, quando executado, torna o texto da variável em minúsculo.


maimin
------

.. function:: maimin nomeDaVariavel


:Comentário:
	Este comando, quando executado, torna a primeira letra de cada palavra do texto da variável em maiúscula.


esq
---

.. function:: esq nomeDaVariavel1 nomeDaVariavel2 5


:Comentário:
	Este comando, quando executado, seleciona os caracteres da esquerda da primeira varíavel, e os copia para a segunda variável dada como argumento. O número dado como último argumento é referente à quatidade de caracteres a serem copiados.


dir
---

.. function:: dir nomeDaVariavel1 nomeDaVariavel2 5


:Comentário:
	Este comando, quando executado, seleciona os caracteres da direita da primeira varíavel, e os copia para a segunda variável dada como argumento. O número dado como último argumento é referente à quatidade de caracteres a serem copiados.


poe
---

.. function:: poe nomeDaVariavel '-' 5 6


:Comentário:
	Este comando, quando executado, insere, na variável indicada, o caracter indicado, na posição indicada (neste caso '5'), quantas vezes for necessário (neste caso '6').


imeio
-----

.. function:: imeio nomeDaVariavel 'stringIndicada' 6


:Comentário:
	Este comando, quando executado, insere na variável indicada, a string indicada, na posição indicada (neste caso '6').


retchar
-------

.. function:: retchar nomeDaVariavel '-'


:Comentário:
	Este comando, quando executado, retira o caracter indicado da variavel, ou, os troca por outro, como indicado abaixo.

.. function:: retchar nomeDaVariavel '-' ';'


compchar
--------

.. function:: compchar nomeDaVariavel '\n'


:Comentário:
	Este comando, quando executado, compacta os caracteres específicos de uma string.


semAcentos
----------

.. function:: semAcentos nomeDaVariavel


:Comentário:
	Este comando, quando executado, retira os acentos do texto da variável.


semAcentosM
-----------

.. function:: semAcentosM nomeDaVariavel


:Comentário:
	Este comando, quando executado, retira os acentos do texto da variável e torna as letras maiúsculas.


soLetrasNumeros
---------------

.. function:: soLetrasNumeros nomeDaVariavel


:Comentário:
	Este comando, quando executado, retira os acentos e caracteres especiais (incluindo espaços em branco) do texto da variável.


contem
------

.. code-block:: python

	if nomeDaVariavel contem 'string'{
		mensagem 'Contem string'
	}


:Comentário:
	Este comando, quando executado, verifica se há a string indicada dentro do texto da variável


parte
-----

.. function:: parte 'exemplo,de,string' nomeDaVariavel ',' 3


:Comentário:
	Este comando, quando executado, divide a string indicada de acordo com o delimitador indicado (neste caso ','), pega a parte indicada (neste caso '3') e insere na variável de resposta.


novaparte
---------

.. function:: novaparte nomeDaVariavel1 nomeDaVariavel2 '\n'


:Comentário:
	Este comando, quando executado, adiciona o conteudo da segunda variável, na primeira variável, separado pelo terceiro argumento (neste caso '\n').


qtparte
-------

.. function:: qtparte nomeDaVariavel ',' variavelDeRetorno


:Comentário:
	Este comando, quando executado, conta a quantidade de partes separadas pelo caracter indicado (neste caso ',') contidas no texto da variável indicada, e retorna esse número para a variávelDeRetorno.


iparte
------

.. function:: iparte nomeDaVariavel 'stringAInserir' ',' 2


:Comentário:
	Este comando, quando executado, insere a string indicada, na parte indica (neste caso '2') da variável indicada, considerando o separador dado como argumento (neste caso ',').


eparte
------

.. function:: eparte 'string,maior,a,ser,consultada' 'a' ',' retorno


:Comentário:
	Este comando, quando executado, verifica se a string indicada(a) existe na string maior, considerando-se o separador dado como argumento (neste caso ','), e retorna a posição que a mesma se encontra.


cparte
------

.. function:: cparte nomeDaVariavel 'stringAConsultar' ',' 2 variavelRetorno1 variavelRetorno2


:Comentário:
	Este comando, quando executado, verifica se a string indicada é contida pelo por uma segunda string, na posição indicada (neste caso '2'), considerando-se o separador dado como argumento (neste caso ','), e retorna a string completa da consulta como primeiro retorno e a posição em que a mesma está contida como retorno 2.


dparte
------

.. function:: dparte nomeDaVariavel ',' 2


:Comentário:
	Este comando, quando executado, retira uma determinada parte (neste caso '2') do texto da variavel dada, considerando-se o separador dado como argumento (neste caso ',')


setvalig
--------

.. function:: setvalig nomeDaLista 'Valor1' 'chave' ','


:Comentário:
	Este comando, quando executado, coloca valores na lista indicada, considerando-se a chave dada como argumento, adicionando como separador o argumento (neste caso, ',').


getvalig
--------

.. function:: getvalig nomeDaLista variavelRetorno 'chave' ','


:Comentário:
	Este comando, quando executado, pesquisa na lista indicada o valor dado (neste caso 'chave') e retorna na variavel de retorno o valor da sua correspondência, considerando-se o separador (neste caso, ','), e caso você não dê um argumento como separador, ele considera como default o ';'.


M.sort
------

.. function:: M.sort tabela #coluna delColuna delLinha


:Comentário:
	Este comando, quando executado, ordena uma matriz a partir de uma determinada coluna


monta
-----

.. function:: monta lista_1 dados_retorno '^'

:Comentário:
	Este comando, quando executado, monta uma string com os dados da lista_1 dada como argumento separada pelo '^', e a insere na variavel dados_retorno.


numerico
--------

.. function:: if numerico '123' { exib 'É numérico' }

:Comentário:
	Este comando, quando executado, retorna true caso a variavel dada como argumento seja numérica ou esteja VAZIA.


separa
------

.. function:: separa 'variavel1,variavel2,variavel3' lista '|'

:Comentário:
	Este comando, separa os itens de uma lista dada, considerando-se o separador dado, nas variaveis indicadas entre aspas.