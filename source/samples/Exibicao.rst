Exibição de mensagens no console
================================

limpatela
---------

.. function:: limpatela


	:Comentário: 
	Ao ser executado, limpa o console.


exib
----
.. function:: exib 'Esta mensagem será escrita no console.'


	:Comentário: 
	Exibe itens como variáveis e strings dentro das aspas simples.


mensagem
--------
.. function:: mensagem 'Haverá uma quebra de linha e a escrita dessa mensagem'


	:Comentário: 
	Faz quebra de linha e exibe variáveis e strings dentro das aspas simples.


checaOk
-------
.. function:: checaOk 'Esta mensagem aparecerá em um pop-up com as opções Ok e Cancelar.'

	:Comentário: 
	Se o usuário clicar em Ok, a checaOk retorna true, caso contrário a mesma retorna false.


alerta
------
.. function:: Alerta.noCkOk=false;(nome)


	:comentário: :param nome:

.. function:: Alerta.ck([]args)


	:comentário: :param []args:

.. function:: Alerta.msg(msg)


	:comentário: :param msg:

.. function:: Alerta.msg(msg,tout)


	:comentário: :param msg:
	:param tout:

.. function:: Alerta.mensagem(msg)


	:comentário: :param msg:

.. function:: Alerta.mensagem(msg)


	:comentário: :param msg:

.. function:: Alerta.mensagem(msg,tout)


	:comentário: :param msg:
	:param tout:

.. function:: Alerta.cmdAceito()


	:comentário: 