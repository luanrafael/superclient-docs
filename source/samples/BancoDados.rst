Banco de dados
==============


conectaBD
---------

.. function:: conectaBD 'NomeDoBD'


:Comentário:
	Este comando, quando executado, cria a conexão com o banco de dados, configurando todas as variáveis nomeadas com o sufixo dado como argumento (neste caso, NomeDoBD). Ele sempre vai criar uma variável chamada con, que será posteriormente utilizada por comandos como select e insUpd, e, caso você queira nomeá-la de outra forma, deve adicionar ao nome do banco de dados uma string contendo o sinal '-' e o nome desejado na varíável, resultando em uma variável com o prefixo con e o sufixo definido por você, como no exemplo abaixo:

.. function:: conectaBD 'MeuBanco-NomeDaConexao'

	Resultando na variável de conexão chamada, neste exemplo, de 'conNomeDaConexao'.


select
------

.. function:: select con variavelRetorno variavelArgSQL 1 50


:Comentário:
	Este comando, quando executado, retorna à variávelRetorno uma matriz com o resultado da pesquisa. A instrução SQL para o banco de dados está contida na variavelArgSQL, e os números ('1 50') representam os dados a serem captados, neste caso do 1º dado ao 50º dado. O argumento 'con' é referente à variável criada para a conexão criada com o banco de dados anteriormente.


insUpd
------

.. function:: insUpd con variavelRetorno 'nomeDaTabela' listaDeCampos variavelArgSQL 'n'


:Comentário:
	Este comando, quando executado, verifica se existe o registro na tabela indicada e, se não existir, insere o dado indicado (neste caso, listaDeCampos). O ultimo argumento (neste caso, variavelArgSQL), pode definir por exemplo, um 'where' para refinamento de busca. Coloque um último argumento como 'n' para não alterar campo com null.


delete
------

.. function:: delete con variavelRetorno 'nomeDaTabela' variavelArgSQL


:Comentário:
	Este comando, quando executado, deleta uma entrada na tabela indicada, onde a variavelArgSQL pode, por exemplo, conter um 'where id=0' para refinar a busca.


fechaBd
-------

.. function:: fechaBd con


:Comentário:
	Este comando, quando executado, fecha a conexão dada como argumento.


insert
------

.. function:: insert con variavelRetorno 'nomeDaTabela' 'listaDeValores'


:Comentário:
	Este comando, quando executado, insere o valor indicado na tabela dada como argumento.


Xsql
----

.. function:: Xsql variavelRetorno


:Comentário:
	Este comando, quando executado, retorna em variavelRetorno a solução da pesquisa.


update
------

.. function:: update


:Comentário:
	Este comando, ainda não tem descrição.


criacampo
---------

.. function:: G.criacampo ("`idFaqr`int(11)NOTNULLAUTO_INCREMENT,", "me=`id`int(11)NOTNULLAUTO_INCREMENT,;co=campo;sq=`id`int(11)NOTNULLAUTO_INCREMENT,;no=`id`int(11)NOTNULLAUTO_INCREMENT,;ta=15;sm=");


:Comentário:
	O comando criacampo pode ser usado para criar tabelas em um banco de dados. Para tal é necessário copiar para a área de transferencia as definições dos campos como o exemplo abaixo:

.. code-block:: python

	`idFaqr` int(11) NOT NULL AUTO_INCREMENT,
	`tstp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`idCli` int(11) NOT NULL,
	`ref` varchar(100) DEFAULT NULL,
	`objetivo` varchar(255) DEFAULT NULL,
	`resp` varchar(8000) NOT NULL,
	`contexto` varchar(100) DEFAULT NULL,
	`abs` varchar(8000) DEFAULT NULL,
	`template` varchar(100) DEFAULT NULL,

:Comentário:
	Após isso, é necessário abrir o configurador da árvore de menus (CTRL + CTRL, CTRL + E) na opção 0 para compilar os abstrais necessários e após isso clicar na opção Configurador > Cria campos Java.
	Na tela que se abrirá teremos as opções abaixo:
		Sufixo, é um conjunto de letras que será colocado no final dos nomes das variáveis;
		Tabela é o nome da tabela;
		E var tabela é o nome da lista a ser criada;
	Após clicar em OK será copiado para a àrea de transferencia o código para a criação da tabela, como o exemplificado abaixo:

.. code-block:: python
	
	Bds `idFaqr`int(11)NOTNULLAUTO_INCREMENT,exemplo;
	Bds `tstp`timestampNOTNULLDEFAULTCURRENT_TIMESTAMPONUPDATECURRENT_TIMESTAMP,exemplo;
	Bds `idCli`int(11)NOTNULL,exemplo;
	Bds `ref`varchar(100)DEFAULTNULL,exemplo;
	Bds `objetivo`varchar(255)DEFAULTNULL,exemplo;
	Bds `resp`varchar(8000)NOTNULL,exemplo;
	Bds `contexto`varchar(100)DEFAULTNULL,exemplo;
	Bds `abs`varchar(8000)DEFAULTNULL,exemplo;
	Bds `template`varchar(100)DEFAULTNULL,exemplo;
	Bds listaexemplo=B.getvar("listaexemplo");

	G.criacampo ("`idFaqr`int(11)NOTNULLAUTO_INCREMENT,exemplo", "me=`idFaqr`int(11)NOTNULLAUTO_INCREMENT,;co=campo;sq=`idFaqr`int(11)NOTNULLAUTO_INCREMENT,;no=`idFaqr`int(11)NOTNULLAUTO_INCREMENT,;ta=15;sm=");
	G.criacampo ("`tstp`timestampNOTNULLDEFAULTCURRENT_TIMESTAMPONUPDATECURRENT_TIMESTAMP,exemplo", "me=`tstp`timestampNOTNULLDEFAULTCURRENT_TIMESTAMPONUPDATECURRENT_TIMESTAMP,;co=campo;sq=`tstp`timestampNOTNULLDEFAULTCURRENT_TIMESTAMPONUPDATECURRENT_TIMESTAMP,;no=`tstp`timestampNOTNULLDEFAULTCURRENT_TIMESTAMPONUPDATECURRENT_TIMESTAMP,;ta=15;sm=");
	G.criacampo ("`idCli`int(11)NOTNULL,exemplo", "me=`idCli`int(11)NOTNULL,;co=campo;sq=`idCli`int(11)NOTNULL,;no=`idCli`int(11)NOTNULL,;ta=15;sm=");
	G.criacampo ("`ref`varchar(100)DEFAULTNULL,exemplo", "me=`ref`varchar(100)DEFAULTNULL,;co=campo;sq=`ref`varchar(100)DEFAULTNULL,;no=`ref`varchar(100)DEFAULTNULL,;ta=15;sm=");
	G.criacampo ("`objetivo`varchar(255)DEFAULTNULL,exemplo", "me=`objetivo`varchar(255)DEFAULTNULL,;co=campo;sq=`objetivo`varchar(255)DEFAULTNULL,;no=`objetivo`varchar(255)DEFAULTNULL,;ta=15;sm=");
	G.criacampo ("`resp`varchar(8000)NOTNULL,exemplo", "me=`resp`varchar(8000)NOTNULL,;co=campo;sq=`resp`varchar(8000)NOTNULL,;no=`resp`varchar(8000)NOTNULL,;ta=15;sm=");
	G.criacampo ("`contexto`varchar(100)DEFAULTNULL,exemplo", "me=`contexto`varchar(100)DEFAULTNULL,;co=campo;sq=`contexto`varchar(100)DEFAULTNULL,;no=`contexto`varchar(100)DEFAULTNULL,;ta=15;sm=");
	G.criacampo ("`abs`varchar(8000)DEFAULTNULL,exemplo", "me=`abs`varchar(8000)DEFAULTNULL,;co=campo;sq=`abs`varchar(8000)DEFAULTNULL,;no=`abs`varchar(8000)DEFAULTNULL,;ta=15;sm=");
	G.criacampo ("`template`varchar(100)DEFAULTNULL,exemplo", "me=`template`varchar(100)DEFAULTNULL,;co=campo;sq=`template`varchar(100)DEFAULTNULL,;no=`template`varchar(100)DEFAULTNULL,;ta=15;sm=");

	f.listaexemplo.s("`idFaqr`int(11)NOTNULLAUTO_INCREMENT,exemplo,`tstp`timestampNOTNULLDEFAULTCURRENT_TIMESTAMPONUPDATECURRENT_TIMESTAMP,exemplo,`idCli`int(11)NOTNULL,exemplo,`ref`varchar(100)DEFAULTNULL,exemplo,`objetivo`varchar(255)DEFAULTNULL,exemplo,`resp`varchar(8000)NOTNULL,exemplo,`contexto`varchar(100)DEFAULTNULL,exemplo,`abs`varchar(8000)DEFAULTNULL,exemplo,`template`varchar(100)DEFAULTNULL,exemplo");

	f.`idFaqr`int(11)NOTNULLAUTO_INCREMENT,exemplo=B.getvar("`idFaqr`int(11)NOTNULLAUTO_INCREMENT,exemplo");
	f.`tstp`timestampNOTNULLDEFAULTCURRENT_TIMESTAMPONUPDATECURRENT_TIMESTAMP,exemplo=B.getvar("`tstp`timestampNOTNULLDEFAULTCURRENT_TIMESTAMPONUPDATECURRENT_TIMESTAMP,exemplo");
	f.`idCli`int(11)NOTNULL,exemplo=B.getvar("`idCli`int(11)NOTNULL,exemplo");
	f.`ref`varchar(100)DEFAULTNULL,exemplo=B.getvar("`ref`varchar(100)DEFAULTNULL,exemplo");
	f.`objetivo`varchar(255)DEFAULTNULL,exemplo=B.getvar("`objetivo`varchar(255)DEFAULTNULL,exemplo");
	f.`resp`varchar(8000)NOTNULL,exemplo=B.getvar("`resp`varchar(8000)NOTNULL,exemplo");
	f.`contexto`varchar(100)DEFAULTNULL,exemplo=B.getvar("`contexto`varchar(100)DEFAULTNULL,exemplo");
	f.`abs`varchar(8000)DEFAULTNULL,exemplo=B.getvar("`abs`varchar(8000)DEFAULTNULL,exemplo");
	f.`template`varchar(100)DEFAULTNULL,exemplo=B.getvar("`template`varchar(100)DEFAULTNULL,exemplo");

