Compilação e processamento de scripts
=====================================

cp
---

.. function:: cp '!nomeDoArquivoAbs'


	:Comentário:
	Compila e roda o arquivo *.abs.



proc
----

.. function:: proc '!nomeDoScriptFig'


	:Comentário:
	Roda o script *.fig.