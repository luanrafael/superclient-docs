Tela
====


tela
----

.. function:: tela nomeDeLista 'Nome da Janela' 500 500 10 10


:Comentário:
	Este comando cria uma janela com os campos contidos em uma lista definida previamente, com o nome definido por uma string, e seu tamanho definido pelos números a seguir (neste caso '10 10'), e a sua posição (neste caso '500 500').


criacampo
---------

.. code-block:: python

	criacampo 'nomeDoCampo' 'me=<label>;co=<tpcampo>;px=<posx>;py=<posy>;lx=<tamx>;ly=<tamy>;cs=<nomeAbs>;cr=<criterio>;at=<atalho>'


:Comentário:
	Este comando cria um campo.

.. code-block:: python

	Lista de atributos:
		me=NomeLabel - Label que aparece na tela;
		px=25 - Posição x na tela;
		py=25 - Posição y na tela;
		lx=25 - Largura da janela;
		ly=25 - Altura da janela;
		cr=N - Critério apenas numéricos;
		cr=U - Critério apenas maiúsculos;
		cr=B - Critério de campo não alterável;
		at=teclaDeAtalho - Atalho;
		sq=argumento SQL - Conexão de campo com banco de dados;
		cs=nomeDeMetodo - Ligação de campo ou botão com método;
		co=combo/radio/tabela/botao/checkbox - Definição de tipo de campo;
		op=nomeDeOpcoes - Opções para combos ou campos de tabelas por exemplo;


:Comentário:
	Exemplo de criação de um botão que chama um método chamado csProx:
.. code-block:: python

	criacampo 'btnprox' 'px=446;py=26;ly=34;lx=100;co=botao;me=Próximo; cs=csProx'


oculta
------

.. function:: oculta nomeDeLista


:Comentário:
	Este comando, quando executado, oculta a tela passada como argumento.


zeralista
---------

.. function:: zeralista nomeDeLista


:Comentário:
	Este comando, quando executado, zera os campos da tela indicada. É comumente utilizado o comando getTxt após o uso do zeralista para de fato zerar os campos.


setTxt
------

.. function:: setTxt nomeDeCampo nomeDeLista


:Comentário:
	Este comando, quando executado, pode preencher automaticamente os campos de uma tela.


getTxt
------

.. function:: getTxt nomeDeCampo nomeDeLista


:Comentário:
	Este comando, quando executado, pode coletar os dados de um campo.


foca
----

.. function:: foca indiceListaCampos nomeDeLista


:Comentário:
	Este comando, quando executado, dá foco em determinado campo, considerando-se o indice da lista de campos.


focaComp
--------

.. function:: focaComp nomeDeCampo nomeDeLista


:Comentário:
	Este comando, quando executado, dá foco em determinado campo


G.setParmsCampos
----------------

.. function:: G.setParmsCampos 'campo01,campo02' 'stringDeParamsCampos'


:Comentário:
	Este comando, quando executado, adiciona a string indicada (neste caso, 'stringDeParamsCampos') aos campos dados como argumento (neste caso, 'campo01,campo02'). Pode ser usado, por exemplo, para definir os campos de um combo box, como no exemplo abaixo, desde que usado com um setTxt após seu uso:

.. function:: G.setParmsCampos 'campo_nome,campo_sub' 'op=1,2,3,4,5'; setTxt lst


Andamento.msg
-------------

.. function:: Andamento.msg 'Mensagem acima do andamento.'

:Comentário:
	Este comando, quando executado, exibe uma pequena janela no canto inferior esquerdo com uma barra de progresso e uma mensagem.


Andamento.fecha
---------------

.. function:: Andamento.fecha

:Comentário:
	Este comando, quando executado, fecha a tela de andamento presente no momento de execução.