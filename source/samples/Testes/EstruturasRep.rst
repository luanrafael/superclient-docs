Estruturas de repetição
=======================

while
-----

.. function:: while ( nomeDeVariavel < 100 ) exib 'Esta é a ' nomeDeVariavel ' repetição.'


	:Comentário:
	Não se esqueca de incrementar seu contador e evitar um loop infinito.


for
---

.. function:: for nomeDeVariavel { exib 'Repetindo ' nomeDeVariavel ' vezes.'}


	:Comentário:
	Ocorreram repetições relativas ao número inteiro armazenado na variável.