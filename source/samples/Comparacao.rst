Métodos de comparação
=====================

tamanho
-------

.. function:: tamanho nomeDaVariavel


	:Comentário: 
	Retorna um inteiro representando o tamanho de uma string por exemplo. 
	Não pode ser usado diretamente em condicionais, antes, atribua o valor à uma variável.

contem
------

.. function:: if ( nomeDeVariavel contem '@') exib 'Tem @.'


	:Comentário:
	Retorna true caso haja o caracter na string e false caso não haja.



numerico
--------

.. function:: if ( numerico '1' ) exib 'É numérico.'


	:Comentário:
	Retorna true caso o argumento seja numérico e false caso não seja.



ig
---

.. function:: if ( ig nomeDaVariavel1 nomeDaVariavel2 ) exib 'É igual.'


	:Comentário:
	Retorna true caso as strings sejam iguais e false caso não sejam.



cmp
----

.. function:: cmp  nomeDaVariavel1 nomeDaVariavel2 operadorLógico


	:Comentário:
	Retorna true ou false de acordo com a comparação das strings, usando os operadores >, <, !=, :=, :>, :<, :!=.


