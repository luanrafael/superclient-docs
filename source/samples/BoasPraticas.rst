Manual de boas práticas
=======================

Nomeação e definição de variáveis, métodos e arquivos
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Variáveis
---------

	.. code-block:: python

		()mostrarExemplo arg_exemplo LOCAIS: variavel variavel_nome_composto

			faz variavel '35'
			faz variavel_nome_composto '25'

	:Comentário:
		Os nomes devem ser criados apenas com letras minúsculas, caso seja um nome composto, as palavras devem ser separadas pelo caractere "_". Dê preferência à discriminar as variáveis que serão usadas apenas num método específico na definição do método, e evite criar variáveis com nomes em idiomas diferentes.

Constantes
----------

	.. code-block:: python

		faz $CONSTANTE '35'
		faz $CONSTANTO_NOME_COMPOSTO '25'

	:Comentário:
		Valem as mesmas regras de variáveis, exceto pelo fato de deverem ser criadas com o nome completamente em caixa alta.

Métodos
-------

	.. code-block:: python

		()mostrarExemplo

			mensagem 'Nâo se esqueça de que o nome deve ser no infinitivo.'


	:Comentário:
		Os nomes devem começar com letras minúsculas, sendo criados em um único idioma e usando verbos no infinitivo. Caso o nome seja composto, as próximas palavras devem começar com letras maiúsculas.

Arquivos
--------

	.. code-block:: python

		BoasPraticas.abs

		Exemplo.fig

		ArvoreExemplo.scl

	:Comentário:
		Devem ser nomeados usando as regras de métodos, apenas modificando a primeira letra do arquivo para maiúscula.


Comentários
^^^^^^^^^^^

	.. code-block:: python

		//Este abstral faz tal coisa além de suprir tais necessidades do projeto

		//()mostrarExemplo 'Exemplo de método' [Este método exibe exemplos]
		()mostrarExemplo arg_exemplo LOCAIS: variavel variavel_nome_composto

			faz variavel '35'
			faz variavel_nome_composto '25'
	
	:Comentário:
		É interessante que se considere a criação de comentários no início de cada arquivo especificando o que ele deve e pode fazer.
		É uma boa prática comentar uma linha antes da definição de um método, como chamá-los, que parametros passar e qual a sua função.



Organização, indentação e legibilidade
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Indentação
----------

	.. code-block:: python

		if (ig string 'Exemplo') {
			mensagem string
		}
	
	:Comentário:
		A indentação deve ser feita usando-se tabulação. Sendo assim, o programador deve usar a tecla TAB abaixo da definição de um método, while, whilenot, if, ifnot e else. Isso melhora a qualidade do seu código.

Organização
-----------

	.. code-block:: python

		//()mostrarExemplo 'Exemplo de método' [Este método exibe exemplos]
		()mostrarExemplo arg_exemplo LOCAIS: variavel variavel_nome_composto

			faz variavel 35
			faz variavel_nome_composto 25
			faz string 'Exemplo'

			if (ig string 'Exemplo') {	
				mensagem string
			}

			while (variavel > variavel_nome_composto) {
				+ variavel_nome_composto
			}
	
	:Comentário:
		É bom para a organização do código que se use linhas em branco em algumas situações, como para separar seções de código distintas, antes e após blocos condicionais e de repetição como if, else, while e etc. É bom para organização do código também que se use duas linhas em branco após a finalização de um método.

Legibilidade
------------

	.. code-block:: python

		//()mostrarExemplo 'Exemplo de método' [Este método exibe exemplos]
		()mostrarExemplo arg_exemplo LOCAIS: variavel variavel_nome_composto

			faz variavel 35
			faz variavel_nome_composto 25
			faz string 'Exemplo'

			if (ig string 'Exemplo') {	
				mensagem string
			
			} else {
				mensagem 'Não'
				return 0
			}

			while (variavel > variavel_nome_composto) {
				+ variavel_nome_composto
			}

	
	:Comentário:
		Quanto à legibilidade de um código, é interessante que se use parenteses em condicionais, e espaços antes da abertura de chaves de ifs e whiles. 
		O else deve ser definido na mesma linha de fechamento de um if.

Logs
----
	
	:Comentário:
		O seguinte padrão de logs melhora a identificação de erros na execução dos métodos.


	.. code-block:: python

		log '[nomeDoMetodo - ERRO] Mensagem de erro desejada.'

		log '[nomeDoMetodo - ANDAMENTO] Mensagem de andamento desejada.'

		log '[nomeDoMetodo - SUCESSO] Mensagem de sucesso desejada.'


Nomeação de commits
^^^^^^^^^^^^^^^^^^^

	:Comentário:
		Os commits devem seguir a seguinte lógica:
		Iniciar com 'FIX - ' quando houver correções de erros;
		Iniciar com 'FEATURE - ' quando houver adição de recursos;
		Iniciar com 'RELEASE - ' quando for feito um lançamento de nova versão.