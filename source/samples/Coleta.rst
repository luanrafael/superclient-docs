Coleta de dados
===============

pegaDados
---------

.. function:: pegaDados 'nomeDaVariavel:Mensagem exibida na tela.'


:Comentário: 
	Esta função abre um pop-up com uma mensagem e um campo de recebimento de dados após o Ok do usuário, ela retorna os dados digitados. Pode ser usado para a coleta de vários dados de uma vez, como no exemplo abaixo:

.. function:: pegaDados 'nome:Digite seu nome,cpf:Digite seu CPF,email:Digite seu email'

data
----

.. function:: data variavelResposta 'b'

:Comentário:
	Este comando, quando executado, retorna à variavelResposta a data atual de acordo com a opção do último argumento, podendo esta ser:

.. code-block:: python

	'i' > 120605
	':' > 10:02:06
	'l' > 1338901368 (data para o número de segundos, usada para marcar dados únicos)
	'e' > 120605.100301
	'b' > 2012-06-05 10:03:16
	'e' > 120605.100301

Io.getWindowsUserName
---------------------

.. function:: Io.getWindowsUserName variavel_resp

:Comentário:
	Este comando, quando executado, pega o nome do usuário do Windows na máquina e o retorna na variável de resposta.


getScreen
---------

.. function:: getScreen tlarg talt

:Comentário:
	Este comando, quando executado, pega os dados de largura e altura da tela em pixels e os retorna nas variáveis indicadas, na primeira a largura e na segunda a altura. Nota-se que ele apenas retorna os valores da tela principal, não considerando outros monitores conectados ao computador.


IJmdk.pegaTxt
-------------

.. function:: IJmdk.pegaTxt 'Nome de janela aberta' variavel_resp 24 0

:Comentário:
	Este comando, quando executado, pega o texto da janela indicada pelo nome, e o retorna na variavel dada como argumento, considerando os indices passados como últimos argumentos. É bem usado previamente em um for para mapeamento da janela e posteriormente para efetivamente exetrair o texto desejado.