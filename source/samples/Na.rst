Na
===

Na.inic
-------

.. function:: Na.inic

:Comentário:
	Este comando, quando executado, inicia o JFX, devendo ser utilizado no inicio do script.

Na.emblema
----------

.. function:: Na.emblema 'caminho/para/a/imagem/emblema.png'

:Comentário:
	Este comando, quando executado, inicia o emblema de interação com o usuário a partir da imagem dada como argumento.


Na.toast
--------

.. function:: Na.toast 'Mensagem para usuário.' 5

:Comentário:
	Este comando, quando executado, exibe um toast(balão) com uma mensagem para o usuário partindo do emblema definido previamente. Pode ser usado dando um outro argumento, numérico, que define o tempo de exibição da mensagem em segundos.


Na.alerta
---------

.. function:: Na.alerta 'Mensagem a ser exibida para usuário em alerta'

:Comentário:
	Este comando, quando executado, exibe um pop-up com a mensagem indicada e um botão 'Ok' para dinspensá-lo.


Na.checaOk
----------

.. function:: Na.checaOk 'Mensagem a ser exibida para usuário em alerta'

:Comentário:
	Este comando, quando executado, exibe um pop-up com a mensagem indicada e botões 'Ok' e 'Cancelar'. Retorna true or false dependendo da resposta do usuário.


Na.balao
--------

.. function:: Na.balao 'Mensagem para usuário.' 5

:Comentário:
	Este comando, quando executado, exibe um balão com uma mensagem para o usuário partindo do emblema definido previamente. Pode ser usado dando um outro argumento, numérico, que define o tempo de exibição da mensagem em segundos.


Na.fechaEmblema
---------------

.. function:: Na.fechaEmblema

:Comentário:
	Este comando, quando executado, fecha o emblema previamente criado.