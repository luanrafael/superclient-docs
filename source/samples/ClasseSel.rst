Classe Sel
==========


Sel.cria
--------

.. function:: Sel.cria nomeDoHandler 'nomeDoNavegador'


:Comentário:
	Ao ser executado, este comando abre uma janela, neste exemplo, o navegador escolhido entre as aspas simples e usa o primeiro argumento como handler.

Sel.close
----------

.. function:: Sel.close nomeDoHandler


:Comentário:
	Ao ser executado, este comando fecha o handler escolhido, neste exemplo, o handler escolhido entre as aspas.



Sel.kill
-----------

.. function:: Sel.kill 'nomeDoNavegador'

:Comentário:
	Ao ser executado, este comando fecha o navegador escolhido, neste exemplo, o navegador escolhido entre as aspas.


Sel.get
-------

.. function:: Sel.get nomeDoHandler 'http://urlEscolhida.com'


:Comentário:
	Ao ser executado, este comando abre a url dada como argumento.



Sel.type
--------

.. function:: Sel.type nomeDoHandler 'seletorEscolhido' 'textoSeraDigitado'


:Comentário:
	Ao ser executado, este comando digita o 'textoSeraDigitado' no campo do 'seletorEscolhido'. Suas variantes são:

.. code-block:: python

	Sel.typeById
	Sel.typeByCss
	Sel.typeByClass
	Sel.typeByName
	Sel.typeByLink
	Sel.typeByPartialLink
	Sel.typeByTag
	Sel.typeByXpath




Sel.forEach
-----------

.. function:: Sel.forEach nomeDoHandler 'seletorEscolhido' 'getText'


:Comentário:
	Ao ser executado, este comando realiza o getText (ou outro comando) para cada item do seletor escolhido.


Sel.selectWindow
----------------

.. function:: Sel.selectWindow nomeDoHandler 1


:Comentário:
	Ao ser executado, este comando troca a aba do navegador.


Sel.waitForElementPresent
-------------------------

.. function:: Sel.waitForElementPresent nomeDoHandler 'seletorCss'


:Comentário:
	Ao ser executado, este comando aguarda a presensa do elemento CSS indicado, no handler indicado.


Sel.waitForPageLoad
-------------------

.. function:: Sel.waitForPageLoad nomeDoHandler 25


:Comentário:
	Ao ser executado, este comando aguarda o total carregamento da página para dar continuidade ao código.
	Pode ser usado com um segundo argumento, que representa o tempo de espera em segundos.


Sel.selectMainWindow
--------------------

.. function:: Sel.selectMainWindow nomeDoHandler


:Comentário:
	Ao ser executado, este comando seleciona o frame principal.


Sel.selectFrame
---------------

.. function:: Sel.selectFrame nomeDoHandler nomeOuIdDoFrame


:Comentário:
	Ao ser executado, este comando carrega o frame desejado para a manipulação.


Sel.isElementPresent
--------------------

.. function:: Sel.isElementPresent nomeDoHandler 'seletorCss'


:Comentário:
	Ao ser executado, este comando retorna true ou false de acordo com a presença do seletor indicado. Suas variantes são:

.. code-block:: python

	Sel.isElementPresentById
	Sel.isElementPresentByCss
	Sel.isElementPresentByClass
	Sel.isElementPresentByName
	Sel.isElementPresentByLink
	Sel.isElementPresentByPartialLink
	Sel.isElementPresentByTag
	Sel.isElementPresentByXpath


Sel.isElementVisible
--------------------

.. function:: Sel.isElementVisible nomeDoHndler 'seletorCss'


:Comentário:
	Ao ser executado, este comando retorna true ou false de acordo com a presença do seletor indicao. Suas variantes são:

.. code-block:: python

	Sel.isElementVisibleById
	Sel.isElementVisibleByCss
	Sel.isElementVisibleByClass
	Sel.isElementVisibleByName
	Sel.isElementVisibleByLink
	Sel.isElementVisibleByPartialLink
	Sel.isElementVisibleByTag
	Sel.isElementVisibleByXpath


Sel.clear
---------

.. function:: Sel.clear nomeDoHndler 'seletorCss'


:Comentário:
	Ao ser executado, este comando limpa os valores do seletor indicado de acordo com a presença do seletor, e retorna true caso tenha o feito e false caso não tenha conseguido. Suas variantes são:

.. code-block:: python

	Sel.clearById
	Sel.clearByCss
	Sel.clearByClass
	Sel.clearByName
	Sel.clearByLink
	Sel.clearByPartialLink
	Sel.clearByTag
	Sel.clearByXpath


Sel.focus
---------

.. function:: Sel.focus nomeDoHndler 'seletorCss'


:Comentário:
	Ao ser executado, este comando foca no elemento indicado. Suas variantes são:

.. code-block:: python

	Sel.focusById
	Sel.focusByCss
	Sel.focusByClass
	Sel.focusByName
	Sel.focusByLink
	Sel.focusByPartialLink
	Sel.focusByTag
	Sel.focusByXpath

Sel.click
---------------

.. function:: Sel.click nomeDoHndler 'seletorCss'


:Comentário:
	Ao ser executado, este comando executa um clique no elemento indicado. Suas variantes são:

.. code-block:: python

	Sel.clickById
	Sel.clickByCss
	Sel.clickByClass
	Sel.clickByName
	Sel.clickByLink
	Sel.clickByPartialLink
	Sel.clickByTag
	Sel.clickByXpath


Sel.doubleClick
---------------

.. function:: Sel.doubleClick nomeDoHndler 'seletorCss'


:Comentário:
	Ao ser executado, este comando executa um duplo clique no elemento indicado. Suas variantes são:

.. code-block:: python

	Sel.doubleClickById
	Sel.doubleClickByCss
	Sel.doubleClickByClass
	Sel.doubleClickByName
	Sel.doubleClickByLink
	Sel.doubleClickByPartialLink
	Sel.doubleClickByTag
	Sel.doubleClickByXpath


Sel.rightClick
--------------

.. function:: Sel.rightClick nomeDoHndler 'seletorCss'


:Comentário:
	Ao ser executado, este comando executa um clique direito(menu de contexto) no elemento indicado. Suas variantes são:

.. code-block:: python

	Sel.rightClickById
	Sel.rightClickByCss
	Sel.rightClickByClass
	Sel.rightClickByName
	Sel.rightClickByLink
	Sel.rightClickByPartialLink
	Sel.rightClickByTag
	Sel.rightClickByXpath


Sel.select
----------

.. function:: Sel.select nomeDoHndler 'seletorCss'


:Comentário:
	Ao ser executado, este comando seleciona o elemento indicado. Suas variantes são:

.. code-block:: python

	Sel.selectById
	Sel.selectByCss
	Sel.selectByClass
	Sel.selectByName
	Sel.selectByLink
	Sel.selectByPartialLink
	Sel.selectByTag
	Sel.selectByXpath

Sel.pause
---------

.. function:: Sel.pause 13520


:Comentário:
	Ao ser executado, este comando pausa a execução do código de acordo com o argumento dado em milissegundos.




Coleta de dados com Sel.
------------------------


Sel.getText
^^^^^^^^^^^

.. function:: Sel.getText nomeDoHandler 'seletorCss' nomeDaVariavel


:Comentário:
	Ao ser executado, este comando recebe o texto do seletor na variável dada como terceiro argumento.


Sel.getValue
^^^^^^^^^^^^

.. function:: Sel.getValue nomeDoHandler 'seletorCss' nomeDaVariavel


:Comentário:
	Ao ser executado, este comando recebe o valor do seletor na variável dada como terceiro argumento.

Sel.getAttribute
^^^^^^^^^^^^^^^^
.. function:: Sel.getAttribute nomeDoHandler seletorCss nomeAtributo variavelResposta
	

:Comentário:
	Ao ser executado, este comando recebe o valor do atributo na variável dada como quarto argumento.	
	



Sel.setAttribute
^^^^^^^^^^^^^^^^

.. function:: Sel.setAttribute nomeDoHandler selectorCss nomeAtributo variavelAserInserida


:Comentário:
	Ao ser executado, este comando atribui o valor da variavel no atributo dada como terceiro argumento.



Sel.isPopUpPresent
^^^^^^^^^^^^^^^^^^

.. function:: Sel.isPopUpPresent nomeDoHandler


:Comentário:
	Este comando, ao ser excutado, retorna true caso haja um popUp na tela do handler indicado ou false caso não haja.


Sel.acceptPopUp
^^^^^^^^^^^^^^^

.. function:: Sel.acceptPopUp nomeDoHandler

:Comentário:
	Este comando, ao ser executado, aceita o popUp do handler indicado.


Sel.dismissPopUp
^^^^^^^^^^^^^^^^

.. function:: Sel.dismissPopUp nomeDoHandler

:Comentário:
	Este comando, ao ser executado, cancela um popUp na tela do handler indicado.


Sel.elementScreenshot
^^^^^^^^^^^^^^^^^^^^^

.. function:: Sel.elementScreenshot nomeDoHandler 'seletorCss' 'local/para/salvar/o/screenshot.png'

:Comentário:
	Este comando, quando executado, faz um screenshot do elemento indicado.


Sel.captureEntirePageScreenshot
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. function:: Sel.captureEntirePageScreenshot nomeDoHandler 'local/para/salvar/o/screenshot.jpg'

:Comentário:
	Este comando, quando executado, faz um screenshot da página e salva a imagem no diretório indicado com o nome dado.


Sel.captureEntirePageAsPdf
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. function:: Sel.captureEntirePageAsPdf nomeDoHandler 'local/para/salvar/o/arquivo.pdf '

:Comentário:
	Este comando, quando executado, faz um screenshot da página inteira e salva em PDF no diretório indicado com o nome dado.