Manipulação do desktop
======================

jan
---

.. function:: jan 'nomeDaJanela' 'atalho'


:Comentário:
	Este comando, quando executado, dá foco em uma janela determinada, como no exemplo abaixo:

.. function:: jan 'cmd'


posiciona
---------

.. function:: posiciona 25 100 'nomeDaJanela'


:Comentário:
	Este comando, quando executado, posiciona a janela determinada na posição determinada.


dimensiona
----------

.. function:: dimensiona 100 150 'nomeDaJanela'


:Comentário:
	Este comando, quando executado, dimensiona a janela determinada com o tamanho passado como argumento.


IJmdk.listaJanelas
------------------

.. function:: IJmdk.listaJanelas variavelRetorno '1'


:Comentário:
	Este comando, quando executado, retorna a lista de nomes das janelas abertas. Caso você adicione um argumento numérico entre aspas simples (neste caso, 1), ele retorna apenas o nome da janela correspondente á esse número.


pausa
-----

.. function:: pausa '25'


:Comentário:
	Este comando, quando executado, executa uma pausa entre comandos.


pegaJan
-------

.. function:: pegaJan nomeDeHandler filtro janelasVetadas


:Comentário:
	Este comando, quando executado, encontra a última janela.


IS.maximizar
---------

.. function:: IS.maximizar 'nomeDaJanela'


:Comentário:
	Este comando, quando executado, maximizar a janela dada como argumento. Pode-se substituir o nomeDaJanela por um handler.


IS.minimizar
------------

.. function:: IS.minimizar 'nomeDaJanela'


:Comentário:
	Este comando, quando executado, minimiza a janela dada como argumento. Pode-se substituir o nomeDaJanela por um handler.


IS.restaurar
------------

.. function:: IS.restaurar 'nomeDaJanela'


:Comentário:
	Este comando, quando executado, restaura a janela dada como argumento. Pode-se substituir o nomeDaJanela por um handler.


IS.focaJan
----------

.. function:: IS.focaJan 'nomeDaJanela'


:Comentário:
	Este comando, quando executado, dá foco a janela dada como argumento. Pode-se substituir o nomeDaJanela por um handler.


IJmdk.esperaFocoJan 'nomeDaJanela'
----------------------------------

.. code-block:: python

	if IJmdk.esperaFocoJan 'nomeDaJanela'{
		alerta 'A janela foi criada.'
	}


:Comentário:
	Este comando, quando executado, aguarda a janela ser criada (e neste caso, exibe o alerta.).


start
-----

.. function:: start 'nomeDoAplicativo'


:Comentário:
	Este comando, quando executado, inicia o aplicativo ou roda a expreção do sistema dada como argumento. Caso você use o start, coloque um '&' no fim da expreção do sistema para que ele rode em background e não trave a tread.

sis
---

.. function:: sis 'comandoDeSistema'


:Comentário:
	Este comando, quando executado, executa o comando de sistema entre aspas. No exemplo abaixo, abrimos uma pasta:

.. function:: sis 'explorer.exe c:\\teste'


click
-----

.. function:: click '500' '250' 'nomeDaJanela'


:Comentário:
	Este comando, quando executado, clica na janela indicada, na posição dada como argumentos(neste caso, 5 e 25).


dirClick
--------

.. function:: dirClick '500' '250' 'nomeDaJanela'


:Comentário:
	Este comando, quando executado, abre o menu de contexto(click direito do mouse) na posição dada como argumento(neste caso, 500 e 250) na janela indicada.


dClick
------

.. function:: dClick '500' '250' 'nomeDaJanela'


:Comentário:
	Este comando, quando executado, faz um duplo click na janela indicada, nas posições dadas como argumento.


cursor
------

.. function:: curso '500' '250' 'nomeDaJanela'


:Comentário:
	Este comando, quando executado, move o cursor para a posição indicada, na janela indicada.


teclas
------

.. function:: teclas 'stringASerDigitada' 'nomeDaJanela'


:Comentário:
	Este comando, quando executado, digita a string indicada na janela indicada.
	As teclas de comando são as seguites:
	{A} -> Alt
	{C} -> Ctrl
	{S} -> Shift
	{ENTER}
	{DOWN}
	{UP}
	{HOME}
	{END}


IJmdk.pegaHandlers
------------------

.. function:: IJmdk.pegaHandlers 'nomeDaJanela' variavelRetorno1 variavelRetorno2


:Comentário:
	Este comando, quando executado, busca os handlers da janela dada como argumento e retorn o handler da janela na variavelRetorno1 e a lista de handlers dos campos da janela na variavelRetorno2.


IJmdk.listarHandlers
--------------------

.. function:: IJmdk.listarHandlers variavelRetorno


:Comentário:
	Este comando, quando executado, retorna na variavelRetorno uma lista de todos os handlers do Windows.


IJmdk.clickM
------------

.. function:: IJmdk.clickM 'nomeDaJanela' posObj


:Comentário:
	Este comando, quando executado, clica no objeto indicado da janela indicada.


IJmdk.pegaTxt
-------------

.. function:: IJmdk.pegaTxt 'nomeDaJanela' variavelRetorno posObj


:Comentário:
	Este comando, quando executado, pega o texto do objeto indicado, da janela indicada.


IJmdk.duploClick
----------------

.. function:: IJmdk.duploClick 'nomeDaJanela' posObj


:Comentário:
	Este comando, quando executado, dá um duplo clique no objeto indicado, na janela indicada.


IJmdk.getCombo
--------------

.. function:: IJmdk.getCombo 'nomeDaJanela' variavelResp posObj


:Comentário:
	Este comando, quando executado, pega o indice do combo indicado.


IJmdk.insereTxt
---------------

.. function:: IJmdk.insereTxt 'nomeDaJanela' 'stringADigitar' posObj 0


:Comentário:
	Este comando, quando executado, insere a string dada como argumento no objeto indicado da janela apontada.


IJmdk.selecionaTxt
------------------

.. function:: IJmdk.selecionaTxt 'nomeDaJanela' posObj


:Comentário:
	Este comando, quando executado, seleciona o texto do objeto indicado na janela dada como argumento.


IJmdk.selecionaCombo
--------------------

.. function:: IJmdk.selecionaCombo 'nomeDaJanela' index posObj


:Comentário:
	Este comando, quando executado, seleciona o indice do combo indicado na janela indicada.


IJmdk.acessaMenuLevel
---------------------

.. function:: IJmdk.acessaMenuLevel 'nomeDaJanela' menu 0


:Comentário:
	Este comando, quando executado, escolhe a opção indicada do menu indicado da janela apontada.