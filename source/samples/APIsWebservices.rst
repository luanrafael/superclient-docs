APIs e Webservices
==================

Http1.limpadata
---------------

.. function:: Http1.limpadata


:Comentário:
	Este comando, quando executado, limpa os dados a serem utilizados no post/get.


Http1.addProp
-------------

.. function:: Http1.addProp data propriedade conteudo


:Comentário:
	Este comando, quando executado, adiciona uma propriedade.


Http1.post
----------

.. function:: Http1.post url data jsonResposta


:Comentário:
	Este comando, quando executado, executa o post.


Http1.get
---------

.. function:: Http1.get url jsonResposta


:Comentário:
	Este comando, quando executado, executa o get.


CaptchaDecoder.quebrar
----------------------

.. function:: CaptchaDecoder.quebrar APIKEY method pathIMG resp

:Comentário:
	Este comando, quando executado, quebra um captcha de imagem usando a API Captcha Decoder.Este comando retorna true ou false e seus parametros são:
		
.. code-block:: python

		APIKEY - d9c1cbf2fbdeb5501f71cdf09d7292af73d2de75ce6cae409019ec8266d9d1d0d03e9798df12897120733f57a8aff960;
		method - string contendo a palavra 'solve';
		pathIMG - Caminho da imagem do captcha;
		resp - Variável de resposta.


CaptchaDecoder.recaptcha
------------------------

.. function:: CaptchaDecoder.recaptcha APIKEY sitekey pageurl resp

:Comentário:
	Este comando, quando executado, quebra um ReCaptcha do Google. Seus parametros são:

.. code-block:: python

		APIKEY - d9c1cbf2fbdeb5501f71cdf09d7292af73d2de75ce6cae409019ec8266d9d1d0d03e9798df12897120733f57a8aff960;
		sitekey - Key do site, geralmente encontrada como data-site;
		pageurl - URL da página;
		resp - Variável de resposta.