Métodos de uso recorrente
=========================

Uti.rand
--------

.. function:: Uti.rand variavelRetorno '25'


:Comentário:
	Este comando, quando executado, retorna à variávelRetorno um número aleatório de 0 ao limite dado como argumento (neste caso, 25-1, ou seja, considerase até o 24).

log
---

.. function:: log '[nomeDoMetodo]Esta mensagem será inserida no log.'

:Comentário:
	Este comando, quando executado, insere a mensagem indicada ao log. É considerada uma boa prática se colocar o nome do método ondo a mensagem está inserida para posterior localização do problema ou parte do código em que foi executada.

Run.exec
--------

.. function:: Run.exec 'exib `comando a ser executado`'

:Comentário:
	Este comando executa o comando dado com argumento. Este comando pode ser contido por uma variável também.

WebView1.inic
-------------

.. function:: WebView1.inic

:Comentário:
	Este comando, quando executado, inicia o JFX, devendo ser utilizado no inicio do script..


WebView1.abrir
--------------

.. function:: WebView1.abrir handler caminhoDoArquivo 800 500 1 10 'UT'

:Comentário:
	Este comando, quando executado, cria um webview, coloca o handler na variavel informada, abre a página informada no argumento caminhoDoArquivo, além de a janela ter as dimensões informadas(no caso, 800 por 500), na posição informada (1 e 10). O último argumento é referente à como a janela será criada, exibindo barra superior ou não.


WebView.setTitle
----------------

.. function:: WebView.setTitle handler 'Nome da janela'

:Comentário:
	Este comando, quando executado, configura a string dada como o título da janela do webview identificado pelo handler passado como argumento.


WebView1.execJS
---------------

.. function:: WebView1.execJS handler 'angular.element(document.getElementById("notasController")).scope().loadInfos(true);' variavel_resp

:Comentário:
	Este comando, quando executado, executa comandos JavaScript no webview indicado pelo handler dado como argumento e retorna o status da execução na variavel_resp.