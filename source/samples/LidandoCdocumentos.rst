Lidando com documentos
======================

Leitura, escrita e mais
-----------------------

rfile
^^^^^

.. function:: rfile nomeDaVariavel 'localDoArquivo/nomeDoArquivo'


	:Comentário:
	Este comando lê um arquivo no loocal indicado no segundo argumento e o guarda na variável indicada no primeiro argumento.


wfile
^^^^^

.. function:: wfile nomeDaVariavel 'localDoArquivo/nomeDoArquivo' 'cp1252:a'


	:Comentário:
	Este comando escreve o conteudo da variável(ou texto entre aspas) dada como primeiro argumento no arquivo indicado no segundo argumento.
	Caso deseje apenas adicionar um texto no arquivo, adicione o 'a' como terceiro argumento. Além disso, é possível tambem mudar a codificação do arquivo, adicionando entre aspas simples UTF-8, CP1252, ou outros.


tfile
^^^^^

.. function:: tfile 'localDoArquivo/nomeDoArquivo' nomeDaVariavel


	:Comentário:
	Este comando retorna no tamanho do arquivo para a varivel indicada. Pode ser usado em uma condicional para verificar a existencia de um arquivo, ao ignorar-se a variável de retorno e colocando-o em uma condicional com o endereço do arquivo por exemplo.


tamfile
^^^^^^^

.. function:: tamfile 'localDoArquivo/nomeDoArquivo' nomeDeVariavel


	:Comentário:
	Este comando retorna à variável o tamanho do arquivo especificado.


sort
^^^^

.. function:: sort 'localDoArquivo/nomeDoArquivo'


	:Comentário:
	Este comando faz a ordenação dos arquivos.


tdir
^^^^

.. function:: if tdir 'local'


	:Comentário:
	Este comando verifica a existencia de um diretório.


remover
^^^^^^^

.. function:: remover 'localDoArquivo/nomeDoArquivo'


	:Comentário:
	Este comando apaga o arquivo do diretório indicado.


qtarquivos
^^^^^^^^^^

.. function:: qtarquivos nomeDeVariavel 'local'


	:Comentário:
	Este comando retorna à variável a quantidade de arquivos no local indicado.


pesqarq
^^^^^^^

.. function:: pesqarq 'localDePesquisa' 'stringFiltro' nomeDeVariavel 'argumento'


	:Comentário:
	Este comando retorna o nome de arquivos presentes no local indicado na variável resposta, usando como filtro uma string indicada. O argumento pode ser:
	n - Não recursivo;
	r - Recursivo (subdiretórios);
	s - Nome completo do arquivo.


pegadata
^^^^^^^^

.. function:: pegadata nomeDeVariavel 'localDoArquivo/nomeDoArquivo'


	:Comentário:
	Este comando retorna à variável resposta a última data em que o arquivo foi modificado.


mkdir
^^^^^

.. function:: mkdir 'local'


	:Comentário:
	Este comando cria o diretório indicado.


mcopy
^^^^^

.. function:: mcopy 'localDeOrigem' 'localDestino' 'nomeDoArquivo'


	:Comentário:
	Este comando copia a lista de arquivos indicada do local origem para o local destino.


Io.fastRead
^^^^^^^^^^^

.. function:: Io.fastRead variavel_resp 'caminho/arquivo.extensao' 'cp1252'


	:Comentário:
	Este comando, quando executado, lê o arquivo indicado, com a codificação indicada (cp1252) e coloca esses dados na variavel de resposta. Apesar de ter a mesma função do rfile, há uma importante diferença, ele é usado para arquivos maiores que o limite de leitura do rfile, por tanto, pode consumir uma quantidade considerável de memória.


Área de transferencia
---------------------

copiaTrab
^^^^^^^^^

.. function:: copiaTrab 'textoASerExibido'


	:Comentário:
	Esse comando copia um texto ou o conteudo da variável para a área de transferencia.


trazTrab
^^^^^^^^

.. function:: trazTrab nomeDaVariavel

	
	:Comentário:
	Esse comando insere na variável destino o valor da área de transferência.