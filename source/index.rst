.. SuperClient RPA documentation master file, created by
   sphinx-quickstart on Tue Jun 13 16:37:39 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SuperClient RPA
===============

BDJava
------

O BDJava é uma linguagem de Script com comandos em português, criada pela Epsoft contando com sua experiência de 29 anos no setor. Seus comandos foram criados em Java. É capaz de se integrar diretamente com janelas, mouse e teclados do Windows, permitindo a máxima otimização de processos.

.. toctree::
	:caption: Introdução
	:glob:

	introducao/*


.. toctree::
	:caption: Exemplos
	:glob:

	samples/*
	samples/Testes/*


.. toctree::
	:caption: Sintaxe
	:glob:

	sintax/*