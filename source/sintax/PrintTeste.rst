
PrintTeste
==========
.. function:: PrintTeste.imp texto


	:comentário: Esse método imprime a string Bds passada como parâmetro. Após a execução do
		método, o aplicativo apresentará uma tela de diálogo referente as
		especificações da impressão.
		
		
		

	:param  texto: especifica string Bds a ser impressa.
		

