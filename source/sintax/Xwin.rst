
Xwin
====
.. function:: Xwin.listaJanelas jans filtrosTotais


	:comentário: Lista o nome e o handle de todas as janelas abertas esta lista é devolvida
		como uma sequencia de linhas separadas por "\n" e no formato
		"<janela>:<handle>" retorna a primeira janela que atender a qualquer um dos
		filtros desde que nao esteja dentro de um filtro especificado com uma
		negacao.
		
		
		

	:param  jans: janelas pesquisadas
		
		

	:param  filtros: filtros considerados separados por "^" em dois grupos separados
		por '~'. Exemplo de filtros "Internet
		Explorer^Chrome^~SuperClient" : pega qualquer janela que tenha
		Internet Explorer ou Chrome no titulo desde que nao tenha
		"Superclient" tambem no titulo
		
		
	:rtype: boolean  Um array de string contendo o nome e o handle de todas as janelas
		abertas separados pelo delimitador
		
		
	:throws:  ErrBds
		
		

.. function:: Xwin.focaJanela nomeJanela


	:comentário: Exibe a janela e coloca o foco nela, assim ela passa a receber as entradas do
		usuário, como teclado e mouse
		
		
		

	:param  nomeJanela: O nome ou parte do nome da janela
		
		
	:rtype: boolean  Se fou possível dar o foco na janela
		
		
	:throws:  ErrBds
		

.. function:: Xwin.focaPegaDados nomeJanela


	:comentário: Exibe a janela e coloca o foco nela, assim ela passa a receber as entradas do
		usuário, como teclado e mouse
		
		
		

	:param  nomeJanela: O nome ou parte do nome da janela
		
		
	:rtype: boolean  Se fou possível dar o foco na janela
		
		
	:throws:  ErrBds
		

.. function:: Xwin.focaJanela nomeJanela


	:comentário: Exibe a janela e coloca o foco nela, assim ela passa a receber as entradas do
		usuário, como teclado e mouse
		
		
		

	:param  nomeJanela: O nome ou parte do nome da janela
		
		
	:rtype: boolean  Se fou possível dar o foco na janela
		
		
	:throws:  ErrBds
		

.. function:: Xwin.dimensionaJanela altura largura nomeJanela


	:comentário: Dimensiona a janela
		
		
		

	:param  nomeJanela: O nome ou parte do nome da janela
		
		

	:param  altura: A nova largura em pixels
		
		

	:param  largura: A nova altura em pixels
		
		
	:rtype: boolean  Se foi possível dimensionar a janela
		
		
	:throws:  ErrBds
		

.. function:: Xwin.dimensionaJanela esquerda topo largura altura nomeJanela


	:comentário: 
	:param esquerda:
		
	:param topo:
		
	:param largura:
		
	:param altura:
		
	:param nomeJanela:

.. function:: Xwin.posicionaJanela esquerda topo nomeJanela


	:comentário: Posiciona a janela
		
		
		

	:param  nomeJanela: O nome ou parte do nome da janela
		
		

	:param  esquerda: A posição na horizontal em pixels
		
		

	:param  topo: A posição na vertical em pixels
		
		
	:rtype: boolean  Se foi possível posicionar a janela
		
		
	:throws:  ErrBds
		

.. function:: Xwin.posicionaJanela esquerda topo largura altura nomeJanela


	:comentário: 
	:param esquerda:
		
	:param topo:
		
	:param largura:
		
	:param altura:
		
	:param nomeJanela:

.. function:: Xwin.posicionaMouse x y


	:comentário: Posiciona o mouse no local especificado
		
		
		

	:param  x: A posição horizontal em pixels
		
		

	:param  y: A posição vertical em pixels
		
		
	:rtype: boolean  Se foi possível posicionar o mouse
		
		
	:throws:  ErrBds
		

.. function:: Xwin.posicionaMouse x y nomeJanela


	:comentário: Posiciona o mouse no local especificado dentro da janela
		
		
		

	:param  x: A posição horizontal em pixels
		
		

	:param  y: A posição vertical em pixels
		
		

	:param  nomeJanela: O nome ou parte do nome da janela
		
		
	:rtype: boolean  Se foi possível posicionar o mouse
		
		
	:throws:  ErrBds
		

.. function:: Xwin.duploClique x y botao


	:comentário: Executa um duplo clique
		
		
		

	:param  x: A posição horizontal em pixels
		
		

	:param  y: A posição vertical em pixels
		
		

	:param  botao: O botão a ser clicado
		
		
	:rtype: boolean  Se foi possível clicar com o mouse
		
		
	:throws:  ErrBds
		

.. function:: Xwin.duploClique x y botao nomeJanela


	:comentário: Executa um duplo clique
		
		
		

	:param  x: A posição horizontal em pixels
		
		

	:param  y: A posição vertical em pixels
		
		

	:param  botao: O botão a ser clicado
		
		

	:param  nomeJanela: O nome ou parte do nome da janela
		
		
	:rtype: boolean  Se foi possível clicar com o mouse
		
		
	:throws:  ErrBds
		

.. function:: Xwin.click x y


	:comentário: Executa um clique do botao esquerdo do mouse
		
		
		

	:param  x: A posição horizontal em pixels
		
		

	:param  y: A posição vertical em pixels
		
		
	:rtype: boolean  Se foi possível clicar com o mouse
		
		
	:throws:  ErrBds
		

.. function:: Xwin.duploClick x y


	:comentário: Executa um clique do botao esquerdo do mouse
		
		
		

	:param  x: A posição horizontal em pixels
		
		

	:param  y: A posição vertical em pixels
		
		
	:rtype: boolean  Se foi possível clicar com o mouse
		
		
	:throws:  ErrBds
		

.. function:: Xwin.dirClick x y


	:comentário: Executa um clique do botao direito do mouse
		
		
		

	:param  x: A posição horizontal em pixels
		
		

	:param  y: A posição vertical em pixels
		
		
	:rtype: boolean  Se foi possível clicar com o mouse
		
		
	:throws:  ErrBds
		

.. function:: Xwin.click x y nomeJanela


	:comentário: Executa um clique do botao esquerdo do mouse
		
		
		

	:param  nomeJan: nome da janela
		
		

	:param  x: A posição horizontal em pixels
		
		

	:param  y: A posição vertical em pixels
		
		
	:rtype: boolean  Se foi possível clicar com o mouse
		
		
	:throws:  ErrBds
		

.. function:: Xwin.duploClick x y nomeJanela


	:comentário: Executa um duplo clique do botao esquerdo do mouse
		
		
		

	:param  nomeJan: nome da janela
		
		

	:param  x: A posição horizontal em pixels
		
		

	:param  y: A posição vertical em pixels
		
		
	:rtype: boolean  Se foi possível clicar com o mouse
		
		
	:throws:  ErrBds
		

.. function:: Xwin.dirClick x y nomeJan


	:comentário: Executa um clique do botao direito do mouse
		
		
		

	:param  nomeJan: nome da janela
		
		

	:param  x: A posição horizontal em pixels
		
		

	:param  y: A posição vertical em pixels
		
		
	:rtype: boolean  Se foi possível clicar com o mouse
		
		
	:throws:  ErrBds
		

.. function:: Xwin.clicaMouse x y botao


	:comentário: Executa um clique do botao esquerdo do mouse
		
		
		

	:param  x: A posição horizontal em pixels
		
		

	:param  y: A posição vertical em pixels
		
		

	:param  botao: O botão a ser clicado
		
		
	:rtype: boolean  Se foi possível clicar com o mouse
		
		
	:throws:  ErrBds
		

.. function:: Xwin.clicaMouse x y botao quantidadeDeClique


	:comentário: Executa um duplo clique
		
		
		

	:param  x: A posição horizontal em pixels
		
		

	:param  y: A posição vertical em pixels
		
		

	:param  botao: O botão a ser clicado
		
		

	:param  quantidadeDeClique: A quantidade de clique a serexecutada
		
		
	:rtype: boolean  Se foi possível clicar com o mouse
		
		
	:throws:  ErrBds
		

.. function:: Xwin.clicaMouse px py botao quantidadeDeClique nomeJanela


	:comentário: Executa um duplo clique
		
		
		

	:param  x: A posição horizontal em pixels
		
		

	:param  y: A posição vertical em pixels
		
		

	:param  botao: O botão a ser clicado
		
		

	:param  quantidadeDeClique: A quantidade de clique a serexecutada
		
		

	:param  nomeJanela: O nome ou parte do nome da janela
		
		
	:rtype: boolean  Se foi possível clicar com o mouse
		
		
	:throws:  ErrBds
		

.. function:: Xwin.enviaTecla teclas


	:comentário: Envia teclas
		
		
		

	:param  teclas: As teclas a serem enviadas
		
		
	:rtype: boolean  Se foi possível enviar as teclas
		
		
	:throws:  ErrBds
		

.. function:: Xwin.enviaTecla teclas


	:comentário: Envia teclas
		
		
		

	:param  teclas: As teclas a serem enviadas
		
		
	:rtype: boolean  Se foi possível enviar as teclas
		
		
	:throws:  ErrBds
		

.. function:: Xwin.enviaTecla pteclas nomeJanela


	:comentário: Envia teclas para uma janela, o foco é dado automaticamente
		
		
		

	:param  teclas: As teclas a serem enviadas
		
		

	:param  nomeJanela: O nome ou parte do nome da janela
		
		
	:rtype: boolean  Se foi possível enviar as teclas
		
		
	:throws:  ErrBds
		

.. function:: Xwin.enviaTecla pteclas nomeJanela


	:comentário: Envia teclas para uma janela, o foco é dado automaticamente
		
		
		

	:param  teclas: As teclas a serem enviadas
		
		

	:param  nomeJanela: O nome ou parte do nome da janela
		
		
	:rtype: boolean  Se foi possível enviar as teclas
		
		
	:throws:  ErrBds
		

.. function:: Xwin.posMouse x y


	:comentário: Retorna a posição atual do mouse
		
		
		

	:param  x: retorna posicao horizontal (da esquerda para a direita)
		
		

	:param  y: retorna a posicao vertical (de cima para baixo)
		
		
	:rtype: void  O ponto em pixels contendo a posição do mouse
		
		
	:throws:  ErrBds
		

.. function:: Xwin.posMouse x y nomeJanela


	:comentário: Retorna a posição atual do mouse
		
		
		

	:param  x: retorna posicao horizontal (da esquerda para a direita)
		
		

	:param  y: retorna a posicao vertical (de cima para baixo)
		
		
	:rtype: boolean  O ponto em pixels contendo a posição do mouse
		
		
	:throws:  ErrBds
		

.. function:: Xwin.posMouse janela


	:comentário: Retorna a posição atual do mouse em determinada janela (nao se se é
		necessário colca os resultados nas variaveis globais da Thread "posXMouse" e
		"posYMouse" (sao visiveis normalmente pelo bdjava)
		
		
		

	:param  janela: 
		= Nome da janela ou handle da janela
		
		
	:rtype: void  O ponto em pixels contendo a posição do mouse
		
		
	:throws:  ErrBds
		

.. function:: Xwin.dimJan x y largura altura nomeJanela


	:comentário: Retorna a dimensão da janela, sua posição x e y e seu tamanho altura e
		largura
		
		
		

	:param  nomeJanela: O nome ou parte do nome da janela, ou se for numero, o handle da
		janela
		
		
	:rtype: boolean  falso se nao encontrar a janela
		
		
	:throws:  ErrBds
		

.. function:: Xwin.escondeJanelaCortina 


	:comentário: Esconde uma janela cortina em exibição
		
		
		
	:throws:  ErrBds
		

.. function:: Xwin.gravaMacro arquivo


	:comentário: Inicia uma gravação de Macro
		
		
		

	:param  arquivo: O nome do arquivo que será gravado contendo a macro
		
		
	:rtype: boolean  Se foi possível iniciar a gravação
		
		
	:throws:  ErrBds
		

.. function:: Xwin.gravaMacro arquivo


	:comentário: Inicia uma gravação de Macro
		
		
		

	:param  arquivo: O nome do arquivo que será gravado contendo a macro
		
		
	:rtype: boolean  Se foi possível iniciar a gravação
		
		
	:throws:  ErrBds
		

.. function:: Xwin.paraGravacaoMacro 


	:comentário: Para uma gravação de macro em andamento
		
		
		
	:rtype: boolean  Se foi possível parar a gravação
		
		
	:throws:  ErrBds
		

.. function:: Xwin.playMacro arquivo


	:comentário: Inicia um play back de Macro
		
		
		

	:param  arquivo: O nome do arquivo que contem uma macro gravada
		
		
	:rtype: boolean  Se foi possível iniciar o play back
		
		
	:throws:  ErrBds
		

.. function:: Xwin.playMacro arquivo


	:comentário: Para um play back de macro em andamento
		
		
		
	:rtype: boolean  Se foi possível parar o play back
		
		
	:throws:  ErrBds
		
		
		Inicia um play back de Macro
		
		
		

	:param  arquivo: O nome do arquivo que contem uma macro gravada
		
		
	:rtype: boolean  Se foi possível iniciar o play back
		
		
	:throws:  ErrBds
		

.. function:: Xwin.paraPlayMacro 


	:comentário: Para um play back de macro em andamento
		
		
		
	:rtype: boolean  Se foi possível parar o play back
		
		
	:throws:  ErrBds
		

.. function:: Xwin.paraMacro 


	:comentário: Para uma gravação ou playback de macro em andamento
		
		
		
	:rtype: boolean  Se foi possível parar a gravação ou playback
		
		
	:throws:  ErrBds
		

.. function:: Xwin.bloqueiaTecladoMouse 


	:comentário: Bloqueia a entrada de dados via teclado e mouse (para desbloquer pressione
		Ctrl+Alt+Del)
		
		
		
	:rtype: boolean  Se foi possível bloquear o teclado
		
		
	:throws:  ErrBds
		

.. function:: Xwin.desbloqueiaTecladoMouse 


	:comentário: Desloqueia a entrada de dados via teclado e mouse
		
		
		
	:rtype: boolean  Se foi possível desbloquear o teclado
		
		
	:throws:  ErrBds
		

.. function:: Xwin.arrastaMouse xInicio yInicio xFim yFim botao


	:comentário: Arrasta o botão do mouse de um ponto até outro ponto
		
		
		

	:param  xInicio: A posição inicial horizontal em pixels do início do arrastamento
		
		

	:param  yInicio: A posição inicial vertical em pixels do início do arrastamento
		
		

	:param  xFim: A posição final horizontal em pixels do início do arrastamento
		
		

	:param  yFim: A posição final vertical em pixels do início do arrastamento
		
		

	:param  botao: O botão a ser arrastado
		
		
	:rtype: boolean  Se foi possível arrastar com o mouse
		
		
	:throws:  ErrBds
		

.. function:: Xwin.arrastaMouse xInicio yInicio xFim yFim botao nomeJanela


	:comentário: 
	:param xInicio:
		
	:param yInicio:
		
	:param xFim:
		
	:param yFim:
		
	:param botao:
		
	:param nomeJanela:

.. function:: Xwin.procuraJanela nomeJanela


	:comentário: Procura e retorna o handle da janela
		
		
		

	:param  nomeJanela: O nome ou parte do nome da janela
		
		
	:rtype: boolean  O handle da janela encontrada
		
		
	:throws:  ErrBds
		

.. function:: Xwin.criaLogTeclas 


	:comentário: Pretratamento de uma sequencia de maneira que o MoniKey possa tratar as
		teclas corretamente (torna os comandos mais amigaveis podendo-se usar por
		exemplo {A} ou {ALT} para indicar o acionamento da tecla alt, por exemplo
		
		
		

	:param  args: argumento contendo as teclas a serem tratadas
		
		
		
	:throws:  ErrBds
		

.. function:: Xwin.trataTeclas args


	:comentário: 
	:param args:

