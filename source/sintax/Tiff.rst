
Tiff
====
.. function:: Tiff.info arq


	:comentário: 
	:param arq:

.. function:: Tiff.info arq resp


	:comentário: 
	:param arq:
		
	:param resp:

.. function:: Tiff.info arq0 tipoarq tamarq prof_cores resolx resoly paginas tx ty


	:comentário: 
	:param arq0:
		
	:param tipoarq:
		
	:param tamarq:
		
	:param prof_cores:
		
	:param resolx:
		
	:param resoly:
		
	:param paginas:
		
	:param tx:
		
	:param ty:

.. function:: Tiff.junta lista_arqs arq largura comprimento


	:comentário: 
	:param lista_arqs:
		
	:param arq:
		
	:param largura:
		
	:param comprimento:

.. function:: Tiff.comptiff arq pixels


	:comentário: 
	:param arq:
		
	:param pixels:

.. function:: Tiff.limpa arq


	:comentário: 
	:param arq:

.. function:: Tiff.trunca arq pixels


	:comentário: 
	:param arq:
		
	:param pixels:

