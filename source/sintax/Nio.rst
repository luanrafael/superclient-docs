
Nio
===
.. function:: Nio.open handler hostname port onRead onDisconnect


	:comentário: Abre uma nova conexão com o hostame:porta especificados, efetuando uma
		chamada ao método presente no parâmetro onRead para cada nova mensagem
		recebida
		
		
		

	:param  handler: Variável que receberá o handler
		
		

	:param  hostname: Hostname
		
		

	:param  port: Porta
		
		

	:param  onRead: Método que deve ser chamado a cada nova leitura
		
		

	:param  onDisconnect: Método que deve ser chamado ao ser desconectado
		
		
	:rtype: boolean  Identificador do canal
		
		
	:throws:  IOException
		

.. function:: Nio.open hostname port onRead onDisconnect


	:comentário: Abre uma nova conexão com o hostame:porta especificados, efetuando uma
		chamada ao método presente no parâmetro onRead para cada nova mensagem
		recebida
		
		
		

	:param  hostname: Hostname
		
		

	:param  port: Porta
		
		

	:param  onRead: Método que deve ser chamado a cada nova leitura
		
		

	:param  onDisconnect: Método que deve ser chamado ao ser desconectado
		
		
	:rtype: Bds  Identificador do canal
		
		
	:throws:  IOException
		

.. function:: Nio.close handler


	:comentário: Fecha o canal com o respectivo identificador
		
		
		

	:param  handler: Identificador do canal
		
		
	:throws:  IOException
		

.. function:: Nio.write handler message


	:comentário: Escreve uma mensagem no canal especificado
		
		
		

	:param  handler: Identificador do canal
		
		

	:param  message: A mensagem que deve ser escrita
		

.. function:: Nio.setHeartbeat handler request reply timeout


	:comentário: Define uma mensagem 'heartbeat' que será enviada no intervalo definido. Essa
		mecanismo serve para validar a conexão a nível de aplicação, já que o
		protocolo TCP por vezes não consegue identificar uma queda de conexão.
		
		
		

	:param  handler: Identificador do canal
		
		

	:param  request: Texto da mensagem que será enviada
		
		

	:param  reply: Resposta esperada
		
		

	:param  timeout: Intervalo, em milisegundos, entre cada batida
		

