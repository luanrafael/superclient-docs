
MenuFlash
=========
.. function:: MenuFlash.idLinhaMenuFlash=new 


	:comentário: 

.. function:: MenuFlash.inic Op


	:comentário: 
	:param Op:

.. function:: MenuFlash.arv2planilha parqsArv pjanPlanilha


	:comentário: 
	:param parqsArv:
		
	:param pjanPlanilha:

.. function:: MenuFlash.criaArqPlan parqPlan pjanPlanilha


	:comentário: 
	:param parqPlan:
		
	:param pjanPlanilha:

.. function:: MenuFlash.poeMenuMapa pletras registro opcaoLetra linha linhaErro


	:comentário: 
	:param pletras:
		
	:param registro:
		
	:param opcaoLetra:
		
	:param linha:
		
	:param linhaErro:

.. function:: MenuFlash.trataLetra arg novaLetra


	:comentário: 
	:param arg:
		
	:param novaLetra:

.. function:: MenuFlash.geraMapas parqPlan arqArvore


	:comentário: 
	:param parqPlan:
		
	:param arqArvore:

.. function:: MenuFlash.limpaPVirgs pcampo


	:comentário: 
	:param pcampo:

.. function:: MenuFlash.idLinhaAnterior=new 


	:comentário: 

.. function:: MenuFlash.montaErro strErro


	:comentário: 
	:param strErro:

.. function:: MenuFlash.percorreMapaMenu strArv


	:comentário: 
	:param strArv:

.. function:: MenuFlash.procPlanilha arvoreDestino


	:comentário: 
	:param arvoreDestino:

.. function:: MenuFlash.analisaPlanilha ehCompilação


	:comentário: 
	:param ehCompilação:

