
ScUti
=====
.. function:: ScUti.inic op


	:comentário: 
	:param op:

.. function:: ScUti.criaDefault nomeCp lista op cp dadx code


	:comentário: cria um cs default em um campo a partir de um nome
		
	:param nomeCp:
		
	:param lista:
		
	:param op:
		
	:param cp:
		
	:param dadx:
		
	:param code:

.. function:: ScUti.csCriaCampo op cp dadx code


	:comentário: cria um cs default em um campo a partir de um nome
		
	:param op:
		
	:param cp:
		
	:param dadx:
		
	:param code:

.. function:: ScUti.telaScript 


	:comentário: 

.. function:: ScUti.telaDefs 


	:comentário: 

.. function:: ScUti.telaPesq 


	:comentário: 

.. function:: ScUti.telaCampos 


	:comentário: 

.. function:: ScUti.telaBotoes 


	:comentário: 

.. function:: ScUti.popularCs dest lista


	:comentário: 
		
		

	:param  dest: arquivo texto que tem que todos os Cs da Lista
		
		

	:param  lista: tela correspondente onde os cs e pr devem ser lidos e populados
		em dest
		

.. function:: ScUti.metodosCs dest lista


	:comentário: Atencao - este metodo deve ser deslocado para o DadosSc
		
		
		

	:param  dest: arquivo texto que tem que todos os Cs da Lista
		
		

	:param  lista: tela correspondente onde os cs e pr devem ser lidos e populados
		em dest
		

.. function:: ScUti.tt cp


	:comentário: 
	:param cp:

.. function:: ScUti.setaCps lista args


	:comentário: seta os parametros internos dos campos constantes da lista
		
		
		

	:param  lista: lista de campos a serem setados em conjunto
		
		

	:param  args: string com os argumentos desejados
		
		

.. function:: ScUti.setaCps lista args


	:comentário: seta os parametros internos dos campos constantes da lista
		
		
		

	:param  lista: lista de campos a serem setados em conjunto
		
		

	:param  args: string com os argumentos desejados
		
		

.. function:: ScUti.modoPesq 


	:comentário: No modo pesq, a edicao dos itens do Scrip está bloqueada e o modo de pesquisa
		liberado
		
		

.. function:: ScUti.modoEdt 


	:comentário: No modo edt, a edicao dos itens do Scrip está liberada e o modo de pesquisa
		bloqueado
		
		

.. function:: ScUti.modoRenomeia 


	:comentário: renomear. Apenas se pode mudar os nomes e salvar
		
		

.. function:: ScUti.csScript op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.csVaiDefs op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.csVoltaScripts op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.csNovoCampo op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.csCarregaCampo op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.csSalva op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.csPesq op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.csPesq1 op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.csPesqBks op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.pesqScrs ehBks


	:comentário: monta a pesquisa de scripts ou a pesquisa de bks conforme for desejado
		
		
		

	:param  ehBks: se =1 , pesquisa apenas dos registros de backup temporario
		
		

.. function:: ScUti.csGeraHtml op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.csRemove op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.csRenomear op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.csEditarScript op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.csExecSelecao op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.csNovoScript op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.csTrazEdicao op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.csEditarDefs op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.csModoPesq op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.csTemplateLinha op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.csExComp op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.csExMacro op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.csTable op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.csEdtSelecao op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.csPegaScript op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: ScUti.pegaScript 


	:comentário: pega o script selecionado e o transforma no comando ScUti.leScript
		nomeScriptTable campanhaTable clienteTable alem disto, chama a tela
		ScUti.telaCampos
		

.. function:: ScUti.leScript nomeScript campanha cliente


	:comentário: 
	:param nomeScript:
		
	:param campanha:
		
	:param cliente:

.. function:: ScUti.limpaBks 


	:comentário: 

.. function:: ScUti.gravaArq radical cliente campanha nomeScr


	:comentário: 
	:param radical:
		
	:param cliente:
		
	:param campanha:
		
	:param nomeScr:

.. function:: ScUti.gravaBk 


	:comentário: 

.. function:: ScUti.gravaBk arqBk cliente campanha nomeScr


	:comentário: Salva um conjunto de registros de Script
		
		
		

	:param  arqBk: nome do arquivo onde vao ser salvos
		
		

	:param  cliente: nome do cliente a que se refere a campanha (colocar % antes ou
		depois se desejar pegar um grupo de registros que contenha a chave
		indicada)
		
		

	:param  campanha: nome da campanha a qual se refere o cliente (colocar % antes ou
		depois se desejar pegar um grupo de registros que contenha a chave
		indicada)
		
		

	:param  nomeScr: nome do script (colocar % antes ou depois se desejar pegar um
		grupo de registros que contenha a chave indicada)
		
		

.. function:: ScUti.trazBk arqBk cliente campanha nomeScr


	:comentário: traz de um arquivo de log para o banco de dados pode-se usar o conceito de
		para. os argumentos cliente, campanha e nomeScr podem ser usados para isto se
		forem nulos, os registros do arquivo nao sao renomeados.
		
		se existirem , ocorre a renomeacao. cada um deve ser informado do seguinte
		jeito" str1=str2 . Deste modo, todo campos que tiver str1, vai ter str1
		trocado por str2
		
		
		
	:param arqBk:
		
	:param cliente:
		
	:param campanha:
		
	:param nomeScr:

.. function:: ScUti.poeBuf cliente campanha nome


	:comentário: coloca mais uma string no buffer verifica se ela esta em algum lugar. se
		estiver, a retira e poe no inicio
		
		
		

	:param  cliente: cliente do script
		
		

	:param  campanha: nome da campanha
		
		

	:param  nome: nome do script
		
		

.. function:: ScUti.poeBuf cliente campanha nome


	:comentário: coloca mais uma string no buffer verifica se ela esta em algum lugar. se
		estiver, a retira e poe no inicio
		
		
		

	:param  cliente: cliente do script
		
		

	:param  campanha: nome da campanha
		
		

	:param  nome: nome do script
		
		

.. function:: ScUti.poeCondBuf cliente campanha nome


	:comentário: coloca mais uma string no buffer, condicional, se nao existir antes verifica
		se ela esta em algum lugar. se estiver, a retira e poe no inicio
		
		
		

	:param  cliente: cliente do script
		
		

	:param  campanha: nome da campanha
		
		

	:param  nome: nome do script
		
		

.. function:: ScUti.poeCondBuf cliente campanha nome


	:comentário: coloca mais uma string no buffer verifica se ela esta em algum lugar. se
		estiver, a retira e poe no inicio
		
		
		

	:param  cliente: cliente do script
		
		

	:param  campanha: nome da campanha
		
		

	:param  nome: nome do script
		
		

.. function:: ScUti.ctlPgUp cliente campanha nome


	:comentário: 
		
		Volta uma tela para a penultima editada (lembra-se de todas)
		
	:param cliente:
		
	:param campanha:
		
	:param nome:

.. function:: ScUti.ctlPgDn cliente campanha nome


	:comentário: 
		
		Volta uma tela para a penultima editada (lembra-se de todas)
		
	:param cliente:
		
	:param campanha:
		
	:param nome:

.. function:: ScUti.pgUp 


	:comentário: 
		
		Volta uma tela na sequencia do table resposta da pesquisa
		

.. function:: ScUti.pgDn 


	:comentário: 
		
		Avanca uma tela na sequencia do table resposta da pesquisa
		

.. function:: ScUti.csCtlE 


	:comentário: Chama a edicao com os ultimos campos da tela de Script
		
		
		

.. function:: ScUti.carregaCampo 


	:comentário: pega o comando de criacao de um campo da area de trabalho e monta os dados
		para sua formatacao
		

.. function:: ScUti.carregaCampo str


	:comentário: pega o comando de criacao de um campo e monta os dados para sua formatacao
		
		
		

	:param  str: string a ser analisada
		

.. function:: ScUti.pegaNomesCampos 


	:comentário: pega a string correspondente as definicoes dos campos da definicoes Bds defs
		texto onde estao das definicoes
		
		

.. function:: ScUti.pegaNomesCampos defs


	:comentário: pega a string correspondente as definicoes dos campos da definicoes Bds defs
		texto onde estao das definicoes
		
		
	:param defs:

.. function:: ScUti.pegaDefCampo nomeCampo def


	:comentário: pega a definicao de definicoes correspondente a um campo
		
		
		

	:param  nomeCampo: nome do campo do qual se quer a definicao
		
		

	:param  def: definicao que sera obtida
		
		

.. function:: ScUti.pegaDefCampo defs nomeCampo def


	:comentário: pega a string correspondente a um campo
		
		
		

	:param  defs: definicoes onde se vai pegar o campo
		
		

	:param  nomeCampo: nome do campo do qual se quer a definicao
		
		

	:param  def: definicao que sera obtida
		

.. function:: ScUti.poeDefCampo nomeCampo def


	:comentário: poe em defs as a definicao do campo
		
		
		

	:param  defs: string que ira receber a definicao do campo
		
		

	:param  nomeCampo: nome do campo
		
		

	:param  def: definicao do campo
		
		

.. function:: ScUti.poeDefCampo defs nomeCampo def


	:comentário: poe em defs as a definicao do campo
		
		
		

	:param  defs: string que ira receber a definicao do campo
		
		

	:param  nomeCampo: nome do campo
		
		

	:param  def: definicao do campo
		
		

.. function:: ScUti.montaStrCampo 


	:comentário: pega o comando de criacao de um campo da area de trabalho e monta os dados
		para sua formatacao
		

.. function:: ScUti.montaStrCampo str


	:comentário: pega o comando de criacao de um campo e monta os dados para sua formatacao
		
		
		

	:param  str: string a ser analisada
		

.. function:: ScUti.atStrCampo 


	:comentário: Atualiza a string de um campo na str def
		

.. function:: ScUti.atStrCampo defs


	:comentário: Atualiza a string de um campo na str def
		
	:param defs:

.. function:: ScUti.limpaDefCampo listaCampos


	:comentário: limpa a definicao de um campo de caracteres que nao podem existir
		
	:param listaCampos:

.. function:: ScUti.pegaOpCombo cp resp


	:comentário: este metodo parte do principio que a opcao real do combo esta no inicio (lado
		esquerdo do hifen) ou parte direita do ":" exemplo : antes checa " 1 - Opcao
		primeira,2 - opcao xxx, etc neste caso pega a parte a esquerda do "-" se nao
		tem o - como segundo pedacao considerando o branco, ve, se tem ":" como
		delimitador no final: "opcao primeira:1, opcao xxx:2, etc" se nao der certo
		nenhuma das possibilidades acima,resp fica igual ao cp
		
		
		

	:param  cp: campo de onde se vai tirar a opcao
		
		

	:param  resp: resposta desejada
		

