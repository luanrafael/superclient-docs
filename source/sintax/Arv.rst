
Arv
===
.. function:: Arv.inic op


	:comentário: 
	:param op:

.. function:: Arv.proj 


	:comentário: 

.. function:: Arv.criaCp nomeCp prompt pstrCp


	:comentário: cria um campo
		
		
		

	:param  nomeCp: nome da variaval do campo = nome logico
		
		

	:param  prompt: Nome do prompt que deve constar na tela
		
		

	:param  pstrCp: str de complementos a constar do campo
		

.. function:: Arv.criaCp nomeCp prompt pstrCp


	:comentário: cria um campo
		
		
		

	:param  nomeCp: nome da variaval do campo = nome logico
		
		

	:param  prompt: Nome do prompt que deve constar na tela
		
		

	:param  pstrCp: str de complementos a constar do campo
		

.. function:: Arv.le projeto


	:comentário: carrega o projeto indicado em memoria
		
		
		

	:param  projeto: nome do projeto (arquivo de abstrais) se nao tiver extensao, vai
		procurar em todos os caminhos
		
		

.. function:: Arv.grava projeto


	:comentário: salva projeto indicado em memoria
		
		
		

	:param  projeto: nome do projeto (arquivo de abstrais) se nao tiver extensao, vai
		procurar em todos os caminhos. Se nao encontrar, vai tentar salvar
		no path principal
		
		

.. function:: Arv.montaCs lcs arq


	:comentário: Usado para montar os Cs automaticamente
		
		
		

	:param  lcs: lista dos campos cs
		
		

	:param  arq: arquivo de trabalho
		

.. function:: Arv.csProjetos 


	:comentário: 

.. function:: Arv.csProjetos acao arg dados code


	:comentário: 
	:param acao:
		
	:param arg:
		
	:param dados:
		
	:param code:

.. function:: Arv.csAbstrais acao arg dados code


	:comentário: 
	:param acao:
		
	:param arg:
		
	:param dados:
		
	:param code:

.. function:: Arv.csSalvaProj 


	:comentário: 

.. function:: Arv.csSalvaProj acao arg dados code


	:comentário: 
	:param acao:
		
	:param arg:
		
	:param dados:
		
	:param code:

.. function:: Arv.csEditaProj 


	:comentário: 

.. function:: Arv.csEditaProj acao arg dados code


	:comentário: 
	:param acao:
		
	:param arg:
		
	:param dados:
		
	:param code:

.. function:: Arv.csCompilaProj 


	:comentário: 

.. function:: Arv.csCompila acao arg dados code


	:comentário: 
	:param acao:
		
	:param arg:
		
	:param dados:
		
	:param code:

.. function:: Arv.csExecAbs 


	:comentário: 

.. function:: Arv.csExecAbs acao arg dados code


	:comentário: 
	:param acao:
		
	:param arg:
		
	:param dados:
		
	:param code:

.. function:: Arv.csExecBuf 


	:comentário: 

.. function:: Arv.csExecBuf acao arg dados code


	:comentário: 
	:param acao:
		
	:param arg:
		
	:param dados:
		
	:param code:

.. function:: Arv.pegaAbstrais listaAbstrais


	:comentário: 
	:param listaAbstrais:

.. function:: Arv.prVariaveis op dados sdados teclas


	:comentário: 
	:param op:
		
	:param dados:
		
	:param sdados:
		
	:param teclas:

.. function:: Arv.csVariaveis op dados sdados teclas


	:comentário: 
	:param op:
		
	:param dados:
		
	:param sdados:
		
	:param teclas:

.. function:: Arv.csVariavel op dados sdados teclas


	:comentário: 
	:param op:
		
	:param dados:
		
	:param sdados:
		
	:param teclas:

.. function:: Arv.csDescVar op dados sdados teclas


	:comentário: 
	:param op:
		
	:param dados:
		
	:param sdados:
		
	:param teclas:

.. function:: Arv.csDadoVar op dados sdados teclas


	:comentário: 
	:param op:
		
	:param dados:
		
	:param sdados:
		
	:param teclas:

.. function:: Arv.csDadoOK op dados sdados teclas


	:comentário: 
	:param op:
		
	:param dados:
		
	:param sdados:
		
	:param teclas:

.. function:: Arv.pegaDado 


	:comentário: Pega um dado para colocacao na programacao do SuperClient Trata tambem com as
		variaveis fixas do sistema
		
		

