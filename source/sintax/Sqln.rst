
Sqln
====
.. function:: Sqln.conecta con


	:comentário: 
	:param con:

.. function:: Sqln.conecta con bd user pass base


	:comentário: 
	:param con:
		
	:param bd:
		
	:param user:
		
	:param pass:
		
	:param base:

.. function:: Sqln.strcriatab lista0 nometab strtab


	:comentário: 
	:param lista0:
		
	:param nometab:
		
	:param strtab:

.. function:: Sqln.strcriatab lista0 nometab strtab tipo


	:comentário: 
	:param lista0:
		
	:param nometab:
		
	:param strtab:
		
	:param tipo:

.. function:: Sqln.strinstab lista0 nometab strtab


	:comentário: 
	:param lista0:
		
	:param nometab:
		
	:param strtab:

.. function:: Sqln.xsql con resp arg


	:comentário: static public boolean xsql(Bds con, Bds resp, Bds arg) throws ErrBds    executa o comando arg em cima do banco de dados bd o argumento do comando sql  esta em arg, retorna verdadeiro se o comando foi bem sucedido e se no caso de  update, delete, etc conseguir agir em pelo menos uma linha no caso de insert  update e delete, resp retorna a qt de linhas inseridas, atualizadas ou  deletadas   O commit eh automatico
	:param con:
		
	:param resp:
		
	:param arg:

.. function:: Sqln.xsql con resp arg commit


	:comentário: static public boolean xsql(Bds con, Bds resp, Bds arg, Bds commit) throws ErrBds    executa o comando arg em cima do banco de dados bd o argumento do comando sql  esta em arg, retorna verdadeiro se o comando foi bem sucedido e se no caso de  update, delete, etc conseguir agir em pelo menos uma linha no caso de insert  update e delete, resp retorna a qt de linhas inseridas, atualizadas ou  deletadas   commit="S" - da um commit automatico
	:param con:
		
	:param resp:
		
	:param arg:
		
	:param commit:

.. function:: Sqln.query con cursor arg


	:comentário: static public boolean query(Bds con, Bds cursor, Bds arg) throws ErrBds    executa o comando arg em cima do banco de dados bd o argumento do comando sql  esta em arg, retorna verdadeiro se o comando foi bem sucedido e se no caso de  update, delete, etc conseguir agir em pelo menos uma linha no caso de insert  update e delete, resp retorna a qt de linhas inseridas, atualizadas ou  deletadas   commit="S" - da um commit automatico
	:param con:
		
	:param cursor:
		
	:param arg:

.. function:: Sqln.query con arg


	:comentário: 
	:param con:
		
	:param arg:

.. function:: Sqln.update con resp tab lista select


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:

.. function:: Sqln.update con resp tab lista select tudo


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:
		
	:param tudo:

.. function:: Sqln.insert con resp tab lista


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:

.. function:: Sqln.delete con resp tab select


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param select:

.. function:: Sqln.select con resp tab lista select sop


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:
		
	:param sop:

.. function:: Sqln.select con resp tab lista select sop


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:
		
	:param sop:

.. function:: Sqln.fecha con


	:comentário: 
	:param con:

.. function:: Sqln.t sqt


	:comentário: 
	:param sqt:

.. function:: Sqln.Opersql con resp tab lista select oper


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:
		
	:param oper:

.. function:: Sqln.Opersql0 con resp tab campo select oper


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param campo:
		
	:param select:
		
	:param oper:

.. function:: Sqln.count con resp tab campo select


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param campo:
		
	:param select:

.. function:: Sqln.sum con resp tab lista select


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:

.. function:: Sqln.avg con resp tab lista select


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:

.. function:: Sqln.max con resp tab lista select


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:

.. function:: Sqln.min con resp tab lista select


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:

.. function:: Sqln.criaerro arg


	:comentário: 
	:param arg:

.. function:: Sqln.criaerro arg


	:comentário: 
	:param arg:

.. function:: Sqln.ledad chave dado


	:comentário: 
	:param chave:
		
	:param dado:

.. function:: Sqln.xx 


	:comentário: 
		

.. function:: Sqln.gravadad chave dado


	:comentário: 
	:param chave:
		
	:param dado:

.. function:: Sqln.trataerrosql con resp arg


	:comentário: 
	:param con:
		
	:param resp:
		
	:param arg:

.. function:: Sqln.trataerrosql con resp arg


	:comentário: 
	:param con:
		
	:param resp:
		
	:param arg:

.. function:: Sqln.acertalista lista


	:comentário: 
	:param lista:

.. function:: Sqln.serv 


	:comentário: 

.. function:: Sqln.rselect con resp arg


	:comentário: 
	:param con:
		
	:param resp:
		
	:param arg:

.. function:: Sqln.pegaupd codupd


	:comentário: 
	:param codupd:

.. function:: Sqln.gravabk con cmd args


	:comentário: 
	:param con:
		
	:param cmd:
		
	:param args:

