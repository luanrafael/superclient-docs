
M2gLogger
=========
.. function:: M2gLogger.init 


	:comentário: 

.. function:: M2gLogger.logArquivo logName logFile


	:comentário: Método responsável por criar o arquivo de log.
		
		
		
	:rtype: boolean
		
		

	:param  logName: nome do Log
		
		

	:param  logFile: arquivo que conterá o log
		
		
	:throws:  ErrBds
		

.. function:: M2gLogger.logArquivo logName logFile appenders


	:comentário: Método responsável por criar o arquivo de log.
		
		
		
	:rtype: boolean
		
		

	:param  logName: nome do Log
		
		

	:param  logFile: arquivo que conterá o log
		
		

	:param  appenders: se contem "B" - indica que o log será gravado em banco também
		
		
	:throws:  ErrBds
		

.. function:: M2gLogger.logSocket logName host


	:comentário: Cria um appender to tipo socket
		
		
		

	:param  logName: nome do Log
		
		

	:param  host: endereço de onde o socket deve se conectar
		
		

.. function:: M2gLogger.logSocket logName host port


	:comentário: Cria um appender to tipo socket
		
		
		

	:param  logName: nome do Log
		
		

	:param  host: endereço de onde o socket deve se conectar
		
		

	:param  port: porta de acesso - default 25827
		

.. function:: M2gLogger.jsonLog listaLog resp


	:comentário: Converte uma lista BDS para JSON
		
		
		
	:rtype: boolean
		
		

	:param  listaLog: lista BDS com campos
		
		

	:param  resp: json criado a partir da lista
		
		

.. function:: M2gLogger.log dataLog


	:comentário: Salva log
		
		
		
	:rtype: boolean
		
		

	:param  dataLog: Log a ser salvo - pode ser um json ou uma string comum, quando
		string, transforma num json com a seguinte estrutura { "data":
		`string`}
		

.. function:: M2gLogger.log level dataLog


	:comentário: Salva log
		
		
		
	:rtype: boolean
		
		

	:param  level: 
		
		

	:param  dataLog: Log a ser salvo - pode ser um json ou uma string comum, quando
		string, transforma num json com a seguinte estrutura { "data":
		`string`}
		

.. function:: M2gLogger.logLista listaLog


	:comentário: Salva novo log
		
		
		
	:rtype: boolean
		
		

	:param  listaLog: lista de campos
		

.. function:: M2gLogger.logLista level listaLog


	:comentário: Salva novo log com level
		
		
		
	:rtype: boolean
		
		

	:param  level: nivel do LOG LEVEL_DEBUG = "D"; LEVEL_INFO = "I"; LEVEL_WARN =
		"W"; LEVEL_ERROR = "E"; LEVEL_FATAL = "F";
		
		
		

	:param  listaLog: lista de campos
		

