
Fon
===
.. function:: Fon.f fon str0


	:comentário: 
	:param fon:
		
	:param str0:

.. function:: Fon.conv strFon strOrig tipo


	:comentário: 
	:param strFon:
		
	:param strOrig:
		
	:param tipo:

.. function:: Fon.criaRand0 str resp


	:comentário: cria uma string com o randomico de uma string
		
		
		

	:param  str: string origem
		
		

	:param  resp: resposta com o randomico correspondente
		
		

.. function:: Fon.procTab resp str tipo tipoTab


	:comentário: 
	:param resp:
		
	:param str:
		
	:param tipo:
		
	:param tipoTab:

.. function:: Fon.insTab tabela indice conteudo tipos


	:comentário: 
	:param tabela:
		
	:param indice:
		
	:param conteudo:
		
	:param tipos:

.. function:: Fon.insTab tabela indice conteudo tipos diferenciaMin


	:comentário: 
	:param tabela:
		
	:param indice:
		
	:param conteudo:
		
	:param tipos:
		
	:param diferenciaMin:

.. function:: Fon.pqTab tabela indice conteudo tipo


	:comentário: 
	:param tabela:
		
	:param indice:
		
	:param conteudo:
		
	:param tipo:

.. function:: Fon.cargaTab tipoTab


	:comentário: 
	:param tipoTab:

.. function:: Fon.cargaTab arqTabela tipoTab


	:comentário: 
	:param arqTabela:
		
	:param tipoTab:

.. function:: Fon.testaTab arqTabela tipoTab


	:comentário: 
	:param arqTabela:
		
	:param tipoTab:

.. function:: Fon.cargaBd con nomeTabela tipoTab update


	:comentário: 
	:param con:
		
	:param nomeTabela:
		
	:param tipoTab:
		
	:param update:

.. function:: Fon.salvaTab tipoTab


	:comentário: 
	:param tipoTab:

.. function:: Fon.salvaTab arqTabela tipoTab


	:comentário: 
	:param arqTabela:
		
	:param tipoTab:

.. function:: Fon.salvaBd con nomeTabela tipoTab


	:comentário: 
	:param con:
		
	:param nomeTabela:
		
	:param tipoTab:

.. function:: Fon.trataAtalho tabela atalho


	:comentário: 
	:param tabela:
		
	:param atalho:

.. function:: Fon.trataAtalho tabela atalho componente


	:comentário: 
	:param tabela:
		
	:param atalho:
		
	:param componente:

.. function:: Fon.trataTrad tabela atalho componente


	:comentário: 
	:param tabela:
		
	:param atalho:
		
	:param componente:

.. function:: Fon.pqTrad traducao origem


	:comentário: 
	:param traducao:
		
	:param origem:

.. function:: Fon.inicFon 


	:comentário: 
		
		inicializa os vetores de trabalho da fonetizacao
		

.. function:: Fon.fonPorto fonet resp


	:comentário: fonPorto
		
		devolve os tres indices do fonetico separados por branco
		
		
		

	:param  String: a ser fonetizada
		
		
		

	:param  Indices: foneticos de resposta
		
		
		

	:param  tipo: 
	: Tipo de fonetizacao desejada (Pessoa, Juridico, Endereco ou
		Normalizacao
		
		

