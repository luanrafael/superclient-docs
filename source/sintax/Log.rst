
Log
===
.. function:: Log.criaLog nomeproc ordemproc localhost idLog idSeveridade nTag dsLog


	:comentário: 
	:param nomeproc:
		
	:param ordemproc:
		
	:param localhost:
		
	:param idLog:
		
	:param idSeveridade:
		
	:param nTag:
		
	:param dsLog:

.. function:: Log.procLog 


	:comentário: 

.. function:: Log.insert tipoLog


	:comentário: 
		
		Atencao - assume que a lista esta montada e disponivel
		
		
		

	:param  tipoLog: 
		= "C" se comentario ou "E" se erro
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Log.procComents 


	:comentário: 

.. function:: Log.procErros 


	:comentário: 

.. function:: Log.procDisplay 


	:comentário: 

.. function:: Log.flush 


	:comentário: 

.. function:: Log.convData dt


	:comentário: converte a data dt para a data de banco de dados (aaaa-mm-dd hh:mm:ss)
		
		
		

	:param  dt: 
		
		
	:throws:  ErrBds
		

.. function:: Log.insert tipoLog


	:comentário: 
		
		
		

	:param  tipoLog: 
		= "C" se comentario ou "E" se erro
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Log.prepLog regLog


	:comentário: 
	:param regLog:

.. function:: Log.montaDesc con resp cod arg


	:comentário: pega as descricoes de cada codigo de erro ATENCAO - a tabela nao pode ter
		buracos!
		
		
		

	:param  con: 
		
		

	:param  resp: 
		
		

	:param  cod: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Log.cargaDescsBdmax arq


	:comentário: Carrega as descricoes do Bdmax e coloca na tabela descLog
		
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		
	:param arq:

.. function:: Log.pesqLog arqFiltrado tipolang tipo dt horainic horafinal thread filtro1 filtro2 bytesInic qtLinhas


	:comentário: 
	:param arqFiltrado:
		
	:param tipolang:
		
	:param tipo:
		
	:param dt:
		
	:param horainic:
		
	:param horafinal:
		
	:param thread:
		
	:param filtro1:
		
	:param filtro2:
		
	:param bytesInic:
		
	:param qtLinhas:

.. function:: Log.pqlog arqtrab drive tipo dt filtros


	:comentário: Esse método executa uma pesquisa referente aos logs existentes no sistema de
		arquivos.
		
		
		
		
		

	:param  drive: drive mapeado raiz do ambiente de onde se deseja o log
		
		

	:param  tipo: especifica o tipo do log consultado (Java, BDMAX).
		
		

	:param  dia: filtra os logs referentes ao dia especificado.
		
		

	:param  filtros: especifica os filtros desejados separados por vírgula. O método
		lista todas as linhas de log que contém TODOS os filtros.
		

.. function:: Log.lelog arqlog arqtrab filtros


	:comentário: Esse método efetua uma pesquisa no diretório especificado exibindo os
		arquivos referentes aos filtros.
		
		
		

	:param  arqlog: especifica o caminho e o nome do arquivo.
		
		

	:param  filtros: especifica o filtro interno do arquivo.
		

.. function:: Log.countTronco arqlog vazios cheios


	:comentário: Esse método efetua uma pesquisa no diretório especificado exibindo os
		arquivos referentes aos filtros.
		
		
		

	:param  arqlog: especifica o caminho e o nome do arquivo.
		
		

	:param  filtros: especifica o filtro interno do arquivo.
		

.. function:: Log.tratadt dt0 dtrel


	:comentário: 
	:param dt0:
		
	:param dtrel:

.. function:: Log.trataintervalo intervalo dt0


	:comentário: Esse método trata um intervalo de datas passado pelo usuário e retorna um Bds
		contendo todas as datas entre o intervalo com a formatação específica do
		arquivo log.
		
		<pre>
		Exemplo1:
		
		Log.trataintervalo resp '12-23'
		------------------------------------------
		B.exib resp
		040612 040613 040614 040615 040616 040617 040618 040619 040620 040621 040622 040623
		
		
		Exemplo2:
		
		Log.trataintervalo resp '23/01-05/02'
		------------------------------------------
		B.exib resp
		040123 040124 040125 040126 040127 040128 040129 040130 040131 040201 040202 040203
		040204 040205
		
		
		Exemplo3:
		
		Log.trataintervalo resp '25/12/2001-05/01/2002'
		------------------------------------------
		B.exib resp
		011225 011226 011227 011228 011229 011230 011231 020101 020102 020103 020104 020105
		
		OBS.: Otimização - tratar as extremidades dos meses.
		</pre>
		
		
		

	:param  intervalo: retorna um intervalo de datas especificado em dt0.
		
		

	:param  dt0: especifica o intervalo a ser pesquisado.
		

.. function:: Log.temLog flagLog


	:comentário: 
	:param flagLog:

.. function:: Log.sempreMostraLog flagLog


	:comentário: 
	:param flagLog:

.. function:: Log.montaTabLog 


	:comentário: monta todos os dados da tabela de log verifica se em dsComplLogTrab existem
		strings separadas por virgula do tipo: xxxx,dsLog1=dado1,dsLog2=dado2,xxx,
		etc verifica nesta string se existem dados correspondentes a :
		
		id1 id2 id3 id4 id5 dsLog1 dsLog2 dsLog3 dsLog4 dsLog5 dsLog6 dsLog7 dsLog8
		dsLog9 dsLog10 versao
		
		se existir, cria o dado correspondente e retira a string da tabela
		
		
		
	:throws:  ErrBds
		

.. function:: Log.criaDadoLog strLog dadoLog nomeDado


	:comentário: 
		
		
		
	:throws:  ErrBds
		
	:param strLog:
		
	:param dadoLog:
		
	:param nomeDado:

