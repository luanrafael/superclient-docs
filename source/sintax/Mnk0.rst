
Mnk0
====
.. function:: Mnk0.listaJanelas listaJanelas


	:comentário: Coloca em
		
		
		
	:rtype: void  Um array de string contendo todas as janelas abertas
		
		
	:throws:  ErrBds
		
	:param listaJanelas:

.. function:: Mnk0.focaJanela nomeJanela


	:comentário: Exibe a janela e coloca o foco nela, assim ela passa a receber as entradas do
		usuário, como teclado e mouse
		
		
		

	:param  nomeJanela: O nome inteirto ou parte dO nome inteirto ou parte do nome da
		janela
		
		
	:rtype: boolean  Se fou possível dar o foco na janela
		
		
	:throws:  ErrBds
		

.. function:: Mnk0.focaJanela nomeJanela


	:comentário: Exibe a janela e coloca o foco nela, assim ela passa a receber as entradas do
		usuário, como teclado e mouse
		
		
		

	:param  nomeJanela: O nome inteirto ou parte dO nome inteirto ou parte do nome da
		janela
		
		
	:rtype: boolean  Se fou possível dar o foco na janela
		
		
	:throws:  ErrBds
		

.. function:: Mnk0.DimensionaJanela nomeJanela altura largura


	:comentário: Dimensiona a janela
		
		
		

	:param  nomeJanela: O nome inteirto ou parte do nome da janela
		
		

	:param  altura: A nova largura em pixels
		
		

	:param  largura: A nova altura em pixels
		
		
	:rtype: boolean  Se foi possível dimensionar a janela
		
		
	:throws:  ErrBds
		

.. function:: Mnk0.DimensionaJanela nomeJanela esquerda topo altura largura


	:comentário: 
	:param nomeJanela:
		
	:param esquerda:
		
	:param topo:
		
	:param altura:
		
	:param largura:

.. function:: Mnk0.PosicionaJanela nomeJanela esquerda topo altura largura


	:comentário: 
	:param nomeJanela:
		
	:param esquerda:
		
	:param topo:
		
	:param altura:
		
	:param largura:

.. function:: Mnk0.posicionaMouse x y


	:comentário: Posiciona o mouse no local especificado
		
		
		

	:param  x: A posição horizontal em pixels
		
		

	:param  y: A posição vertical em pixels
		
		
	:rtype: boolean  Se foi possível posicionar o mouse
		
		
	:throws:  ErrBds
		

.. function:: Mnk0.enviaTecla teclas


	:comentário: Envia teclas
		
		
		

	:param  teclas: As teclas a serem enviadas
		
		
	:rtype: boolean  Se foi possível enviar as teclas
		
		
	:throws:  ErrBds
		

.. function:: Mnk0.enviaTecla teclas


	:comentário: Envia teclas
		
		
		

	:param  teclas: As teclas a serem enviadas
		
		
	:rtype: boolean  Se foi possível enviar as teclas
		
		
	:throws:  ErrBds
		

.. function:: Mnk0.enviaTecla teclas nomeJanela


	:comentário: Envia teclas para uma janela, o foco é dado automaticamente
		
		
		

	:param  teclas: As teclas a serem enviadas
		
		

	:param  nomeJanela: O nome inteirto ou parte do nome da janela
		
		
	:rtype: boolean  Se foi possível enviar as teclas
		
		
	:throws:  ErrBds
		

.. function:: Mnk0.enviaTecla teclas nomeJanela


	:comentário: 
	:param teclas:
		
	:param nomeJanela:

.. function:: Mnk0.escondeJanelaCortina 


	:comentário: Esconde uma janela cortina em exibição
		
		
		
	:throws:  ErrBds
		

.. function:: Mnk0.gravaMacro arquivo


	:comentário: Inicia uma gravação de Macro
		
		
		

	:param  arquivo: O nome do arquivo que será gravado contendo a macro
		
		
	:rtype: boolean  Se foi possível iniciar a gravação
		
		
	:throws:  ErrBds
		

.. function:: Mnk0.paraGravacaoMacro 


	:comentário: Para uma gravação de macro em andamento
		
		
		
	:rtype: boolean  Se foi possível parar a gravação
		
		
	:throws:  ErrBds
		

.. function:: Mnk0.playMacro arquivo


	:comentário: Inicia um play back de Macro
		
		
		

	:param  arquivo: O nome do arquivo que contem uma macro gravada
		
		
	:rtype: boolean  Se foi possível iniciar o play back
		
		
	:throws:  ErrBds
		

.. function:: Mnk0.paraPlayMacro 


	:comentário: Para um play back de macro em andamento
		
		
		
	:rtype: boolean  Se foi possível parar o play back
		
		
	:throws:  ErrBds
		

.. function:: Mnk0.paraMacro 


	:comentário: Para uma gravação ou playback de macro em andamento
		
		
		
	:rtype: boolean  Se foi possível parar a gravação ou playback
		
		
	:throws:  ErrBds
		

