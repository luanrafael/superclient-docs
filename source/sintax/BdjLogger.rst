
BdjLogger
=========
.. function:: BdjLogger.logArquivo logName logFile


	:comentário: M?todo responsável por criar o arquivo de log.
		
		
		
	:rtype: boolean
		
		

	:param  logName: nome do Log
		
		

	:param  logFile: arquivo que conter? o log
		
		
	:throws:  ErrBds
		

.. function:: BdjLogger.logArquivo logName logFile appenders


	:comentário: M?todo responsável por criar o arquivo de log.
		
		
		
	:rtype: boolean
		
		

	:param  logName: nome do Log
		
		

	:param  logFile: arquivo que conter? o log
		
		

	:param  appenders: se contem "B" - indica que o log ser? gravado em banco tamb?m
		
		
	:throws:  ErrBds
		

.. function:: BdjLogger.logSocket logName host


	:comentário: Cria um appender to tipo socket
		
		
		

	:param  logName: nome do Log
		
		

	:param  host: endere?o de onde o socket deve se conectar
		
		

.. function:: BdjLogger.logSocket logName host port


	:comentário: Cria um appender to tipo socket
		
		
		

	:param  logName: nome do Log
		
		

	:param  host: endere?o de onde o socket deve se conectar
		
		

	:param  port: porta de acesso - default 25827
		

.. function:: BdjLogger.jsonLog listaLog resp


	:comentário: Converte uma lista BDS para JSON
		
		
		
	:rtype: boolean
		
		

	:param  listaLog: lista BDS com campos
		
		

	:param  resp: json criado a partir da lista
		
		

.. function:: BdjLogger.criaBdjLog filaLog forca


	:comentário: se nao existe uma fila de log, cria
		
		
		

	:param  filaLog: 
		
		

	:param  forca: se igual 1 forca a criacao da fila anulando a fila anterior
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: BdjLogger.poeFilaLog filaLog tipo strLog


	:comentário: poe na fila do log e se a fila esta cheia, grava local
		
		
		

	:param  filaLog: 
		
		

	:param  tipo: 
		
		

	:param  strLog: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: BdjLogger.log dataLog


	:comentário: Salva log
		
		
		
	:rtype: boolean
		
		

	:param  dataLog: Log a ser salvo - pode ser um json ou uma string comum, quando
		string, transforma num json com a seguinte estrutura { "data":
		`string`}
		

.. function:: BdjLogger.log level dataLog


	:comentário: Salva log
		
		
		
	:rtype: boolean
		
		

	:param  level: 
		
		

	:param  dataLog: Log a ser salvo - pode ser um json ou uma string comum, quando
		string, transforma num json com a seguinte estrutura { "data":
		`string`}
		

.. function:: BdjLogger.log0 level dataLog


	:comentário: Salva log
		
		
		
	:rtype: boolean
		
		

	:param  level: 
		
		

	:param  dataLog: Log a ser salvo - pode ser um json ou uma string comum, quando
		string, transforma num json com a seguinte estrutura { "data":
		`string`}
		

.. function:: BdjLogger.logLista listaLog


	:comentário: Salva novo log
		
		
		
	:rtype: boolean
		
		

	:param  listaLog: lista de campos
		

.. function:: BdjLogger.logLista level listaLog


	:comentário: Salva novo log com level
		
		
		
	:rtype: boolean
		
		

	:param  level: nivel do LOG LEVEL_DEBUG = "D"; LEVEL_INFO = "I"; LEVEL_WARN =
		"W"; LEVEL_ERROR = "E"; LEVEL_FATAL = "F";
		
		
		

	:param  listaLog: lista de campos
		

.. function:: BdjLogger.resetaLog 


	:comentário: usado para resetar o log no caso de se quizer fazer uma nova conexao para o
		log
		
		
		
	:rtype: void
		
		
	:throws:  ErrBds
		

.. function:: BdjLogger.inic rastro pambiente


	:comentário: 
		
		inicia o log , assume que o ip ja esta em $ipLog
		
		
		

	:param  rastro: rastro do processo (Ex: "SLA,PROT,etc"
		
		

	:param  pambiente: tipo de operacao (PROD,DEV,
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: BdjLogger.inic host rastro pambiente


	:comentário: 
		
		
		

	:param  host: 
		
		

	:param  rastro: 
		
		

	:param  pambiente: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: BdjLogger.jafoi host rastro pambiente


	:comentário: 
	:param host:
		
	:param rastro:
		
	:param pambiente:

.. function:: BdjLogger.addVar nome


	:comentário: Adiciona o nomede uma variavel ? lista de variaveis a serem limpas
		
		
		

	:param  nome: 
		
		
	:throws:  ErrBds
		

.. function:: BdjLogger.addVar nome


	:comentário: Adiciona o nomede uma variavel ? lista de variaveis a serem limpas
		
		
		

	:param  nome: 
		
		
	:throws:  ErrBds
		

.. function:: BdjLogger.limpaVars nome


	:comentário: 
	:param nome:

.. function:: BdjLogger.limpaVars 


	:comentário: 

.. function:: BdjLogger.criaCp nome


	:comentário: 
	:param nome:

.. function:: BdjLogger.criaCp nome


	:comentário: se ainda "nome" nao for Cp, cria-lo como Cp
		
		
		

	:param  nome: 
		
		
	:throws:  ErrBds
		

.. function:: BdjLogger.uneJsons jresp j1 j2


	:comentário: Une dois jsons e devolve como string
		
		
		

	:param  jresp: resposta com a uniao dos dois jsons. Pode conter tambem mensagem
		de erro
		
		

	:param  j1: 
		
		

	:param  j2: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: BdjLogger.set nome valor


	:comentário: seta uma variavel bdjava = a um valor
		
		
		

	:param  nome: 
		
		

	:param  valor: 
		
		
	:throws:  ErrBds
		

.. function:: BdjLogger.set nome valor


	:comentário: seta uma variavel bdjava = a um valor
		
		
		

	:param  nome: 
		
		

	:param  valor: 
		
		
	:throws:  ErrBds
		

.. function:: BdjLogger.get valor nome


	:comentário: retorna uma string com o valor da variavel bdjava de nome = nome
		
		
		

	:param  nome: 
		
		
	:rtype: void
		
		
	:throws:  ErrBds
		

.. function:: BdjLogger.getLista lista pargs


	:comentário: 
	:param lista:
		
	:param pargs:

.. function:: BdjLogger.getProp nome prop


	:comentário: retorna o nome de uma propriedade depois de guarda-la em memoria
		
		
		

	:param  lista: com o nome da propriedade
		
		

	:param  prop: 
		
		
	:rtype: void
		
		
	:throws:  ErrBds
		

.. function:: BdjLogger.log xargs


	:comentário: 
	:param xargs:

.. function:: BdjLogger.tUDP ip porta


	:comentário: 
	:param ip:
		
	:param porta:

.. function:: BdjLogger.criaBdjLog filaLog


	:comentário: 
		
		
		
		String getJsonLista(String nomesLista) idem - retorna o json correspondente a
		todos os dados das lista que estao em nomes Lista (separadas por virgula). -
		Monta um Json correspondente ? uniao das listas
		
		log (String rastro, String passo, String info1, String info2, String
		info3?..) Sequencia de informacoes a constarem do log
		
		O rastro e o passo, sao Strings com o conteudo destas informacoes. Sao
		respectivamente catalogados como os campos "rastro" e "passo" no json que
		ser? enviado para o log4j e consequentemente para o servidor de log.
		
		info1,info2,info3 pode ser um dos seguintes dados: uma sequencia de pares
		<nome>=<valor> de dados, separados por virgula (os dados n?o podem ter
		virgula; uma sequencia de nomes de dados ja armazenados no ambiente bdjava,
		separados por virgula; uma lista de dados j? cadastrada; um json ja formado
		com uma sequencia de "nome","valor" de dados que se deseja enviar para o log
		
		
		
		
		
		
	:param filaLog:

