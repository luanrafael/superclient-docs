
Html
====
.. function:: Html.inic 


	:comentário: 

.. function:: Html.montaCampo strCps cp


	:comentário: 
	:param strCps:
		
	:param cp:

.. function:: Html.monta dadosTela arqTrabHtml nomeTela


	:comentário: 
	:param dadosTela:
		
	:param arqTrabHtml:
		
	:param nomeTela:

.. function:: Html.tela dadosTela prompt largurax larguray posx posy


	:comentário: 
	:param dadosTela:
		
	:param prompt:
		
	:param largurax:
		
	:param larguray:
		
	:param posx:
		
	:param posy:

.. function:: Html.limpaTela dadosTela


	:comentário: limpa uma tela , e na proxima invocacao, chama outra vez do zero
		
		
		

	:param  dadosTela: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Html.setTxt pcampos dadosTela


	:comentário: seta o valor de um campo ou uma lista de campos em uma tela
		
		
		

	:param  campos: 
		
		

	:param  dadosTela: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Html.getTxt pcampos dadosTela


	:comentário: pega o valor de um campo ou uma lista de campos em uma tela
		
		
		

	:param  campos: 
		
		

	:param  dadosTela: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

