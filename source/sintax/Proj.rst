
Proj
====
.. function:: Proj.faq args


	:comentário: 
	:param args:

.. function:: Proj.etodo 


	:comentário: 

.. function:: Proj.ltodo ops filtros


	:comentário: 
	:param ops:
		
	:param filtros:

.. function:: Proj.ptodo arg pos posinic


	:comentário: 
	:param arg:
		
	:param pos:
		
	:param posinic:

.. function:: Proj.pegaopt arg pos posinic


	:comentário: static public Bds pegaopt(Bds arg, Bds pos, Bds posinic) throws ErrBds     retorna a palavra correspondente a posicao pos em arg, o primo char de arg  tem a posicao posinic
	:param arg:
		
	:param pos:
		
	:param posinic:

.. function:: Proj.afazer args


	:comentário: 
	:param args:

.. function:: Proj.atodo arg


	:comentário: 
	:param arg:

.. function:: Proj.sc args janela


	:comentário: Envia comandos ao journalling, com possibilidade de pausa
		
		sintaxe : .sendcmdj comando janela tempopausa1 tempopausa2 qtVezes exemplo : enviar comando esc para janela <xxx> com <pausa 1 500> 6 vezes .sendcmdj
		'{ESC}' 'xxx' 1 500 6
		
		
		OBS : Este comando depende do tipo de teclado que se esta usando.
		
		pode-se setar o tipo de teclado : nulo para teclado US sem acento 1 para
		teclado US internacional - Brasil 2 para teclado ABNT Brasil
		
		
		No Java : v.tecladoJournalling.s(<teclado>); No Clide: faz tecladoJournaling
		'<teclado>'
		
		
		
		

	:param  args:  sequencia de tecladas a serem enviadas para
		
		

.. function:: Proj.sc args janela resp


	:comentário: 
	:param args:
		
	:param janela:
		
	:param resp:

.. function:: Proj.sc str janela resp op teclado


	:comentário: Esse método envia argumentos ao servidor Journalling que faz uma requisição
		pela janela especificada nos parâmetros de entrada.
		
		<pre>
		Exemplo: <b>Proj.sc 'teste epsoft' 'textpad' resp '8'<\b> // escreve "teste
		epsoft" no TextPad. <b>Proj.sc '' 'textpad' resp '9'<\b> // apenas dá o foco
		na janela do TextPad. <\pre>
		
		
		

	:param  str: string a ser enviada
		
		

	:param  janela: epecifica a janela que será ativada pelo servidor Journalling
		
		

	:param  resp: retorna "0" se OK e "-1" se a janela não for encontrada.
		
		

	:param  teclado: especifica o tipo de teclado. 1 para teclado Brazilian US
		International 2 para Portuguese Standard != 1 e 2 o que estiver
		configurado no momento
		

.. function:: Proj.sc args janela resp op


	:comentário: trata acentuação enviada pelo Usuário
		}
		
		Esse método envia argumentos ao servidor Journalling que faz uma requisição
		pela janela especificada nos parâmetros de entrada.
		
		<pre>
		Exemplo: <b>Proj.sc 'teste epsoft' 'textpad' resp '8'<\b> // escreve "teste
		epsoft" no TextPad. <b>Proj.sc '' 'textpad' resp '9'<\b> // apenas dá o foco
		na janela do TextPad. <\pre>
		
		
		

	:param  str: string a ser enviada
		
		

	:param  janela: epecifica a janela que será ativada pelo servidor Journalling
		
		

	:param  resp: retorna "0" se OK e "-1" se a janela não foi encontrada.
		

.. function:: Proj.nfac nomefaq ext


	:comentário: static public void nfac(Bds nomefaq, Bds ext) throws ErrBds    Cria umnovo arquivo de confaquracao com o nome indicado e a extensao desejada  (faq para .faq, tel para .tel, prm para ,prm
	:param nomefaq:
		
	:param ext:

.. function:: Proj.nfaq nomefaq ext templatex


	:comentário: static public void nfaq(Bds nomefaq, Bds ext, Bds templatex) throws ErrBds    Cria umnovo arquivo de confaquracao com o nome indicado e do template  indicado
	:param nomefaq:
		
	:param ext:
		
	:param templatex:

.. function:: Proj.convasc THbd THwindow resp teclado


	:comentário: 
	:param THbd:
		
	:param THwindow:
		
	:param resp:
		
	:param teclado:

.. function:: Proj.convasc THbd resp teclado


	:comentário: 
	:param THbd:
		
	:param resp:
		
	:param teclado:

.. function:: Proj.seqEmsg arquivo


	:comentário: 
	:param arquivo:

.. function:: Proj.psList MaqLogin processo dirLog ultLogs


	:comentário: 
	:param MaqLogin:
		
	:param processo:
		
	:param dirLog:
		
	:param ultLogs:

