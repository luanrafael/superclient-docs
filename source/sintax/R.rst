
R
=
.. function:: R.tparte nome parte


	:comentário: 
	:param nome:
		
	:param parte:

.. function:: R.tget 


	:comentário: pega uma string de comando bdjava e a codifica
		
		
		

	:param  cmd: comando a ser enviado
		
		

	:param  cod: comando codificado
		
		
	:throws:  ErrBds
		

.. function:: R.exec arg resp


	:comentário: 
	:param arg:
		
	:param resp:

.. function:: R.exec ipCloud portaCloud servicoCloud prmte pconsumidor pdest pret ptimeout arg resp


	:comentário: 
	:param ipCloud:
		
	:param portaCloud:
		
	:param servicoCloud:
		
	:param prmte:
		
	:param pconsumidor:
		
	:param pdest:
		
	:param pret:
		
	:param ptimeout:
		
	:param arg:
		
	:param resp:

.. function:: R.proc argEnv argResp


	:comentário: 
	:param argEnv:
		
	:param argResp:

.. function:: R.procx argEnv argResp


	:comentário: 
	:param argEnv:
		
	:param argResp:

.. function:: R.bdjServer ident consumidor timeout


	:comentário: 
	:param ident:
		
	:param consumidor:
		
	:param timeout:

.. function:: R.bdjTester ident dest timeout


	:comentário: 
	:param ident:
		
	:param dest:
		
	:param timeout:

.. function:: R.exemplo 


	:comentário: exemplo para um comando nativo
		
		
		
	:throws:  ErrBds
		

.. function:: R.exemplo arg


	:comentário: exemplo para um comando nativo
		
		
		
	:throws:  ErrBds
		
	:param arg:

.. function:: R.criaThreadsRest inic fim


	:comentário: 
	:param inic:
		
	:param fim:

.. function:: R.ativaThreadRest 


	:comentário: 

