
B
=
.. function:: B.toNoErro 


	:comentário: 

.. function:: B.jaMostrouErro 


	:comentário: public static boolean jaMostrouErro = false;  // EXEC   // interpretador de comandos  // #2    O método B.exec interpreta comandos que são na realidade chamadas a Classes e  Métodos JAVA. Entretanto, a chamada Classe/Método JAVA, atende a sintaxe do  antigo Bdmax, ou seja, apresenta algumas características específicas como por  exemplo:   <pre> 			- necessita de ponto e vírgula ";" apenas para separar comandos de uma linha. Se existir 			 apenas um comando na linha não será necessário a terminação com ponto e vírgula ";". 					Sintaxe:  								1o) comando1 ; comando2 								2o) comnado1  					Exemplo:  								faz teste 'Olá, pessoal' 								exib teste  					ou simplesmente:  								faz teste 'Olá, pessoal' ; exib teste    			- não utiliza parênteses "()" para passagem de parâmetros, ou seja, os argumentos são listados 			 na ordem exigida pelo Método, separados por espaço. As constantes são englobadas por plics " ' " 			 e as variáveis não necessitam de marcadores, ou seja, apenas o nome da variável de estado global 			 é suficiente para o Bdjava consultar a tabela de variáveis da classe C.java. 					Sintaxe:  								1o) comando 'constante1' 'constante2' variável1 'constante3' 								2o) comando variável1 variável2  					Exemplo:  								faz str1 'teste'   </pre>   Essa sobrecarga do método B.exec disponibiliza uma passagem do comando e dos  seus argumentos encapsulados em um objeto do tipo String. Essa característica  possibilita a utilização do método B.exec em qualquer aplicativo implementado  em JAVA.
		

	:param  scomando:             parâmetro responsável pela definição do comando que será executado             por B.exec Obs.: A String deve encapsular o comando e seus             argumentos obedecendo a sintaxe do antigo Bdmax.
		
	:rtype: boolean  retorna falso, caso ocorra alguma falha na execução do comando.

.. function:: B.exec comando


	:comentário: Essa sobrecarga do método B.exec possibilita a passagem do comando e dos seus
		argumentos encapsulados em um objeto do tipo Bds.
		
		
		

	:param  comando: parâmetro responsável pela definição do comando e dos argumentos
		que serão processados pelo método B.exec.
		
		
	:rtype: boolean  retorna falso, caso ocorra alguma falha na execução do comando.
		
		
	:see:  Bds
		

.. function:: B.exec comando sinc


	:comentário: Essa sobrecarga do método B.exec possibilita a passagem do comando e dos seus
		argumentos encapsulados em um objeto do tipo Bds e sendo sincronizados entre
		9 synchronizers (de 1 a 9)
		
		
		

	:param  comando: parâmetro responsável pela definição do comando e dos argumentos
		que serão processados pelo método B.exec.
		
		

	:param  sinc: 
		"synchronizer" - se dois exec com dois sincs iguais forem
		chamados ao mesmo tempo, um aguardará o outro ser executado
		
		
	:rtype: boolean  retorna falso, caso ocorra alguma falha na execução do comando.
		
		
	:see:  Bds
		

.. function:: B.execSinc1 comando


	:comentário: 
	:param comando:

.. function:: B.execLock comando nblock


	:comentário: executa um comando com considerando LOCK (somente executa um de cada vez)
		Nesta funcao, um novo comando quando ja tem um execucao, aguarda para ser
		executado e quando o anterior for liberado, ele é executado Atencao Nao é
		fila, nao existe ordem de prioridade.
		
		
		

	:param  comando: comando a ser executado
		
		

	:param  nblock: numero do bloqueio / lock
		
		
	:rtype: boolean  - retorna o resultado da execucao do comando
		
		
	:throws:  ErrBds
		

.. function:: B.execBloc comando nblock


	:comentário: Se um comando está em execucao, nao executa nenhum outro ao mesmo tempo,
		retornando falso para a funcao que solicitou a execucao do comando que chegou
		extra executa um comando com considerando LOCK (somente executa um de cada
		vez)
		
		
		

	:param  comando: comando a ser executado
		
		

	:param  nblock: numero do bloqueio / lock
		
		
	:rtype: boolean  - retorna o resultado da execucao do comando ou entao falso se se
		tentar executar um comando extra
		
		
	:throws:  ErrBds
		

.. function:: B.parsecmd comando


	:comentário: 
	:param comando:

.. function:: B.bdjava 


	:comentário: 

.. function:: B.bdjava arg


	:comentário: 
	:param arg:

.. function:: B.bdjava arg0 svars parms


	:comentário: Para a terceira sobrecarga, o método bdjava cria uma Thread nova
		possibilitando a personalização de algumas características referente ao
		contexto.
		
		<pre>
		Os args devem vir sempre na seguinte ordem: Argumento de comando (se houver algum plic, deve vir ao contrario ou vir
		como "\027", a qtde de variaveis e parametros para  criacao
		separados por virgula, que podem ser :
		<numero> - se for um numero maior que zero, vai criar o bdjava exatamente
		naquele ponto,
		<T-letra-> vai para a Thread com a letra indicada (11=A, 12 = B, etc)
		
		<!> executa o  comando na thread mesmo se esta ja existir
		
		exemplos : bdjava '' '600' '23,J'
		tenta criar um bdjava com 600 variaveis na thread 23 com uma janela
		
		bdjava '' '600' '23'
		tenta criar um bdjava com 600 variaveis na thread 23 sem janela aberta
		
		bdjava 'exib 1' '600' 'J'
		cria o bdjava na primeira thread apos o 10, com uma janela
		
		bdjava '' '600' 'TF'
		tenta criar um bdjava com 600 variaveis na thread "F" (=10+6=16) sem janela aberta
		
		
		
		</pre>
		
		
		

	:param  arg: especifica o comando a ser executado após a criação do novo CLIDE.
		
		

	:param  svars: especifica o tamanho da tabela de variáveis de estado global.
		
		

	:param  parms: especifica os parâmetros relacionados à Thread desejada.
		

.. function:: B.reiniciaBdjava 


	:comentário: 

.. function:: B.fim 


	:comentário: public static void fim() throws ErrBds    acaba com uma thread, e se for informada a thread, mata a thread indicada

.. function:: B.fim sth


	:comentário: Esse Método acaba com a Thread especificada no parametro de entrada.
		
		<pre>
		Por exemplo:
		
		fim 11
		
		Descrição: fecha a Thread de nome "11" especificada como parâmetro do tipo Bds.
		</pre>
		
		
		

	:param  sth: especifica o nome da Thread a ser fechada.
		
		
		@eppM 040521 - alteracao para registro do fim

.. function:: B.fim sth0 sth1


	:comentário: Esse Método acaba com uma faixa de Threads especificadas nos parâmetros de
		entrada.
		
		<pre>
		Por exemplo:
		
		fim 11 20
		
		Descrição: fecha todas as Threads especificadas entre 11 e 20.
		</pre>
		
		
		

	:param  sth0: especifica a Thread inicial da lista de Threads que serão
		fechadas.
		
		

	:param  sth1: especifica a última Thread a ser fechada, estabelecendo assim, o
		fim da lista de Threads.
		
		

.. function:: B.locthread ith strproc


	:comentário: 
	:param ith:
		
	:param strproc:

.. function:: B.getvar snome


	:comentário: Esse método retorna o objeto do tipo BDS alocado a uma variável global
		referente à tabela de variáveis de estado da Thread correspondente.<br>
		Essa técnica de programação permite a recompilação das Classes Bdjava sem que
		percam o conteúdo das variáveis de estado global, mantendo assim, o contexto
		da aplicação.
		
		<pre>
		Exemplo:
		
		{
		¨					<b>Bds t = B.getvar("t");</b>
		¨					t.s(""+B.locthread("BDJAVA-MAIN")); // atribui a (t) o número da ThreadMain.
		}
		
		No exemplo acima, o método getvar, cria na tabela da classe C.java, uma nova
		referência ao objeto (t).
		
		Mesmo depois da finalização do programa Exemplo, podemos acessar o valor de (t), pois,
		esse objeto continua sendo apontado por uma determinada posição da tabela
		de variáveis da classe C.java referente à Thread corrente.
		
		Digitando no CLIDE o comando,
		
		exib t
		
		teremos como resultado o número (1) referente a Thread princiapl.
		</pre>
		
		
		

	:param  snome: especifica o nome da variável global a ser pesquisada na tabela de
		variáveis da Thread corrente.
		
		
	:rtype: Bds  retorna uma referência ao objeto do tipo Bds alocado à posição da
		tabela de variáveis pesquisada.
		

.. function:: B.getvar nome


	:comentário: 
		@epp040627 - variaveis que comecam com $ vao obrigatgoriamente para a thread
		zero Esse método retorna o objeto do tipo BDS alocado a uma
		variável global referente à tabela de variáveis de estado da
		Thread correspondente.<br>
		Essa técnica de programação permite a recompilação das Classes
		Bdjava sem que percam o conteúdo das variáveis de estado global,
		mantendo assim, o contexto da aplicação.
		
		<pre>
		Exemplo:
		
		{
		¨					<b>Bds t = B.getvar(B.s("t"));</b>
		¨					t.s(""+B.locthread("BDJAVA-MAIN")); // atribui a (t) o número da ThreadMain.
		}
		
		No exemplo acima, o método getvar, cria na tabela da classe C.java, uma nova
		referência ao objeto (t).
		
		Mesmo depois da finalização do programa Exemplo, podemos acessar o valor de (t), pois,
		esse objeto continua sendo apontado por uma determinada posição da tabela
		de variáveis da classe C.java referente à Thread principal.
		
		Digitando no CLIDE o comando,
		
		exib t
		
		teremos como resultado o número (1) referente a Thread princiapl.
		</pre>
		
		
		

	:param  nome: especifica o nome da variável global a ser pesquisada na tabela de
		variáveis da Thread corrente.
		
		
	:rtype: Bds  retorna uma referência ao objeto do tipo Bds alocado à posição da
		tabela de variáveis pesquisada.
		

.. function:: B.setprop nome val


	:comentário: O método setprop seta uma propriedade na JVM.
		
		
		

	:param  nome: nome da variável de ambiente.
		
		

	:param  val: valor atribuído à variável de ambiente especificada.
		

.. function:: B.setprop nome val


	:comentário: O método setprop seta uma propriedade na JVM.
		
		
		

	:param  nome: nome da variável de ambiente.
		
		

	:param  val: valor atribuído à variável de ambiente especificada.
		

.. function:: B.getprop snome val


	:comentário: Esse método pega uma variável de ambiente da JVM.
		
		
		

	:param  snome: especifica o nome da variável de ambiente.
		
		

	:param  val: retorna o valor atribuído à variável de ambiente.
		

.. function:: B.getprop nome val


	:comentário: Esse método pega uma variável de ambiente da JVM.
		
		
		

	:param  nome: especifica o nome da variável de ambiente.
		
		

	:param  val: retorna o valor atribuído à variável de ambiente.
		

.. function:: B.getprop nome


	:comentário: Esse método pega uma variável de ambiente da JVM.
		
		
		

	:param  nome: especifica o nome da variável de ambiente.
		
		
	:rtype: Bds  retorna um objeto Bds contendo o valor da variável especificada.
		

.. function:: B.getprop snome


	:comentário: Esse método pega uma variável de ambiente da JVM.
		
		
		

	:param  snome: especifica o nome da variável de ambiente.
		
		
	:rtype: Bds  retorna um objeto Bds contendo o valor da variável especificada.
		

.. function:: B.propriedades lista


	:comentário: 
	:param lista:

.. function:: B.propriedades lista filtro


	:comentário: 
	:param lista:
		
	:param filtro:

.. function:: B.propriedades listax filtro filtrocont


	:comentário: 
	:param listax:
		
	:param filtro:
		
	:param filtrocont:

.. function:: B.s ss


	:comentário: Esse método retorna uma string Bds referente ao objeto especificado no
		parâmetro de entrada.
		
		<pre>
		OBS = SUPORTA O APONTA
		Exemplo:
		
		{
		¨					String str = new String("Teste de conversão");
		¨					<b>Bds ttt = B.s(str);</b>
		¨					B.exib(ttt);
		}
		
		O exemplo acima apresenta uma conversão do objeto String (str) para um Bds (ttt).
		
		</pre>
		
		
		

	:param  ss: especifica um objeto do tipo String que será adaptado como Bds.
		
		
	:rtype: Bds  retorna um objeto do tipo Bds.
		

.. function:: B.s str


	:comentário: Esse método retorna uma string Bds referente ao objeto especificado no
		parâmetro de entrada.
		
		
		

	:param  str: especifica um objeto do tipo Bds que será adaptado como Bds.
		
		
	:rtype: Bds  retorna um objeto do tipo Bds. OBS = SUPORTA O APONTA
		

.. function:: B.c str sai


	:comentário: Esse método retorna um char referente ao objeto especificado no parâmetro de
		entrada.
		
		
		

	:param  str: especifica um objeto do tipo Bds que será convertido para char.
		
		

	:param  sai: armazena o resultado da conversão em uma variável de estado
		global.
		

.. function:: B.b arg


	:comentário: retorna o Bds de arg
		
		
		

	:param  arg: 
		
		
	:rtype: Bds
		
		
	:throws:  ErrBds
		

.. function:: B.b arg


	:comentário: retorna o Bds de arg
		
		
		

	:param  arg: 
		
		
	:rtype: Bds
		
		
	:throws:  ErrBds
		

.. function:: B.b arg nome


	:comentário: retorna o Bds de arg tambem atribuindo "nome" `a propriedade nome
		
		
		

	:param  arg: 
		
		

	:param  nome: 
		
		
	:rtype: Bds
		
		
	:throws:  ErrBds
		

.. function:: B.b arg nome


	:comentário: retorna o Bds de arg tambem atribuindo "nome" `a propriedade nome
		
		
		

	:param  arg: 
		
		

	:param  nome: 
		
		
	:rtype: Bds
		
		
	:throws:  ErrBds
		

.. function:: B.convAsc str sai


	:comentário: converte o numero que esta em str para asc
		
		
		

	:param  str: 
		
		

	:param  sai: 
		

.. function:: B.g nome


	:comentário: retorna Bds = a variavel de estado cujo nome = nome
		
	:param nome:

.. function:: B.g nome


	:comentário: retorna Bds = a variavel de estado cujo nome = nome
		
	:param nome:

.. function:: B.emsg smsg stipoacao


	:comentário: 
	:param smsg:
		
	:param stipoacao:

.. function:: B.emsg smsg stipoacao


	:comentário: 
	:param smsg:
		
	:param stipoacao:

.. function:: B.emsg smsg stipolog sarghelp stipoacao


	:comentário: Esse método cria mensagens referentes ao tipo de ocorrência da aplicação, ou
		seja, gera logs baseados em tipos de erros ou de comentários.
		
		O emsg quando chamado com "R", "M" ou "S" no segundo argumento sempre gera um
		logo da mensagem indicada.
		
		<pre>
		Tipos de logs: R - log de ocorrência normal.
		S - log de status. <não habilitado>
		M - log de bilhetagem. Registra logs importantes.
		Ex.: movimentação de dinheiro, faturamento.
		
		Tipos de ações: L - Erro leve - indicação de erro apenas de caracter indicativo, o processamento
		não é interrompido.
		S - Erro mais serio - neste caso o processamento é interrompido. Se o processo está
		programado para se reinicializar automaticamente, isto é feito.
		G - Erro grave - o método para totalmente o processamento da thread, mesmo que o preocesso
		tenha recebido o comando para se reinicializar automaticamente
		
		Exemplo:
		{
		¨					<b>int th = B.th();
		¨					C.reinicia[th]=true;
		¨					if (condição)</b>//se condição =true, o método emsg exib o Erro e reinicializa a Thread "th".
		¨						<b>B.emsg"<mensagem de erro","S,"<arq_help>","S"</b>
		}
		
		C - "Comando" ou ação - apenas registra uma ação ou comando - nao é mensagem de erro.
		
		importante - no caso de mensagens de erro (L,S ou G), o emsg apresenta de maneira clara o stack java onde foram criados os erros
		se o fonte indicado esta no ambiente em que se esta desenvolvendo (mesmo package e diretorio mestre),
		basta clicar na linha que indica o fonte para o editor aparecer com o arquivo correto
		e nesta linha
		</pre>
		
		
		

	:param  smsg: especifica uma string referente a ocorrência.
		
		

	:param  stipolog: especifica o tipo de log a ser utilizado.
		
		

	:param  sarghelp: especifica o nome do arquivo de Help a ser criado.
		
		

	:param  stipoacao: especifica a ação adotada na ocorrência de erro.
		

.. function:: B.emsg msg tipolog arghelp tipoacao


	:comentário: 
	:param msg:
		
	:param tipolog:
		
	:param arghelp:
		
	:param tipoacao:

.. function:: B.emsg s


	:comentário: 
	:param s:

.. function:: B.emsg s


	:comentário: 
	:param s:

.. function:: B.suprimeInfo suprime


	:comentário: 
	:param suprime:

.. function:: B.errAbs resp


	:comentário: 
	:param resp:

.. function:: B.nomeAbs abs


	:comentário: 
	:param abs:

.. function:: B.linhaComando linhaCmd


	:comentário: 
	:param linhaCmd:

.. function:: B.j sth


	:comentário: 
	:param sth:

.. function:: B.v sth


	:comentário: 
	:param sth:

.. function:: B.setajan sth sliga


	:comentário: 
	:param sth:
		
	:param sliga:

.. function:: B.setajan sliga


	:comentário: Esse método possibilita a ativação ou desativação da janela corrente. O
		parâmetro sliga pode ser 0 ou 1. Ex.:
		
		<pre>
		1 -> ativa a janela especificada.
		0 -> ocullta a janela especificada.
		</pre>
		
		
		

	:param  sliga: opção de ativação ou desativação da janela.
		

.. function:: B.setThId id


	:comentário: Seta o id bdjava em uma thread
		
		
		

	:param  id: 
		
		
	:throws:  ErrBds
		

.. function:: B.getThId id


	:comentário: Pega o Id Bdjava de uma Thread se ja houver
		
		
		
	:rtype: void
		
		
	:throws:  ErrBds
		
	:param id:

.. function:: B.th sth


	:comentário: Esse método retorna em sth o número referente à da Thread corrente.
		
		<pre>
		Exemplo:
		
		th resposta
		exib resposta
		
		O exemplo acima exibe na tela o número da Thread correspondente ao CLIDE corrente.
		
		</pre>
		
		
		

	:param  sth: armazena o número da Thread corrente. OBS = SUPORTA O APONTA
		

.. function:: B.para 


	:comentário: 

.. function:: B.para sth


	:comentário: 
	:param sth:

.. function:: B.continua sth


	:comentário: 
	:param sth:

.. function:: B.continua sth op


	:comentário: 
	:param sth:
		
	:param op:

.. function:: B.onde_esta 


	:comentário: 

.. function:: B.setadbg bits sop sth


	:comentário: 
	:param bits:
		
	:param sop:
		
	:param sth:

.. function:: B.mdbg msg sdbg


	:comentário: 
	:param msg:
		
	:param sdbg:

.. function:: B.mdbg smsg


	:comentário: 
	:param smsg:

.. function:: B.fdbg bits


	:comentário: seta os bits de mensagem de debug associada e funcoes especificas
		
		
		

	:param  bits: exemplor ("100010") setados os bits 6 e 2
		
		
	:throws:  ErrBds
		

.. function:: B.pausa ssegundos


	:comentário: Esse método efetua uma pausa determinada pelo programador na execução do
		aplicativo.
		
		
		

	:param  ssegundos: tempo em segundos determinado pelo programador.
		

.. function:: B.pausa sa sb


	:comentário: Esse método efetua uma pausa determinada pelo programador na execução do
		aplicativo.
		
		<pre>
		Nessa sobrecarga o tempo é determinado em milesegundos pelo produto de sa por sb.
		Tempo de pausa = (sa x sb)milisegundos.
		</pre>
		
		
		

	:param  sa: especifica um dos fatores multiplicativos do tempo de pausa.
		
		

	:param  sb: especifica um dos fatores multiplicativos do tempo de pausa.
		

.. function:: B.aborta 


	:comentário: 

.. function:: B.aborta msg


	:comentário: aborta o atual processamento da thread
		
		
		

	:param  msg: mensagem a ser dada no comando aborta
		

.. function:: B.aborta msg


	:comentário: aborta o atual processamento da thread
		
		
		

	:param  msg: mensagem a ser dada no comando aborta
		

.. function:: B.aborta msg th


	:comentário: aborta o atual processamento da thread
		
		
		

	:param  msg: mensagem a ser dada no comando aborta
		
		

	:param  th: thread que se deseja abortar o processamento
		

.. function:: B.ajdir str dels schdest stam


	:comentário: Esse método ajusta uma determinada string movendo o conteúdo desejado da
		string para direita. O restante dos caracteres pode ser eliminado ou
		redirecionado para a esquerda da string.
		
		<pre>
		Exemplo:
		
		faz str 'conteudozzzz'
		exib str
		conteudozzzz
		<B>ajdir str 'z' 'y' 12</B>
		exib str
		yyyyconteudo
		
		<B>Obs:</B> O comando acima ajusta o conteúdo válido da string para a direita preenchendo com 'y' o início da string conforme o tamanho especificado.
		Se o tamnho fosse n, o comando strip apresentaria n-(conteúdo) de 'y's no final.
		
		</pre>
		
		
		

	:param  str: especifica a string a ser processada.
		
		

	:param  dels: especifica os caracteres a direita do conteúdo que serão
		eliminados.
		
		

	:param  schdest: especifica o tipo de caracter que será colocado no lugar dos
		caracteres eliminados.
		
		

	:param  stam: especifica o tamanho da string após o processamento do ajuste.
		

.. function:: B.ajdir str dels stam


	:comentário: Esse método ajusta uma determinada string movendo o conteúdo desejado da
		string para direita. O restante dos caracteres pode ser eliminado ou
		redirecionado para a esquerda da string.
		
		<pre>
		faz str 'teste          '
		strip str -> 74 65 73 74 65 20 20 20 20 20 20 20 20 20 20
		<B>ajdir str ' ' 7</B>
		strip str -> 20 20 74 65 73 74 65
		<B>Obs:</B> O comando acima ajusta o conteúdo válido da string para a direita deixando dois espaços no
		início devido a especificação do tamanho = 7. Se o tamnho fosse n, o comando strip apresentaria n-(conteúdo) espaços em branco no início.
		
		</pre>
		
		
		

	:param  str: especifica a string a ser processada.
		
		

	:param  dels: especifica os caracteres a direita do conteúdo que serão
		eliminados.
		
		

	:param  stam: especifica o tamanho da string após o processamento do ajuste.
		

.. function:: B.ajdir str sdels sdest


	:comentário: Esse método ajusta uma determinada string movendo o conteúdo desejado da
		string para direita. O restante dos caracteres pode ser eliminado ou
		redirecionado para a esquerda da string.
		
		<pre>
		
		</pre>
		
		
		

	:param  str: especifica a string a ser processada.
		
		

	:param  sdels: especifica os caracteres a direita do conteúdo que serão
		eliminados.
		
		

	:param  sdest: especifica o tipo de caracter que será colocado no lugar dos
		caracteres eliminados.
		

.. function:: B.ajdir str sdels


	:comentário: Esse método ajusta uma determinada string movendo o conteúdo desejado da
		string para direita. O restante dos caracteres pode ser eliminado ou
		redirecionado para a esquerda da string.
		
		<pre>
		Exemplo:
		
		faz str 'teste          '
		strip str -> 74 65 73 74 65 20 20 20 20 20 20 20 20 20 20
		<B>ajdir str ' '</B>
		strip str -> 74 65 73 74 65
		
		<B>Obs:</B> O comando acima elimina os espaços, representados por 20, ajustando a string com o conteúdo desejado.
		
		</pre>
		
		
		

	:param  str: especifica a string a ser processada.
		
		

	:param  sdels: especifica os caracteres a direita do conteúdo que serão
		eliminados.
		

.. function:: B.ajdir str sdels


	:comentário: Esse método ajusta uma determinada string movendo o conteúdo desejado da
		string para direita. O restante dos caracteres pode ser eliminado ou
		redirecionado para a esquerda da string.
		
		<pre>
		Exemplo:
		
		faz str 'teste          '
		strip str -> 74 65 73 74 65 20 20 20 20 20 20 20 20 20 20
		<B>ajdir str ' '</B>
		strip str -> 74 65 73 74 65
		
		<B>Obs:</B> O comando acima elimina os espaços, representados por 20, ajustando a string com o conteúdo desejado.
		
		</pre>
		
		
		

	:param  str: especifica a string a ser processada.
		
		

	:param  sdels: especifica os caracteres a direita do conteúdo que serão
		eliminados.
		

.. function:: B.ajesq str dels schdest stam


	:comentário: 
		// AJESQ
		// 37
		
		Esse método ajusta uma determinada string movendo o conteúdo desejado da
		string para esquerda. O restante dos caracteres pode ser eliminado ou
		redirecionado para a direita da string.
		
		<pre>
		Exemplo:
		
		faz str 'zzzzconteudo'
		exib str
		zzzzconteudo
		<B>ajdir str 'z' 'y' 12</B>
		exib str
		conteudoyyyy
		
		<B>Obs:</B> O comando acima ajusta o conteúdo válido da string para a esquerda preenchendo com 'y'
		o restante do espaço especificado pelo tamanho.
		Se o tamnho fosse n, o comando strip apresentaria n-(conteúdo) de 'y' no final.
		
		</pre>
		
		
		

	:param  str: especifica a string a ser processada.
		
		

	:param  dels: especifica os caracteres a esquerda do conteúdo que serão
		eliminados.
		
		

	:param  schdest: especifica o tipo de caracter que será colocado no lugar dos
		caracteres eliminados.
		
		

	:param  stam: especifica o tamanho da string após o processamento do ajuste.
		

.. function:: B.ajesq str schdest stam


	:comentário: Esse método ajusta uma determinada string movendo o conteúdo desejado da
		string para direita. O restante dos caracteres pode ser eliminado ou
		redirecionado para a esquerda da string.
		
		
		

	:param  str: especifica a string a ser processada.
		
		

	:param  schdest: especifica o tipo de caracter que será colocado no lugar dos
		caracteres eliminados.
		
		

	:param  stam: especifica o tamanho da string após o processamento do ajuste.
		

.. function:: B.ajesq str sdels


	:comentário: Esse método ajusta uma determinada string movendo o conteúdo desejado da
		string para esquerda. O restante dos caracteres pode ser eliminado ou
		redirecionado para a direita da string.
		
		<pre>
		Exemplo:
		
		faz str '          teste'
		strip str ->  20 20 20 20 20 20 20 20 20 20 74 65 73 74 65
		<B>ajdir str ' '</B>
		strip str -> 74 65 73 74 65
		
		<B>Obs:</B> O comando acima elimina os espaços, representados por 20, ajustando a string com o conteúdo desejado.
		
		</pre>
		
		
		

	:param  str: especifica a string a ser processada.
		
		

	:param  sdels: especifica os caracteres a esquerda do conteúdo que serão
		eliminados.
		

.. function:: B.ajesq str sdels


	:comentário: Esse método ajusta uma determinada string movendo o conteúdo desejado da
		string para esquerda. O restante dos caracteres pode ser eliminado ou
		redirecionado para a direita da string.
		
		<pre>
		Exemplo:
		
		faz str '          teste'
		strip str ->  20 20 20 20 20 20 20 20 20 20 74 65 73 74 65
		<B>ajdir str ' '</B>
		strip str -> 74 65 73 74 65
		
		<B>Obs:</B> O comando acima elimina os espaços, representados por 20, ajustando a string com o conteúdo desejado.
		
		</pre>
		
		
		

	:param  str: especifica a string a ser processada.
		
		

	:param  sdels: especifica os caracteres a esquuerda do conteúdo que serão
		eliminados.
		

.. function:: B.ajesq str sdels sdest


	:comentário: Esse método ajusta uma determinada string movendo o conteúdo desejado da
		string para esquerda. O restante dos caracteres pode ser eliminado ou
		redirecionado para a direita da string.
		
		<pre>
		
		</pre>
		
		
		

	:param  str: especifica a string a ser processada.
		
		

	:param  sdels: especifica os caracteres a esquerada do conteúdo que serão
		eliminados.
		
		

	:param  sdest: especifica o tipo de caracter que será colocado no lugar dos
		caracteres eliminados.
		

.. function:: B.poemeio str sfiller stam


	:comentário: 
	:param str:
		
	:param sfiller:
		
	:param stam:

.. function:: B.contem stodo sparte


	:comentário: 
		// CONTEM
		
		Esse método tem como objetivo determinar se uma dada string está contida em
		outra string especificada nos parâmetros de entrada.
		
		<pre>
		Exemplo: Para verifica se a str1 está contida em strTodo fazemos ->
		<B>contem strTodo str1</B>
		</pre>
		
		
		

	:param  stodo: especifica a string a ser pesquisada.
		
		

	:param  parte: especifica a string pode estar contida em stodo.
		
		
	:rtype: boolean  retorna verdadeiro se o método encontrar uma substring em stodo.
		

.. function:: B.contem stodo parte


	:comentário: Esse método tem como objetivo determinar se uma dada string está contida em
		outra string especificada nos parâmetros de entrada.
		
		<pre>
		Exemplo: Para verifica se a str1 está contida em strTodo fazemos ->
		<B>contem strTodo str1</B>
		</pre>
		
		
		

	:param  stodo: especifica a string a ser pesquisada.
		
		

	:param  parte: especifica a string pode estar contida em stodo.
		
		
	:rtype: boolean  retorna verdadeiro se o método encontrar uma substring em stodo.
		

.. function:: B.contem todo sparte


	:comentário: Esse método tem como objetivo determinar se uma dada string está contida em
		outra string especificada nos parâmetros de entrada.
		
		<pre>
		Exemplo: Para verifica se a str1 está contida em strTodo fazemos ->
		<B>contem strTodo str1</B>
		</pre>
		
		
		

	:param  todo: especifica a string a ser pesquisada.
		
		

	:param  sparte: especifica a string pode estar contida em stodo.
		
		
	:rtype: boolean  retorna verdadeiro se o método encontrar uma substring em stodo.
		

.. function:: B.contem todo parte


	:comentário: Esse método tem como objetivo determinar se uma dada string está contida em
		outra string especificada nos parâmetros de entrada.
		
		<pre>
		Exemplo: Para verifica se a str1 está contida em strTodo fazemos ->
		<B>contem strTodo str1</B>
		</pre>
		
		
		

	:param  todo: especifica a string a ser pesquisada.
		
		

	:param  parte: especifica a string pode estar contida em stodo.
		
		
	:rtype: boolean  retorna verdadeiro se o método encontrar uma substring em stodo.
		

.. function:: B.contem todo parte pos


	:comentário: Esse método tem como objetivo determinar se uma dada string está contida em
		outra string especificada nos parâmetros de entrada.
		
		
		
		

	:param  todo: especifica a string a ser pesquisada.
		
		

	:param  parte: especifica a string pode estar contida em stodo.
		
		

	:param  pos: especifica a posição da string stodo a ser pesquisada.
		
		
	:rtype: boolean  retorna verdadeiro se o método encontrar uma substring em stodo.
		

.. function:: B.contem todo parte pos pos0


	:comentário: Esse método tem como objetivo determinar se uma dada string está contida em
		outra string especificada nos parâmetros de entrada.
		
		
		
		

	:param  todo: especifica a string a ser pesquisada.
		
		

	:param  parte: especifica a string pode estar contida em stodo.
		
		

	:param  pos: especifica a posição inicial da string stodo a ser pesquisada.
		
		

	:param  pos0: especifica a posição final da varredura de peseuisa na string
		stodo.
		
		
	:rtype: boolean  retorna verdadeiro se o método encontrar uma substring em stodo.
		

.. function:: B.contemExtendido stodo sparte op


	:comentário: 
		// contemExtendido
		
		Esse método tem como objetivo determinar se uma dada string está contida em
		outra string especificada nos parâmetros de entrada, e com a possibilidade de
		se indicar no parâmetro "op", que tipo de comparacao se quer fazer (ignorar
		maiuscular e/ou ignorar acentos ).
		
		<pre>
		Exemplo: Para verifica se a str1 está contida em strTodo fazemos ->
		<B>contemExtendido strTodo str1</B>
		</pre>
		
		
		

	:param  stodo: especifica a string a ser pesquisada.
		
		

	:param  parte: especifica a string pode estar contida em stodo.
		
		

	:param  op: se contem "M", ignora maiusculas e minusculas, se contem "A",
		ignora acentos
		
		
	:rtype: boolean  retorna verdadeiro se o método encontrar uma substring em stodo.
		

.. function:: B.contemExtendido stodo parte op


	:comentário: Esse método tem como objetivo determinar se uma dada string está contida em
		outra string especificada nos parâmetros de entrada, e com a possibilidade de
		se indicar no parâmetro "op", que tipo de comparacao se quer fazer (ignorar
		maiuscular e/ou ignorar acentos ).
		
		<pre>
		Exemplo: Para verifica se a str1 está contida em strTodo fazemos ->
		<B>contemExtendido strTodo str1</B>
		</pre>
		
		
		

	:param  stodo: especifica a string a ser pesquisada.
		
		

	:param  parte: especifica a string pode estar contida em stodo.
		
		

	:param  op: se contem "M", ignora maiusculas e minusculas, se contem "A",
		ignora acentos
		
		
	:rtype: boolean  retorna verdadeiro se o método encontrar uma substring em stodo.
		

.. function:: B.contemExtendido todo sparte op


	:comentário: Esse método tem como objetivo determinar se uma dada string está contida em
		outra string especificada nos parâmetros de entrada, e com a possibilidade de
		se indicar no parâmetro "op", que tipo de comparacao se quer fazer (ignorar
		maiuscular e/ou ignorar acentos ).
		
		<pre>
		Exemplo: Para verifica se a str1 está contida em strTodo fazemos ->
		<B>contemExtendido strTodo str1</B>
		</pre>
		
		
		

	:param  todo: especifica a string a ser pesquisada.
		
		

	:param  sparte: especifica a string pode estar contida em stodo.
		
		

	:param  op: se contem "M", ignora maiusculas e minusculas, se contem "A",
		ignora acentos
		
		
	:rtype: boolean  retorna verdadeiro se o método encontrar uma substring em stodo.
		

.. function:: B.contemE todo parte


	:comentário: Esse método tem como objetivo determinar se uma dada string está contida em
		outra string especificada nos parâmetros de entrada considerando que nao se
		diferem maiusculas e nem acentos
		
		<pre>
		Exemplo: Para verifica se a str1 está contida em strTodo fazemos ->
		<B>contemE strTodo str1</B>
		</pre>
		
		
		

	:param  todo: especifica a string a ser pesquisada.
		
		

	:param  parte: especifica a string pode estar contida em stodo.
		
		
	:rtype: boolean  retorna verdadeiro se o método encontrar uma substring em stodo.
		

.. function:: B.contemE todo parte


	:comentário: Esse método tem como objetivo determinar se uma dada string está contida em
		outra string especificada nos parâmetros de entrada considerando que nao se
		diferem maiusculas e nem acentos
		
		<pre>
		Exemplo: Para verifica se a str1 está contida em strTodo fazemos ->
		<B>contemE strTodo str1</B>
		</pre>
		
		
		

	:param  todo: especifica a string a ser pesquisada.
		
		

	:param  parte: especifica a string pode estar contida em stodo.
		
		
	:rtype: boolean  retorna verdadeiro se o método encontrar uma substring em stodo.
		

.. function:: B.contemE todo parte


	:comentário: Esse método tem como objetivo determinar se uma dada string está contida em
		outra string especificada nos parâmetros de entrada considerando que nao se
		diferem maiusculas e nem acentos
		
		<pre>
		Exemplo: Para verifica se a str1 está contida em strTodo fazemos ->
		<B>contemE strTodo str1</B>
		</pre>
		
		
		

	:param  todo: especifica a string a ser pesquisada.
		
		

	:param  parte: especifica a string pode estar contida em stodo.
		
		
	:rtype: boolean  retorna verdadeiro se o método encontrar uma substring em stodo.
		

.. function:: B.contemE todo parte


	:comentário: Esse método tem como objetivo determinar se uma dada string está contida em
		outra string especificada nos parâmetros de entrada considerando que nao se
		diferem maiusculas e nem acentos
		
		<pre>
		Exemplo: Para verifica se a str1 está contida em strTodo fazemos ->
		<B>contemE strTodo str1</B>
		</pre>
		
		
		

	:param  todo: especifica a string a ser pesquisada.
		
		

	:param  parte: especifica a string pode estar contida em stodo.
		
		
	:rtype: boolean  retorna verdadeiro se o método encontrar uma substring em stodo.
		

.. function:: B.contemE todo parte op


	:comentário: Esse método tem como objetivo determinar se uma dada string está contida em
		outra string especificada nos parâmetros de entrada, e com a possibilidade de
		se indicar no parâmetro "op", que tipo de comparacao se quer fazer (ignorar
		maiuscular e/ou ignorar acentos ).
		
		<pre>
		Exemplo: Para verifica se a str1 está contida em strTodo fazemos ->
		<B>contemExtendido strTodo str1</B>
		</pre>
		
		
		

	:param  todo: especifica a string a ser pesquisada.
		
		

	:param  parte: especifica a string pode estar contida em stodo.
		
		

	:param  op: se contem "M", ignora maiusculas e minusculas, se contem "A",
		ignora acentos
		
		
	:rtype: boolean  retorna verdadeiro se o método encontrar uma substring em stodo.
		

.. function:: B.contemE todo parte op


	:comentário: 
	:param todo:
		
	:param parte:
		
	:param op:

.. function:: B.contemE todo parte op


	:comentário: 
	:param todo:
		
	:param parte:
		
	:param op:

.. function:: B.contemE todo parte op


	:comentário: Esse método tem como objetivo determinar se uma dada string está contida em
		outra string especificada nos parâmetros de entrada, e com a possibilidade de
		se indicar no parâmetro "op", que tipo de comparacao se quer fazer (ignorar
		maiuscular e/ou ignorar acentos ).
		
		<pre>
		Exemplo: Para verifica se a str1 está contida em strTodo fazemos ->
		<B>contemExtendido strTodo str1</B>
		</pre>
		
		
		

	:param  todo: especifica a string a ser pesquisada.
		
		

	:param  parte: especifica a string pode estar contida em stodo.
		
		

	:param  op: se contem "M", ignora maiusculas e minusculas, se contem "A",
		ignora acentos
		
		
	:rtype: boolean  retorna verdadeiro se o método encontrar uma substring em stodo.
		

.. function:: B.contemExtendido todo parte op


	:comentário: Esse método tem como objetivo determinar se uma dada string está contida em
		outra string especificada nos parâmetros de entrada, e com a possibilidade de
		se indicar no parâmetro "op", que tipo de comparacao se quer fazer (ignorar
		maiuscular e/ou ignorar acentos ).
		
		<pre>
		Exemplo: Para verifica se a str1 está contida em strTodo fazemos ->
		<B>contemExtendido strTodo str1</B>
		</pre>
		
		
		

	:param  todo: especifica a string a ser pesquisada.
		
		

	:param  parte: especifica a string pode estar contida em stodo.
		
		

	:param  op: se contem "M", ignora maiusculas e minusculas, se contem "A",
		ignora acentos
		
		
	:rtype: boolean  retorna verdadeiro se o método encontrar uma substring em stodo.
		

.. function:: B.contemRegExp todo parte


	:comentário: Esse método tem como objetivo determinar se uma dada string está contida em
		outra string especificada em parte como sendo uma expressao regular
		
		<pre>
		Exemplo: Para verificar se a str contem "xxxyyy", vai retornar verdadeiro
		se str contiver "xxxMEIOyyy" por exemplo
		</pre>
		
		
		

	:param  todo: especifica a string a ser pesquisada.
		
		

	:param  parte: a expressao regular que vai ser pesquisada
		
		
	:rtype: boolean  retorna verdadeiro se o método encontrar uma substring em stodo.
		Muito importante: existe uma precompilacao de parte - ou seja se
		parte for uma variavel Bds, ela ser'a precompilada tornando mais
		rapida a verificacao na segunda vez usando o mesmo filtro SUPORTA
		APONTA
		
		// RegexDemo.java import java.util.regex.; class RegexDemo { public
		static void main (String [] args) { if (args.length != 2) {
		¨         System.err.println ("java RegexDemo regex text"); return; } Pattern
		¨         p; try { p = Pattern.compile (args [0]); } catch
		¨         (PatternSyntaxException e) { System.err.println ( "Regex syntax
		¨         error: " + e.getMessage ()); System.err.println ( "Error description:
		¨         " + e.getDescription ()); System.err.println ("Error index: " +
		¨         e.getIndex ()); System.err.println ( "Erroneous pattern: " +
		¨         e.getPattern ()); return; } String s = cvtLineTerminators (args [1]);
		¨         Matcher m = p.matcher (s); System.out.println ("Regex = " + args
		¨         [0]); System.out.println ( "Text = " + s); System.out.println ();
		¨         while (m.find ()) { System.out.println ("Found " + m.group ());
		¨         System.out.println ( " starting at index " + m.start () + " and
		ending at index " + m.end ()); System.out.println (); } }
		

.. function:: B.posstr todo parte spos


	:comentário: Esse método tem como objetivo determinar a posição de uma dada string em uma
		string secundária especificada nos parâmetros de entrada
		
		<pre>
		Exemplo:
		<b>posstr strTodo strParte</b>
		
		</pre>
		
		
		

	:param  stodo: especifica a string a ser pesquisada.
		
		

	:param  parte: especifica a string pode estar contida em stodo.
		
		

	:param  spos: retorna a posição da parte pesquisada.
		
		
	:rtype: boolean  retorna true se a parte existe
		

.. function:: B.posstr todo parte spos0 spos


	:comentário: Esse método tem como objetivo determinar a posição de uma dada string em uma
		string secundária especificada nos parâmetros de entrada, com verificacao a
		partir de spos0.
		
		<pre>
		Exemplo:
		<b>posstr strTodo strParte</b>
		
		</pre>
		
		
		

	:param  stodo: especifica a string a ser pesquisada.
		
		

	:param  parte: especifica a string pode estar contida em stodo.
		
		

	:param  spos: retorna a posição da parte pesquisada.
		
		
	:rtype: boolean  retorna true se a parte existe
		

.. function:: B.standAlone 


	:comentário: 
		
		
		
	:rtype: boolean  true se estiver na modalidade standalone
		
		
	:throws:  ErrBds
		

.. function:: B.exib sargs


	:comentário: 
	:param sargs:

.. function:: B.exib sth args


	:comentário: 
	:param sth:
		
	:param args:

.. function:: B.exib sth args


	:comentário: 
	:param sth:
		
	:param args:

.. function:: B.exib args


	:comentário: 
	:param args:

.. function:: B.saidaRemota thx


	:comentário: Comando dado normalmente de um acesso remoto. Coloca na variavel que retorna
		dados para o acesso remoto ($respExecRem) o buffer de saida de dados de uma
		determinada thread
		
		
		

	:param  thx: thread desejada para capturar o buffer
		
		
	:rtype: void
		
		
	:throws:  ErrBds
		

.. function:: B.setaBufferRemoto tamBuffer th


	:comentário: seta o buffer de acesso remoto. O buffer(C.string_remota[th]) tem que ser
		setado separadamente para cada thread
		
		
		

	:param  tamBuffer: tamanho do buffer desejado . se = 0 , desativa o buffer
		
		

	:param  th: Thread cujo buffer vai ser parametrizado
		
		
	:throws:  ErrBds
		

.. function:: B.silencioso op


	:comentário: Deixa a saida do servidor completamente silenciosa. é como se nao existisse
		
		
		

	:param  op: 
		= se = 1 o servidor remoto nao gera nenhum outuput local. se !=1,
		gera os outputs locais normais
		
		
		
	:throws:  ErrBds
		

.. function:: B.mensagem sth str


	:comentário: 
	:param sth:
		
	:param str:

.. function:: B.mensagem sth str


	:comentário: 
	:param sth:
		
	:param str:

.. function:: B.mensagem str


	:comentário: 
	:param str:

.. function:: B.mensagem str


	:comentário: 
	:param str:

.. function:: B.disp arg


	:comentário: 
	:param arg:

.. function:: B.disp sarg


	:comentário: 
	:param sarg:

.. function:: B.limpatela 


	:comentário: 

.. function:: B.poe str schdest stam


	:comentário: static public void poe(Bds str, Bds schdest, Bds stam) throws ErrBds   " enche uma stgring com chdest com tam bytes. Se str for menor que pos,  encher com ochar chdest tambem ateh pos Se posfor <0, calcujla pos a partir  do lado direito
	:param str:
		
	:param schdest:
		
	:param stam:

.. function:: B.poe str schdest spos stam


	:comentário: static public void poe(Bds str, Bds schdest, Bds spos, Bds stam) throws ErrBds   " enche uma stgring a partir de pos com chdest com tam bytes. Se str for  menor que pos, encher com ochar chdest tambem ateh pos Se posfor <0, calcujla  pos a partir do lado direito
	:param str:
		
	:param schdest:
		
	:param spos:
		
	:param stam:

.. function:: B.compchar str


	:comentário: O método compchar compacta caracteres específicos de uma string. Essa
		sobrecarga efetua a compactação dos caracteres em branco (" ").
		
		<pre>
		O exemplo abaixo compacta a string str e exibe o resultado da operação com o comando strip.
		
		faz str 'teste      teste '
		strip str
		74 65 73 74 65 20 20 20 20 20 20 74 65 73 74 65 20
		<b>compchar str</b>
		exib str
		teste teste
		strip str
		74 65 73 74 65 20 74 65 73 74 65
		
		<b>Obs.:</b> Note que o comando elimina o excesso de caracteres brancos(" ") deixando
		apenas as ocorrências entre as partes da string.
		O caracter branco(" ") é representado pelo hexadecimal 20.
		
		</pre>
		
		
		

	:param  str: especifica a string a ser compactada.
		
		SUPORTA APONTA
		

.. function:: B.compchar str dels


	:comentário: O método compchar compacta caracteres específicos de uma string. Essa
		sobrecarga efetua a compactação dos caracteres especificados pelo
		programador.
		
		<pre>
		O exemplo abaixo compacta a string str e exibe o resultado da operação.
		
		faz str 'teste####teste###'
		exib str
		teste####teste###
		compchar str '#'
		exib str
		teste#teste
		
		<b>Obs.:</b> Note que o comando elimina o excesso de caracteres ("#") deixando
		apenas as ocorrências entre as partes da string.
		
		</pre>
		
		
		

	:param  str: especifica a string a ser compactada.
		
		

	:param  dels: especifica os caracteres a serem compactados. SUPORTA APONTA
		

.. function:: B.compchar str sdels sdest


	:comentário: O método compchar compacta caracteres específicos de uma string. Essa
		sobrecarga efetua a compactação dos caracteres especificados pelo
		programador.
		
		<pre>
		O exemplo abaixo compacta a string str e exibe o resultado da operação.
		
		{
		¨					Bds str =new Bds("teste####teste####");
		¨					B.mensagem(str);
		¨					B.mensagem("compactando...");
		¨					<b>B.compchar(str,"#","@");</b>
		¨					B.exib(str);
		}
		
		Tela: teste@teste
		
		<b>Obs.:</b> Note que o comando elimina o excesso de caracteres ("#") alterando
		as ocorrências entre as partes por ("@").
		
		</pre>
		
		
		

	:param  str: especifica a string a ser compactada.
		
		

	:param  sdels: especifica os caracteres a serem compactados.
		
		

	:param  sdest: especifica o caracter a ser trocado pelas ocorrência entre as
		partes da string. SUPORTA APONTA
		

.. function:: B.compchar str dels xdest


	:comentário: O método compchar compacta caracteres específicos de uma string. Essa
		sobrecarga efetua a compactação dos caracteres especificados pelo
		programador.
		
		<pre>
		O exemplo abaixo compacta a string str e exibe o resultado da operação.
		
		
		faz str 'teste####teste###'
		exib str
		teste####teste###
		<b>compchar str '#' '@'</b>
		exib str
		teste@teste
		
		<b>Obs.:</b> Note que o comando elimina o excesso de caracteres ("#") alterando
		as ocorrências entre as partes por ("@").
		
		</pre>
		
		
		

	:param  str: especifica a string a ser compactada.
		
		

	:param  dels: especifica os caracteres a serem compactados.
		
		

	:param  xdest: especifica o caracter a ser trocado pelas ocorrência entre as
		partes da string. SUPORTA APONTA
		

.. function:: B.cmp a b ops


	:comentário: 
	:param a:
		
	:param b:
		
	:param ops:

.. function:: B.cmp a b ops


	:comentário: Esse método efetua uma comparação especificada pelo programador na passagem
		de parâmetros.
		
		<pre>
		
		Tabela de operadores entre strings ASC:
		=:  Igual
		<>: Diferente
		>:  Maior
		>=: Maior Igual
		<:  Menor
		<=: Menor Igual
		
		Exemplos: 1000 =: 9		-> false
		1000 <>: 9		-> true
		<b>1000 >: 9  		-> false</b>
		<b>1000 >=: 9		-> false</b>
		<b>1000 <: 9 		-> true</b>
		<b>1000 <=: 9		-> true</b>
		
		
		
		
		Tabela de operadores numéricos:
		=   Igual
		<>  Diferente
		>   Maior
		>=  Maior Igual
		<   Menor
		<=  Menor Igual
		
		Exemplos: 1000 = 9		-> false
		1000 <> 9		-> true
		1000 > 9 		-> true
		1000 >= 9		-> true
		1000 < 9 		-> false
		1000 <= 9		-> false
		
		
		</pre>
		
		
		

	:param  a: especifica uma string a ser comparada.
		
		

	:param  b: especifica a string secundária a ser comparada.
		
		

	:param  ops: especifica o operador.
		
		
	:rtype: boolean  retorna true ou false dependendo do resultado da operação.
		

.. function:: B.del str spos1 stam


	:comentário: 
	:param str:
		
	:param spos1:
		
	:param stam:

.. function:: B.dir todo stam


	:comentário: 
	:param todo:
		
	:param stam:

.. function:: B.dir todo parte stam


	:comentário: Esse método retorna uma substring a direita de uma string especificada na
		passagem de parâmetros.
		
		
		

	:param  todo: especifica uma string a ser processada.
		
		

	:param  parte: retorna em um Bds a parte especificada.
		
		

	:param  stam: especifica a posição da parte desejada.
		
		
	:rtype: boolean  retorna uma substring a direita. SUPORTA APONTA
		

.. function:: B.dparte todo ch pt


	:comentário: Esse método retira uma determinada parte de uma string.
		
		<pre>
		
		faz str 'parte1 parte2 parte3'
		dparte str ' ' 2
		exib str
		parte1 parte3
		
		</pre>
		
		
		

	:param  todo: especifica a string a ser processada.
		
		

	:param  ch: especifica o delimitador referente a cada parte.
		
		

	:param  pt: especifica a posição da parte a ser excluída. SUPORTA APONTA
		

.. function:: B.eparte todo part sch spos spos0


	:comentário: 
	:param todo:
		
	:param part:
		
	:param sch:
		
	:param spos:
		
	:param spos0:

.. function:: B.rparte todo part sch spos spos0


	:comentário: Esse método verifica se alguma parte contém o radical especificado pelo
		programador.
		
		
		

	:param  todo: string a ser processada.
		
		

	:param  part: especifica o radical a ser pesquisado em cada parte.
		
		

	:param  sch: especifica o delimitador referente a cada parte.
		
		

	:param  spos: retorna a posição referente a parte encontrada.
		
		

	:param  spos0: posição inicial de pesquisa. SUPORTA APONTA
		

.. function:: B.eparte todo part sch spos


	:comentário: Esse método verifica se uma determinada string é parte exata de uma string
		especificada pelo programador.
		
		
		

	:param  todo: string a ser processada.
		
		

	:param  part: especifica uma parte para a consulta.
		
		

	:param  sch: especifica o delimitador referente a cada parte.
		
		

	:param  spos: posição inicial de pesquisa. SUPORTA APONTA
		

.. function:: B.rparte todo part sch spos


	:comentário: Esse método verifica se alguma parte contém o radical especificado pelo
		programador.
		
		
		

	:param  todo: string a ser processada.
		
		

	:param  part: especifica o radical a ser pesquisado em cada parte.
		
		

	:param  sch: especifica o delimitador referente a cada parte.
		
		

	:param  spos: posição inicial de pesquisa. SUPORTA APONTA
		

.. function:: B.eparte todo part sch


	:comentário: Esse método verifica se uma determinada string é parte exata de uma string
		especificada pelo programador.
		
		
		

	:param  todo: string a ser processada.
		
		

	:param  part: especifica uma parte para a consulta.
		
		

	:param  sch: especifica o delimitador referente a cada parte. SUPORTA APONTA
		

.. function:: B.rparte todo part sch


	:comentário: Esse método verifica se alguma parte contém o radical especificado pelo
		programador.
		
		
		

	:param  todo: string a ser processada.
		
		

	:param  part: especifica o radical a ser pesquisado em cada parte.
		
		

	:param  sch: especifica o delimitador referente a cada parte SUPORTA APONTA
		SUPORTA APONTA
		

.. function:: B.cparte todo part0 sch spt linha pos


	:comentário: Esse método verifica se uma determinada string é e contida por alguma parte
		da string todo e devolve a parte em que isto ocorre. A busca 'e feita a
		partir da parte seguinte `a parte especificada por pt.
		
		<pre>
		se pt for negativo, a contagem é feita da direita para a esquerda
		</pre>
		
		
		

	:param  todo: string a ser processada.
		
		

	:param  part0: especifica uma parte para a consulta.
		
		

	:param  ch: especifica o delimitador referente a cada parte.
		
		

	:param  pt: posição inicial de pesquisa.
		
		

	:param  linha: devolve a parte que corresponde `a pesquisa
		
		

	:param  pos: retorna a posicao SUPORTA APONTA
		

.. function:: B.loc lista0 item0 resp spos


	:comentário: 
	:param lista0:
		
	:param item0:
		
	:param resp:
		
	:param spos:

.. function:: B.esq todo stam


	:comentário: 
	:param todo:
		
	:param stam:

.. function:: B.esq todo parte stam


	:comentário: Esse método retorna uma substring a esquerda de uma string especificada na
		passagem de parâmetros.
		
		
		

	:param  todo: especifica uma string a ser processada.
		
		

	:param  parte: retorna em um Bds a parte especificada.
		
		

	:param  stam: especifica a posição da parte desejada.
		
		
	:rtype: boolean  retorna uma substring a esquerda da string todo. SUPORTA APONTA
		

.. function:: B.ig a b


	:comentário: 
	:param a:
		
	:param b:

.. function:: B.ig a b


	:comentário: 
	:param a:
		
	:param b:

.. function:: B.ig a b


	:comentário: 
	:param a:
		
	:param b:

.. function:: B.ig a b


	:comentário: 
	:param a:
		
	:param b:

.. function:: B.igM a b


	:comentário: 
	:param a:
		
	:param b:

.. function:: B.igM a b


	:comentário: 
	:param a:
		
	:param b:

.. function:: B.igM a b


	:comentário: 
	:param a:
		
	:param b:

.. function:: B.igM a b


	:comentário: 
	:param a:
		
	:param b:

.. function:: B.igE a b


	:comentário: 
	:param a:
		
	:param b:

.. function:: B.igE a b


	:comentário: 
	:param a:
		
	:param b:

.. function:: B.igE a b


	:comentário: 
	:param a:
		
	:param b:

.. function:: B.igE a b


	:comentário: 
	:param a:
		
	:param b:

.. function:: B.imeio todo parte spos1 stam


	:comentário: Esse método insere uma string em uma parte específica de uma outra string
		especificada pelo programador.
		
		<pre>
		Exemplo: faz str '123456789'
		imeio str 'tttt' 3 2
		<b>12tttt56789</b>
		Obs.: Os números 3 e 4 foram excluídos devido à especificação do argumento stam = 2.
		</pre>
		
		
		

	:param  todo: string a ser processada.
		
		

	:param  parte: especifica uma parte a ser inserida na string.
		
		

	:param  spos1: especifica a posição onde a parte será inserida.
		
		

	:param  stam: especifica o número de caracteres a serem deletados a partir da
		posição indicada. SUPORTA APONTA
		

.. function:: B.imeio todo parte spos1


	:comentário: Esse método insere uma string em uma parte específica de uma outra string
		especificada pelo programador.
		
		<pre>
		Exemplo: faz str '123456789'
		imeio str 'tttt' 3
		<b>12tttt456789</b>
		Obs.: O números 3 foi excluído, pois essa sobrecarga substitui a posição
		especificada pela string desejada.
		</pre>
		
		
		

	:param  todo: string a ser processada.
		
		

	:param  parte: especifica uma parte a ser inserida na string.
		
		

	:param  spos1: especifica a posição onde a parte será inserida. SUPORTA APONTA
		

.. function:: B.ins todo parte spos1


	:comentário: 
	:param todo:
		
	:param parte:
		
	:param spos1:

.. function:: B.ins todo parte


	:comentário: 
	:param todo:
		
	:param parte:

.. function:: B.inverte str


	:comentário: static public void inverte(Bds str) throws ErrBds   inverte uma string BDMAX
	:param str:

.. function:: B.iparte todo parte sch spt


	:comentário: 
	:param todo:
		
	:param parte:
		
	:param sch:
		
	:param spt:

.. function:: B.maiuscula str


	:comentário: 
	:param str:

.. function:: B.maiuscula str


	:comentário: 
	:param str:

.. function:: B.minuscula str


	:comentário: 
	:param str:

.. function:: B.minuscula str


	:comentário: 
	:param str:

.. function:: B.maimin str


	:comentário: 
	:param str:

.. function:: B.maimin arg


	:comentário: 
	:param arg:

.. function:: B.meio todo parte spos1 stam


	:comentário: Esse método retorna uma parte determinada pela posição e pelo tamanho na
		passagem de parâmetros.
		
		<pre>
		Exemplo:
		
		faz teste '123456789'
		meio teste parte 4 3
		exib parte
		<b>456</b>
		
		Obs.: O método retorna no Bds parte o resultado 123 devido a especificação da
		posição inicial = 1 e tamanho = 3.
		
		</pre>
		
		
		

	:param  todo: string a ser processada.
		
		

	:param  parte: Bds responsável pelo retorno da consulta do método.
		
		

	:param  spos1: posição referente ao início da parte desejada.
		
		

	:param  stam: especifica o tamanho da consulta na string partindo da posição
		inicial. SUPORTA APONTA
		

.. function:: B.meio todo parte spos1


	:comentário: Esse método retorna uma parte determinada pela posição e pelo tamanho na
		passagem de parâmetros.
		
		<pre>
		Exemplo:
		
		
		faz str 'abcde'
		meio str parte 2
		exib parte
		<b>b</b>
		
		
		</pre>
		
		
		

	:param  todo: string a ser processada.
		
		

	:param  parte: Bds referente ao retorno do método.
		
		

	:param  spos1: posição referente ao final da parte desejada. SUPORTA APONTA
		

.. function:: B.meio todo destino inicioPedaco fimPedaco strBusca


	:comentário: 
	:param todo:
		
	:param destino:
		
	:param inicioPedaco:
		
	:param fimPedaco:
		
	:param strBusca:

.. function:: B.novaparte todo parte chs


	:comentário: Esse método cria uma nova parte em uma string Bds.
		
		<pre>
		Exemplo:
		
		faz str 'parte1 parte2 parte3
		novaparte str 'parte4' ' '
		exib str
		<b>parte1 parte2 parte3 parte4</b>
		
		</pre>
		
		
		

	:param  todo: string a ser processada.
		
		

	:param  parte: string a ser inserida como uma parte na string todo.
		
		

	:param  chs: especifica o delimitador referente a cada parte.
		

.. function:: B.novaparte todo parte chs op


	:comentário: Esse método cria uma nova parte em uma string Bds.
		
		<pre>
		Opções de Inserção: op <= 0, a inserção da parte será no início da string.
		op > 0, a inserção da parte será no final da string.
		
		Exemplo 1:
		
		faz str 'parte1 parte2 parte3'
		novaparte str 'inicio' ' ' -1
		exib str
		<b>inicio parte1 parte2 parte3</b>
		
		Exemplo 2:
		
		novaparte str 'fim' ' ' 1
		exib str
		<b>inicio parte1 parte2 parte3 fim</b>
		
		
		</pre>
		
		
		

	:param  todo: string a ser processada.
		
		

	:param  parte: string a ser inserida como uma parte na string todo.
		
		

	:param  chs: especifica o delimitador referente a cada parte.
		
		

	:param  op: especiica a opção de inserção da novaparte.
		

.. function:: B.parte todo parte chs pts pt0s


	:comentário: Esse método retorna um número de partes específicas de uma string determinada
		por delimitadores e pela posição referente a string.
		
		<pre>
		Exemplo:
		
		static public void teste()
		{
		¨						Bds str = new Bds("parte1 parte2 parte3");
		¨						Bds resp = new Bds();
		¨						<b>resp= B.parte(str,resp,' ',2,3);</b>
		¨						B.mensagem (resp)
		}
		<b>Exibe na tela:   parte2 parte3</b>
		
		<b>Obs:</b> Se a podição pt for negativa a pesquisa é realizada da direita para esquerda.
		
		</pre>
		
		
		

	:param  todo: especifica string a ser processada.
		
		

	:param  parte: retorna o conjunto de partes desejadas.
		
		

	:param  chs: especifica o delimitador referente a cada parte.
		
		

	:param  pts: especifica a posição inicial do conjunto de partes desejadas.
		
		

	:param  pt0s: especifica a posição final do conjunto de partes desejadas.
		

.. function:: B.parte todo parte chs pts


	:comentário: Esse método retorna um número de partes específicas de uma string determinada
		por delimitadores e pela posição referente a string.
		
		<pre>
		Exemplo:
		
		static public void teste()
		{
		¨						Bds str = new Bds("parte1 parte2 parte3");
		¨						Bds resp = new Bds();
		¨						<b>resp= B.parte(str,resp,' ',2);</b>
		¨						B.mensagem (resp)
		}
		<b>Exibe na tela:   parte2</b>
		
		<b>Obs:</b> Se a podição pt for negativa a pesquisa é realizada da direita para esquerda.
		
		</pre>
		
		
		

	:param  todo: especifica string a ser processada.
		
		

	:param  parte: retorna o conjunto de partes desejadas.
		
		

	:param  chs: especifica o delimitador referente a cada parte.
		
		

	:param  pts: especifica a posição inicial do conjunto de partes desejadas.
		

.. function:: B.mtpts chars


	:comentário: 
	:param chars:

.. function:: B.posmparte str spt pdels ppos


	:comentário: 
	:param str:
		
	:param spt:
		
	:param pdels:
		
	:param ppos:

.. function:: B.mparte todo parte dels spt


	:comentário: 
	:param todo:
		
	:param parte:
		
	:param dels:
		
	:param spt:

.. function:: B.mparte todo parte dels spt spt1


	:comentário: 
	:param todo:
		
	:param parte:
		
	:param dels:
		
	:param spt:
		
	:param spt1:

.. function:: B.ymparte ptodo parte dels pt pt1


	:comentário: 
	:param ptodo:
		
	:param parte:
		
	:param dels:
		
	:param pt:
		
	:param pt1:

.. function:: B.ymiparte ptodo parte dels pt pt1


	:comentário: 
	:param ptodo:
		
	:param parte:
		
	:param dels:
		
	:param pt:
		
	:param pt1:

.. function:: B.qtmparte ptodo dels sqt


	:comentário: 
	:param ptodo:
		
	:param dels:
		
	:param sqt:

.. function:: B.miparte todo parte dels spt


	:comentário: 
	:param todo:
		
	:param parte:
		
	:param dels:
		
	:param spt:

.. function:: B.miparte todo parte dels spt spt1


	:comentário: 
	:param todo:
		
	:param parte:
		
	:param dels:
		
	:param spt:
		
	:param spt1:

.. function:: B.qtparte todo sdel sqt


	:comentário: static public void qtparte(Bds todo, Bds sdel, Bds sqt) throws ErrBds   retorna em sqt quantidade de partes delimitadas por sdel que existe em todo
	:param todo:
		
	:param sdel:
		
	:param sqt:

.. function:: B.compilaStr str delimitadores


	:comentário: 
	:param str:
		
	:param delimitadores:

.. function:: B.retchar str sdels sbdsdest


	:comentário: Esse método retira uma lista de caracteres de uma determinada string.
		
		<pre>
		Exemplo:
		
		
		faz str 'teste troca bbbbbbb'
		retchar str 'b' 'a'
		exib str
		<b>teste troca aaaaaaa</b>
		</pre>
		
		
		

	:param  str: string a ser processada.
		
		

	:param  sdels: especifica o caracter a ser substituído.
		
		

	:param  sbdsdest: especifica o caracter desejado para a substituição. SUPORTA APONTA
		

.. function:: B.retchar str sdels


	:comentário: Esse método retira uma lista de caracteres de uma determinada string.
		
		<pre>
		Exemplo:
		
		faz str 'teste....()))??? Limpo,,,'
		retchar str '.()?,'
		exib str
		<b>teste Limpo</b>
		
		</pre>
		
		
		

	:param  str: string a ser processada.
		
		

	:param  sdels: especifica os caracteres a serem eliminados. SUPORTA APONTA
		

.. function:: B.retchar str dels


	:comentário: Esse método retira uma lista de caracteres de uma determinada string.
		
		<pre>
		Exemplo:
		
		faz str 'teste....()))??? Limpo,,,'
		retchar str '.()?,'
		exib str
		<b>teste Limpo</b>
		
		</pre>
		
		
		

	:param  str: string a ser processada.
		
		

	:param  sdels: especifica os caracteres a serem eliminados. SUPORTA APONTA
		

.. function:: B.retchar str dels bdsdest


	:comentário: static public void retchar(Bds str, Bds dels, Bds bdsdest) throws ErrBds   compacta uma string, colocando em cada byte da string o caracter desejado  compacta uma string transformando todos os chars que estiverem em dels no  char em xdest
	:param str:
		
	:param dels:
		
	:param bdsdest:

.. function:: B.tamanho s


	:comentário: Esse método verifica se o tamanho referente a uma string é maior que 0.
		
		
		

	:param  s: especifica uma string a ser processada.
		
		
	:rtype: boolean  retorna true se o tamanho da string for maior que 0. SUPORTA APONTA
		

.. function:: B.tamanho s stam


	:comentário: Esse método verifica se o tamanho referente a uma string é maior que 0.
		
		
		

	:param  s: especifica uma string a ser processada.
		
		

	:param  stam: retorna o tamanho referente a string.
		
		
	:rtype: boolean  retorna true se o tamanho da string for maior que 0. SUPORTA APONTA
		

.. function:: B.trocaRegex todo regex regexTroca


	:comentário: O trocaRegex aplica uma troca utilizando regex como argumento de busca
		
		
		

	:param  todo: String de busca
		
		

	:param  regex: Regex de busca
		
		

	:param  regexTroca: Argumento de troca
		
		Exemplo: trocaRegex '100.90' '([0-9]+).([0-9]+")' '$1,$2' retorna
		'100,90'
		

.. function:: B.extraiRegex todo regex resp


	:comentário: O extraiRegex extrai do todo as ocorrencias que batem com o padrao informado
		
		
		

	:param  todo: String de busca
		
		

	:param  regex: Regex de busca
		
		

	:param  resp: variavel de resposta
		
		Exemplo: extraiRegex 'a1 b2 c3' '([0-9])' resp retorna '1' '2' e
		'3' separados por '\n'
		

.. function:: B.troca todo novaStr inicioPedaco fimPedaco strBusca


	:comentário: O troca com 4 argumentos, localiza duas strings em um texto (uma antes, outra
		depois, outra com o resultado a colocar e um que diz qual o trecho da string
		que devera estar dentro do trecho a ser substrituido
		
		
		<pre>
		Exemplo:
		
		faz str 'inicTrecho um nao tem nada fimTrecho um, inictrecho2 esteTroca fim trecho3, ....'
		troca str 'inicTrecho' 'fimTrecho' 'estetroca'  'estetroca com novotexto'
		exib str
		<b>parte1 trocada parte3 trocada trocada</b>
		</pre>
		
		
		

	:param  todo: especifica a string a ser processada.
		
		

	:param  novaStr: nova str a ser colocada entre o inicioPedaco e o fimPedaco
		considerados
		
		

	:param  inicioPedaco: especifica o inicio de cada pedaco a ser considerado
		
		

	:param  fimPedacao: especifica o fim de cada pedaco a ser considerado
		
		

	:param  strBusca: str de referencia para localizar o pedaco desejado SUPORTA
		APONTA
		
		
		@eppM040530 - acertar os troca na documentacao !!!
		

.. function:: B.troca todo pedaco novopedaco


	:comentário: public static boolean troca(Bds todo, String pedaco, String novopedaco) throws ErrBds    Troca em todo todas as ocorrencias da string pedaco pelo novopedaco
	:param todo:
		
	:param pedaco:
		
	:param novopedaco:

.. function:: B.troca todo pedaco novopedaco


	:comentário: public static boolean troca(Bds todo, String pedaco, Bds novopedaco) throws ErrBds    Troca em todo todas as ocorrencias da string pedaco pelo novopedaco
	:param todo:
		
	:param pedaco:
		
	:param novopedaco:

.. function:: B.troca todo pedaco novopedaco


	:comentário: public static boolean troca(Bds todo, Bds pedaco, String novopedaco) throws ErrBds    Troca em todo todas as ocorrencias da string pedaco pelo novopedaco
	:param todo:
		
	:param pedaco:
		
	:param novopedaco:

.. function:: B.troca todo pedaco novopedaco


	:comentário: public static boolean troca(Bds todo, Bds pedaco, Bds novopedaco) throws ErrBds    Troca em todo todas as ocorrencias da string pedaco pelo novopedaco
	:param todo:
		
	:param pedaco:
		
	:param novopedaco:

.. function:: B.convshexa snum


	:comentário: public static Bds convshexa(Bds snum) throws ErrBds   retorna um Bds contendo a conversao hexa a partir da string snum
	:param snum:

.. function:: B.convHexa resp snum


	:comentário: 
	:param resp:
		
	:param snum:

.. function:: B.convshexa resp snum


	:comentário: 
	:param resp:
		
	:param snum:

.. function:: B.convDec resp str


	:comentário: 
	:param resp:
		
	:param str:

.. function:: B.convDec str


	:comentário: 
	:param str:

.. function:: B.strip str dstr


	:comentário: public static void strip(Bds str, Bds dstr) throws ErrBds    faz um strip de str a exibindo com chars hexa
	:param str:
		
	:param dstr:

.. function:: B.strip str


	:comentário: public static void strip(Bds str) throws ErrBds    faz um strip de str a exibindo com chars hexa
	:param str:

.. function:: B.nomeproc nome


	:comentário: 
	:param nome:

.. function:: B.nomeproc nome


	:comentário: 
	:param nome:

.. function:: B.nomeproc nome th


	:comentário: 
	:param nome:
		
	:param th:

.. function:: B.user cmd


	:comentário: Esse método seta os comandos especificados pelo programador como sendo
		comandos de usuário, ou seja, esses comandos não precisam de especificação da
		classe referente ao método e também do plic.
		
		
		

	:param  cmd: especifica o comando.
		

.. function:: B.user cmd


	:comentário: Esse método seta os comandos especificados pelo programador como sendo
		comandos de usuário, ou seja, esses comandos não precisam de especificação da
		classe referente ao método e também do plic.
		
		
		

	:param  cmd: especifica o comando.
		

.. function:: B.user cmd opts


	:comentário: 
	:param cmd:
		
	:param opts:

.. function:: B.user cmd opts


	:comentário: 
	:param cmd:
		
	:param opts:

.. function:: B.atalho tecla cmd


	:comentário: 
	:param tecla:
		
	:param cmd:

.. function:: B.alias alias cmd


	:comentário: 
	:param alias:
		
	:param cmd:

.. function:: B.alias salias scmd


	:comentário: Esse método cria uma referencia a um comando existente especificando um nome
		secundário. Exemplo:
		
		
		{
		¨
		¨ <b>B.alias("cls","limpatela");</b>
		¨
		}
		
		
		Agora, digitando no CLIDE o alias "cls" a tela será limpada.
		
		
		

	:param  salias: especifica um nome secundário referente ao comando.
		
		

	:param  scmd: especifica o comando a ser referenciado.
		

.. function:: B.execabsn nomeabs args


	:comentário: 
	:param nomeabs:
		
	:param args:

.. function:: B.storeabs nomeabs args


	:comentário: 
	:param nomeabs:
		
	:param args:

.. function:: B.getenv arg env


	:comentário: 
	:param arg:
		
	:param env:

.. function:: B.setenv arg env


	:comentário: Esse método seta uma variável de ambiente do Sistema Operacional.
		
		<pre>
		Exemplo:
		
		getenv 'localhost' resp
		exib resp
		KADETT
		</pre>
		
		
		

	:param  arg: especifica a variável de ambiente desejada.
		
		

	:param  env: retorna a variável de ambiente especificada.
		

.. function:: B.getenv sarg env


	:comentário: 
	:param sarg:
		
	:param env:

.. function:: B.getenv sarg


	:comentário: 
	:param sarg:

.. function:: B.getenv sarg


	:comentário: 
	:param sarg:

.. function:: B.getReg chave nome valor


	:comentário: retorna o valor de uma chave do registro Importante - utiliza o utilitario
		"reg.exe" do windows. Normalmente esta no xp e no vista se nao estiver, deve
		baixar o arquivo reg.exe da microsoft
		
	:param chave:
		
	:param nome:
		
	:param valor:

.. function:: B.getReg chave dados


	:comentário: retorna todos os dados de uma entrada do registro Importante - utiliza o
		utilitario "reg.exe" do windows. Normalmente esta no xp e no vista se nao
		estiver, deve baixar o arquivo reg.exe da microsoft
		
		
		

	:param  chave: 
	: Chave do Registro
		
		

	:param  dados: variavel que vai receber todos os dados
		

.. function:: B.setReg chave nome valor


	:comentário: Seta dados no registry para efeito do Bjava Considera apenas o valor String
		
		
		

	:param  chave: 
		
		

	:param  nome: 
		
		

	:param  valor: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: B.setReg chave nome valor


	:comentário: Seta dados no registry para efeito do Bjava Considera apenas o valor String
		
		
		

	:param  chave: 
		
		

	:param  nome: 
		
		

	:param  valor: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: B.setReg chave tipo nome valor


	:comentário: Seta dados no registry para efeito do Bjava Considera apenas o valor String
		
		
		

	:param  chave: 
		
		

	:param  nome: 
		
		

	:param  valor: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: B.setReg chave tipo nome valor


	:comentário: Seta dados no registry para efeito do Bjava Considera apenas o valor String
		
		
		

	:param  chave: 
		
		

	:param  nome: 
		
		

	:param  valor: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: B.getval svar


	:comentário: 
	:param svar:

.. function:: B.getval svar resp


	:comentário: public static void getval(Bds svar, Bds resp) throws ErrBds    Localiza e coloca o valor de um campo qq global "visto" pelo processo em resp
	:param svar:
		
	:param resp:

.. function:: B.faz dest orig


	:comentário: 
	:param dest:
		
	:param orig:

.. function:: B.newBds nomevar


	:comentário: 
	:param nomevar:

.. function:: B.newBds nomevar


	:comentário: 
	:param nomevar:

.. function:: B.clona dest orig


	:comentário: 
	:param dest:
		
	:param orig:

.. function:: B.limpaVar nomeVar


	:comentário: limpa uma variavel
		
		
		

	:param  nomeVar: 
		

.. function:: B.fazBds nomeVar


	:comentário: 
	:param nomeVar:

.. function:: B.somadec total parc1 parc2


	:comentário: 
	:param total:
		
	:param parc1:
		
	:param parc2:

.. function:: B.potencia resp x y


	:comentário: Bds mantissa1 =new Bds(); // mantissa do numero decimal Bds inteiro1 =new
		Bds(); // parte inteira do numero decimal Bds mantissa2 =new Bds(); //
		mantissa do numero decimal Bds inteiro2 =new Bds(); // parte inteira do
		numero decimal
		
		
		
		calcula a potencia de x^y, podendo ser qualquer numero
		
		
		

	:param  resp: 
		
		

	:param  x: 
		
		

	:param  y: 
		
		
	:throws:  ErrBds
		

.. function:: B.criaerro arg


	:comentário: 
	:param arg:

.. function:: B.criaerro arg


	:comentário: 
	:param arg:

.. function:: B.dentroCriaErro arg sop


	:comentário: 
	:param arg:
		
	:param sop:

.. function:: B.criaerro0 arg


	:comentário: 
	:param arg:

.. function:: B.onde_esta sth


	:comentário: public static void onde_esta(Bds sth) throws ErrBds    procura localizar onde esta a thread th
	:param sth:

.. function:: B.ativaLogDiario op


	:comentário: se op > 0 , considera que o log é diario
		
		
		

	:param  op: 
		
		
	:throws:  ErrBds
		

.. function:: B.setflush si


	:comentário: 
	:param si:

.. function:: B.setafonte args0


	:comentário: Esse método seta a fonte da tela indicada (cmd ou trab)
		
		<pre>
		Exemplo de chamada: setafonte (B.s("trab:f=CourierNew;e=BOLD;t=12"));
		setafonte (B.s("trab:f=CourierNew;e=BOLD,ITALIC;t=12")); nte f=nome da fonte,
		e=estilo (BOLD;ITALIC;PLAIN ou BOLD,ITALIC) e t=tamanho em pontos
		
		
	:param args0:

.. function:: B.listafontes 


	:comentário: Esse método lista todas as fontes disponibilizadas para o Bdjava.
		

.. function:: B.gravametodos 


	:comentário: 

.. function:: B.incbds arg


	:comentário: 
	:param arg:

.. function:: B.reinicia reinic


	:comentário: 
	:param reinic:

.. function:: B.reinicia sth reinic


	:comentário: 
	:param sth:
		
	:param reinic:

.. function:: B.tratahelp shelp


	:comentário: 
	:param shelp:

.. function:: B.criavar var


	:comentário: 
	:param var:

.. function:: B.execn argproc0


	:comentário: static public boolean execn(Bds argproc0) throws ErrBds    faz um proc igual ao do bdmax IMPORTANTE : segue compilar nem executar for,  while, if etc entende entretanto o ';' como separador no futuro pode ser que  tenhamos um interpretador bdmax embutido
	:param argproc0:

.. function:: B.execx scomando


	:comentário: O método B.execx interpreta comandos que são na realidade chamadas a Classes
		e Métodos JAVA. Entretanto, a chamada Classe/Método JAVA, atende a sintaxe do
		antigo Bdmax, ou seja, apresenta algumas características específicas como por
		exemplo:
		
		<pre>
		necessita de ponto e vírgula ";" apenas para separar comandos de uma linha. Se existir
		apenas um comando na linha não será necessário a terminação com ponto e vírgula ";".
		Sintaxe:
		
		1o) comando1 ; comando2
		2o) comnado1
		
		Exemplo:
		
		faz teste 'Olá, pessoal'
		exib teste
		
		ou simplesmente:
		
		faz teste 'Olá, pessoal' ; exib teste
		
		
		
		não utiliza parênteses "()" para passagem de parâmetros, ou seja, os argumentos são listados
		na ordem exigida pelo Método, separados por espaço. As constantes são englobadas por plics " ' "
		e as variáveis não necessitam de marcadores, ou seja, apenas o nome da variável de estado global
		é suficiente para o Bdjava consultar a tabela de variáveis da classe C.java.
		Sintaxe:
		
		1o) comando 'constante1' 'constante2' variável1 'constante3'
		2o) comando variável1 variável2
		
		Exemplo:
		
		faz str1 'teste'
		
		</pre>
		
		Essa sobrecarga do método B.execx disponibiliza uma passagem do comando e dos
		seus argumentos encapsulados em um objeto do tipo String. Essa característica
		possibilita a utilização do método B.execx em qualquer aplicativo
		implementado em JAVA.
		
		
		

	:param  scomando: parâmetro responsável pela definição do comando que será executado
		por B.execx Obs.: A String deve encapsular o comando e seus
		argumentos obedecendo a sintaxe do antigo Bdmax.
		
		
	:rtype: boolean  retorna falso, caso ocorra alguma falha na execução do comando.
		SUPORTA APONTA
		

.. function:: B.execx comando


	:comentário: Essa sobrecarga do método B.execx possibilita a passagem do comando e dos
		seus argumentos encapsulados em um objeto do tipo Bds.
		
		
		

	:param  comando: parâmetro responsável pela definição do comando e dos argumentos
		que serão processados pelo método B.execx.
		
		
	:rtype: boolean  retorna falso, caso ocorra alguma falha na execução do comando.
		
		
	:see:  Bds SUPORTA APONTA
		

.. function:: B.pilhaEval resp pargs


	:comentário: 
	:param resp:
		
	:param pargs:

.. function:: B.eval pargs


	:comentário: Faz um "eval" de uma pilha de dados
		
		
		

	:param  pargs: 
		
		
	:rtype: Bds
		
		
	:throws:  ErrBds
		

.. function:: B.round a num


	:comentário: 
	:param a:
		
	:param num:

.. function:: B.parsecmdx comando


	:comentário: 
	:param comando:

.. function:: B.execproc argproc0


	:comentário: static public boolean execproc(Bds argproc0) throws ErrBds    faz um proc igual ao do bdmax IMPORTANTE : segue compilar nem executar for,  while, if etc entende entretanto o ';' como separador no futuro pode ser que  tenhamos um interpretador bdmax embutido cria os abstrais e xprocs internos
	:param argproc0:

.. function:: B.execproc argproc0 arq


	:comentário: static public boolean execproc(Bds argproc0, Bds arq) throws ErrBds    faz um proc igual ao do bdmax IMPORTANTE : segue compilar nem executar for,  while, if etc entende entretanto o ';' como separador no futuro pode ser que  tenhamos um interpretador bdmax embutido cria os abstrais e xprocs internos
	:param argproc0:
		
	:param arq:

.. function:: B.limpaInfLinhas str


	:comentário: 
	:param str:

.. function:: B.locabs pfiltro


	:comentário: 
	:param pfiltro:

.. function:: B.adbg 


	:comentário: 

.. function:: B.ddbg 


	:comentário: 

.. function:: B.hash a b


	:comentário: 
	:param a:
		
	:param b:

.. function:: B.hashepp a b


	:comentário: 
	:param a:
		
	:param b:

.. function:: B.existe abs


	:comentário: 
	:param abs:

.. function:: B.existe abs


	:comentário: 
	:param abs:

.. function:: B.saimesmo arg


	:comentário: 
	:param arg:

.. function:: B.toSaindo arg


	:comentário: 
	:param arg:

.. function:: B.mantemExecucao mantem


	:comentário: Mantem a execucao de qq maneira
		
		
		

	:param  mantem: se ="1", mantem a execucao indefinidamente
		
		

.. function:: B.limpacmds 


	:comentário: 

.. function:: B.listacmds 


	:comentário: 

.. function:: B.admin 


	:comentário: 

.. function:: B.exit 


	:comentário: 

.. function:: B.exit scod


	:comentário: 
	:param scod:

.. function:: B.exit scod rambo


	:comentário: 
	:param scod:
		
	:param rambo:

.. function:: B.ckrambo arg


	:comentário: 
	:param arg:

.. function:: B.ckCharsNome nome chars


	:comentário: 
	:param nome:
		
	:param chars:

.. function:: B.ckCharsNome nome chars


	:comentário: retorna falso se nome contiver algum caractere que nao esteja em chars
		
		
		

	:param  nome: 
		
		

	:param  chars: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: B.limpachars str inic fim


	:comentário: 
	:param str:
		
	:param inic:
		
	:param fim:

.. function:: B.removeIguais lista delimitador resp


	:comentário: Método responsável por retirar Strings duplicadas de uma lista
		split(delimitador) - permite inserir os dados no array Arrays.asList -
		transforma o array numa lista LinkedHashSet - realiza todo o trabalho de
		providenciar uma string unica na lista Os replaces é para retirar as
		pontuações extras que são inseridas pelo o método LinkedHashSet
		
		@lista (lista com as strings a serem tratadas)
		@delimitador delimitador que separa os conteudos das strings
		@resp resposta da lista já tratada com as strings
		
	:param lista:
		
	:param delimitador:
		
	:param resp:

.. function:: B.removeIguais lista delimitador


	:comentário: 
		
		Método responsável por retirar Strings duplicadas de uma lista
		split(delimitador) - permite inserir os dados no array Arrays.asList -
		transforma o array numa lista LinkedHashSet - realiza todo o trabalho de
		providenciar uma string unica na lista Os replaces é para retirar as
		pontuações extras que são inseridas pelo o método LinkedHashSet
		
		@lista (lista com as strings a serem tratadas) - é trocada por uma lista que
		nao tenha entradas duplicadas
		@delimitador delimitador que separa os conteudos das strings
		
		
		

	:param  lista: 
		
		

	:param  delimitador: 
		

.. function:: B.setaoffsetsegs offset


	:comentário: 
	:param offset:

.. function:: B.beep 


	:comentário: 

.. function:: B.entrapausa 


	:comentário: 

.. function:: B.saipausa 


	:comentário: 

.. function:: B.usomemoria 


	:comentário: 

.. function:: B.usomemoria memoria


	:comentário: 
	:param memoria:

.. function:: B.procold ehold


	:comentário: 
	:param ehold:

.. function:: B.soma tot parcelas decs


	:comentário: 
	:param tot:
		
	:param parcelas:
		
	:param decs:

.. function:: B.asc pchar pasc


	:comentário: 
	:param pchar:
		
	:param pasc:

.. function:: B.asc2char pchar pasc


	:comentário: 
	:param pchar:
		
	:param pasc:

.. function:: B.usaHook ehHook


	:comentário: 
	:param ehHook:

.. function:: B.quick quick


	:comentário: 
	:param quick:

.. function:: B.limpaV 


	:comentário: 

.. function:: B.breakpoint 


	:comentário: 

.. function:: B.SuperClientII 


	:comentário: 
		
		
	:rtype: boolean  retorna verdadeiro se estive no modo SuperClientII
		

.. function:: B.semTelaPrincipal arg


	:comentário: 
	:param arg:

.. function:: B.setaPosCaret arg


	:comentário: 
	:param arg:

.. function:: B.procTrace arg flag


	:comentário: 
	:param arg:
		
	:param flag:

.. function:: B.trace flagstrace


	:comentário: seta o trace para a thread corrente
		
		
		

	:param  trace: trace desejado (bit1: comandos bit2 : abstrais, bit 4: vai para
		arquivo
		
		
	:throws:  ErrBds
		

.. function:: B.trace flagstrace th


	:comentário: seta o trace para a thread indicada
		
		
		

	:param  th: thread onde se deseja ter o trace
		
		

	:param  trace: trace desejado (bit1: comandos bit2 : abstrais, bit 4: vai para
		arquivo
		
		
	:throws:  ErrBds
		

.. function:: B.numerico str dest


	:comentário: passa para destino apenas os caracteres numericos
		
		
		

	:param  str: 
		
		

	:param  dest: 
		
		
	:rtype: boolean  verdadeiro se o numero esta correto
		
		
	:throws:  ErrBds
		

.. function:: B.numerico pstr


	:comentário: retorna falso se tiver caracteres nao numericos
		
		
		

	:param  str: 
		
		
	:throws:  ErrBds
		

.. function:: B.ehNomeBdjava str


	:comentário: retorna falso nao for um nome valido de variavel bdjava / java
		
		
		

	:param  str: 
		
		
	:throws:  ErrBds
		

.. function:: B.ehLinux 


	:comentário: 

.. function:: B.semAcentosM str


	:comentário: TRansforma todos os acentos de str em letras comuns
		
		
		

	:param  str: 
		
		
	:throws:  ErrBds
		

.. function:: B.semAcentos str


	:comentário: TRansforma todos os acentos de str em letras comuns
		
		
		

	:param  str: 
		
		
	:throws:  ErrBds
		

.. function:: B.limpaCharsEstranhos str


	:comentário: TRansforma todos os acentos de str em letras comuns
		
		
		

	:param  str: 
		
		
	:throws:  ErrBds
		

.. function:: B.soLetrasNumeros str


	:comentário: retira todos os caracteres de str que nao sejam letras ou numeros,
		
		
		

	:param  str: cadeia de caracteres que vai ser filtrada
		
		
	:throws:  ErrBds
		
		
	:rtype: boolean  true se tiver que tratar acentos nas janelas
		

.. function:: B.desligaCp desliga


	:comentário: 
		
		Liga e desliga a compilacao
		
		
		

	:param  desliga: 
		= 1, DESLIGA A COMPILACAO, = 0, liga a compilacao.
		

.. function:: B.atvCp 


	:comentário: 
		
		Liga a compilacao
		

.. function:: B.desligaCp 


	:comentário: 
		
		desliga a compilacao
		

.. function:: B.ativaCloud 


	:comentário: seta a modalidade "nuvem" onde os ambientes de cada usuario sao protegidos de
		outros usuarios.
		
		Este comando apenas "liga" a modalidade Cloud
		
		
		
	:throws:  ErrBds
		

.. function:: B.calc arg


	:comentário: calculadora - calcula a expressao de arg e exibe o resultado
		
		
		

	:param  arg: 
		
		

	:param  resp: 
		
		
	:throws:  ErrBds
		

.. function:: B.calc arg resp


	:comentário: calculadora - calcula a expressao de arg e coloca o resultado em resp
		
		
		

	:param  arg: 
		
		

	:param  resp: 
		
		
	:throws:  ErrBds
		

.. function:: B.pid 


	:comentário: devolve o pid do java
		
		
		
	:throws:  ErrBds
		

.. function:: B.pid pid


	:comentário: 
	:param pid:

.. function:: B.licenca nome hash


	:comentário: 
	:param nome:
		
	:param hash:

.. function:: B.naotemMnk naotem


	:comentário: 
	:param naotem:

.. function:: B.licenca nome


	:comentário: 
	:param nome:

.. function:: B.trataArgIf linha linhatrab


	:comentário: 
	:param linha:
		
	:param linhatrab:

.. function:: B.reentrante r


	:comentário: 
	:param r:

.. function:: B.recuo niveis absRetorno motivo


	:comentário: forca um return false, recuando o numero indicado de niveis Alem disto marca
		o Bds "__recuoRealizado" com dados do recuo. Parte-se do pressuposto que se
		"__recuoRealizado" não esta nulo, o problema ainda nao foi tratado
		
		
		

	:param  niveis: quantidade de niveis acima que deve sair de abstrais
		
		

	:param  absRetorno: Se chegar neste abs de retorno, nao recua mais
		
		

	:param  motivo: motivo do recuo para exibicao do motivo
		
		
	:throws:  ErrBds
		

.. function:: B.ehdebugpilha op


	:comentário: 
	:param op:

.. function:: B.blockAbs nome


	:comentário: 
	:param nome:

.. function:: B.liberaThread resp mensagem timeout sth


	:comentário: comando para suspender ou liberar a thread onde o comando é dado
		
		
		

	:param  suspende: se ="1" ativa suspencao, se = 0, cancela
		
		

	:param  comando: especifico para ser executado
		
		

	:param  ordem: ordem - o que fazer apos a suspencao ou quando cancelar a
		suspenscao
		
		
	:throws:  ErrBds
		

.. function:: B.paraThread resp mensagem timeout sth


	:comentário: comando para suspender ou liberar a thread onde o comando é dado
		
		
		

	:param  suspende: se ="1" ativa suspencao, se = 0, cancela
		
		

	:param  comando: especifico para ser executado
		
		

	:param  ordem: ordem - o que fazer apos a suspencao ou quando cancelar a
		suspenscao
		
		
	:throws:  ErrBds
		

.. function:: B.suspendeThread resp mensagem timeout sth


	:comentário: comando para suspender ou liberar a thread onde o comando é dado
		
		
		

	:param  suspende: se ="1" ativa suspencao, se = 0, cancela
		
		

	:param  comando: especifico para ser executado
		
		

	:param  ordem: ordem - o que fazer apos a suspencao ou quando cancelar a
		suspenscao
		
		
	:throws:  ErrBds
		

.. function:: B.suspendeThread suspende mantem mensagem comando ordem


	:comentário: comando para suspender ou liberar a thread onde o comando é dado
		
		
		

	:param  suspende: se ="1" ativa suspencao, se = 0, cancela
		
		

	:param  comando: especifico para ser executado
		
		

	:param  ordem: ordem - o que fazer apos a suspencao ou quando cancelar a
		suspenscao
		
		
	:throws:  ErrBds
		

.. function:: B.suspendeThread resp suspende mantem mensagem comando ordem timeout sth


	:comentário: 
	:param resp:
		
	:param suspende:
		
	:param mantem:
		
	:param mensagem:
		
	:param comando:
		
	:param ordem:
		
	:param timeout:
		
	:param sth:

.. function:: B.comandaThread resp mantem comando mensagem ordem timeout sth


	:comentário: 
	:param resp:
		
	:param mantem:
		
	:param comando:
		
	:param mensagem:
		
	:param ordem:
		
	:param timeout:
		
	:param sth:

.. function:: B.pegaRespostaComando resp sth


	:comentário: Pega resposta de um comando dado remotamente Usa para isto, os dados dados
		como mensagem na janela onde foi executado o comando. Funciona tambem no modo
		standalone
		
		
		

	:param  resp: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: B.noErroExec recuo nomeAbs comandos mantem acumula prosseguimento


	:comentário: 
	:param recuo:
		
	:param nomeAbs:
		
	:param comandos:
		
	:param mantem:
		
	:param acumula:
		
	:param prosseguimento:

.. function:: B.noErroExec recuo nomeAbs comandos mantem acumula prosseguimento th


	:comentário: 
	:param recuo:
		
	:param nomeAbs:
		
	:param comandos:
		
	:param mantem:
		
	:param acumula:
		
	:param prosseguimento:
		
	:param th:

.. function:: B.mchs 


	:comentário: 

.. function:: B.exibBds args


	:comentário: 
	:param args:

.. function:: B.tamFonte tamFonte


	:comentário: 
	:param tamFonte:

.. function:: B.robot args


	:comentário: robot fala o que esta acontecendo
		
		
		

	:param  arg: 
		
		
	:throws:  ErrBds
		

.. function:: B.jaRobot arg


	:comentário: 
	:param arg:

