
Flash
=====
.. function:: Flash.agTelaJanPossiveis=false; 


	:comentário: 

.. function:: Flash.csjansExistentes a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: Flash.csprimeira a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: Flash.csbuscar a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: Flash.getXsYsx 


	:comentário: cria os proncipais parametros
		
		
		
	:throws:  ErrBds
		

.. function:: Flash.escondeAntigos remanescentes


	:comentário: esconde os flashes mais antigos, para somente sobrarem os remanescentes
		
		
		

	:param  remanescentes: 
		
		
	:rtype: void
		
		
	:throws:  ErrBds
		

.. function:: Flash.limpaultFlash 


	:comentário: Limpa o handler da ultima janela
		
		

	:param  handler: 
		
		
	:throws:  ErrBds
		

.. function:: Flash.limpaHandlers 


	:comentário: Limpa todos os handlers, possibilitando que organize tudo
		

.. function:: Flash.listaAntigos remanescentes


	:comentário: lista os flashes mais antigos, para somente sobrarem os remanescentes
		
		
		

	:param  remanescentes: 
		
		
	:rtype: void
		
		
	:throws:  ErrBds
		

.. function:: Flash.mostraNovos qtNovos


	:comentário: mostra flashes mais novos
		
		
		
	:rtype: void
		
		
	:throws:  ErrBds
		
	:param qtNovos:

.. function:: Flash.listaNovos qtNovos


	:comentário: lista os flashes mais novos
		
		
		
	:rtype: void
		
		
	:throws:  ErrBds
		
	:param qtNovos:

.. function:: Flash.posiciona hdFlash op tipoJan


	:comentário: forca o posicionamento de uma janela Flash de acordo com suas posicoes
		originais (op-"0") ou por formatação de janelas por grupos (op="1")
		
		
		

	:param  hdFlash: flash correspondente à janela
		
		

	:param  :        tipoJan=ipo Jan- pode ser
		"ESQ","MASTER","CHEIA","MASTER1","MASTER2"
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Flash.removeMapas handler


	:comentário: procura um flash pelo handler e se existir o remove dos mapas
		
		

	:param  handler: 
		
		
	:throws:  ErrBds
		

.. function:: Flash.prov hdFlash screen px py lx ly


	:comentário: Posiciona provisoriamente uma janela com px,py,lx e ly provisorios os
		parametros sao colocados como "prov". A quaquer momento se pode fazer a
		janela retornar para a "posição oficial"
		
		
		

	:param  hdFlash: Bds que contem o flash
		
		

	:param  screen:  numero do monitor, 1 é o default
		
		

	:param  px:      - px provisorio
		
		

	:param  py:      - py provisorio
		
		

	:param  lx:      - lx provisorio
		
		

	:param  ly:      - ly provisorio
		
		
	:rtype: boolean  true se bem sucedudi
		
		
	:throws:  ErrBds
		

.. function:: Flash.prov hdFlash


	:comentário: Restaura para a posicção provisoria, se é que ela existe
		
		
		

	:param  hdFlash: 
		
		
	:rtype: boolean  true, se existe, ou false e nao faz nada se nao existit
		
		
	:throws:  ErrBds
		

.. function:: Flash.escondeAntigas remanescentes


	:comentário: 
	:param remanescentes:

.. function:: Flash.restauraTudo escondidas


	:comentário: 
	:param escondidas:

.. function:: Flash.restauraUltimaJan 


	:comentário: 

.. function:: Flash.restauraPorHd handler


	:comentário: Restaura o Flash para a janela original pelo handler, se existirgt
		
		
		

	:param  handler:  handler desejado
		
		
	:rtype: boolean  true se bem sucedido
		
		
	:throws:  ErrBds
		

.. function:: Flash.restaura hdFlash


	:comentário: Restaura o Flash para a janela original
		
		
		

	:param  hdFlash: Bds que contem o flash
		
		
	:rtype: boolean  true se bem sucedudi
		
		
	:throws:  ErrBds
		

.. function:: Flash.esconde hdFlash


	:comentário: esconde (oculta) o flash
		
		
		

	:param  hdFlash: Bds que contem o flash
		
		
	:rtype: boolean  true se bem sucedudi
		
		
	:throws:  ErrBds
		

.. function:: Flash.exib hdFlash


	:comentário: exibe o flash
		
		
		

	:param  hdFlash: Bds que contem o flash
		
		
	:rtype: boolean  true se bem sucedudi
		
		
	:throws:  ErrBds
		

.. function:: Flash.escalona screen qtde filtroJan ordem


	:comentário: 
	:param screen:
		
	:param qtde:
		
	:param filtroJan:
		
	:param ordem:

.. function:: Flash.escalonaProv hdFlash screen posRelativa incx incy lx ly


	:comentário: 
	:param hdFlash:
		
	:param screen:
		
	:param posRelativa:
		
	:param incx:
		
	:param incy:
		
	:param lx:
		
	:param ly:

.. function:: Flash.ultFlash; 


	:comentário: 

.. function:: Flash.browserStart purl


	:comentário: inicializa uma janela de browser com a janela indicada
		
		
		

	:param  url: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Flash.limpa 


	:comentário: limpa os handlers existentes, retirando os que nao estão mais ativos
		
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Flash.limpa handler


	:comentário: retira um handler dos mapas
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		
	:param handler:

.. function:: Flash.killJan handler forca


	:comentário: 
	:param handler:
		
	:param forca:

.. function:: Flash.salva parquivo


	:comentário: Salva o arquivo de handlers na maquina do usuario Se o arquivo nao for
		espcificado, criar o arquivo: driveeps+/d/_user/mapaHandlers.flash
		
	:rtype: boolean
		
		
		
	:throws:  ErrBds
		
	:param parquivo:

.. function:: Flash.carrega parquivo


	:comentário: carrega os handlers que estavam ativos na maquina do usuario quando o ultimo
		flash estava ativo na maquina. Se o arquivo nao for espcificado, criar o
		arquivo: driveeps+/d/_user/mapaHandlers.flash
		
	:rtype: boolean
		
		
		
	:throws:  ErrBds
		
	:param parquivo:

.. function:: Flash.zeraMapa 


	:comentário: limpa todos os handlers ativos
		
		
		
	:throws:  ErrBds
		

.. function:: Flash.erroAlerta []args


	:comentário: emite diversas alternativas de alerta se arg contiver "alerta", emitirá um
		alerta do Bdjava se contiver "emsg", também fará um log
		
		
		

	:param  arg: 
		
		
	:throws:  ErrBds
		

.. function:: Flash.erroAlerta arg


	:comentário: emite diversas alternativas de alerta se arg contiver "alerta", emitirá um
		alerta do Bdjava se contiver "emsg", também fará um log
		
		
		

	:param  arg: 
		
		
	:throws:  ErrBds
		

