
EdtTab
======
.. function:: EdtTab.inic op


	:comentário: 
	:param op:

.. function:: EdtTab.tela 


	:comentário: inicializa o EDTTAB chamando a tabela Assume
		
		
		
	:throws:  ErrBds
		

.. function:: EdtTab.tela userScript


	:comentário: inicializa o EDTTAB chamando a tabela Assume
		
		
		
	:throws:  ErrBds
		
	:param userScript:

.. function:: EdtTab.cscargaScript a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: EdtTab.loadScript nomeScript


	:comentário: 
	:param nomeScript:

.. function:: EdtTab.csnovoScript a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: EdtTab.csedtScript a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: EdtTab.cssalvaScript a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: EdtTab.cslimpaDados a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: EdtTab.cspgUp a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: EdtTab.cspgDn a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: EdtTab.csatDados a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: EdtTab.csdelDados a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: EdtTab.csedtDados a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: EdtTab.csdspesqTab a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: EdtTab.cspesqDados a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: EdtTab.setaDados 


	:comentário: grava na tela os dados constantes de listaEdtDados
		
		
		
	:throws:  ErrBds
		

.. function:: EdtTab.pegaScript nomeScript


	:comentário: 
	:param nomeScript:

.. function:: EdtTab.alertLog str


	:comentário: 
	:param str:

.. function:: EdtTab.criaListaXml lista0


	:comentário: 
	:param lista0:

.. function:: EdtTab.novaTab script user


	:comentário: 
	:param script:
		
	:param user:

