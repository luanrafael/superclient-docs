
Crontab
=======
.. function:: Crontab.list resp


	:comentário: retorna a lista de arqivos em execução pela cron
		
		

	:param  resp: 
		

.. function:: Crontab.isStarted 


	:comentário: 

.. function:: Crontab.isStarted arquivo


	:comentário: verifica se um determinado arquivo está sendo processado pela cron
		
		

	:param  arquivo: 
		

.. function:: Crontab.start 


	:comentário: inicia a execução da crontab
		utiliza um aquivo chamado cron4jtab.txt como base para iniciar a crontab
		

.. function:: Crontab.start arquivo


	:comentário: inicia a execução da crontab
		
		

	:param  arquivo:  arquivo base para a crontab
		

.. function:: Crontab.stop 


	:comentário: para a execução da crontab
		

