
Ftp
===
.. function:: Ftp.clientFtp server user pass resp


	:comentário: Metodo : ftpClient Funcao : Realiza conexao com servidor FTP e realiza login
		Parametros : server : Nome do servidor ou IP user : Usuario do servidor FTP
		pass : Senha do servidor FTP
		
	:param server:
		
	:param user:
		
	:param pass:
		
	:param resp:

.. function:: Ftp.ftpConnect server resp


	:comentário: Metodo : ftpConnect Funcao : Realiza conecao com servidor FTP Parametros : server : Nome do servidor ou IP
		
	:param server:
		
	:param resp:

.. function:: Ftp.ftpLogin user pass resp


	:comentário: Metodo : ftpLogin Funcao : Realiza login no servidor FTP Parametros : user : Usuario do FTP pass : Senha do FTP
		
	:param user:
		
	:param pass:
		
	:param resp:

.. function:: Ftp.cmd cmd resp


	:comentário: Metodo : ftpSendCmd Funcao : Envia comandos para o servidor FTP Parametros : cmd : Comando
		
	:param cmd:
		
	:param resp:

.. function:: Ftp.ftpSetDir dir


	:comentário: Metodo : ftpSetDir Funcao : Seta diretorio remoto Parametros : dir : Diretorio remoto
		
	:param dir:

.. function:: Ftp.ftpSetDirLocal dir


	:comentário: Metodo : ftpSetDir Funcao : Seta diretorio remoto Parametros : dir : Diretorio remoto
		
	:param dir:

.. function:: Ftp.ftpLogout 


	:comentário: Metodo : ftpLogout Funcao : Fecha conexao com FTP
		

.. function:: Ftp.ftpSetTransferType asc


	:comentário: Metodo : ftpSetTransferType Funcao : seta o tipo de conexao
		
	:param asc:

.. function:: Ftp.upload remotedir file localdir asc resp


	:comentário: Metodo : upload Funcao : Pega data do Servidor Ftp conectado
		
	:param remotedir:
		
	:param file:
		
	:param localdir:
		
	:param asc:
		
	:param resp:

.. function:: Ftp.listaFtp dir resp


	:comentário: Metodo : listaFtp Funcao : Pega data do Servidor Ftp conectado
		
	:param dir:
		
	:param resp:

.. function:: Ftp.downloadBinaryFile dir file localfile


	:comentário: Metodo : downloadBinaryFile Funcao : Pega data do Servidor Ftp conectado
		
	:param dir:
		
	:param file:
		
	:param localfile:

.. function:: Ftp.fdir pdirremoto pfiltro presp


	:comentário: metodo : fdir funcao : le um diretorio dentro do servidor FTP parametros : pdirremoto : diretorio remoto - pasta do ftp pfiltro : pode ser utilizado com
		a extensao ou nome do arquivo presp : retorna uma lista de arquivos obs : e
		necessario declarar as variaveis de ambiente variaveis : hostFtp loginFtp
		senhaFtp
		
	:param pdirremoto:
		
	:param pfiltro:
		
	:param presp:

.. function:: Ftp.fget pdirlocal pdirremoto pfiltro resp


	:comentário: metodo : fread funcao : traz arquivo(s) do diretorio remoto para o diretorio
		local parametros : pdirlocal : diretorio local - pasta windows : pdirremoto : diretorio remoto - pasta do ftp pfiltro : pode ser utilizado com a extensao
		ou nome do arquivo obs : e necessario declarar as variaveis de ambiente
		variaveis : hostFtp loginFtp senhaFtp
		
	:param pdirlocal:
		
	:param pdirremoto:
		
	:param pfiltro:
		
	:param resp:

.. function:: Ftp.fwrite pdirlocal pdirremoto pfiltro resp


	:comentário: metodo : fwrite funcao : grava um arquivo dentro do servidor FTP parametros : pdirlocal : diretorio local de trabalho pdirremoto : diretorio remoto - pasta
		do ftp pfiltro : utiliza nome do arquivo a ser gravado obs : e necessario
		declarar as variaveis de ambiente variaveis : hostFtp loginFtp senhaFtp
		
	:param pdirlocal:
		
	:param pdirremoto:
		
	:param pfiltro:
		
	:param resp:

.. function:: Ftp.fmove pdirremoto pfiltro pfiltro2 resp


	:comentário: metodo : fmove funcao : renomeia um arquivo dentro do servidor FTP parametros
	: pdirremoto : diretorio remoto - pasta do ftp pfiltro : nome do arquivo de
		origem pfiltro2 : nome do arquivo de destino obs : e necessario declarar as
		variaveis de ambiente variaveis : hostFtp loginFtp senhaFtp
		
	:param pdirremoto:
		
	:param pfiltro:
		
	:param pfiltro2:
		
	:param resp:

.. function:: Ftp.fdel pdirremoto pfiltro resp


	:comentário: metodo : fdel funcao : deleta um arquivo dentro do servidor FTP parametros : pdirremoto : diretorio remoto - pasta do ftp pfiltro : nome do arquivo a ser
		deletado obs : e necessario declarar as variaveis de ambiente variaveis : hostFtp loginFtp senhaFtp
		
	:param pdirremoto:
		
	:param pfiltro:
		
	:param resp:

.. function:: Ftp.fexec phost plogin psenha pdirlocal pdirremoto pcmd pfiltro presp


	:comentário: 
	:param phost:
		
	:param plogin:
		
	:param psenha:
		
	:param pdirlocal:
		
	:param pdirremoto:
		
	:param pcmd:
		
	:param pfiltro:
		
	:param presp:

