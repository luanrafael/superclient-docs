
MdkOld
======
.. function:: MdkOld.mOpen hMdk nome classe


	:comentário: cria um handler MDK
		
		
		

	:param  hMdk: recebe os dados do handler
		
		

	:param  strMdk: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: MdkOld.mFormata handler


	:comentário: Mostra todos os filhos do handler
		
		
		

	:param  handler: handler mdk
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: MdkOld.mLista 


	:comentário: mostra as janelas do ultimo handler localizado
		
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: MdkOld.mLista handler


	:comentário: mostra as janelas de um handler
		
		
		

	:param  handler: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: MdkOld.cancelaTela a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: MdkOld.pegaLinhaCursor janela linha


	:comentário: 
	:param janela:
		
	:param linha:

.. function:: MdkOld.pegaJanelaClickada pos


	:comentário: 
	:param pos:

.. function:: MdkOld.getJan handler janelas filtros


	:comentário: localiza um hander de janela dentro de uma lista de janelas a partir de uma
		sequencia de filtros Cada sequencia de filtros deve ter, separados por '|',
		um conjunto de qtde':' filtro, onde na busca a rotina vai procurar a "qtde"a
		ocorrencia que tiver contendo o filtro. A partir dai, o buscador busca do
		seguinte modo o proximo filtro e assim por diante. Por exemplo, se filtros
		for a string '3:Atx|1:edit', primeiro o buscador tentará achar a 3a linha que
		contiver "atx" e depois irá localizar a primeira linha que contiver "edit". A
		busca nao diferencia maiuscula d eminuscula
		
		
		

	:param  handler: 
		
		

	:param  janelas: 
		
		

	:param  filtros: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: MdkOld.mposLinha posLinha posInic janelas linha parms ord


	:comentário: 
	:param posLinha:
		
	:param posInic:
		
	:param janelas:
		
	:param linha:
		
	:param parms:
		
	:param ord:

.. function:: MdkOld.sendMdk resp op msg


	:comentário: Abre o handler master da janela, a partir do titulo informado em strMdk
		
		
		

	:param  hMdk: 
		
		

	:param  resp: 
		
		

	:param  op: 
		
		

	:param  msg: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: MdkOld.sendMdk resp op msg


	:comentário: Abre o handler master da janela, a partir do titulo informado em strMdk
		
		
		

	:param  hMdk: 
		
		

	:param  resp: 
		
		

	:param  op: 
		
		

	:param  msg: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: MdkOld.sendMdk handler parms resp opMdk msgEnv


	:comentário: Envia uma mensagem com seguranca, pois se a janela for reiniciada com outro
		handler, ele acha
		
		
		

	:param  handler: 
		
		

	:param  parms: 
		
		

	:param  resp: 
		
		

	:param  opMdk: 
		
		

	:param  msgEnv: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: MdkOld.mMsg handler parms resp opMsg parmMsg strMsg


	:comentário: envia uma mensagem para uma windows filha da window localizada pelo handler a
		partir dos parms (filtros informados de acordo com o documentato no metodo
		getJan
		
		
		

	:param  handler: handler mestre
		
		

	:param  parms: parametros de filtro na forma <qtde ocorrs str1>:<str1> "|"
		<qtde ocorrs str2>:<str2>.... exemplo : "3:Afx|1:HsEdit" - acha a
		primeira linha que contenha HsEdit apos terem passados 3
		ocorrencias que contenham a strinf "AFx"
		
		

	:param  resp: resposta retornada pela mensagem
		
		

	:param  opMsg: Codigo da mensagem desejada
		
		

	:param  parmMsg: parametro extra da mensagem, normalmente um "0" (ZERO)
		
		

	:param  strMsg: conteudo da mensagem - pode ser nulo tambem
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: MdkOld.mSetTxt handler parms txt


	:comentário: 
	:param handler:
		
	:param parms:
		
	:param txt:

.. function:: MdkOld.mSetTxt handler lista


	:comentário: Set nas respectivas janelas um conjunto de campos listados em lista se algum
		campo da lista nao tem o mdk indicado, nao faz nada
		
		
		

	:param  handler: 
		
		

	:param  lista: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: MdkOld.mGetTxt handler parms txt


	:comentário: 
	:param handler:
		
	:param parms:
		
	:param txt:

.. function:: MdkOld.mGetTxt handler lista


	:comentário: pega o conteudo dos componentes / janelas e poe em um conjunto de campos
		listados em lista se algum campo da lista nao tem o mdk indicado, nao faz
		nada
		
		
		

	:param  handler: 
		
		

	:param  lista: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: MdkOld.mClick handler parms resp


	:comentário: 
	:param handler:
		
	:param parms:
		
	:param resp:

.. function:: MdkOld.lTxtPad linha


	:comentário: 
	:param linha:

.. function:: MdkOld.mPisca 


	:comentário: 

