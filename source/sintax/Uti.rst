
Uti
===
.. function:: Uti.data sops


	:comentário: 
	:param sops:

.. function:: Uti.data ops


	:comentário: static public Bds data(String ops) throws ErrBds   devolve uma Bds com a data de acorco com as opcoes incicadas
	:param ops:

.. function:: Uti.data dt ops


	:comentário: static public void data(Bds dt, Bds ops) throws ErrBds   devolve uma Bds com a data de acorco com as opcoes incicadas
	:param dt:
		
	:param ops:

.. function:: Uti.data dt ops


	:comentário: static public void data(Bds dt, String ops) throws ErrBds   devolve uma Bds com a data de acorco com as opcoes incicadas
	:param dt:
		
	:param ops:

.. function:: Uti.data 


	:comentário: 

.. function:: Uti.data dthora ops dt hh


	:comentário: 
	:param dthora:
		
	:param ops:
		
	:param dt:
		
	:param hh:

.. function:: Uti.data dt ops psmilis


	:comentário: 
	:param dt:
		
	:param ops:
		
	:param psmilis:

.. function:: Uti.estaNoMeio min number max


	:comentário: Verifica se um numero está no meio entre um min e máximo
		
		
		

	:param  min: 
		
		

	:param  number: 
		
		

	:param  max: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Uti.dataCalc dt calc


	:comentário: Calcula datas para tráz e frente além de devolver o dia da semana quando op =
		s se op for = ap retorna se a hora é am ou pm
		
		
		

	:param  dt: 
		
		

	:param  calc: 
		
		
	:throws:  ErrBds
		

.. function:: Uti.dataCalc dt calc ops


	:comentário: 
	:param dt:
		
	:param calc:
		
	:param ops:

.. function:: Uti.datediff resp dtinic dtfim formato


	:comentário: 
	:param resp:
		
	:param dtinic:
		
	:param dtfim:
		
	:param formato:

.. function:: Uti.invData2 dt


	:comentário: 
	:param dt:

.. function:: Uti.formataData dtinfo resp forig fresp


	:comentário: 
	:param dtinfo:
		
	:param resp:
		
	:param forig:
		
	:param fresp:

.. function:: Uti.formataData dtc dt


	:comentário: ajusta uma data para ser sempre do tipo dd/mm/aa Exemplo: Uti.formataData
		'10/10/17' r 'dd/MM/yy' 'dd/MM/yyyy'
		
		
		

	:param  dtc: retorna a data com tracos e sempre com 4 caracteres no ano
		
		

	:param  dt: pode receber uma data completa ou incompleta, com ou sem tracos,
		mas sempre no formato dd mm aa
		
		
	:rtype: boolean  falso se a informacao de dt nao for uma data valida
		
		
	:throws:  ErrBds
		

.. function:: Uti.formataHora hhc hh


	:comentário: ajusta uma hora para ser sempre do tipo hh:mm:ss
		
		
		

	:param  hhc: retorna a hora com ":" entre hh, mm e ss
		
		

	:param  hh: pode receber uma hora completa ou incompleta, com ou sem ":", mas
		sempre no formato hh mm ss
		
		
	:rtype: boolean  falso se a informacao de hh nao for uma hora valida
		
		
	:throws:  ErrBds
		

.. function:: Uti.datan ndias sdia smes sano


	:comentário: 
	:param ndias:
		
	:param sdia:
		
	:param smes:
		
	:param sano:

.. function:: Uti.diaSemana diaSemana ndias op


	:comentário: 
	:param diaSemana:
		
	:param ndias:
		
	:param op:

.. function:: Uti.formataData resp forig fresp


	:comentário: Formata uma data para uma formatação especifica
		
		
		

	:param  resp: 
		'variavel que vai ser alterada e ser alterada
		
		

	:param  forig: formatação original da string
		
		

	:param  fresp: formatação que deseja receber
		
		
	:rtype: boolean  Ex: faz dt '03/04/2013';Uti.formataData dt 'dd/MM/yyyy' 'yyMMdd'
		

.. function:: Uti.ndata ndias dt


	:comentário: 
	:param ndias:
		
	:param dt:

.. function:: Uti.ndata ndias sdia smes sano


	:comentário: 
	:param ndias:
		
	:param sdia:
		
	:param smes:
		
	:param sano:

.. function:: Uti.calcData destino formato parg1 parg2


	:comentário: Opera "somando" ou "subtraindo" os argugmentos arg1 e arg2 que sao
		automaticamente reconhecidos de acordo com o seguinte:
		
		data no formato 'aaaa-mm-dd hh:mm:ss" - data padrao de banco de dados Favor
		notar que o ano é separado do mes e do dia por hifen "-" data no formato
		dd/mm/aaaa hh:mm:ss ou dd/mm/aa hh:mm:ss de acordo com o seguinte: - a
		separacao de dia mes e ano TEM que ser traço ("/") se for apenas dd/mm/aaaa -
		assume a data a zero horas. se o ano for somente aa, considera de 1930 a 2029
		se arg1 ="" (nulo), pega a data de hoje!!!!! arg2 tambem pode ser expresso em
		somente hh:mm:ss, ou no formado <dias>d <hh>h <min>m <seg>s exemplos: calcData dt 'b' '2/12/17' '300d' - soma 300 dias a esta data calcData dt 'b'
		'2/12/17' '-300d' - subtrai 300 dias a esta data (subtracao!!) calcData dt
		'b' '2/12/17' '300d 3segundos' - soma 300 dias e 3 segundos a esta data
		calcData dt 'b' '2/12/17' '300d 3s' - soma 300 dias e 3 segundos a esta data
		
		
		

	:param  destino: 
		
		

	:param  formato: 
		
		

	:param  arg1: 
		
		

	:param  arg2: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Uti.calcJData destino formato pdtOriginal pdiffDias


	:comentário: Opera "somando" ou "subtraindo" os argugmentos arg1 e arg2 que sao
		automaticamente reconhecidos de acordo com o seguinte:
		
		data no formato 'yyyy<DEL>MM<DEL>dd' em qualquer ordem, podendo DEL ser
		'/,-,etc'
		
		
		

	:param  destino: 
		
		

	:param  formato: 
		
		

	:param  pdtOriginal: data original no formato especificado
		
		

	:param  diferenca: em dias podendo ser positiva ou negativa
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Uti.expandeData data dia mes ano hh mm ss


	:comentário: 
	:param data:
		
	:param dia:
		
	:param mes:
		
	:param ano:
		
	:param hh:
		
	:param mm:
		
	:param ss:

.. function:: Uti.geraData data formato dia mes ano hh mm ss


	:comentário: 
	:param data:
		
	:param formato:
		
	:param dia:
		
	:param mes:
		
	:param ano:
		
	:param hh:
		
	:param mm:
		
	:param ss:

.. function:: Uti.milisData smilis data


	:comentário: devolve em milisegundos a data fornecida
		

	:param  data: 
		
		
		
	:rtype: boolean
		

.. function:: Uti.checaData dia mes ano hora min seg


	:comentário: checa se a data fornecida pode ser considerada valida para fins do
		processamento do calc Data
		
		
		

	:param  dia: 
		
		

	:param  mes: 
		
		

	:param  ano: 
		
		

	:param  hora: 
		
		

	:param  min: 
		
		

	:param  seg: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Uti.diffData dt1 dt2 milis segs


	:comentário: 
	:param dt1:
		
	:param dt2:
		
	:param milis:
		
	:param segs:

.. function:: Uti.diffData dt1 dt2 milis segs mins horas dias


	:comentário: 
	:param dt1:
		
	:param dt2:
		
	:param milis:
		
	:param segs:
		
	:param mins:
		
	:param horas:
		
	:param dias:

.. function:: Uti.diffData dt1 dt2 diffhh


	:comentário: 
	:param dt1:
		
	:param dt2:
		
	:param diffhh:

.. function:: Uti.rand num sqt


	:comentário: 
	:param num:
		
	:param sqt:

.. function:: Uti.inicFon dirTabs


	:comentário: Inicializa a fonetizacao, carregando as tabelas de correcao fonetica
		
		
		

	:param  dirTabs: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Uti.fonM2g resp arg tipo


	:comentário: Fonetiza a String arg, de acordo com o tipo indicado
		
		
		

	:param  arg: string a ser fonetizada
		
		

	:param  tipo: tipo do dado: <br>
		tipo="N" - nome comum : cria o fonetico do nome de acordo com as
		regras foneticas <br>
		tipo="U" - Numero comum : nao processa o fonetico mas enxuga o
		numero de brancos, tracos, etc <br>
		tipo="C" - CPF ou CNPJ : retira todos os caracteres estranhos e
		faz a consistencia de um ou de outro <br>
		automaticamente considera se é CPF ou CNPJ e faz a consistencia
		correspondente <br>
		tipo="M" - tratamento do email. A unica diferen
		
		
	:rtype: boolean  o fonetico correto ou string iniciada por "ERRO" e seguida pela
		descricao do erro
		
		
	:throws:  ErrBds
		

.. function:: Uti.criafila 


	:comentário: 

.. function:: Uti.poenafila fila arg sop


	:comentário: 
	:param fila:
		
	:param arg:
		
	:param sop:

.. function:: Uti.poenafila fila arg op


	:comentário: 
	:param fila:
		
	:param arg:
		
	:param op:

.. function:: Uti.tratafila fila arg op


	:comentário: 
	:param fila:
		
	:param arg:
		
	:param op:

.. function:: Uti.pegadafila fila arg sop


	:comentário: 
	:param fila:
		
	:param arg:
		
	:param sop:

.. function:: Uti.pegadafila fila arg op


	:comentário: 
	:param fila:
		
	:param arg:
		
	:param op:

.. function:: Uti.tpoefila fila


	:comentário: 
	:param fila:

.. function:: Uti.tpegfila fila


	:comentário: 
	:param fila:

.. function:: Uti.listafila fila


	:comentário: 
	:param fila:

.. function:: Uti.deldafila fila arg


	:comentário: 
	:param fila:
		
	:param arg:

.. function:: Uti.limpafila fila


	:comentário: 
	:param fila:

.. function:: Uti.qtfila fila sqt


	:comentário: 
	:param fila:
		
	:param sqt:

.. function:: Uti.datalog dthh


	:comentário: 
	:param dthh:

.. function:: Uti.entra msg arg stam


	:comentário: 
	:param msg:
		
	:param arg:
		
	:param stam:

.. function:: Uti.tecla arg


	:comentário: 
	:param arg:

.. function:: Uti.tecla msg arg mascara stout


	:comentário: 
	:param msg:
		
	:param arg:
		
	:param mascara:
		
	:param stout:

.. function:: Uti.ctecla clides stout


	:comentário: 
	:param clides:
		
	:param stout:

.. function:: Uti.xcod str schinic


	:comentário: 
	:param str:
		
	:param schinic:

.. function:: Uti.xdcd str schinic


	:comentário: 
	:param str:
		
	:param schinic:

.. function:: Uti.xtt str schinic


	:comentário: 
	:param str:
		
	:param schinic:

.. function:: Uti.xxcod str pschinic


	:comentário: 
	:param str:
		
	:param pschinic:

.. function:: Uti.xxdcd str pschinic


	:comentário: 
	:param str:
		
	:param pschinic:

.. function:: Uti.logoper log


	:comentário: 
	:param log:

.. function:: Uti.logoper log


	:comentário: 
	:param log:

.. function:: Uti.getDescricao descricao referencia listaRefs listaDescs del


	:comentário: 
	:param descricao:
		
	:param referencia:
		
	:param listaRefs:
		
	:param listaDescs:
		
	:param del:

.. function:: Uti.getvalig args val nome


	:comentário: 
	:param args:
		
	:param val:
		
	:param nome:

.. function:: Uti.getvalig args val nome


	:comentário: 
	:param args:
		
	:param val:
		
	:param nome:

.. function:: Uti.getvalig args val nome dels


	:comentário: 
	:param args:
		
	:param val:
		
	:param nome:
		
	:param dels:

.. function:: Uti.getvalig args val nome dels


	:comentário: 
	:param args:
		
	:param val:
		
	:param nome:
		
	:param dels:

.. function:: Uti.setProp args val nome


	:comentário: 
	:param args:
		
	:param val:
		
	:param nome:

.. function:: Uti.setProp args val nome


	:comentário: 
	:param args:
		
	:param val:
		
	:param nome:

.. function:: Uti.getProp args val nome


	:comentário: 
	:param args:
		
	:param val:
		
	:param nome:

.. function:: Uti.getProp args val nome


	:comentário: 
	:param args:
		
	:param val:
		
	:param nome:

.. function:: Uti.getXml args val grupo


	:comentário: 
	:param args:
		
	:param val:
		
	:param grupo:

.. function:: Uti.argXml 


	:comentário: 

.. function:: Uti.setXml args val grupo


	:comentário: 
	:param args:
		
	:param val:
		
	:param grupo:

.. function:: Uti.setXml args val grupo


	:comentário: 
	:param args:
		
	:param val:
		
	:param grupo:

.. function:: Uti.setvalig args val nome


	:comentário: 
	:param args:
		
	:param val:
		
	:param nome:

.. function:: Uti.setvalig args val nome


	:comentário: 
	:param args:
		
	:param val:
		
	:param nome:

.. function:: Uti.setvalig args val nome dels


	:comentário: 
	:param args:
		
	:param val:
		
	:param nome:
		
	:param dels:

.. function:: Uti.setvalig args val nome del


	:comentário: 
	:param args:
		
	:param val:
		
	:param nome:
		
	:param del:

.. function:: Uti.unevalig args novosargs del


	:comentário: 
	:param args:
		
	:param novosargs:
		
	:param del:

.. function:: Uti.validaDado dado consistencia


	:comentário: 
	:param dado:
		
	:param consistencia:

.. function:: Uti.validacpf cpf


	:comentário: 
	:param cpf:

.. function:: Uti.validacpf pstrCpf


	:comentário: 
	:param pstrCpf:

.. function:: Uti.validaMail arg


	:comentário: Método responsável por validar o e-mail inserido pelo o usuário
		
		
		

	:param  recebe: como argumento o e-mail e retorna true/false
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Uti.validacnpj cnpj


	:comentário: 
	:param cnpj:

.. function:: Uti.validacnpj pstr_cnpj


	:comentário: 
	:param pstr_cnpj:

.. function:: Uti.jaInicTabDados 


	:comentário: 

.. function:: Uti.loca con ref


	:comentário: faz um lock via banco de dados.
		
		
		

	:param  con: 
		
		

	:param  ref: nome do dado que vai usar para lock
		
		

	:param  instancia: identificacao da instancia que vai tentar o lock
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Uti.libera con ref


	:comentário: Libera o lock criado por uma determinada instancia java
		
		
		

	:param  con: 
		
		

	:param  ref: nome do dado que vai usar para lock
		
		

	:param  instancia: identificacao da instancia que vai tentar liberar o lock
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Uti.sort str del


	:comentário: 
	:param str:
		
	:param del:

.. function:: Uti.sort resp str del


	:comentário: 
	:param resp:
		
	:param str:
		
	:param del:

.. function:: Uti.sort sIndice chave asc


	:comentário: 
	:param sIndice:
		
	:param chave:
		
	:param asc:

.. function:: Uti.cod8bits strChar str8


	:comentário: Passa uma string normal para uma onde todos os bytes sao menores que 128
		
		
		

	:param  strChar: string normal
		
		

	:param  str8: string convertida
		
		

.. function:: Uti.deCod8bits strChar str8


	:comentário: Passa uma string onde todos os bytes sao menores que 128 para uma string
		normal
		
		
		

	:param  strChar: string normal
		
		

	:param  str8: string convertida usada como origem
		
		

.. function:: Uti.calcDv11 resp num


	:comentário: Calcula dv com os digitos indicados truncando o resto epara o modulo 11
		considera apenas
		
		
		

	:param  num: numero a ser calculado
		
		

	:param  mod: modulo do dv desejado
		
		

	:param  digitos: qtde de digitos desejados para o dv
		
		
	:rtype: void  o numero acrescido do dv
		
		
	:throws:  ErrBds
		

.. function:: Uti.addLista lista arg del


	:comentário: ADICIONA A uma lista um item, se ele nao estiver, se nao o passa para o
		inicio
		
		
		

	:param  lista: lista de itens separados por del
		
		

	:param  arg: argumento a ser atualizado na lista
		
		

	:param  del: delimitador considerado
		
		
	:throws:  ErrBds
		

.. function:: Uti.pegaDecod64 resp encodedText inverte


	:comentário: Método responsável por decriptografar uma string de 64 bits utilizando o
		Base64 fornecido pelo o Apache
		
		
		

	:param  encodedText: String contendo o valor criptografado em 64bits
		
		

	:param  resp: variavel que recebe o valor da descriptografia
		
		

	:param  inverte: se = 1 considera que resp deve ser invertida permite prever
		tipos de segurança aonde a string tem que ser invertida
		
	:rtype: boolean
		resp
		
	:author:  Claudio Salas
		

.. function:: Uti.pegaEncod64 texto resp inverte


	:comentário: Método responsável por criptografar uma string de 64 bits utilizando o
		Base64 fornecido pelo o Apache
		

	:param  texto: String contendo o valor
		criptografado em 64bits
		

	:param  resp: variavel que recebe o valor já
		criptografado
		

	:param  inverte: permite um adicional de segurança aonde
		habilita a inversão da String antes de criptografar
		
	:rtype: boolean  resp
		
	:author: Claudio Salas
		

.. function:: Uti.criamap map


	:comentário: 
	:param map:

.. function:: Uti.poeMap map arg chave


	:comentário: 
	:param map:
		
	:param arg:
		
	:param chave:

.. function:: Uti.poemap map arg chave


	:comentário: 
	:param map:
		
	:param arg:
		
	:param chave:

.. function:: Uti.pegamap map arg chave


	:comentário: 
	:param map:
		
	:param arg:
		
	:param chave:

.. function:: Uti.pegamap map arg chave


	:comentário: 
	:param map:
		
	:param arg:
		
	:param chave:

.. function:: Uti.testamap map chave


	:comentário: 
	:param map:
		
	:param chave:

.. function:: Uti.testamap map chave


	:comentário: 
	:param map:
		
	:param chave:

.. function:: Uti.map2tab map lista del


	:comentário: 
	:param map:
		
	:param lista:
		
	:param del:

.. function:: Uti.map2list map lista del delChave


	:comentário: 
	:param map:
		
	:param lista:
		
	:param del:
		
	:param delChave:

.. function:: Uti.pegaChavesMap map resp del


	:comentário: 
	:param map:
		
	:param resp:
		
	:param del:

.. function:: Uti.delmap map chave


	:comentário: 
	:param map:
		
	:param chave:

.. function:: Uti.limpamap map


	:comentário: 
	:param map:

.. function:: Uti.qtmap map sqt


	:comentário: 
	:param map:
		
	:param sqt:

.. function:: Uti.interage strExec


	:comentário: Este metodo interage indefinidamente executando um metodo que na realidade
		executa uma string que pode ser modificada em cada iteracao Exemplos de uso
		deste comando, ver documentação no google docs. Um de seus objetivos é o uso
		em um processo interativo de trocar comandos e respostas com um executavel do
		sistema operacional (cmd.exe, por exemplo);
		
		
		

	:param  strExec: string que é executada continuamente pelo metodo. O metodo
		somente sai do loop se esta string ficar nula. Ou seja, se um
		metodo ou abstral for executado e fizer esta string nula, o
		interage irá parar. O mesmo acontecerá se o ultimo strExec tiver
		retornado falso. importante se a variavel interageTimeout for nula
		ou zero o loop somente parará de ser executado quando a execucao
		de strExec retornar false.
		
	:rtype: boolean  - true se nao se encerrar por
		timeout e a variavel "interageErro" tiver tamanho zero, e false se
		o loop cair por causa do tempo esgotado ou a variavel interageErro
		tiver alguma mensagem de erro.
		
		
	:throws:  ErrBds
		

.. function:: Uti.arq2map mapa listaChaves listaTab parquivo


	:comentário: Carrega para um mapa os dados de uma tabela em arquivo
		
		
		

	:param  mapa: mapa bdjava
		
		

	:param  listaChaves: lista dos campos chave
		
		

	:param  listaTab: lista dos campos da tabela
		
		

	:param  parquivo: arquivo onde estao os campos
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Uti.map2arq mapa listaTab parquivo


	:comentário: Salva o conteudo de um arquivo assumindo
		
		
		

	:param  mapa: mapa bdjava
		
		

	:param  listaTab: lista dos campos da tabela
		
		

	:param  parquivo: arquivo onde estao os campos
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Uti.getxyPlan plan x y


	:comentário: utilitarios para planilhas
		
		pega uma informacao de celula (<letras><numero>) e trainsforma em coordenadas
		xy
		
		
		

	:param  plan: celula desejada
		
		

	:param  x: coordenada x da celula. coordenada y da celula
		
		

	:param  y: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Uti.getCelPlan plan x y


	:comentário: converte de decimal para base 26 (A...Z)
		
		
		

	:param  az: 
		
		

	:param  dec: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		
		
		retorna uma celula (<letras><numero>) a partir da informacao xy xy
		
		
		

	:param  plan: celula desejada
		
		

	:param  x: coordenada x da celula. coordenada y da celula
		
		

	:param  y: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Uti.somaxyPlan resp cel x y


	:comentário: Soma x y a uma celula para achar o nome da celula correspondente
		
		
		

	:param  resp: 
		
		

	:param  cel: 
		
		

	:param  x: 
		
		

	:param  y: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Uti.difCelPlan celorig celref x y


	:comentário: Coloca em x e y a "distancia" entre celorig e celref
		
		
		

	:param  resp: 
		
		

	:param  celorig: 
		
		

	:param  celref: 
		
		

	:param  x: 
		
		

	:param  y: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Uti.getValPlan val strPlan celorig celref


	:comentário: Este metodo serve para "trabalhar" uma selecao copiada de uma planilha,
		pegando valor que está ali dentro
		
		
		

	:param  val: valor que é retornadp
		
		

	:param  strPlan: string que contem o conteudo da selecao (celulas separadas por
		tab e linhas separadas por "\n" )
		
		

	:param  celorig: celula origem
		
		

	:param  celref: celula a qual se deseja saber o valor
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Uti.setValPlan val strPlan celorig celref


	:comentário: Este metodo serve para "trabalhar" uma selecao copiada de uma planilha,
		setando o valor "val" na celula "celref" no seu ligar relativo em relacao a
		celorig que se encontra em strPlan
		
		
		

	:param  val: valor a ser setado
		
		

	:param  strPlan: string que contem o conteudo da selecao (celulas separadas por
		tab e linhas separadas por "\n" )
		
		

	:param  celorig: celula origem
		
		

	:param  celref: celula onde se deseja setar o valor val
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Uti.delimitadorDecimal delDec


	:comentário: 
	:param delDec:

.. function:: Uti.diaDaSemana data resp


	:comentário: Este metodo retorna o dia da semana de uma determinada data. Este metodo
		presume que a data está no seguinte formato: yyyy-MM-dd - 1999-12-01
		
		
		

	:param  data: data a ser verificada
		
		

	:param  resp: variavel de resposta SUNDAY = 1; MONDAY = 2; TUESDAY = 3;
		WEDNESDAY = 4; THURSDAY = 5; FRIDAY = 6; SATURDAY = 7;
		
		
	:rtype: void
		
		
	:throws:  ErrBds
		

.. function:: Uti.diaDaSemana data formato resp


	:comentário: Este metodo retorna o dia da semana de uma determinada data.
		
		
		

	:param  data: data a ser verificada
		
		

	:param  resp: variavel de resposta
		
		

	:param  formato: formato da data
		
		
	:rtype: void
		
		
	:throws:  ErrBds
		

.. function:: Uti.diaUtil data formato add diasNaoUteis resp


	:comentário: Este metodo retorna o dia util de acordo com o calculo de uma data
		
		Se add for negativo ( -1 por exemplo ) calcula o dia útil anterior
		
		Exemplo: Uti.diaUtil '10/10/2017' 'dd/MM/yy' '-1' '' r ==> r = 09/10/2017
		Uti.diaUtil '10/10/2017' 'dd/MM/yy' '1' '' r ==> r = 11/10/2017
		
		
		

	:param  data: data start
		
		

	:param  formato: formato da data start o mesmo sera usado para retorno
		
		

	:param  add: dias a serem incrementados
		
		

	:param  diasNaoUteis: lista de dias nao uteis separados por , seguindo o mesmo formato
		de "data"
		
		

	:param  resp: variavel de resposta
		
		

.. function:: Uti.ordenaDatas listaDatas formato


	:comentário: Metodo responsavel por ordenar uma lista de datas
		
		Exemplo: faz listaDatas '30/05/2018.01/06/2018' 'dd/MM/yyyy' Uti.ordenaDatas
		listaDatas
		
		
		

	:param  listaDatas: 
		
		

	:param  formato: 
		
		

.. function:: Uti.ordenaDatas listaDatas formato inverter


	:comentário: 
	:param listaDatas:
		
	:param formato:
		
	:param inverter:

.. function:: Uti.convertEncode str decode encode


	:comentário: Este metodo retorna o dia da semana de uma determinada data.
		
		
		

	:param  str: String atual
		
		

	:param  decode: encode atual
		
		

	:param  encode: novo encode atual
		
		
	:rtype: void
		
		
	:throws:  ErrBds
		

.. function:: Uti.convertEncode str decode encode


	:comentário: 
	:param str:
		
	:param decode:
		
	:param encode:

