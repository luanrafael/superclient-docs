
ManutTabs
=========
.. function:: ManutTabs.cargaArq con nomeTab listaTab arq


	:comentário: carrega uma tabela com os dados de um arquivo separados por tab listaTab tem
		que representar exatamente os campos que estao separados por tab
		
	:param con:
		
	:param nomeTab:
		
	:param listaTab:
		
	:param arq:

.. function:: ManutTabs.transfTab conLe conGrava nomeTab listaTab select


	:comentário: Transfere uma tabela entre bancos de dados diferentes
		
		
		

	:param  conLe: conector para o bando de dados de onde vem a tabela
		
		

	:param  conGrava: conector para o banco de dados que vai recever
		
		

	:param  nomeTab: nome da tabela
		
		

	:param  listaTab: lista de campos da tabela
		
		

	:param  select: select que sera usado
		
		
	:throws:  ErrBds
		

