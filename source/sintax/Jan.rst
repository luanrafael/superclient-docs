
Jan
===
.. function:: Jan.vars filtro


	:comentário: 
	:param filtro:

.. function:: Jan.vars filtro sth


	:comentário: 
	:param filtro:
		
	:param sth:

.. function:: Jan.vars pfiltro sth strout


	:comentário: 
	:param pfiltro:
		
	:param sth:
		
	:param strout:

.. function:: Jan.ivars filtrovar


	:comentário: 
	:param filtrovar:

.. function:: Jan.ivars filtrovar sth


	:comentário: 
	:param filtrovar:
		
	:param sth:

.. function:: Jan.ivars filtrovar sth filtro


	:comentário: 
	:param filtrovar:
		
	:param sth:
		
	:param filtro:

.. function:: Jan.ivars filtrovar sth filtro strout


	:comentário: 
	:param filtrovar:
		
	:param sth:
		
	:param filtro:
		
	:param strout:

.. function:: Jan.listaAbs filtrovar strout


	:comentário: 
	:param filtrovar:
		
	:param strout:

.. function:: Jan.procs 


	:comentário: 

.. function:: Jan.procs filtro opcoes


	:comentário: 
	:param filtro:
		
	:param opcoes:

.. function:: Jan.procs filtro opcoes resp


	:comentário: 
	:param filtro:
		
	:param opcoes:
		
	:param resp:

.. function:: Jan.existe th


	:comentário: verifica se uma thread de numero th existe
		
		
		

	:param  th: numero da thread
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jan.abre sjan


	:comentário: 
	:param sjan:

.. function:: Jan.vai sjan


	:comentário: 
	:param sjan:

.. function:: Jan.pproc arg pos posinic


	:comentário: 
	:param arg:
		
	:param pos:
		
	:param posinic:

.. function:: Jan.pabs filtro


	:comentário: 
	:param filtro:

.. function:: Jan.checacpu segundos


	:comentário: 
	:param segundos:

.. function:: Jan.checacpu segundos resp


	:comentário: 
	:param segundos:
		
	:param resp:

.. function:: Jan.replica th1 th2 pfiltro


	:comentário: 
	:param th1:
		
	:param th2:
		
	:param pfiltro:

.. function:: Jan.limpaAbs idConsumidor


	:comentário: 
	:param idConsumidor:

.. function:: Jan.pProcAbs arg pos posinic


	:comentário: utiliza um hash map para pegar um indice de variavel
		{
		¨Object o;
		¨int th = 0;
		¨Bds filtro = new Bds("$$" + idConsumidor + "~");
		¨if (C.mapvars[th] == null) // se ainda nao foi criado cria o novo mapa
		¨C.mapvars[th] = Collections.synchronizedMap(new HashMap(200));
		¨Map map = (Map) C.mapvars[th];
		¨int qt = map.size();
		¨TreeSet set = new TreeSet(map.keySet()); // devolve o conjunto de todas as variaveis
		¨Iterator iterator = set.iterator();
		¨boolean temfiltro = B.tamanho(filtro);
		¨boolean pode = true;
		¨Bds str = new Bds();
		¨Bds strm = new Bds();
		¨Bds left = new Bds();
		¨Bds listvar = new Bds();
		¨int qtvars = 0;
		¨while (iterator.hasNext()) {
		¨¨o = iterator.next();
		¨¨str.s = (String) o;
		¨¨if (!temfiltro)
		¨¨pode = true;
		¨¨else {
		¨¨¨pode = B.contem(str, filtro);
		¨¨}
		¨¨if (pode) {
		¨¨¨++qtvars;
		¨¨¨int i = B.pegavar(B.s(str), th);
		¨¨¨int xx = (Integer) map.remove("" + str);
		¨¨¨B.mensagem("LIMPANDO ABS INDICE " + i + "=" + str);
		¨¨¨C.varsSistemaUsadas[i] = false;
		¨¨¨C.vars[0][i] = new Bds();
		¨¨}
		¨}
		}
		}
	:param arg:
		
	:param pos:
		
	:param posinic:

