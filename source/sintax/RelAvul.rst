
RelAvul
=======
.. function:: RelAvul.vetor; linhas vars


	:comentário: 
	:param linhas:
		
	:param vars:

.. function:: RelAvul.tabula con tabelas lista selectRel dts


	:comentário: 
	:param con:
		
	:param tabelas:
		
	:param lista:
		
	:param selectRel:
		
	:param dts:

.. function:: RelAvul.geraDadosLista con mascara colunasRel tabelasRel listaRelTot listaPesq listaRel selectRel arqTemplate dtInic dtFim radArqSai descRel nomeRel


	:comentário: 
	:param con:
		
	:param mascara:
		
	:param colunasRel:
		
	:param tabelasRel:
		
	:param listaRelTot:
		
	:param listaPesq:
		
	:param listaRel:
		
	:param selectRel:
		
	:param arqTemplate:
		
	:param dtInic:
		
	:param dtFim:
		
	:param radArqSai:
		
	:param descRel:
		
	:param nomeRel:

.. function:: RelAvul.geraDadosLista con mascara colunasRel tabelasRel listaRelTot listaPesq listaRel selectRel arqTemplate dts radArqSai descRel nomeRel


	:comentário: 
	:param con:
		
	:param mascara:
		
	:param colunasRel:
		
	:param tabelasRel:
		
	:param listaRelTot:
		
	:param listaPesq:
		
	:param listaRel:
		
	:param selectRel:
		
	:param arqTemplate:
		
	:param dts:
		
	:param radArqSai:
		
	:param descRel:
		
	:param nomeRel:

.. function:: RelAvul.editaParms 


	:comentário: 

.. function:: RelAvul.geraParametros nomeAbs parms filtro


	:comentário: 
	:param nomeAbs:
		
	:param parms:
		
	:param filtro:

.. function:: RelAvul.acomodaLinha linha str


	:comentário: 
	:param linha:
		
	:param str:

.. function:: RelAvul.processa lista


	:comentário: 
	:param lista:

.. function:: RelAvul.execLista cps


	:comentário: 
	:param cps:

.. function:: RelAvul.geraHrefs radical dtInic nomeRel strTemplates strHrefs nomeArq


	:comentário: 
	:param radical:
		
	:param dtInic:
		
	:param nomeRel:
		
	:param strTemplates:
		
	:param strHrefs:
		
	:param nomeArq:

.. function:: RelAvul.pegaTemplate arqTemplate preambulo postambulo templates


	:comentário: 
	:param arqTemplate:
		
	:param preambulo:
		
	:param postambulo:
		
	:param templates:

.. function:: RelAvul.totaliza lista


	:comentário: 
	:param lista:

.. function:: RelAvul.totaliza lista nomeTotal


	:comentário: 
	:param lista:
		
	:param nomeTotal:

.. function:: RelAvul.montaStr str mascara lista colunasRel


	:comentário: 
	:param str:
		
	:param mascara:
		
	:param lista:
		
	:param colunasRel:

.. function:: RelAvul.instab tab lista


	:comentário: 
	:param tab:
		
	:param lista:

.. function:: RelAvul.gravaTabulacao mascara colunasRel listaRel arqTemplate dts radArqSai descRel nomeRel


	:comentário: 
	:param mascara:
		
	:param colunasRel:
		
	:param listaRel:
		
	:param arqTemplate:
		
	:param dts:
		
	:param radArqSai:
		
	:param descRel:
		
	:param nomeRel:

