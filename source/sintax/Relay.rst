
Relay
=====
.. function:: Relay.open socks hostsRelay hostsErro


	:comentário: Abre uma quantidade variavel de soquetes
		
		
		

	:param  socks: vetor com os soquetes
		
		

	:param  hostsRelay: lista dos hosts e respectivas portas - ex:
		'122.10.1.1:7506,12.12.13.1:2001'
		
		

	:param  hostsErro: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Relay.envPacote relayServ nsock pacote resp


	:comentário: Envia um pacote para determinado endereco, a partir de um indice do sock
		desejado
		
		
		

	:param  relayServ: 
		
		

	:param  nsock: 
		
		

	:param  pacote: 
		
		

	:param  resp: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

