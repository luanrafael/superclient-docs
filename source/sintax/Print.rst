
Print
=====
.. function:: Print.imp texto font size


	:comentário: Esse método imprime a string Bds passada como parâmetro. Após a execução do
		método, o aplicativo apresentará uma tela de diálogo referente as
		especificações da impressão.
		
		
		

	:param  texto: especifica string Bds a ser impressa.
		
		

	:param  font: especifica a fonte desejada Ex: SansSherif,Arial e etc.
		
		

	:param  size: especifica o tamanho da letra utilizada.
		

.. function:: Print.pcl args


	:comentário: 
	:param args:

