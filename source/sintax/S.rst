
S
=
.. function:: S.cursor posx posy


	:comentário: 
	:param posx:
		
	:param posy:

.. function:: S.getClick pnomeJan


	:comentário: 
	:param pnomeJan:

.. function:: S.getdClick pnomeJan


	:comentário: 
	:param pnomeJan:

.. function:: S.getdirClick pnomeJan


	:comentário: 
	:param pnomeJan:

.. function:: S.click 


	:comentário: 

.. function:: S.sClick posx posy nomeJan


	:comentário: 
	:param posx:
		
	:param posy:
		
	:param nomeJan:

.. function:: S.sClick nomeJan


	:comentário: 
	:param nomeJan:

.. function:: S.mClick teclas posx posy pnomeJan


	:comentário: 
	:param teclas:
		
	:param posx:
		
	:param posy:
		
	:param pnomeJan:

.. function:: S.click posx posy pnomeJan


	:comentário: 
	:param posx:
		
	:param posy:
		
	:param pnomeJan:

.. function:: S.click posx posy


	:comentário: 
	:param posx:
		
	:param posy:

.. function:: S.dClick posx posy pnomeJan


	:comentário: 
	:param posx:
		
	:param posy:
		
	:param pnomeJan:

.. function:: S.dClick posx posy


	:comentário: 
	:param posx:
		
	:param posy:

.. function:: S.dirClick posx posy pnomeJan


	:comentário: 
	:param posx:
		
	:param posy:
		
	:param pnomeJan:

.. function:: S.dirClick posx posy


	:comentário: 
	:param posx:
		
	:param posy:

.. function:: S.jan pjanela atalho


	:comentário: 
	:param pjanela:
		
	:param atalho:

.. function:: S.jan pjanela


	:comentário: 
	:param pjanela:

.. function:: S.dimensiona largura altura pnomeJanela


	:comentário: 
	:param largura:
		
	:param altura:
		
	:param pnomeJanela:

.. function:: S.posDim posx posy largura altura pnomeJanela


	:comentário: 
	:param posx:
		
	:param posy:
		
	:param largura:
		
	:param altura:
		
	:param pnomeJanela:

.. function:: S.posiciona posx posy pnomeJanela


	:comentário: 
	:param posx:
		
	:param posy:
		
	:param pnomeJanela:

.. function:: S.posMouse posx posy


	:comentário: 
	:param posx:
		
	:param posy:

.. function:: S.posMouse posx posy pnomeJan


	:comentário: 
	:param posx:
		
	:param posy:
		
	:param pnomeJan:

.. function:: S.mteclas args


	:comentário: 
	:param args:

.. function:: S.texto args


	:comentário: 
	:param args:

.. function:: S.copiaCampo argSelec resp pjanela


	:comentário: 
	:param argSelec:
		
	:param resp:
		
	:param pjanela:

.. function:: S.copiaCampo posx posy argSelec resp pjanela


	:comentário: 
	:param posx:
		
	:param posy:
		
	:param argSelec:
		
	:param resp:
		
	:param pjanela:

.. function:: S.copiaTexto posx0 posy0 posx1 posy1 resp pjanela


	:comentário: 
	:param posx0:
		
	:param posy0:
		
	:param posx1:
		
	:param posy1:
		
	:param resp:
		
	:param pjanela:

.. function:: S.seleciona posx0 posy0 posx1 posy1 pjanela


	:comentário: 
	:param posx0:
		
	:param posy0:
		
	:param posx1:
		
	:param posy1:
		
	:param pjanela:

.. function:: S.seleciona posx0 posy0 posx1 posy1


	:comentário: 
	:param posx0:
		
	:param posy0:
		
	:param posx1:
		
	:param posy1:

.. function:: S.poeCursor x y pnomeJanela


	:comentário: poe o cursor no local especificado dentro da janela
		
		
		

	:param  x: A posição horizontal em pixels
		
		

	:param  y: A posição vertical em pixels
		
		

	:param  nomeJanela: O nome ou parte do nome da janela
		
		
	:rtype: boolean  Se foi possível posicionar o cursor
		
		
	:throws:  ErrBds
		

.. function:: S.poeCursor x y


	:comentário: poe o cursor no local especificado na tela
		
		
		

	:param  x: A posição horizontal em pixels
		
		

	:param  y: A posição vertical em pixels
		
		

	:param  nomeJanela: O nome ou parte do nome da janela
		
		
	:rtype: boolean  Se foi possível posicionar o cursor
		
		
	:throws:  ErrBds
		

.. function:: S.log args


	:comentário: faz um log comum dos argumentos
		
		
		
	:throws:  ErrBds
		
	:param args:

.. function:: S.logErro args


	:comentário: faz um log de erro
		
		
		

	:param  args: vetor string - ultimo argumento tipo de erro (L-leve,
		S-Serio, G-Grave) - Outros argumentos sao concatenados para
		formar o log
		
		
	:throws:  ErrBds
		

.. function:: S.criaCampo nomecampo args


	:comentário: 
	:param nomecampo:
		
	:param args:

.. function:: S.setaCampo campo corFonte corFundo fonte tamFonte tipoFonte


	:comentário: 
	:param campo:
		
	:param corFonte:
		
	:param corFundo:
		
	:param fonte:
		
	:param tamFonte:
		
	:param tipoFonte:

.. function:: S.criaLista nomeLista listacps


	:comentário: 
	:param nomeLista:
		
	:param listacps:

.. function:: S.zeraLista lista


	:comentário: 
	:param lista:

.. function:: S.monta nomeLista reg del


	:comentário: 
	:param nomeLista:
		
	:param reg:
		
	:param del:

.. function:: S.separa nomeLista reg del


	:comentário: 
	:param nomeLista:
		
	:param reg:
		
	:param del:

.. function:: S.pegaDados lista titulo


	:comentário: 
		
		cria uma tela com os campos indicados na lista
		
		
		

	:param  lista: 
		
		

	:param  titulo: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: S.limpaTela lista


	:comentário: 
		
		cria uma tela com os campos indicados na lista
		
		
		

	:param  lista: 
		
		

	:param  titulo: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: S.pegaDados lista


	:comentário: 
		
		cria uma tela com os campos indicados na lista
		
		
		

	:param  lista: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: S.trazTrab str


	:comentário: traz da area de trabalho os dados existentes e coloca em str
		
		
		

	:param  str: 
		
		
	:throws:  ErrBds
		

.. function:: S.tela lista titulo


	:comentário: monta uma tela e exibe
		
		
		

	:param  lista: contem os dados que serao montados na tela
		
		

	:param  titulo: titulo de janela da tela que será montada
		
		
	:rtype: boolean  verdadeiro se operacao OK
		
		
	:throws:  ErrBds
		

.. function:: S.ocultaTela lista


	:comentário: oculta a tela indicada
		
		
		

	:param  lista: contem os dados que serao montados na tela
		
		
	:rtype: boolean  verdadeiro se operacao OK
		
		
	:throws:  ErrBds
		

.. function:: S.popTela lista


	:comentário: faz um pop da tela indicada (deixa de ficar invisivel)
		
		
		

	:param  lista: contem os dados que serao montados na tela
		
		
	:rtype: boolean  verdadeiro se operacao OK
		
		
	:throws:  ErrBds
		

.. function:: S.trazCampos lista


	:comentário: pega o conteudo digitado dentro da tela e traz para os respectivos campos
		
		
		

	:param  lista: contem os dados que serao montados na tela
		
		
	:rtype: boolean  verdadeiro se operacao OK
		
		
	:throws:  ErrBds
		

.. function:: S.poeCampos lista


	:comentário: poe o conteudo dos campos na tela
		
		
		

	:param  lista: contem os dados que serao montados na tela
		
		
	:rtype: boolean  verdadeiro se operacao OK
		
		
	:throws:  ErrBds
		

.. function:: S.copiaTrab str


	:comentário: copia para area de trabalho os dados existentes em str
		
		
		

	:param  str: 
		
		
	:throws:  ErrBds
		

.. function:: S.ct str


	:comentário: 
	:param str:

.. function:: S.getScreen arq tela tempo


	:comentário: 
	:param arq:
		
	:param tela:
		
	:param tempo:

.. function:: S.getScreen parq x1 y1 x2 y2 tempo


	:comentário: 
	:param parq:
		
	:param x1:
		
	:param y1:
		
	:param x2:
		
	:param y2:
		
	:param tempo:

.. function:: S.xexec args


	:comentário: monta uma string com argumentos e esta string é executada como sendo um
		comando bdjava
		
		
		

	:param  str: 
		
		
	:throws:  ErrBds
		
		
	:rtype: boolean  retorna o resultado do comando B.exec
		

.. function:: S.rexec args


	:comentário: monta uma string com argumentos e esta string é executada remotamente
		
		
		

	:param  str: 
		
		
	:throws:  ErrBds
		

.. function:: S.sistema args


	:comentário: monta uma string com argumentos e esta string é executada remotamente
		
		
		

	:param  str: 
		
		
	:throws:  ErrBds
		
		
	:rtype: boolean  - retorna verdadeiro se o comando de sistema retornar zero
		

.. function:: S.alerta args


	:comentário: equivalente a um "msgBox" concatena os argumentos e os apresenta em uma
		janela
		
		
		

	:param  str: 
		
		
	:throws:  ErrBds
		

.. function:: S.info args


	:comentário: equivalente a apresentar uma mensagem por um periodo de milisegundos. Se
		o ultimo argumento for numerico, considera como sendo os milisegundos
		desejados para exibir a mensagem Se nao for numerico, a mensagem é
		exibida por 1 segundo concatena os argumentos e os apresenta em uma
		janela
		
		
		

	:param  str: 
		
		
	:throws:  ErrBds
		

.. function:: S.infoR args


	:comentário: equivalente a apresentar uma mensagem por um periodo de milisegundos. é
		executado em uma thread remota Se o ultimo argumento for numerico,
		considera como sendo os milisegundos desejados para exibir a mensagem Se
		nao for numerico, a mensagem é exibida por 1 segundo concatena os
		argumentos e os apresenta em uma janela
		
		
		

	:param  str: 
		
		
	:throws:  ErrBds
		

.. function:: S.infoR0 arg tout


	:comentário: 
	:param arg:
		
	:param tout:

.. function:: S.checaOk args


	:comentário: Abre uma janela onde se pode confirmar ou nao se concorda com uma questao
		concatena os argumentos e os apresenta em uma janelat
		
		
		

	:param  args: argumentos que sao montados para apresentacao na janela
		
		
	:rtype: boolean  true se se confirmar com OK
		
		
	:throws:  ErrBds
		

.. function:: S.pegaJan jan


	:comentário: Pega a ultima janela clicada desde que nao seja a do SuperClient do
		proprio IDE ou o Bdjava
		
		
		

	:param  jan: retorna o titulo completo da janela
		
		
	:throws:  ErrBds
		

.. function:: S.pegaJan jan handle


	:comentário: Pega a ultima janela clicada desde que nao seja a do SuperClient do
		proprio IDE ou o Bdjava
		
		
		

	:param  jan: retorna o titulo completo da janela
		
		

	:param  handle: retorna o handle da janela
		
		
	:throws:  ErrBds
		

.. function:: S.pegaJan jan handle pjanFiltro


	:comentário: Pega a ultima janela clicada desde que nao seja a do SuperClient do
		proprio IDE ou o Bdjava. Retorna a primeira janela que contiver
		<janFiltro>.
		
		
		

	:param  jan: recebe a primeira janela que contiver <janFiltro>
		
		

	:param  handle: handle a ser informado
		
		

	:param  janFiltro: contem o titulo ou um pedaco de titulos das janelas que
		podem ser aceitas separados por ^ e ainda separados por "~" de
		outro grupo de janelas que nao sao aceitas tambem separadas
		por "^" exemplo: pegaJan jan handle '~<lista de janelas
		constantes do pin, separadas por "^": ira pegar a janela mais
		significante retirando-se as janelas do pin
		
		
	:throws:  ErrBds
		
		

.. function:: S.vteclas teclas


	:comentário: 
	:param teclas:

.. function:: S.teclas teclas


	:comentário: 
	:param teclas:

.. function:: S.teclass ...teclas


	:comentário: 
	:param ...teclas:

.. function:: S.jTeclas pteclas pnomeJan


	:comentário: 
	:param pteclas:
		
	:param pnomeJan:

.. function:: S.selecionaTudo resp


	:comentário: 
	:param resp:

.. function:: S.teclasVbs pteclas pjan


	:comentário: 
	:param pteclas:
		
	:param pjan:

.. function:: S.teclasVbs pteclas pjan


	:comentário: 
	:param pteclas:
		
	:param pjan:

.. function:: S.temZica str


	:comentário: tem zica significa ter um texto entre dois {} onde tem acento ou qq outra
		coisa que nao seja letras maiusculas ou numeros
		
		
		

	:param  str: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: S.caracteres 


	:comentário: 

.. function:: S.pausaTeclas pausa


	:comentário: ajusta a pausa minima entre cada comando teclas
		
		
		

	:param  pausa: pausa minima em milisegundos
		
		
	:throws:  ErrBds
		

.. function:: S.usaRobot usa


	:comentário: se usa = 1, usa a solucao do robot java
		
		
		

	:param  usa: 
		
		
	:throws:  ErrBds
		

.. function:: S.leCtlz cp arq


	:comentário: le o conteudo do CTL-Z de um campo a partir de um arquivo
		
		
		

	:param  cp: 
		
		

	:param  arq: 
		
		
	:throws:  ErrBds
		

.. function:: S.toFixando 


	:comentário: 

.. function:: S.marcaChecaJans jans


	:comentário: marca janerlas a serem monitoradas Cada nome de janela tem um ":" que
		indica o abstral a ser executado O Ultimo argumento é o tempo de
		verificacao
		
		

	:param  jans:  sequencia de argumentos conforme explicado
		acima
		

.. function:: S.checaJans 


	:comentário: 

.. function:: S.cancelaChecaJans 


	:comentário: Cancela o fixaJan
		
		
		
	:throws:  ErrBds
		

.. function:: S.fixaJans jans


	:comentário: 
	:param jans:

.. function:: S.mantemJans 


	:comentário: 

.. function:: S.cancelaFixaJan 


	:comentário: Cancela o fixaJan
		
		
		
	:throws:  ErrBds
		

.. function:: S.brw url janBrowser op


	:comentário: Chama um browser considerando o seguinte.
		
		Se janBrowser indica o handle de uma janela de Browser ativa, esta janela
		ativa é usada para este comando. Se nao estiver ativa, abre uma nova
		janela, capturando o handle e pondo em janBrowser. Se for a primeira vez
		que a janela for chamada, a url indicada é chamada. Se a janela já
		existir, se op for 1, chamada a url indicada na janela, fazendo esta
		"voltar para o inicio" se op=0, e a janela ja existir, apenas Dá foco
		nesta janela importante - Brw funciona com qualquer browser que entenda
		ALT-D como sendo o foco necessario para se digitar uma nova url Para
		isto, deve setar a variavel "$caminhoBrowser" com o caminho necessario
		para chamar o browser desejado exemplos: faz $caminhoBrowser '
		"c:\\Program Files\\internet explorer\\iexplore.exe"' para o internet
		explorer e faz $caminhoBrowser '""C:\\Program
		Files\\Google\\Chrome\\Application\\chrome.exe""' para o google chrome
		
		
		

	:param  url: url desejada para a janela se iniciar
		
		

	:param  janBrowser: variavel que deve receber o valor do handle - ao ser chamada
		a primeira vez deve conter o titulo esperado para a janela
		
		

	:param  op: se = 1 - mesmo a janela existindo , chama a url inicial
		(volta para o inicio)
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: S.brw url janBrowser op px py lx ly


	:comentário: Chama um browser considerando o seguinte.
		
		Se janBrowser indica o handle de uma janela de Browser ativa, esta janela
		ativa é usada para este comando. Se nao estiver ativa, abre uma nova
		janela. Se op >=0, captura o handle e pondo em janBrowser. Se for a
		primeira vez que a janela for chamada, a url indicada é chamada. Se a
		janela já existir, se op for 1, chamada a url indicada na janela, fazendo
		esta "voltar para o inicio" se op=0, e a janela ja existir, apenas Dá
		foco nesta janela importante - Brw funciona com qualquer browser que
		entenda ALT-D como sendo o foco necessario para se digitar uma nova url
		Para isto, deve setar a variavel "$caminhoBrowser" com o caminho
		necessario para chamar o browser desejado exemplos: faz $caminhoBrowser '
		"c:\\Program Files\\internet explorer\\iexplore.exe"' para o internet
		explorer e faz $caminhoBrowser '
		"C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe"' para o
		google chrome
		
		
		

	:param  url: url desejada para a janela se iniciar
		
		

	:param  janBrowser: variavel que deve receber o valor do handle - ao ser chamada
		a primeira vez deve conter o titulo esperado para a janela
		
		

	:param  op: se = 1 - mesmo a janela existindo , chama a url inicial
		(volta para o inicio) se op <> 1 - na inicializacao, o brw nao
		busca o handler, simplesmente apresenta a janela na tela
		
		

	:param  px: posicao inicial (x) da janela
		
		

	:param  py: posicao inicial (y)
		
		

	:param  lx: largura em pixels
		
		

	:param  ly: altura em pixels
		
		
	:throws:  ErrBds
		

.. function:: S.locReg handler arquivo plista chave delcp delreg pop


	:comentário: 
	:param handler:
		
	:param arquivo:
		
	:param plista:
		
	:param chave:
		
	:param delcp:
		
	:param delreg:
		
	:param pop:

.. function:: S.onTop telas


	:comentário: marca as janelas indicadas para sempre ficarem por cima Marca as janelas
		indicadas para ficarem sempre no topo Somente aceita as janelas criadas
		no proprio bdjava
		
		
		

	:param  telas: argumentos: um argumento para cada tela exemplo
		"onTop listaTela listaHist"
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: S.getPixel px py janela r g b


	:comentário: Retorna os componentes RGB de um pixel (r: red-vermelho, g: green-verde e
		b:blue - azul)
		
		
		

	:param  px: pos horizontal do pixel em relacao à janela desejada
		
		

	:param  py: pos vertical
		
		

	:param  janela: nome da janela - se for nula, considera a tela do computador
		
		

	:param  r: componente "red" - vemelho
		
		

	:param  g-: compnente "green" - verde
		
		

	:param  b: componente "blue" - azul -
		
	:rtype: boolean  retorna falso se nao
		encontrar a janela ou se o pixel estiver fora da janela
		
		
	:throws:  ErrBds
		

.. function:: S.naoTop telas


	:comentário: marca as janelas indicadas para nao mais ficarem ficarem por cima Marca
		as janelas indicadas para nao ficarem mais no topo Somente aceita as
		janelas criadas no proprio bdjava
		
		
		

	:param  telas: argumentos: um argumento para cada tela exemplo
		"onTop listaTela listaHist"
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: S.pjan pjan


	:comentário: processa uma especificacao de janela. Se contiver acento, troca pelo
		handle
		
		
		
	:rtype: Bds
		
		
	:throws:  ErrBds
		
	:param pjan:

.. function:: S.mlog []xargs


	:comentário: Log referente à m2g etc
		se o servidor nao esta disponivel, grava no emsg
		
		

	:param  xargs: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

