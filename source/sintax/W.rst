
W
=
.. function:: W.poecampotxt jan nome stam spos


	:comentário: 
	:param jan:
		
	:param nome:
		
	:param stam:
		
	:param spos:

.. function:: W.poelabel jan label stam spos


	:comentário: 
	:param jan:
		
	:param label:
		
	:param stam:
		
	:param spos:

.. function:: W.mostra jan listax


	:comentário: 
	:param jan:
		
	:param listax:

.. function:: W.mostra jan


	:comentário: 
	:param jan:

.. function:: W.oculta jan


	:comentário: @epp040722 alterei daqui
		// OCULTA
		public static boolean oculta(Bds jan) throws ErrBds
		public static boolean oculta(Bds jan) throws ErrBds
	:param jan:

.. function:: W.janela jan op


	:comentário: 
	:param jan:
		
	:param op:

.. function:: W.janela jan op


	:comentário: 
	:param jan:
		
	:param op:

.. function:: W.apaga jan


	:comentário: @epp040722 ateh aqui
		// APAGA
		public static boolean apaga(Bds jan) throws ErrBds
		public static boolean apaga(Bds jan) throws ErrBds
	:param jan:

.. function:: W.poeopcoes jan opcoes


	:comentário: 
	:param jan:
		
	:param opcoes:

.. function:: W.montacomp bcp


	:comentário: 
	:param bcp:

.. function:: W.clabelacao jan label


	:comentário: 
	:param jan:
		
	:param label:

.. function:: W.poelistav jan lista0 nomeborda local


	:comentário: 
	:param jan:
		
	:param lista0:
		
	:param nomeborda:
		
	:param local:

.. function:: W.poejan jan panel


	:comentário: 
	:param jan:
		
	:param panel:

.. function:: W.setay bcp slin


	:comentário: seta a largura de um campo
		
		
	:param bcp:
		
	:param slin:

.. function:: W.selec bcp inic fim


	:comentário: seleciona os dados de arg no JTextComponent que esta' em bcp
		
	:param bcp:
		
	:param inic:
		
	:param fim:

.. function:: W.poetexto bcp arg


	:comentário: 
	:param bcp:
		
	:param arg:

.. function:: W.poetexto bcp arg


	:comentário: Poe os dados de arg no JTextComponent que esta' em bcp
		
	:param bcp:
		
	:param arg:

.. function:: W.pegatexto bcp arg


	:comentário: Pega os dados de um campo JTextComponent e poe em arg
		
	:param bcp:
		
	:param arg:

.. function:: W.traztexto arg


	:comentário: 
		
		Copia da area de trabalho de um computador para uma variavel
		
		
	:param arg:

.. function:: W.traztexto arg tout


	:comentário: 
		
		Copia da area de trabalho de um computador para uma variavel
		
		
	:param arg:
		
	:param tout:

.. function:: W.copiatexto arg


	:comentário: 
		
		Copia os dados da area de texto de uma variavel para a area de trabalho de um
		computador
		
	:param arg:

.. function:: W.setacomp bcp args0


	:comentário: seta determinado determinado atributo de um campo Esse método seta a fonte da
		tela indicada (cmd ou trab)
		
		<pre>
		Exemplo de chamada: setacomp (B.s("f=CourierNew;e=BOLD;t=12")); setacomp
		(B.s("f=CourierNew;e=BOLD,ITALIC;t=12")); nte f=nome da fonte, e=estilo
		(BOLD;ITALIC;PLAIN ou BOLD,ITALIC) e t=tamanho em pontos
		
		
	:param bcp:
		
	:param args0:

.. function:: W.montapanel jan lista orientacao


	:comentário: 
	:param jan:
		
	:param lista:
		
	:param orientacao:

.. function:: W.poelistah jan lista nomeborda local


	:comentário: 
	:param jan:
		
	:param lista:
		
	:param nomeborda:
		
	:param local:

.. function:: W.montatela jan lista0 nomeborda local


	:comentário: 
	:param jan:
		
	:param lista0:
		
	:param nomeborda:
		
	:param local:

.. function:: W.criatela jan lista0 titulo frase opcoes


	:comentário: 
	:param jan:
		
	:param lista0:
		
	:param titulo:
		
	:param frase:
		
	:param opcoes:

.. function:: W.tmontajan args


	:comentário: 
	:param args:

.. function:: W.montajan args


	:comentário: 
	:param args:

.. function:: W.proctela lista0 eventos stipoproc


	:comentário: 
	:param lista0:
		
	:param eventos:
		
	:param stipoproc:

.. function:: W.proctela lista0 eventos


	:comentário: 
	:param lista0:
		
	:param eventos:

.. function:: W.proctela lista0 eventos tipoproc ptela


	:comentário: 
	:param lista0:
		
	:param eventos:
		
	:param tipoproc:
		
	:param ptela:

.. function:: W.atlista listax


	:comentário: 
	:param listax:

.. function:: W.atlista0 vlistax


	:comentário: 
	:param vlistax:

.. function:: W.attela listax


	:comentário: 
	:param listax:

.. function:: W.attela0 vlistax


	:comentário: 
	:param vlistax:

.. function:: W.proc_para 


	:comentário: 

.. function:: W.msg stipomsg smensagem sopcoes resp


	:comentário: 
	:param stipomsg:
		
	:param smensagem:
		
	:param sopcoes:
		
	:param resp:

.. function:: W.msg tipomsg mensagem opcoes resp


	:comentário: 
	:param tipomsg:
		
	:param mensagem:
		
	:param opcoes:
		
	:param resp:

.. function:: W.browser browser strInfo


	:comentário: 
	:param browser:
		
	:param strInfo:

.. function:: W.browser browser tituloJan strInfo


	:comentário: 
	:param browser:
		
	:param tituloJan:
		
	:param strInfo:

.. function:: W.browser browser tituloJan strInfo largura altura


	:comentário: 
	:param browser:
		
	:param tituloJan:
		
	:param strInfo:
		
	:param largura:
		
	:param altura:

