
Cvs
===
.. function:: Cvs.cvs_imp estrutura comentario modulo diretorio


	:comentário: Responsavel por importar um modulo no Servidor CVS Paramentros: Comentario -
		Serve para identificar o que esta sendo inserido, uso obrigatorio Modulo -
		Nome do projeto a ser inserido no sistema Diretorio - Utilizado como
		parametro casa necessidade de se importar apenas uma subpasta Linguagem -
		BDMAX ou BDJAVA, serve para definir a estrutura p/ importar o projeto
		
	:param estrutura:
		
	:param comentario:
		
	:param modulo:
		
	:param diretorio:

.. function:: Cvs.sendcvs argenv


	:comentário: 
	:param argenv:

.. function:: Cvs.sendcvs argenv


	:comentário: 
	:param argenv:

.. function:: Cvs.cvs_chk estrutura modulo diretorio release


	:comentário: 
	:param estrutura:
		
	:param modulo:
		
	:param diretorio:
		
	:param release:

.. function:: Cvs.cvs_upd estrutura comentario modulo diretorio arquivo data


	:comentário: 
	:param estrutura:
		
	:param comentario:
		
	:param modulo:
		
	:param diretorio:
		
	:param arquivo:
		
	:param data:

.. function:: Cvs.cvs_commit estrutura comentario modulo diretorio


	:comentário: 
	:param estrutura:
		
	:param comentario:
		
	:param modulo:
		
	:param diretorio:

.. function:: Cvs.cvs_release estrutura modulo diretorio


	:comentário: 
	:param estrutura:
		
	:param modulo:
		
	:param diretorio:

.. function:: Cvs.espera segs


	:comentário: 
	:param segs:

.. function:: Cvs.add estrutura comentario modulo arquivo


	:comentário: //
		
		Método utilizado para adicionar arquivos no repositório.
		
		
		

	:param  estrutura: define o tipo de estrutura utilizada.
		
		

	:param  comentario: especifica o comentario da operacao.
		
		

	:param  modulo: especifica o modulo existente na estrutura selecionada.
		
		

	:param  arquivo: especifica o nome do arquivo que será adicionado no repositório
		CVS.
		

.. function:: Cvs.remove estrutura comentario modulo arquivo


	:comentário: //
		
		Método utilizado para remover arquivos do repositório.
		
		
		

	:param  estrutura: define o tipo de estrutura utilizada.
		
		

	:param  comentario: especifica o comentario da operacao.
		
		

	:param  modulo: especifica o modulo existente na estrutura selecionada.
		
		

	:param  arquivo: especifica o nome do arquivo que será removido no repositório CVS.
		

.. function:: Cvs.tag estrutura modulo release


	:comentário: //
		
		Método utilizado para marcar um modulo com uma tag referente ao release atual
		da área de trabalho.
		
		
		

	:param  estrutura: define o tipo de estrutura utilizada.
		
		

	:param  comentario: especifica o comentario da operacao.
		
		

	:param  modulo: especifica o modulo existente na estrutura selecionada.
		

.. function:: Cvs.montadir estrutura modulo diretorio cvscomando


	:comentário: //
		
		Esse metodo monta a estrutura de arquivos utilizada na implementação do CVS
		
		<pre>
		Exemplo:
		<b>Cvs.montadir 'users' 'c12' 'e:\teste' 'chk'</b>
		
		esse comando criará a seguinte estrutura: e:-teste
		|
		users
		|
		c12
		conteúdo
		</pre>
		
		
		

	:param  estrutura: define a estrutura de diretórios utilizada pelo CVS
		
		

	:param  modulo: define o modulo localizado no CVS
		
		

	:param  diretorio: define o diretório base para checkout do CVS
		
		

	:param  cvscomando: define o tipo de comando utilizado pelo CVS.
		

