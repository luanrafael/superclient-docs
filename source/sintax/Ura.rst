
Ura
===
.. function:: Ura.abrefax pret pcanal


	:comentário: Abre um canal fax
		
		
		

	:param  pret: retorna o codigo de retorno
		
		

	:param  pcanal: canal para o qual se quer inicializar o fax
		
		

.. function:: Ura.iniccan pcanal


	:comentário: Este metodo inicia o canal indicado
		
		
		

	:param  pcanal: Indica qual canal que vai ser iniciado
		
		@uthor epp 2006, car 2004
		@Since 040913
		
		

.. function:: Ura.discan pcanal pani pnum ptmp pret


	:comentário: 
	:param pcanal:
		
	:param pani:
		
	:param pnum:
		
	:param ptmp:
		
	:param pret:

.. function:: Ura.disca pret pnumero


	:comentário: 
	:param pret:
		
	:param pnumero:

.. function:: Ura.disca pret pnumero


	:comentário: 
	:param pret:
		
	:param pnumero:

.. function:: Ura.fechafax 


	:comentário: 

.. function:: Ura.atende pcanal ptout pret


	:comentário: 
	:param pcanal:
		
	:param ptout:
		
	:param pret:

.. function:: Ura.atende pcanal ptout pani pdni pret


	:comentário: 
	:param pcanal:
		
	:param ptout:
		
	:param pani:
		
	:param pdni:
		
	:param pret:

.. function:: Ura.pegateclas pteclas ptam timeoutInic timeoutDecay popcoes teclasEsp


	:comentário: 
	:param pteclas:
		
	:param ptam:
		
	:param timeoutInic:
		
	:param timeoutDecay:
		
	:param popcoes:
		
	:param teclasEsp:

.. function:: Ura.play parquivo


	:comentário: 
	:param parquivo:

.. function:: Ura.play parquivo


	:comentário: 
	:param parquivo:

.. function:: Ura.play parquivo popcoes


	:comentário: 
	:param parquivo:
		
	:param popcoes:

.. function:: Ura.play parquivo popcoes


	:comentário: 
	:param parquivo:
		
	:param popcoes:

.. function:: Ura.play pret parquivo popcoes


	:comentário: Este metodo fala na linha telefonica o arquivo que esta em parq
		
		
		

	:param  parquivo: indica o arquivo / arquivos a serem falados atencao - se for
		mais de um arquivo, vem separados por ';'. Os arquivos podem vir
		com o caminho completo ou entao vir com a indicacao do diretorio
		no formato "<...>" no inicio da string que indica os arquivos
		exemplos : <e:/d/voz/numeros> - indica um diretorio especifico
		para todos os arquivos <dirNum> - caso especial - indica que todos
		os arquivos estao no diretorio onde estao as frases de numeros
		Ainda mais: Se nao vier a indicacao de diretorio seja no arquivo
		ou no interior da string de arquivos, o arquivo de fala é montado
		com a concatenacao do "drive" de voz, wav ou txt conforme o caso
		com o diretorio respectivo e o nome do arquivo
		
		

	:param  popcoes: indica as opcoes de fala. No momento 'P' indica que o play para
		se se tecla algo no telefone
		
		

	:param  pret: retorna o resultado da operacao - =1,0 - operacao OK =2 -
		retornou porque houve um hup menor que zero - houve erro e retorna
		o codigo do erro
		
		
	:rtype: boolean  true - se o play foi bem sucedido e nao houve hup false - em
		contrario
		
		
	:author:  epp 2006, max 2004
		@Since 061229
		

.. function:: Ura.play pret parquivo popcoes


	:comentário: Este metodo fala na linha telefonica o arquivo que esta em parq
		
		
		

	:param  parquivo: indica o arquivo / arquivos a serem falados atencao - se for
		mais de um arquivo, vem separados por ';'. Os arquivos podem vir
		com o caminho completo ou entao vir com a indicacao do diretorio
		no formato "<...>" no inicio da string que indica os arquivos
		exemplos : <e:/d/voz/numeros> - indica um diretorio especifico
		para todos os arquivos <dirNum> - caso especial - indica que todos
		os arquivos estao no diretorio onde estao as frases de numeros
		Ainda mais: Se nao vier a indicacao de diretorio seja no arquivo
		ou no interior da string de arquivos, o arquivo de fala é montado
		com a concatenacao do "drive" de voz, wav ou txt conforme o caso
		com o diretorio respectivo e o nome do arquivo
		
		

	:param  popcoes: indica as opcoes de fala. No momento 'P' indica que o play para
		se se tecla algo no telefone
		
		

	:param  pret: retorna o resultado da operacao - =1,0 - operacao OK =2 -
		retornou porque houve um hup menor que zero - houve erro e retorna
		o codigo do erro
		
		
	:rtype: boolean  true - se o play foi bem sucedido e nao houve hup false - em
		contrario
		
		
	:author:  epp 2006, max 2004
		@Since 061229
		

.. function:: Ura.hup pcanal ptout


	:comentário: 
	:param pcanal:
		
	:param ptout:

.. function:: Ura.rec pret parquivo popcoes ptimeout


	:comentário: testa se houve algum erro operacional
		if (B.i(iret) < 0) {
		¨B.mensagem("Retornou erro ");
		¨return false;
		}
		return true;
		}
		// REC
		//
		
		Este metodo grava um arquivo de voz
		
		
		

	:param  pret: codigo de retorno 0 ou 1 = OK, 2 - o canal caiu por hup, menor
		que zero - retorna com erro
		
		

	:param  parquivo: arquivo a ser gravado. Se nao tiver "/" , é concatenado com o
		drive e diretorios correspondentes (voz,wav ou txt)
		
		

	:param  opcoes: opcoes desejadas. No momento 'P' indica que a gravacao deve
		parar com com qq tecla teclada. Se contiver codigos de teclas,
		deve parar ao se teclar a tecla indicada. Normalmente o rec tem
		como opcao '#' (quando se tecla '#', é como se teclasse o "enter"
		
		

	:param  ptimeout: tempo maximo em segundos a ser gravado
		
		
		
	:rtype: boolean  true se a gravacao foi bem sucedida e a chamada nao caiu
		
		
		
	:author:  epp 2006, max 2004
		@Since 040330
		

.. function:: Ura.beep ptom ptempo


	:comentário: testa se houve algum erro operacional
		if (B.i(iret) < 0) {
		¨B.mensagem("Retornou erro ");
		¨return false;
		}
		return true;
		}
		// BEEP
		//
		
		Este metodo digita tons da linha
		
		
		

	:param  ptom: tons a serem "teclados"na linha pela ira digitados
		(0123456789#) ptout
		
		

	:param  ptempo: TimeOut do cmdUra (o tempo maximo que a placa vai levar para
		digitar os tons)
		
		
		
	:author:  epp 2006, max 2004
		@Since 040330
		

.. function:: Ura.sendIVR pmsg pret ptmt prsp


	:comentário: testa se houve algum erro operacional
		if (B.i(iret) < 0) {
		¨B.mensagem("Retornou erro ");
		¨return false;
		}
		.dbg 'Resp:[' irsp '] Ret=[' iret ']'
		B.parte(irsp, iargs, C.CHAR1, 5);
		B.parte(iargs, irets, C.CHAR3, 1);
		return true;
		}
		// SENDIVR
		//
		
		
		envia a mensagem de comunicacao entre o aplicativo Ura ea Ura fisica (o
		canal)
		
		
		

	:param  pmsg: mensagem montada a ser processada pela ura fisica
		
		

	:param  pret: codigo de retorno da operacao
		
		

	:param  ptmp: timeout para esperar resposta da ura fisica
		
		

	:param  prsp: resposta da operacao processada pela ura (teclas acionadas, por
		exemplo)
		
		

.. function:: Ura.montapacote popr pcanal ptmt pcmd pargs ppac


	:comentário: 
	:param popr:
		
	:param pcanal:
		
	:param ptmt:
		
	:param pcmd:
		
	:param pargs:
		
	:param ppac:

.. function:: Ura.ouvewav parq


	:comentário: 
		
		"toca" no computador local o arquivo wav indicado
		
		
		
	:author:  epp 2006, max 2004
		
		
	:param parq:

.. function:: Ura.criaArqTxt arqtxt iarq


	:comentário: 
	:param arqtxt:
		
	:param iarq:

.. function:: Ura.playwav p_ret p_arquivo p_opcoes


	:comentário: 
	:param p_ret:
		
	:param p_arquivo:
		
	:param p_opcoes:

.. function:: Ura.limpateclas 


	:comentário: 

.. function:: Ura.msgdbg msg


	:comentário: 
	:param msg:

.. function:: Ura.pegaret pret


	:comentário: 
	:param pret:

.. function:: Ura.flash pret


	:comentário: 
	:param pret:

.. function:: Ura.transfere pnumero


	:comentário: 
	:param pnumero:

.. function:: Ura.playtxt string opt


	:comentário: testa se houve algum erro operacional
		if (B.i(iret) < 0) {
		¨msgdbg(B.s("retorno do transfere menor q zero!"));
		¨return false;
		}
		if (!hup(gcanal, B.s("5"))) {
		¨msgdbg(B.s("NAO FEZ O HUP NO TRANSFERE!"));
		¨return false;
		}
		// B.pausa(1);
		B.mensagem("Transferido para " + iarg + " OK!");
		return true;
		}
		// PLAYTXT
		//
		
		Utilizando o tts, converte uma frase de texto em voz e fala
		
		
		

	:param  string: texto a ser "falado"
		
		
		

	:param  opt: opcoes . 'P' = parar quando se teclar algo
		

.. function:: Ura.fazarqTxtConv p_arq p_hash


	:comentário: monta o diretorio base a partir de um hash gerado pela string a ser usada no
		tts
		
		
		
	:param p_arq:
		
	:param p_hash:

.. function:: Ura.convTexto p_str p_arqvoz


	:comentário: 
	:param p_str:
		
	:param p_arqvoz:

.. function:: Ura.testatts pstring


	:comentário: 
	:param pstring:

.. function:: Ura.testaconv arqconv arqconv0


	:comentário: 
	:param arqconv:
		
	:param arqconv0:

.. function:: Ura.playantes arq_voz_antes


	:comentário: 
	:param arq_voz_antes:

.. function:: Ura.mensagem msg


	:comentário: 
	:param msg:

.. function:: Ura.modo modoUra


	:comentário: 
	:param modoUra:

.. function:: Ura.recWav parq


	:comentário: 
	:param parq:

