
ClienteSftp
===========
.. function:: ClienteSftp.enviaArqSFTP ftpRemoteDirectory fileToTransmit


	:comentário: Método responsável por enviar um arquivo via sftp passando como parametros o
		local no servidor e o arquivo a ser transmitido
		
		
		

	:param  ftpRemoteDirectory: 
		
		

	:param  fileToTransmit: formato "\\versoes\\teste.txt"
		
		
	:rtype: boolean  as variaveis de conxao (ftpHost, ftpUserName, e ftpPassword devem ser
		configuradas no bdsuperclient.fig ou informadas antes de chamar o
		metodo
		
		
	:throws:  ErrBds
		

.. function:: ClienteSftp.pegaArqSFTP arqDirFinal ftpRemoteDirectory fileToReceive


	:comentário: Método responsável por buscar um arquivo no Servidor SFTP
		
		
		

	:param  arqDirFinal: 
		'exembplo '\\v\\pad\\etc'
		
		

	:param  ftpRemoteDirectory: 
		
		

	:param  fileToReceive: 
		
		
	:rtype: boolean  as variaveis de conxao (ftpHost, ftpUserName, e ftpPassword devem ser
		configuradas no bdsuperclient.fig ou informadas antes de chamar o
		metodo
		
		
	:throws:  ErrBds
		

.. function:: ClienteSftp.fechaConexoesSFT 


	:comentário: Método responsável por fechar as conexões
		

