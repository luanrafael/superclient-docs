
WebView1
========
.. function:: WebView1.inic 


	:comentário: 

.. function:: WebView1.abrir webView url


	:comentário: 
		
		Metodo responsavel por abrir/criar um novo handler webview, Ex: WebView1.abrir wb 'http://google.com'
		
		
		

	:param  webView: variavel de controle, armazena webview criado
		
		

	:param  url: url a ser acessada pelo webview
		
		
	:throws:  ErrBds
		
		

.. function:: WebView1.abrir webView url lx ly


	:comentário: 
		
		Metodo responsavel por abrir/criar um novo handler webview, Ex: WebView1.abrir wb 'http://google.com' 250 600
		
		
		

	:param  webView: 
		
		

	:param  url: 
		
		

	:param  lx: 
		
		

	:param  ly: 
		
		
	:throws:  ErrBds
		
		

.. function:: WebView1.abrir webView url lx ly px py


	:comentário: 
		
		Metodo responsavel por abrir/criar um novo handler webview, Ex: WebView1.abrir wb 'http://google.com' 250 600 1 30
		
		
		

	:param  webView: 
		
		

	:param  url: 
		
		

	:param  lx: 
		
		

	:param  ly: 
		
		

	:param  px: 
		
		

	:param  py: 
		
		
	:throws:  ErrBds
		

.. function:: WebView1.abrir webView url lx ly px py titulo


	:comentário: 
		
		Metodo responsavel por abrir/criar um novo handler webview, Ex: WebView1.abrir wb 'http://google.com' 250 600 1 30
		
		
		

	:param  webView: 
		
		

	:param  url: 
		
		

	:param  lx: 
		
		

	:param  ly: 
		
		

	:param  px: 
		
		

	:param  py: 
		
		

	:param  titulo: titulo da janela
		
		
	:throws:  ErrBds
		

.. function:: WebView1.abrir webview purl lx ly px py titulo style


	:comentário: 
	:param webview:
		
	:param purl:
		
	:param lx:
		
	:param ly:
		
	:param px:
		
	:param py:
		
	:param titulo:
		
	:param style:

.. function:: WebView1.atualizar webview


	:comentário: atualiza webWiew
		
		
		

	:param  webview: 
		

.. function:: WebView1.fechar webview


	:comentário: fecha um webview
		
		
		

	:param  webview: 
		

.. function:: WebView1.get webview url


	:comentário: executa um get em um url dentro do webview
		
		
		

	:param  webview: 
		
		

	:param  url: 
		

.. function:: WebView1.execJS webview argjs resp


	:comentário: Executa um javascript dentro do webview
		
		Ex: WebView1.execJS wb 'return 1+1' resp Ex: WebView1.execJS wb
		'document.querySelector("body").textContent' textContent
		
		
		

	:param  webview: 
		
		

	:param  argjs: 
		
		

	:param  resp: armazena retorno do script executado
		

.. function:: WebView1.register brw


	:comentário: 
		
		
		

	:param  brw: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: WebView1.register brw esperaRegistro


	:comentário: registra um handler e cria uma conexao entre o javascript e o bdjava
		
		
		

	:param  brw: 
		
		

	:param  esperaRegistro: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: WebView1.setTitle brw


	:comentário: seta o titulo da janela do webview com o nome do atributo title do DOM. (
		document.title
		
		
		

	:param  brw: 
		
		
	:throws:  ErrBds
		

.. function:: WebView1.setTitle brw title


	:comentário: seta o titulo da janela do webview
		
		
		

	:param  brw: 
		
		

	:param  title: 
		
		
	:throws:  ErrBds
		

.. function:: WebView1.setVar brw var value


	:comentário: seta o valor de uma variavel javascript dentro do webview
		
		
		

	:param  brw: 
		
		

	:param  var: 
		
		

	:param  value: 
		

