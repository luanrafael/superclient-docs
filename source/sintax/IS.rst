
IS
==
.. function:: IS.focaJan nomeJanela


	:comentário: Método responsável por dar foco em uma janela, recebe como parametro parte do
		nome da janela possibilidade de dar jan com nome exato desde que passe 1 como
		2 argumento
		
		
		

	:param  bwindow: 
		
		
	:throws:  ErrBds
		

.. function:: IS.focaJan handler ehHandler


	:comentário: 
	:param handler:
		
	:param ehHandler:

.. function:: IS.focaJan nomeJanela janOculta ehHandler


	:comentário: 
	:param nomeJanela:
		
	:param janOculta:
		
	:param ehHandler:

.. function:: IS.pegaTamanhoJan nomeJanela lx ly


	:comentário: 
	:param nomeJanela:
		
	:param lx:
		
	:param ly:

.. function:: IS.dimensiona nomeJanela lx ly


	:comentário: Dimensionador de janelas, necessário infomar nova largura e altura
		
		
		

	:param  nomeJanela: 
		
		

	:param  lx: 
		
		

	:param  ly: 
		
		
	:throws:  ErrBds
		

.. function:: IS.dimensiona nomeJanela lx ly janOculta


	:comentário: 
	:param nomeJanela:
		
	:param lx:
		
	:param ly:
		
	:param janOculta:

.. function:: IS.posiciona nomeJanela posx posy


	:comentário: Posicionador de janelas, necessário informar nova posição e altura
		
		
		

	:param  nomeJanela: 
		
		

	:param  posx: 
		
		

	:param  posy: 
		
		
	:throws:  ErrBds
		

.. function:: IS.posiciona nomeJanela posx posy janOculta


	:comentário: 
	:param nomeJanela:
		
	:param posx:
		
	:param posy:
		
	:param janOculta:

.. function:: IS.dimPos nomeJanela posx posy lx ly


	:comentário: Posicionador e dimensionador de janelas, necessário infomar nova posição
		(x,y) e dimensão (x,y)
		
		
		

	:param  nomeJanela: 
		
		

	:param  posx: 
		
		

	:param  posy: 
		
		

	:param  lx: 
		
		

	:param  ly: 
		
		
	:throws:  ErrBds
		

.. function:: IS.dimPos nomeJanela posx posy lx ly tipo janOculta


	:comentário: 
	:param nomeJanela:
		
	:param posx:
		
	:param posy:
		
	:param lx:
		
	:param ly:
		
	:param tipo:
		
	:param janOculta:

.. function:: IS.dimPos nomeJanela posx posy lx ly janOculta


	:comentário: 
	:param nomeJanela:
		
	:param posx:
		
	:param posy:
		
	:param lx:
		
	:param ly:
		
	:param janOculta:

.. function:: IS.arrastaMouse posx posy posxfim posyfim


	:comentário: Realiza a ação de clicar e selecionar com o arrasta do mouse
		
		
		

	:param  posx: 
		
		

	:param  posy: 
		
		

	:param  posxfim: 
		
		

	:param  posyfim: 
		
		
	:throws:  AWTException
		
		
	:throws:  ErrBds
		

.. function:: IS.arrastaMouse posx posy posxfim posyfim nomeJanela


	:comentário: 
	:param posx:
		
	:param posy:
		
	:param posxfim:
		
	:param posyfim:
		
	:param nomeJanela:

.. function:: IS.posicionaMouse posx posy nomeJanela


	:comentário: Posiciona o cursor do mouse dentro de uma janela
		
		
		

	:param  posx: 
		
		

	:param  posy: 
		
		

	:param  nomeJanela: 
		
		
	:throws:  ErrBds
		

.. function:: IS.posicionaMouse posx posy


	:comentário: Posiciona o cursor do mouse levando em conta a tela
		
		
		

	:param  posx: 
		
		

	:param  posy: 
		
		
	:throws:  ErrBds
		

.. function:: IS.click posx posy


	:comentário: 
	:param posx:
		
	:param posy:

.. function:: IS.click posx posy nomeJanela


	:comentário: 
	:param posx:
		
	:param posy:
		
	:param nomeJanela:

.. function:: IS.duploClick posx posy


	:comentário: 
	:param posx:
		
	:param posy:

.. function:: IS.duploClick posx posy nomeJanela


	:comentário: 
	:param posx:
		
	:param posy:
		
	:param nomeJanela:

.. function:: IS.dirClick posx posy


	:comentário: 
	:param posx:
		
	:param posy:

.. function:: IS.dirClick posx posy nomeJanela


	:comentário: 
	:param posx:
		
	:param posy:
		
	:param nomeJanela:

.. function:: IS.cursor posx posy


	:comentário: 
	:param posx:
		
	:param posy:

.. function:: IS.cursor posx posy nomeJanela


	:comentário: 
	:param posx:
		
	:param posy:
		
	:param nomeJanela:

.. function:: IS.cursor posx posy nomeJanela ehObj


	:comentário: 
	:param posx:
		
	:param posy:
		
	:param nomeJanela:
		
	:param ehObj:

.. function:: IS.maximizar handler


	:comentário: 
		
		maximiza uma janela
		
		
		

	:param  handler: handler ou nome da janela a ser maximizada
		
		

.. function:: IS.minimizar handler


	:comentário: 
		
		maximiza uma janela
		
		
		

	:param  handler: handler ou nome da janela a ser minimizada
		
		

.. function:: IS.restaurar handler


	:comentário: 
		
		restaura uma janela a seu tamanho
		
		
		

	:param  handler: handler ou nome da janela a ser restaurada
		
		

.. function:: IS.print img


	:comentário: 
	:param img:

.. function:: IS.print img monitor


	:comentário: 
	:param img:
		
	:param monitor:

.. function:: IS.setaResolucao px py


	:comentário: 
	:param px:
		
	:param py:

