
Io
==
.. function:: Io.sisgrava fpo linha


	:comentário: Esse mxtodo grava uma linha no arquivo especificado pelo programador
		colocando um delimitador '\n' referente ao fim da linha.
		
		
		

	:param  fpo: especifica o arquivo a ser manipulado.
		
		

	:param  linha: especifica a linha a ser gravada no arquivo fpo.
		
		
	:rtype: boolean  <b>true</b> caso o mxtodo tenha sucesso na gravaxxo do arquivo.
		
		
	:rtype: boolean  <b>false</b> caso o mxtodo tenha problemas na gravaxxo do arquivo.
		

.. function:: Io.oldsisgrava fpo linha


	:comentário: Esse mxtodo grava uma linha no arquivo especificado pelo programador
		colocando um delimitador '\n' referente ao fim da linha.
		
		
		

	:param  fpo: especifica o arquivo a ser manipulado.
		
		

	:param  linha: especifica a linha a ser gravada no arquivo fpo.
		
		
	:rtype: boolean  <b>true</b> caso o mxtodo tenha sucesso na gravaxxo do arquivo.
		
		
	:rtype: boolean  <b>false</b> caso o mxtodo tenha problemas na gravaxxo do arquivo.
		

.. function:: Io.blocgrava fpo sbloco


	:comentário: Esse mxtodo grava um bloco de dados do tipo String Java em um arquivo
		especificado pelo programador.
		
		
		

	:param  fpo: especifica o nome do arquivo a ser manipulado.
		
		

	:param  sbloco: determina o bloco a ser gravado no arquivo fpo.
		
		
	:rtype: boolean  <b>true</b> caso o mxtodo tenha sucesso na gravaxxo do arquivo.
		
		
	:rtype: boolean  <b>false</b> caso o mxtodo tenha problemas na gravaxxo do arquivo.
		

.. function:: Io.blocgrava fpo bloco


	:comentário: Esse mxtodo grava um bloco de dados do tipo String Java em um arquivo
		especificado pelo programador.
		
		
		

	:param  fpo: especifica o nome do arquivo a ser manipulado.
		
		

	:param  sbloco: determina o bloco a ser gravado no arquivo fpo.
		
		
	:rtype: boolean  <b>true</b> caso o mxtodo tenha sucesso na gravaxxo do arquivo.
		
		
	:rtype: boolean  <b>false</b> caso o mxtodo tenha problemas na gravaxxo do arquivo.
		

.. function:: Io.blocgrava fpo bloco tam


	:comentário: Esse mxtodo grava um bloco de dados do tipo String Java em um arquivo
		especificado pelo programador.
		
		
		

	:param  fpo: especifica o nome do arquivo a ser manipulado.
		
		

	:param  bloco: determina o bloco a ser gravado no arquivo fpo.
		
		

	:param  tam: especifica o tamanho do bloco a ser gravado.
		
		
	:rtype: boolean  <b>true</b> caso o mxtodo tenha sucesso na gravaxxo do arquivo.
		
		
	:rtype: boolean  <b>false</b> caso o mxtodo tenha problemas na gravaxxo do arquivo.
		

.. function:: Io.pegatask tsk


	:comentário: 
	:param tsk:

.. function:: Io.pegatask tsk host sporta


	:comentário: 
	:param tsk:
		
	:param host:
		
	:param sporta:

.. function:: Io.registra tsk host sporta


	:comentário: Esse mxtodo registra uma Thread so Bdjava como sendo um servidor UDP. Essa
		Thread passa a receber e responder mensagens UDP.
		
		
		

	:param  tsk: retorna a task referente ao servidor UDP.
		
		

	:param  host: especifica o host referente ? task.
		
		

	:param  sporta: especifica a porta refrente ao servidor.
		

.. function:: Io.sendc tsk msgsend msgresp stam stimeout smetodo


	:comentário: 
	:param tsk:
		
	:param msgsend:
		
	:param msgresp:
		
	:param stam:
		
	:param stimeout:
		
	:param smetodo:

.. function:: Io.receivec tskresp tsk bloco stam stimeout


	:comentário: Esse mxtodo cria um socket correspondente ao cliente que mandou a mensagem
		para que a resposta possa ser redirecionada.
		
		
		

	:param  tskresp: retorna um socket referente ao cliente.
		
		

	:param  tsk: especifica a task jx aberta pelo pega task.
		
		

	:param  bloco: especifica o bloco referente ? mensagem.
		
		

	:param  stam: especifica o tamanho m?ximo referente ao bloco.
		
		

	:param  stimeout: especifica o timeout em milisegundos.
		

.. function:: Io.replyc tskrsp msg stam


	:comentário: Esse mxtodo envia uma resposta referente ? uma requisixxo determinada pela
		task.
		
		
		

	:param  tskrsp: especifica a task de resposta.
		
		

	:param  msg: especifica a mensagem.
		
		

	:param  tam: especifica o tamanho referente ? mensagem.
		

.. function:: Io.flush fp


	:comentário: 
	:param fp:

.. function:: Io.sisJ args


	:comentário: 
	:param args:

.. function:: Io.sisJan args


	:comentário: 
	:param args:

.. function:: Io.sisJan args


	:comentário: 
	:param args:

.. function:: Io.sisJan jogafora1 args jogaFora2


	:comentário: 
	:param jogafora1:
		
	:param args:
		
	:param jogaFora2:

.. function:: Io.sisJan ret args


	:comentário: 
	:param ret:
		
	:param args:

.. function:: Io.paraSis 


	:comentário: 

.. function:: Io.abreCmd handler comando


	:comentário: Abre um processo colocando os dados em um handler os objetos criados sao
		colocados em vetor Objetc [] no campo handler.o: Apos este comandos, podemos
		entrar com dados no processo pelo stdin usando "escreveCmd" e tambem
		monitorar os stdout/stderr (saidas) atravÃ©s do comando monitoraCmd Outros
		comandos relacionados: limpaBufCmd - limpa o buffer ainda nao processado da
		saida do processo que estÃ¡ sendo executado fechaCmd - finaliza o comando
		
		A seguir o campo handler.o Ã© ocupado por um vetor de objetos que sao usados
		na interaÃ§Ã£o com o processo.
		
		
		Os objetos sÃ£o os seguintes: oo[0]=p; // identifica o processo oo[1]=rout;
		// Thread disparada para coletar os dados de saida de um comando; oo[2]=rerr;
		// thread disparada para pegar os dados de saida de erro de um comando;
		oo[3]=strout; // dados acumulados de saida de um comando; oo[4]=strerr; //
		dados acumulados da saida de erro de um comando; oo[5]=cmdIn; // output
		stream usado para inputar dados em um comando de sistema (executavel)
		oo[6]=strtrab; // string nao processada ainda pelo monitoraCmd (monitoraÃ§Ã£o
		dos stdout/stderr do processo que estÃ¡ sendo executado
		
		
		

	:param  handler: objeto onde sao colocados os dados do processo
		
		

	:param  comando: String do comando
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Io.monitoraCmd handler strsaida filtros nfiltro resp del timeout


	:comentário: 
	:param handler:
		
	:param strsaida:
		
	:param filtros:
		
	:param nfiltro:
		
	:param resp:
		
	:param del:
		
	:param timeout:

.. function:: Io.escreveCmd args


	:comentário: 
	:param args:

.. function:: Io.limpaBufCmd handler


	:comentário: 
	:param handler:

.. function:: Io.fechaCmd handler


	:comentário: 
	:param handler:

.. function:: Io.fechaCmd handler strsai resp


	:comentário: 
	:param handler:
		
	:param strsai:
		
	:param resp:

.. function:: Io.abreSessao comando


	:comentário: 
	:param comando:

.. function:: Io.pegaConteudo resp


	:comentário: 
	:param resp:

.. function:: Io.escreveConteudo conteudo


	:comentário: 
	:param conteudo:

.. function:: Io.fechaSessao 


	:comentário: 

.. function:: Io.tratafile pfile


	:comentário: 
	:param pfile:

.. function:: Io.cd xdir


	:comentário: 
	:param xdir:

.. function:: Io.cd 


	:comentário: 

.. function:: Io.tdir xdir


	:comentário: 
	:param xdir:

.. function:: Io.tdir xdir


	:comentário: static public boolean tdir(Bds xdir) throws ErrBds    verifica se file existe
	:param xdir:

.. function:: Io.tread file


	:comentário: static public boolean tread(Bds file) throws ErrBds    verifica se file existe
	:param file:

.. function:: Io.twrite file


	:comentário: static public boolean twrite(Bds file) throws ErrBds    verifica se file existe
	:param file:

.. function:: Io.tfile file


	:comentário: static public boolean tfile(String file) throws ErrBds    verifica se file existe
	:param file:

.. function:: Io.tfile file


	:comentário: static public boolean tfile(Bds file) throws ErrBds    verifica se file existe
	:param file:

.. function:: Io.tfile file tam


	:comentário: 
	:param file:
		
	:param tam:

.. function:: Io.dtfile file millis stringdt


	:comentário: 
	:param file:
		
	:param millis:
		
	:param stringdt:

.. function:: Io.saida sarq


	:comentário: 
	:param sarq:

.. function:: Io.saida sarq


	:comentário: 
	:param sarq:

.. function:: Io.saida sarq sargs


	:comentário: 
	:param sarq:
		
	:param sargs:

.. function:: Io.saida sarq sargs


	:comentário: 
	:param sarq:
		
	:param sargs:

.. function:: Io.saida sarq sargs


	:comentário: 
	:param sarq:
		
	:param sargs:

.. function:: Io.sisabre sarq sargs


	:comentário: Esse mxtodo abre um ponteiro gen?rico de arquivo.
		
		<pre>
		Dependendo do tipo que est? em sargs, vai abrir um objeto dos tipos
		RandomAccessFile (rw), DataInputStream (r) e DataOutputStream (w). Al?m
		disto, trata da gravaxxo de bytes (truncando as strings) ou ent?o de strings
		usando o c?digo UTF - este dado se encontra no .i do Bds. <b>abaixo estao os
		delimitadores</b> "r" ARQUIVO DE ENTRADA "w" ARQUIVO PARA SAIDA "a" APPEND
		"+" eh arquivo rand?mico
		
		EXEMPLOS : "r" arquivo de stream para ler - o mais comum "rw+" arquivo
		rand?mico
		
		<b>Deprecados:</b> "c" CODIFICACAO UNICODE "f" CODIFICACAO UTF8 "k" SOQUETE
		"i" PIPE "u" UDP "s" SAIDA PARA STRING "p" ? uma porta serial
		
		
		

	:param  sarq: especifica o arquivo.
		
		

	:param  sargs: especifica as op??es relacionadas acima.
		

.. function:: Io.sisabre arq args


	:comentário: Esse mxtodo abre um ponteiro gen?rico de arquivo.
		
		<pre>
		Dependendo do tipo que est? em sargs, vai abrir um objeto dos tipos
		RandomAccessFile (rw), DataInputStream (r) e DataOutputStream (w). Al?m
		disto, trata da gravaxxo de bytes (truncando as strings) ou ent?o de strings
		usando o c?digo UTF - este dado se encontra no .i do Bds. <b>abaixo estao os
		delimitadores</b> "r" ARQUIVO DE ENTRADA "w" ARQUIVO PARA SAIDA "a" APPEND
		"+" eh arquivo rand?mico
		
		EXEMPLOS : "r" arquivo de stream para ler - o mais comum "rw+" arquivo
		rand?mico
		
		<b>Deprecados:</b> "c" CODIFICACAO UNICODE "f" CODIFICACAO UTF8 "k" SOQUETE
		"i" PIPE "u" UDP "s" SAIDA PARA STRING "p" ? uma porta serial
		
		
		

	:param  arq: especifica o arquivo.
		
		

	:param  args: especifica as op??es relacionadas acima.
		

.. function:: Io.sisabre sfp sarq sargs


	:comentário: 
	:param sfp:
		
	:param sarq:
		
	:param sargs:

.. function:: Io.sisabre sfp arq args


	:comentário: static public boolean sisabre(Bds sfp, Bds arq, Bds args) throws ErrBds    abre um ponteiro generico de arquivo. dependendo do tipo que est? em sargs,  vai abri um objeto dos tipos RandomAccessFile (rw), DataInputStream (r) e  DataOutputStream (w). Alem disto tambem marca se se trata da gravacao de  bytes (truncando as strings) ou entao de strings usando o codigo UTF - este  dado se encontra no .i do Bds
	:param sfp:
		
	:param arq:
		
	:param args:

.. function:: Io.sisabre sfp arq sargs


	:comentário: static public boolean sisabre(Bds sfp, Bds arq, String sargs) throws ErrBds    abre um ponteiro generico de arquivo. dependendo do tipo que est? em sargs,  vai abri um objeto dos tipos RandomAccessFile (rw), DataInputStream (r) e  DataOutputStream (w). Alem disto tambem marca se se trata da gravacao de  bytes (truncando as strings) ou entao de strings usando o codigo UTF - este  dado se encontra no .i do Bds
	:param sfp:
		
	:param arq:
		
	:param sargs:

.. function:: Io.sisfecha fp


	:comentário: Esse comando fecha um arquivo.
		
		
		

	:param  fp: especifica o Bds que contxm a inst?ncia referente ao arquivo
		desejado.
		

.. function:: Io.sisfecha fp ignora


	:comentário: Esse comando fecha um arquivo.
		
		
		

	:param  fp: especifica o Bds que contxm a inst?ncia referente ao arquivo
		desejado.
		
		
		

	:param  ignora: = 1, nao da mensagem de erro se o arquivo ja estiver fechado ou
		naofor um fp
		

.. function:: Io.wfile str arq


	:comentário: static public boolean wfile(String str, String arq) throws ErrBds    coloca "STR" no arquivo "arq", apagando o conteudo anterior
	:param str:
		
	:param arq:

.. function:: Io.wfile str arq


	:comentário: static public boolean wfile(Bds str, Bds arq) throws ErrBds    coloca "STR" no arquivo "arq", apagando o conteudo anterior
	:param str:
		
	:param arq:

.. function:: Io.wfile str arq op


	:comentário: static public boolean wfile(String str, String arq, String op) throws ErrBds    escreve o conteudo de str em um arquivo se op=w, apaga o conteudo antigo e  faz o arquivo = str se op=a, apenda ao arquivo o conteudo de str
	:param str:
		
	:param arq:
		
	:param op:

.. function:: Io.wfile str arq op


	:comentário: static public boolean wfile(Bds str, Bds arq, String op) throws ErrBds    escreve o conteudo de str em um arquivo se op=w, apaga o conteudo antigo e  faz o arquivo = str se op=a, apenda ao arquivo o conteudo de str
	:param str:
		
	:param arq:
		
	:param op:

.. function:: Io.oldwfile str arq op


	:comentário: static public boolean oldwfile(Bds str, Bds arq, Bds op) throws ErrBds    escreve o conteudo de str em um arquivo se op=w, apaga o conteudo antigo e  faz o arquivo = str se op=a, apenda ao arquivo o conteudo de str
	:param str:
		
	:param arq:
		
	:param op:

.. function:: Io.wfile dados arquivo opcao_tipo


	:comentário: 
		
		Comando: wfile
		
		Objetivo do comando: pega o conteÃºdo de uma string e coloca em um arquivo.
		
		sintaxe :wfile str arquivo [tipo-op]
		
		onde:
		
		str - string que vai ser gravada no arquivo arquivo - nome do arquivo -
		aceita a notaÃ§Ã£o do unix e se comecar com "/", considera que o nome do
		arquivo comeca com o "driveeps" posicao inicial (opcional) - quando existente
		este argumento, indica a posicao inicial de onde deve ler os dados tipo-op -
		combinacao do tipo de codificacao com o tipo de operaÃ§Ã£o desejada (normal
		apagando o conteudo anterior do arquivo ou apendando). Este argumento Ã©
		opcional. A operaÃ§Ã£o default Ã© â€œwâ€� (escreve apagando o conetudo
		anterior do arquivo). Se especificada, pode ser â€œwâ€� ou â€œaâ€� para
		apendar
		
		retorna true se deu tudo certo.
		
		O atual wfile pode lidar com qualquer pagina de codificacao de caracteres. O
		argumento tipo_op pode ser uma das seguintes alternativas:
		
		tamanho nulo: significa que Ã© um wfile normal, irÃ¡ escrever no arquivo
		apagando o conteudo anterior - se for â€œwâ€� ou â€œaâ€� - mantem a pagina de
		codificacao default do micro (normalmente no brasil CP852) e se â€œwâ€� apaga
		o conteudo do arquivo e se â€œaâ€� apenda o conteudo de str no final do
		arquivo - Se não for â€œwâ€� ou â€œaâ€� indicarÃ¡ o codigo da pagina de
		codificacao (as paginas disponiveis em java estao em: http
	://docs.oracle.com/javase/7/docs/technotes/guides/intl/encoding.doc. html
		
		Se tipo_op for composto por dois argumentos separados por ":" - o primeiro
		argumento serÃ¡ o codigo da pagina e o segundo o tipo de opeaÃ§Ã£o (â€œwâ€�
		ou â€œaâ€�).
		
		CodificaÃ§Ãµes normais encontradas: Normalmente no PC: CP852 ou ISO8859_1
		(Latin1) Existe a codificacao universal unicode que necessita de 2 byta, mas
		tambem se usa para codificar o unicode a codificacao UTF8 criada para com
		codigos de tamanho variavel (de 1 a 4 bytes) (ver
		http://pt.wikipedia.org/wiki/UTF-8)
		
		
		Exemplos:
		
		wfile str arquivo : escreve a string str no arquivo, apagando o seu conteudo
		anterior wfile str arquivo â€˜aâ€™ - escreve a string str no FINAL do arquivo
		(apendendo o seu contudo), não apagando portanto o conteudo do arquivo wfile
		str arquivo â€˜UTF8â€™ - escreve em um arquivo usando a codificaÃ§Ã£o UTF8 ,
		e considera a operacao default â€œwâ€� que apaga o conteudo anterior do
		arquivo wfile str arquivo â€˜UTF8:aâ€™ - escreve em um arquivo usando a
		codificaÃ§Ã£o UTF8 , e considera a operaÃ§Ã£o â€œaâ€� que apenda no conteudo
		anterior do arquivo o conteudo de str.
		
		
		

	:param  dados: dados a serem escritos no arquivo
		
		

	:param  arquivo: arquivo onde serao escritos ou apendados os dados
		
		

	:param  opcao_tipo: vwr descricao acima - pode conter a codificacao da pagina e/ou o
		tipo de operação (w para apagar o conteudo anterior ou a para
		apendar)
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Io.tamfile arq stam


	:comentário: 
	:param arq:
		
	:param stam:

.. function:: Io.rfile arq


	:comentário: 
	:param arq:

.. function:: Io.rfile arq


	:comentário: 
	:param arq:

.. function:: Io.rfile str arq stam


	:comentário: static public boolean rfile(Bds str, Bds arq, Bds stam) throws ErrBds    Le um arquivo at? o limite indicado por stam o coloca em str. bytes
	:param str:
		
	:param arq:
		
	:param stam:

.. function:: Io.oldrfile str arq sinic stam


	:comentário: static public boolean oldrfile(Bds str, Bds arq, Bds sinic, Bds stam) throws ErrBds    Le um arquivo a partir de sinic inclusive at? o limite indicado por stam e  coloca os dados coloca em str. bytes falta completar
	:param str:
		
	:param arq:
		
	:param sinic:
		
	:param stam:

.. function:: Io.rfile resp arquivo posinic tipo_tam


	:comentário: 
		
		
		Comando: rfile
		
		Objetivo do comando: pega o conteÃºdo de um arquivo e coloca em str. Se qt
		bytes não Ã© especificado, pega um mÃ¡ximo de 100.000 bytes, se não pega o
		mÃ¡ximo que estiver especificados em qtbytes
		
		sintaxe rfile resp arquivo [posicao inicial] [tipo-tamanho]
		
		O atual rfile pode lidar com qualquer pagina de codificacao de caracteres. O
		argumento tipo_tamanho pode ser uma das seguintes alternativas:
		
		tamanho nulo: significa que Ã© um rfile normal, irÃ¡ carregar no maximo
		C.TAMMAXLE caracteres (setado como default em 100000 caracteres). - um numero
		diferente de zero: significa o tamanho maximo que irÃ¡ ler. Se for negativo,
		irÃ¡ ler a partir do final - se nao for numero: indicarÃ¡ o codigo da pagina
		de codificacao (as paginas disponiveis em java estao em: http
	://docs.oracle.com/javase/7/docs/technotes/guides/intl/encoding.doc. html -
		Se tipo_tamanho for composto por dois argumentos separados por ":" - o
		primeiro argumento serÃ¡ o codigo da pagina e o segundo o tamanho.
		CodificaÃ§Ãµes normais encontradas: Normalmente no PC: CP852 ou ISO8859_1
		(Latin1) Existe a codificacao universal unicode que necessita de 2 byta, mas
		tambem se usa para codificar o unicode a codificacao UTF8 criada para com
		codigos de tamanho variavel (de 1 a 4 bytes) (ver
		http://pt.wikipedia.org/wiki/UTF-8)
		
		Exemplos:
		
		rfile str arquivo : lÃª atÃ© 100000 caracteres de um arquivo, usando a
		codificaÃ§Ã£o default do computador. Se houver mais de 100000, le os
		primeiros 100000 mas retorna falso
		
		rfile str arquivo 10000 : le os primeiros 10000 bytes de um arquivo
		
		rfile str arquivo 40000 10000 : le os 10000 bytes de um arquivo apos os
		primeiros 40000
		
		rfile str arquivo 40000 -10000 : le os 10000 bytes de um arquivo apos os
		primeiros 40000, CONTANDO DO FINAL
		
		rfile str arquivo -10000 : le os ultimos 10000 bytes de um arquivo
		
		rfile str arquivo 'UTF8' : le os primeiros 100000 bytes do arquivo ,
		considerando a codificaÃ§Ã£o UTF8
		
		rfile str arquivo 'UTF8:5000' : le os primeiros 5000 bytes do arquivo ,
		considerando a codificaÃ§Ã£o UTF8
		
		rfile str arquivo 'UTF8:-5000' : le os ultimos 5000 bytes do arquivo ,
		considerando a codificaÃ§Ã£o UTF8
		
		rfile str arquivo 30000 'UTF8:5000' : le os 5000 bytes do arquivo apos os
		primeiros 30000 bytes, considerando a codificaÃ§Ã£o UTF8
		
		Parametros de chamada:
		
		
		

	:param  resp: string resposta
		
		

	:param  arquivo: nome do arquivo - aceita a notaÃ§Ã£o do unix e se comecar com
		"/", considera que o nome do arquivo comeca com o "driveeps"
		
		

	:param  posinic: quando existente este argumento, indica a posicao inicial de
		onde deve ler os dados
		
		

	:param  tipo_tam: tipo_tam - combinacao do tipo de codificacao com a quantidade
		desejada de bytes a serem lidos
		
		
	:rtype: boolean  - retorna true se deu tudo certo. retorna falso se erro ou se ao ser
		chamada sem especificar o tamanho, retorna menos que o tamanho do
		arquivo
		

.. function:: Io.rfile str arq


	:comentário: static public boolean rfile(Bds str, String arq) throws ErrBds    Le um arquivo inteiro e o coloca em str. COnsidera no m?ximo C.MAXTAMLE bytes
	:param str:
		
	:param arq:

.. function:: Io.rfile str arq


	:comentário: static public boolean rfile(Bds str, Bds arq) throws ErrBds    Le um arquivo inteiro e o coloca em str. COnsidera no m?ximo C.MAXTAMLE bytes
	:param str:
		
	:param arq:

.. function:: Io.pula fp spos


	:comentário: 
	:param fp:
		
	:param spos:

.. function:: Io.sisle fp str stammax


	:comentário: Esse mxtodo l? uma linha de um determinado arquivo.
		
		
		

	:param  fp: especifica o arquivo.
		
		

	:param  str: retorna uma string referente ? leitura do arquivo
		
		

	:param  stammax: especifica o tamanho m?ximo da string de resposta.
		

.. function:: Io.xmlLe fp str delReg stammax


	:comentário: Esse metodo le um registro XML de um "fp", a partir da indicacao de como ?
		referenciado o registro
		
		
		

	:param  fp: especifica o arquivo.
		
		

	:param  str: retorna uma string referente ? leitura do arquivo
		
		

	:param  delReg: contem o a identificacao do delimitador entre cada Registro do
		XML que se quer ler
		
		

	:param  stammax: especifica o tamanho m?ximo da string de resposta.
		

.. function:: Io.blocle fp bloco stam


	:comentário: comversao do bds dels para uma matriz de 256 caracteres
		ret = tam - tamfalta; // posstr od bytes que ja foram lidos
		int off = 0; // offset onde por a string
		int ii = -1; // posicao na leitura onde encontra o delimitador!
		// inicio da operacao de lecao por tipo de arquivo
		B.entrapausa();
		try {
		¨DataInputStream fps = (DataInputStream) oarq[1];
		¨int ret0 = fps.read(byteBloco, 0, tam);
		¨ret = ret0;
		¨} catch (IOException e) {
		¨¨B.emsg("<Bds-00015> Nao consegui ler no arquivo " + fp.s);
		¨¨B.saipausa();
		¨¨return 0;
		¨¨} catch (Exception e) {
		¨¨¨B.emsg("<Bds-00016> nao consegui processar o IO");
		¨¨}
		¨¨B.saipausa();
		¨¨if (ret > 0) {
		¨¨¨bloco.s = "Li bloco com " + ret + " bytes";
		¨¨¨bloco.i = ret;
		¨¨¨} else {
		¨¨¨¨bloco.s = "Nxo consegui ler nada";
		¨¨¨¨bloco.o = null;
		¨¨¨}
		¨¨¨return (ret);
		¨¨}
		¨¨// BLOCLE
		¨¨public static boolean blocle(Bds fp, Bds bloco, Bds stam) throws ErrBds
		¨¨public static boolean blocle(Bds fp, Bds bloco, Bds stam) throws ErrBds
	:param fp:
		
	:param bloco:
		
	:param stam:

.. function:: Io.blocle fp bloco stam tipo_cod


	:comentário: 
	:param fp:
		
	:param bloco:
		
	:param stam:
		
	:param tipo_cod:

.. function:: Io.addpath caminho


	:comentário: public static void addpath(Bds caminho) throws ErrBds    Acrescenta um caminho adicional noinicio do classpath
	:param caminho:

.. function:: Io.convhexa resp snum


	:comentário: 
	:param resp:
		
	:param snum:

.. function:: Io.sort arq


	:comentário: 
	:param arq:

.. function:: Io.limpalinha arq0 arg


	:comentário: 
	:param arq0:
		
	:param arg:

.. function:: Io.limpaarqs dir dias


	:comentário: Esse mxtodo localiza arquivos em um determinado diret?rio e seus
		subdiret?rios com data de modificaxxo anterior ao par?metro dias e salva um
		arquivo de log com todas as a??es efetuadas no diret?rio e:\j\log nomeado
		como logdel<data>.
		
		<pre>
		O par?metro num?rico DIAS pode ser passado como um n?mero positivo ou negativo, executando assim
		duas fun??es distintas.
		
		POSITIVO - deleta, permanentemente, todos os arquivos com data de modificaxxo
		anterior ao limite de dias especificado.
		
		Exemplos:
		
		Iot.limpaarqs 'c:/eric/teste' 0
		<b>deleta todos os arquivos incluindo o dia atual</b>
		
		Iot.limpaarqs 'c:/eric/teste' 1
		<b>deleta todos os arquivos exceto os arquivos modificados no dia atual.</b>
		
		
		
		NEGATIVOS - delta, permanetemente, todos os arquivos com data de modificaxxo
		posterior ao limite de dias especificado.
		
		Exemplos:
		
		Iot.limpaarqs 'c:/eric/teste' -1
		<b>deleta somente os arquivos modificados na data atual</b>
		
		Iot.limpaarqs 'c:/eric/teste' -2
		<b>deleta somente os arquivos modificados na data atual e no dia anterior</b>
		</pre>
		
		
		

	:param  dir: especifica diret?rio a ser pesquisado.
		
		

	:param  dias: localiza arquivos com data de modificaxxo ateriores a dias.
		
		
		
	:author:  eric camalionte
		

.. function:: Io.pesqarq diretorio filtro


	:comentário: 
	:param diretorio:
		
	:param filtro:

.. function:: Io.pesqarq pdiretorio filtro arqs


	:comentário: Comando pesqarq parecido com o do BDMAX diretorio: diretorio onde se quer
		pesquisar filtro: contem o filtro que o nome do arquivo tem que atender arqs
	: lista de arquivos separados por "\N" retorna falso se nao encontrar nenhum
		arquivo
		
		
		Esse mxtodo pesquisa arquivos contidos no diret?rio especificado.
		
		
		

	:param  diretorio: especifica o diretorio onde se quer pesquisar.
		
		

	:param  filtro: especifica uma string que o nome do arquivo tem que conter.
		Exemplo: ".abs". Nao 'e necessario colocar o asterisco Se o
		Asterisco for colocado, nao sera considerado
		
		

	:param  arqs: retorna lista de arquivos contidos no diret?rio obedecendo a
		restrixxo do filtro.
		

.. function:: Io.pesqarq diretorio pfiltro arqs reentr


	:comentário: 
	:param diretorio:
		
	:param pfiltro:
		
	:param arqs:
		
	:param reentr:

.. function:: Io.pesqarq diretorio filtro arqs reentr filtroger


	:comentário: 
	:param diretorio:
		
	:param filtro:
		
	:param arqs:
		
	:param reentr:
		
	:param filtroger:

.. function:: Io.pegadata dt arq


	:comentário: Pega a ultima datahora de modificacao do arquivo em padrao epsoft e coloca em
		dth
		
		
		

	:param  dth: ultima data de modificacao do arquivo (formato aammdd.hhmmss)
		
		

	:param  arq: arquivo
		
		
	:throws:  ErrBds
		

.. function:: Io.move orig dest


	:comentário: 
	:param orig:
		
	:param dest:

.. function:: Io.move porig pdest


	:comentário: 
	:param porig:
		
	:param pdest:

.. function:: Io.copy porig pdest


	:comentário: 
	:param porig:
		
	:param pdest:

.. function:: Io.copy porig pdest


	:comentário: 
	:param porig:
		
	:param pdest:

.. function:: Io.mcopy dirOrig dirDest arqs


	:comentário: 
	:param dirOrig:
		
	:param dirDest:
		
	:param arqs:

.. function:: Io.bkk porig radic dirs


	:comentário: 
	:param porig:
		
	:param radic:
		
	:param dirs:

.. function:: Io.cpn porig pdest dirs filtro


	:comentário: 
	:param porig:
		
	:param pdest:
		
	:param dirs:
		
	:param filtro:

.. function:: Io.mkdir pdiretorio


	:comentário: static public boolean mkdir(Bds pdiretorio) throws ErrBds    cria um diretorio
		

	:param  diretorio:             - diretorio a ser criado

.. function:: Io.mkdir diretorio


	:comentário: static public boolean mkdir(String diretorio) throws ErrBds    cria um diretorio
		

	:param  diretorio:             - diretorio a ser criado

.. function:: Io.start args


	:comentário: 
	:param args:

.. function:: Io.start arquivo


	:comentário: 
	:param arquivo:

.. function:: Io.startTeste pargStart


	:comentário: 
	:param pargStart:

.. function:: Io.start pargStart


	:comentário: 
	:param pargStart:

.. function:: Io.backStart argStart


	:comentário: 
	:param argStart:

.. function:: Io.startx argStart


	:comentário: 
	:param argStart:

.. function:: Io.del orig


	:comentário: 
	:param orig:

.. function:: Io.del orig


	:comentário: 
	:param orig:

.. function:: Io.del porig caiforasenex


	:comentário: 
	:param porig:
		
	:param caiforasenex:

.. function:: Io.locarq patharq paths arq ext def


	:comentário: 
	:param patharq:
		
	:param paths:
		
	:param arq:
		
	:param ext:
		
	:param def:

.. function:: Io.locarq patharq paths arq ext def


	:comentário: 
	:param patharq:
		
	:param paths:
		
	:param arq:
		
	:param ext:
		
	:param def:

.. function:: Io.locjarfile ppatharq


	:comentário: 
	:param ppatharq:

.. function:: Io.ljar nomejar


	:comentário: 
	:param nomejar:

.. function:: Io.tjar caminhojar


	:comentário: 
	:param caminhojar:

.. function:: Io.abreSock host porta blocante


	:comentário: 
		
		
		
	:param host:
		
	:param porta:
		
	:param blocante:

.. function:: Io.abreSock con host porta blocante


	:comentário: 
	:param con:
		
	:param host:
		
	:param porta:
		
	:param blocante:

.. function:: Io.abreSock con


	:comentário: 
	:param con:

.. function:: Io.sendJ con msg resp timeout


	:comentário: 
	:param con:
		
	:param msg:
		
	:param resp:
		
	:param timeout:

.. function:: Io.fechaSock con


	:comentário: 
	:param con:

.. function:: Io.filtraArq arqOrigem arqFiltrado filtros byteInic qtLinhas del tituloArq


	:comentário: 
	:param arqOrigem:
		
	:param arqFiltrado:
		
	:param filtros:
		
	:param byteInic:
		
	:param qtLinhas:
		
	:param del:
		
	:param tituloArq:

.. function:: Io.loopGetSync 


	:comentário: 

.. function:: Io.getSyncUnregister serverIP serverPort idEmpresa nuRamal


	:comentário: 
	:param serverIP:
		
	:param serverPort:
		
	:param idEmpresa:
		
	:param nuRamal:

.. function:: Io.pegaIPExt ip ips


	:comentário: 
		
		

	:param  addr: Recebera o ip e o nome da maquina
		
		

	:param  hostAddress: Variavel que recebera nome da maquina
		
		

	:param  ipAddress: Variavel que recebera endereco IP
		

.. function:: Io.pegaIP 


	:comentário: 
		
		

	:param  addr: Recebera o ip e o nome da maquina
		
		

	:param  hostAddress: Variavel que recebera nome da maquina
		
		

	:param  ipAddress: Variavel que recebera endereco IP
		

.. function:: Io.geraPDF caminhoArqFinal conteudo


	:comentário: mxtodo respons?vel por gerar um pdf
		
		
		

	:param  caminhoArqFinal: Caminho do diret?rio\arquivo onde deverx ser salvo o pdf
		
		

	:param  conteudo: Conteudo que deverx estar no pdf
		
		
	:throws:  ErrBds
		NOTA - deve ser mais geral, incluindo a especificacao da fonte,
		negrito,etc - refazer no futuro
		

.. function:: Io.faz str orig


	:comentário: quebra galho para compatibilizar o trabalho do Claudio
		
		
		

	:param  str: 
		
		

	:param  orig: 
		
		
	:throws:  ErrBds
		

.. function:: Io.zipaArquivos dirOrig dirFin arqFin


	:comentário: 
	:param dirOrig:
		
	:param dirFin:
		
	:param arqFin:

.. function:: Io.zipaArquivos dirOrig dirFin arqFin exceptions


	:comentário: 
	:param dirOrig:
		
	:param dirFin:
		
	:param arqFin:
		
	:param exceptions:

.. function:: Io.getWindowsUserName resp


	:comentário: 
	:param resp:

.. function:: Io.recUdp porta tamMax timeout


	:comentário: 
	:param porta:
		
	:param tamMax:
		
	:param timeout:

.. function:: Io.sendc ip porta envia resp tam timeout tipo


	:comentário: 
	:param ip:
		
	:param porta:
		
	:param envia:
		
	:param resp:
		
	:param tam:
		
	:param timeout:
		
	:param tipo:

.. function:: Io.existeMesmoScl usuario


	:comentário: 
	:param usuario:

.. function:: Io.killProcess nomeProcesso


	:comentário: M?todo que mata um processo do Windows, dado o nome completo
		
		
		

	:param  nomeProcesso: Ex: Monikey.exe
		
		
	:rtype: boolean
		

.. function:: Io.gravaProcessoUsrAtual usuario


	:comentário: 
	:param usuario:

.. function:: Io.mover source target


	:comentário: 
	:param source:
		
	:param target:

.. function:: Io.mover source target resp


	:comentário: 
	:param source:
		
	:param target:
		
	:param resp:

.. function:: Io.copiar source target


	:comentário: 
	:param source:
		
	:param target:

.. function:: Io.copiar resp source target


	:comentário: 
	:param resp:
		
	:param source:
		
	:param target:

.. function:: Io.copiar resp source target exclude


	:comentário: 
	:param resp:
		
	:param source:
		
	:param target:
		
	:param exclude:

.. function:: Io.remover arquivo


	:comentário: 
	:param arquivo:

.. function:: Io.remover resp dir


	:comentário: 
	:param resp:
		
	:param dir:

.. function:: Io.qtarquivos resp dir


	:comentário: 
	:param resp:
		
	:param dir:

.. function:: Io.listaArquivos resp dir formato


	:comentário: 
	:param resp:
		
	:param dir:
		
	:param formato:

.. function:: Io.descompactar argZipFile argExtractFolder


	:comentário: 
	:param argZipFile:
		
	:param argExtractFolder:

.. function:: Io.pdf2Txt pdf resp


	:comentário: 
		
		pdf2Img Metodo responsavel por converter um arquivo pdf em texto
		
		
		
	:author:  Luan Rafael
		
		
		

	:param  pdf:  arquivo pdf
		
		

	:param  resp:  variavel de resposta
		
		
		
	:rtype: boolean
		
		

.. function:: Io.pdf2Img pdf dirDest


	:comentário: 
		
		pdf2Img Metodo responsavel por converter um arquivo pdf em imagens
		
		
		
	:author:  Luan Rafael
		
		
		

	:param  pdf: arquivo pdf
		
		

	:param  dirDest: diretório de destino da imagens
		
		
		
	:rtype: boolean
		
		
		

.. function:: Io.pdf2Img pdf dirDest ext


	:comentário: 
		
		pdf2Img Metodo responsavel por converter um arquivo pdf em imagens
		
		
		
	:author:  Luan Rafael
		
		
		

	:param  pdf: arquivo pdf
		
		

	:param  dirDest: diretório de destino da imagens
		
		

	:param  ext: extensao da imagem (png, jpg, gif, jpeg, bmp)
		
		
		
	:rtype: boolean
		
		

.. function:: Io.pdf2Img pdf dirDest ext pgIni qtdPags


	:comentário: 
		
		pdf2Img Metodo responsavel por converter um arquivo pdf em imagens
		
		
		
	:author:  Luan Rafael
		
		
		

	:param  pdf: arquivo pdf
		
		

	:param  dirDest: diretório de destino da imagens
		
		

	:param  ext: extensao da imagem (png, jpg, gif, jpeg, bmp)
		
		
		
	:rtype: boolean
		
		

.. function:: Io.pdf2Img pdf dirDest ext pgIni qtdPags senha


	:comentário: 
		
		pdf2Img Metodo responsavel por converter um arquivo pdf em imagens
		
		
		
	:author:  Luan Rafael
		
		
		
		

	:param  pdf: arquivo pdf
		
		

	:param  dirDest: diretório de destino da imagens
		
		

	:param  ext: extensao da imagem (png, jpg, gif, jpeg, bmp)
		
		

	:param  pgIni: pagina de inicio de conversao
		
		

	:param  qtdPags: quantiade de paginas a serem convertidas
		
		

	:param  senha: senha do arquivo se existir
		
		
		
	:rtype: boolean
		
		

.. function:: Io.concatDir dir dest


	:comentário: concatena arquivos que estão presentes em um diretório
		
		
		

	:param  dir: diretório onde os arquivos se encontram
		
		

	:param  dest: arquivo de destino
		

.. function:: Io.concatDir dir dest filtro


	:comentário: concatena arquivos que estão presentes em um diretório
		
		
		

	:param  dir: diretório onde os arquivos se encontram
		
		

	:param  dest: arquivo de destino
		
		

	:param  filtro: filtro de arquivos a serem concatenados
		

.. function:: Io.concatDir dir dest filtro argsPerqArq


	:comentário: concatena arquivos que estão presentes em um diretório
		
		
		

	:param  dir: diretório onde os arquivos se encontram
		
		

	:param  dest: arquivo de destino
		
		

	:param  filtro: filtro de arquivos a serem concatenados
		
		

	:param  argsPerqArq: argumentos do pesqarq - "r" recursivo - "s" diretório completo
		

.. function:: Io.concatDir dir dest filtro argsPerqArq del max


	:comentário: concatena arquivos que estão presentes em um diretório
		
		
		

	:param  dir: diretório onde os arquivos se encontram
		
		

	:param  dest: arquivo de destino
		
		

	:param  filtro: filtro de arquivos a serem concatenados
		
		

	:param  argsPerqArq: argumentos do pesqarq - "r" recursivo - "s" diretório completo
		
		

	:param  del: delimitador de arquivos
		

.. function:: Io.concat arqs


	:comentário: concatena arquivos recebe como parâmetro uma lista de arquivos, concatenando
		seus conteúdos no último arquivo da lista se o primeiro item da lista não for
		um arquivo, será usado como delimitador entre os contaúdos no arquivo final
		
	:param arqs:

.. function:: Io.fastRead str file


	:comentário: Método responsável por ler um arquivo, diferentemente do rfile, o fastRead não tem limitacao com relação a quantidade de bytes que são lidos, por isso deve-se tomar cuidado com a quantidade de memória que este comando pode alocar
		USA UTF-8 COMO PADRAO
		
		

	:param  str:  variavel de resposta
		
		

	:param  file:  arquivo a ser lido
		
		
		
	:rtype: boolean
		
		
	:author:  Luan Rafael
		
		
	:throws:  ErrBds
		

.. function:: Io.fastRead str file encoding


	:comentário: Método responsável por ler um arquivo, diferentemente do rfile, o fastRead não tem limitacao com relação a quantidade de bytes que são lidos, por isso deve-se tomar cuidado com a quantidade de memória que este comando pode alocar
		
		
		

	:param  str:  variavel de resposta
		
		

	:param  file:  arquivo a ser lido
		
		

	:param  encoding:  encode do arquivo
		
		
	:rtype: boolean
		
		
	:author:  Luan Rafael
		

.. function:: Io.mkdirs diretorio


	:comentário: Método responsável por criar pastas recursivamente
		
		
		

	:param  diretorio: nova pasta
		
		
	:rtype: boolean
		
		
	:author:  Luan Rafael
		

.. function:: Io.download url location


	:comentário: Método responsável por fazer download de arquivo
		
		
		

	:param  url: url do arquivo a ser baixado
		
		

	:param  location: arquivo de destino
		
		
	:rtype: boolean
		
		
		
	:author:  Luan Rafael
		

.. function:: Io.xcmd argexec resp


	:comentário: 
	:param argexec:
		
	:param resp:

.. function:: Io.listarArquivos path respArquivos


	:comentário: Método responsável por listar os arquivos de uma determinada pasta
		
		
		

	:param  path: diretorio de pesquisa
		
		

	:param  respArquivos: variavel de resposta contendo lista de arquivos
		
		
	:rtype: boolean
		
		
	:author:  Luan Rafael
		

.. function:: Io.listarArquivos path respArquivos filtro


	:comentário: Método responsável por listar os arquivos de uma determinada pasta
		
		
		

	:param  path: diretorio de pesquisa
		
		

	:param  respArquivos: variavel de resposta contendo lista de arquivos
		
		

	:param  filtro: filtro de pesquisa se começar com "." (ponto) verificar a
		extensão do arquivo, se começar com "" (asterisco) verifica se o
		nome do arquivo contém o filtro indicado
		
		
	:rtype: boolean
		
		
	:author:  Luan Rafael
		

.. function:: Io.listarArquivos path respArquivos filtro op


	:comentário: Método responsável por listar os arquivos de uma determinada pasta
		
		
		

	:param  path: diretorio de pesquisa
		
		

	:param  respArquivos: variavel de resposta contendo lista de arquivos
		
		

	:param  filtro: filtro de pesquisa se começar com "." (ponto) verificar a
		extensão do arquivo, se começar com "" (asterisco) verifica se o
		nome do arquivo contém o filtro indicado
		
		

	:param  op: opções de busca r - modo recursivo, procura nos subdiretorios d
		pesquisa somente diretórios
		
		
	:rtype: boolean
		
		
	:author:  Luan Rafael
		

.. function:: Io.listarArquivos path respArquivos filtro op sort


	:comentário: Método responsável por listar os arquivos de uma determinada pasta
		
		
		

	:param  path: diretorio de pesquisa
		
		

	:param  respArquivos: variavel de resposta contendo lista de arquivos
		
		

	:param  filtro: filtro de pesquisa se começar com "." (ponto) verificar a
		extensão do arquivo, se começar com "" (asterisco) verifica se o
		nome do arquivo contém o filtro indicado
		
		

	:param  op: opções de busca r - modo recursivo, procura nos subdiretorios d
		pesquisa somente diretórios
		
		

	:param  sort: parâmetro de ordenação da lista de arquivos d - ordena pela data
		de modificação -d - ordena pela data de modificação modo
		decrescente n - ordena pelo nome do arquivo -n - ordena pelo nome
		do arquivo modo decrescente
		
		
	:rtype: boolean
		
		
	:author:  Luan Rafael
		

.. function:: Io.cortarImagem imagem dest px py lx ly


	:comentário: metodo que redireciona o output do java para um arquivo
		
		
		

	:param  imagem: imagem origem a ser cortada
		
		

	:param  dest: destino da nova imagem
		
		

	:param  px: posicao x inicial
		
		

	:param  py: posical y inicial
		
		

	:param  lx: largura
		
		

	:param  ly: altura
		
		
	:rtype: boolean
		
		
	:author:  Luan Rafael
		

.. function:: Io.converteAudioToFlac audio dest


	:comentário: metodo que converte um arquivo de audio para o format flac
		
		
		

	:param  audio: arquivo de entrada
		
		

	:param  dest: arquivo de saida
		
		
	:author:  Luan Rafael
		

.. function:: Io.setaOutput outputFile errFile


	:comentário: metodo que redireciona o output do java para um arquivo
		
		
		

	:param  outputFile: arquivo de saida comum
		
		

	:param  errFile: arquivo de saida erros
		
		
	:rtype: boolean
		
		
	:author:  Luan Rafael
		

.. function:: Io.copiaTrabImg img


	:comentário: metodo que copia uma imagem para a area de transferencia
		
		
		

	:param  img: caminho da imagem a ser tansferida para a area de trabalho
		
		
	:rtype: boolean
		
		
	:author:  Luan Rafael
		

.. function:: Io.img2Pdf imgs dest


	:comentário: metodo transformar uma imagem ou uma lista de imagens em um pdf
		
		
		

	:param  imgs:  lista de imagens separado por "," ( vírgula )
		@oaram dest - arquivo pdf de destino
		
		
		
	:rtype: boolean
		
		
	:author:  Luan Rafael
		

