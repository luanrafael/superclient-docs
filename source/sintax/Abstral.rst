
Abstral
=======
.. function:: Abstral.parse nomeAbs


	:comentário: 
	:param nomeAbs:

.. function:: Abstral.tratalinhaCmd linhaCmd


	:comentário: 1 2 3 4 5 6 7 8 9 10 11 12
		int tipoCmd = B.eparte(new Bds("faz,exib,mensagem,c,+,--,return,++,exa,,/,mais," +
		13 14 15 16 17 18 19 20 21
		"menos,mod,if,ifnot,while,whilenot,for," + C.SDEL7 + "," + C.SDEL8), cmd, ',');
		dadosCodigo[TIPOCMD][poscod] = tipoCmd; // marca em cada
		if (tipoCmd > 14 && tipoCmd < 19) {
		¨B.mensagem("TIPO=" + tipoCmd + " CMD=" + cmd);
		¨B.parte(linhaCmd, linhaCmd, C.DEL1, 2, 999);
		¨codigo[poscod] = new Bds(cmd);
		¨int identacao = 0;
		¨if (tipoCmd == 20) {
		¨¨identacao = dadosCodigo[IDENT][poscod - 1] + 1;
		¨¨abs.posAbre[identacao] = poscod; // marca a posicao atual para pegar de volta
		¨¨dadosCodigo[IDENT][poscod] = identacao;
		¨¨if (identacao > 39) {
		¨¨¨abs.log("Nao podemos aninhar mais de 39 ifs, whiles, etc", "B");
		¨¨}
		¨¨} else if (tipoCmd == 21) {
		¨¨¨identacao = dadosCodigo[IDENT][poscod - 1];
		¨¨¨abs.posFecha[identacao] = poscod;
		¨¨¨dadosCodigo[POSOUTRA][abs.posAbre[identacao]] = poscod;
		¨¨¨--identacao;
		¨¨¨dadosCodigo[IDENT][poscod] = identacao;
		¨¨¨if (identacao < 0) {
		¨¨¨¨abs.log("Erro no fechamento dos ifs, whiles, etc", "B");
		¨¨¨}
		¨¨¨} else
		¨¨¨dadosCodigo[IDENT][poscod] = dadosCodigo[IDENT][poscod - 1];
		¨¨¨checaIdent(abs, linhaCmd, dadosCodigo, codigo);
		¨¨}
		¨¨B.mensagem("TIPO=" + tipoCmd + " CMD=" + cmd + " ARGS=" + args);
		¨¨return;
		¨}
	:param linhaCmd:

