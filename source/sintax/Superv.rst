
Superv
======
.. function:: Superv.addResumo tipo dados


	:comentário: 
	:param tipo:
		
	:param dados:

.. function:: Superv.addResumo tipo dados


	:comentário: 
	:param tipo:
		
	:param dados:

.. function:: Superv.addInfo str info


	:comentário: Adiciona uma linha de informacao em uma pilha de informacoes com a seguinte
		regra: - o tipo da informacao eh o que está antes do igual e eh usado para
		localizar a linha correta, e dados, o que está depois do igual. No final eh
		colocada a hora em que esta informacao esta sendo colocada
		
		
		

	:param  str: pilha que vai receber cada informacao
		
		

	:param  informacao: composta do tipo (`a esquerda do igual, e ao dado propriamente
		dito do lado direito) assume-se tambem que existe uma data
		
		
	:throws:  ErrBds
		

.. function:: Superv.addInfo str tipo dados


	:comentário: Adiciona uma linha de informacao em uma pilha de informacoes com a seguinte
		regra: - o tipo da informacao eh o que está antes do igual e eh usado para
		localizar a linha correta, e dados, o que está depois do igual. No final eh
		colocada a hora em que esta informacao esta sendo colocada
		
		
		

	:param  str: pilha que vai receber cada informacao
		
		

	:param  tipo: tipo da informacao (string antes do igual)
		
		

	:param  dados: dados - dados atualizados da informacao
		
		
	:throws:  ErrBds
		

.. function:: Superv.pegaDados linha0 proc idProc threads handles mem tProc


	:comentário: 
	:param linha0:
		
	:param proc:
		
	:param idProc:
		
	:param threads:
		
	:param handles:
		
	:param mem:
		
	:param tProc:

.. function:: Superv.pegaDadosx linha0 proc idProc threads handles mem tProc


	:comentário: 
	:param linha0:
		
	:param proc:
		
	:param idProc:
		
	:param threads:
		
	:param handles:
		
	:param mem:
		
	:param tProc:

.. function:: Superv.pegaParms arq tipo parms


	:comentário: pega os parametros do arquivo de configuracao da supervisao
		
		
		

	:param  arq: nome do arquivo de configuracao
		
		

	:param  tipo: tipo de configuracao desejado (exs: "queries" "cpusSuperv",etc
		
		

	:param  parms: devolve a string com os parametros desejados
		

.. function:: Superv.pegaParms arq tipo parms


	:comentário: pega os parametros do arquivo de configuracao da supervisao
		
		
		

	:param  arq: nome do arquivo de configuracao
		
		

	:param  tipo: tipo de configuracao desejado (exs: "queries" "cpusSuperv",etc
		
		

	:param  parms: devolve a string com os parametros desejados
		

.. function:: Superv.find dir parqs arqDest pfiltro


	:comentário: 
	:param dir:
		
	:param parqs:
		
	:param arqDest:
		
	:param pfiltro:

.. function:: Superv.criaVersao arq


	:comentário: 
	:param arq:

.. function:: Superv.execCmdsDir diretorio


	:comentário: Percorre diretorio e faz o seguinte: 1 - verifica se surgiu algum arquivo com
		a extensao .cmd 2 - se surgiu, executa e vê se o arquivo <arquivoAchado>.resp
		existe. Se existe, ve o conteudo 3 - Se o conteudo contiver NOK, faz log
		informando que o comando foi naoOk, se o arquivo existir e nao contiver
		NOK, faz log informando que a execucao foi ok, e se houver nenhum arquivo,
		faz log informando execucao sem arquivo de resposta. 4 - Depois disto tudo,
		move, apendando a data de execucao, estes arquivos para o diretorio
		<executados> abaixo do diretorio executado
		
		
		

	:param  diretorio: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

