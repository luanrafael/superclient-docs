
UtiSuperv
=========
.. function:: UtiSuperv.filas filas


	:comentário: 
	:param filas:

.. function:: UtiSuperv.cargaTab con nometab listaTab arq


	:comentário: carrega uma tabela com os dados de um arquivo separados por tab listaTab tem
		que representar exatamente os campos que estao separados por tab
		
	:param con:
		
	:param nometab:
		
	:param listaTab:
		
	:param arq:

.. function:: UtiSuperv.limpaArqs dir filtro dias


	:comentário: Limpa os arquivos mais antigos que "dias"
		
		
		

	:param  dir: diretorio a ser pesquisado
		
		

	:param  filtro: filtro de seleção dos arquivos
		
		

	:param  dias: idade minima do arquivo para ser apagado
		
		
	:throws:  ErrBds
		

