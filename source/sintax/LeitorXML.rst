
LeitorXML
=========
.. function:: LeitorXML.bds_doc; file ret


	:comentário: 
	:param file:
		
	:param ret:

.. function:: LeitorXML.carregarXmlFromFile file ret


	:comentário: 
	:param file:
		
	:param ret:

.. function:: LeitorXML.carregarXml file ret


	:comentário: 
		
		Carrega um arquivo xml
		
		
		
	:rtype: boolean
		
		

	:param  file: 
		= Arquivo xml a ser carregado
		
		

	:param  ret: 
		= String do arquivo carregado
		
		

.. function:: LeitorXML.carregarJsonFromFile strJsonFile ret


	:comentário: 
	:param strJsonFile:
		
	:param ret:

.. function:: LeitorXML.carregarJson strJson ret


	:comentário: 
	:param strJson:
		
	:param ret:

.. function:: LeitorXML.getElement document element key


	:comentário: 
		
		Carrega um elemento do documento lido
		
		
		

	:param  element: 
		= Bds com que conterá o elemento
		
		

	:param  key: 
		= Chave para acessar o elemento
		
		

.. function:: LeitorXML.getValue element resp key


	:comentário: 
		
		Pega o valor de um elemento
		
		
		

	:param  element: 
		= elemento carregado
		
		

	:param  key: 
		= chave para o atributo
		
		

	:param  resp: 
		= String com o valor
		
		
	:throws:  ErrBds
		
		

.. function:: LeitorXML.getChild pai filho key


	:comentário: 
		
		Carrega elemento filho
		
		
		

	:param  pai: 
		
		

	:param  pai: 
		
		

	:param  key: 
		
		

.. function:: LeitorXML.getChildR pai filho key


	:comentário: 
		
		Carrega um elemento filho, mas de permite pegar um filho de forma recursiva
		Ex: pai = <a> <b> <c> </c> </b> </a>
		
		getChildR(pai,filho,"a.b.c")
		
		Retorna em filho o elemento <c></c>
		
		
	:param pai:
		
	:param filho:
		
	:param key:

.. function:: LeitorXML.getItemList list resp index


	:comentário: 
		
		Carrega um elemento que esta contido em um JSONArray.
		
		
		

	:param  list: 
		
		

	:param  resp: 
		
		

	:param  index: 
		
		

.. function:: LeitorXML.getChildList element lista key


	:comentário: 
		
		Retorna uma lista um array de elementos
		
		
	:param element:
		
	:param lista:
		
	:param key:

.. function:: LeitorXML.setDocument document key ret


	:comentário: Altera o documento principal. document = <a> <b> <c> </c> </b> </a>
		
		setDocument(b,resp) document = <b> <c> </c> </b>
		
		
		
	:param document:
		
	:param key:
		
	:param ret:

.. function:: LeitorXML.getI element resp


	:comentário: 
		
		Retorna em resp o valor do atributo i de um Bds.
		
		
		

	:param  element: 
		
		

	:param  resp: 
		
		

.. function:: LeitorXML.isJson strJson


	:comentário: 
	:param strJson:

