
IJmdk
=====
.. function:: IJmdk.saveJanelas 


	:comentário: Coloca na area de trabalho todas as janelas existentes no windows
		
		
		
	:throws:  ErrBds
		

.. function:: IJmdk.atJanVetadas atualiza


	:comentário: Esta função pode ser chamada sempre desejar se atualizar mapa de janelas
		vetadas Utilização pode chamar Esta função através do seguinte comando: IJmdk.atJanVetadas
		
		
		
	:throws:  ErrBds
		
	:param atualiza:

.. function:: IJmdk.mapHandlers 


	:comentário: 

.. function:: IJmdk.pegaHandler nomeJanela resp


	:comentário: 
	:param nomeJanela:
		
	:param resp:

.. function:: IJmdk.pegaJan jan handler filtro janVetadas


	:comentário: 
	:param jan:
		
	:param handler:
		
	:param filtro:
		
	:param janVetadas:

.. function:: IJmdk.existeHandler hdl


	:comentário: 
	:param hdl:

.. function:: IJmdk.ehHandler hdl


	:comentário: 
	:param hdl:

.. function:: IJmdk.trataPosicaoHandler nomeJanela ObjPos


	:comentário: 
	:param nomeJanela:
		
	:param ObjPos:

.. function:: IJmdk.insereTxt handler msg


	:comentário: É capaz de lançar uma string em aplicativos que permitam a inserção via
		SendMessage
		
		
		

	:param  handler: 
		
		

	:param  msg: 
		
		
	:throws:  ErrBds
		
		
	:throws:  UnsupportedEncodingException
		

.. function:: IJmdk.insereTxt handler msg CTRLV


	:comentário: 
	:param handler:
		
	:param msg:
		
	:param CTRLV:

.. function:: IJmdk.insereTxt nomeJanela msg ObjPos CTRLV


	:comentário: 
	:param nomeJanela:
		
	:param msg:
		
	:param ObjPos:
		
	:param CTRLV:

.. function:: IJmdk.selecionaTxt handler


	:comentário: 
	:param handler:

.. function:: IJmdk.selecionaTxt nomeJanela ObjPos


	:comentário: 
	:param nomeJanela:
		
	:param ObjPos:

.. function:: IJmdk.acessaMenu nomeJanela menuItemId


	:comentário: 
	:param nomeJanela:
		
	:param menuItemId:

.. function:: IJmdk.mapMenu nomeJanela resp ObjPos


	:comentário: 
	:param nomeJanela:
		
	:param resp:
		
	:param ObjPos:

.. function:: IJmdk.setSelectBox nomeJanela index


	:comentário: 
	:param nomeJanela:
		
	:param index:

.. function:: IJmdk.setSelectBox nomeJanela index ObjPos


	:comentário: 
	:param nomeJanela:
		
	:param index:
		
	:param ObjPos:

.. function:: IJmdk.getSelectBox nomeJanela resp


	:comentário: 
	:param nomeJanela:
		
	:param resp:

.. function:: IJmdk.getSelectBox nomeJanela resp ObjPos


	:comentário: 
	:param nomeJanela:
		
	:param resp:
		
	:param ObjPos:

.. function:: IJmdk.acessaMenuLevel nomeJanela menuLista ObjPos


	:comentário: 
	:param nomeJanela:
		
	:param menuLista:
		
	:param ObjPos:

.. function:: IJmdk.m nomeJanela ObjPos


	:comentário: 
	:param nomeJanela:
		
	:param ObjPos:

.. function:: IJmdk.fechaApp handler


	:comentário: 
	:param handler:

.. function:: IJmdk.fechaApp nomeJanela ObjPos


	:comentário: 
	:param nomeJanela:
		
	:param ObjPos:

.. function:: IJmdk.selecionaCombo handler index


	:comentário: 
	:param handler:
		
	:param index:

.. function:: IJmdk.selecionaCombo nomeJanela index ObjPos


	:comentário: 
	:param nomeJanela:
		
	:param index:
		
	:param ObjPos:

.. function:: IJmdk.arrastaMouse handler posx posy posxfim posyfim


	:comentário: Capaz de simular um click em um objeto sem estar em foco utilizando o
		sendmessage
		
		
		

	:param  handler: 
		
		
	:throws:  ErrBds
		

.. function:: IJmdk.click handler


	:comentário: 
	:param handler:

.. function:: IJmdk.click nomeJanela ObjPos


	:comentário: 
	:param nomeJanela:
		
	:param ObjPos:

.. function:: IJmdk.clickM handler


	:comentário: 
	:param handler:

.. function:: IJmdk.clickM nomeJanela ObjPos


	:comentário: 
	:param nomeJanela:
		
	:param ObjPos:

.. function:: IJmdk.clickM nomeJanela ObjPos posx posy


	:comentário: 
	:param nomeJanela:
		
	:param ObjPos:
		
	:param posx:
		
	:param posy:

.. function:: IJmdk.sendKeys teclas nomeJanela ObjPos


	:comentário: 
	:param teclas:
		
	:param nomeJanela:
		
	:param ObjPos:

.. function:: IJmdk.SendKey tecla nomeJanela


	:comentário: 
	:param tecla:
		
	:param nomeJanela:

.. function:: IJmdk.SendKey tecla nomeJanela sendChar


	:comentário: 
	:param tecla:
		
	:param nomeJanela:
		
	:param sendChar:

.. function:: IJmdk.SendKey tecla nomeJanela ObjPos sendChar


	:comentário: 
	:param tecla:
		
	:param nomeJanela:
		
	:param ObjPos:
		
	:param sendChar:

.. function:: IJmdk.click nomeJanela posx posy


	:comentário: 
	:param nomeJanela:
		
	:param posx:
		
	:param posy:

.. function:: IJmdk.click jan posx posy ehObjeto


	:comentário: 
	:param jan:
		
	:param posx:
		
	:param posy:
		
	:param ehObjeto:

.. function:: IJmdk.duploClick jan posx posy


	:comentário: 
	:param jan:
		
	:param posx:
		
	:param posy:

.. function:: IJmdk.duploClick jan posx posy ehObjeto


	:comentário: 
	:param jan:
		
	:param posx:
		
	:param posy:
		
	:param ehObjeto:

.. function:: IJmdk.pegaTxt handler resp


	:comentário: Capaz de coletar texto de objetos usando o sendmessage do IWindows
		
		
		

	:param  handler: 
		
		

	:param  msg: 
		
		
	:throws:  ErrBds
		

.. function:: IJmdk.pegaTxt handler resp CTRLC


	:comentário: 
	:param handler:
		
	:param resp:
		
	:param CTRLC:

.. function:: IJmdk.pegaTxt nomeJanela resp ObjPos CTRLC


	:comentário: 
	:param nomeJanela:
		
	:param resp:
		
	:param ObjPos:
		
	:param CTRLC:

.. function:: IJmdk.pegaTxt nomeJanela resp ObjPos CTRLC tamByte


	:comentário: 
	:param nomeJanela:
		
	:param resp:
		
	:param ObjPos:
		
	:param CTRLC:
		
	:param tamByte:

.. function:: IJmdk.escondeJan nomeJanela


	:comentário: Responsável por esconder um aplicativo, mantendo rodando no IWindows mas
		oculto
		
		
		

	:param  nomeJanela: 
		
		
	:rtype: boolean
		

.. function:: IJmdk.escondeJan nomeJanela handlers


	:comentário: Responsável por esconder um aplicativo, mantendo rodando no IWindows mas
		oculto
		
		
		

	:param  nomeJanela: 
		
		
	:rtype: boolean
		

.. function:: IJmdk.mostraJan nomeJanela


	:comentário: Responsável por mostrar um aplicativo pelo nome da janela
		
		
		

	:param  nomeJanela: 
		
		
	:rtype: boolean
		

.. function:: IJmdk.mostraJanFlash nomeJanela


	:comentário: acha a janela mais recente...
		
		
		

	:param  nomeJanela: 
		
		
	:rtype: boolean
		

.. function:: IJmdk.mostraJan nomeJanela handlers


	:comentário: Responsável por esconder um aplicativo, mantendo rodando no IWindows mas
		oculto
		
		
		

	:param  nomeJanela: 
		
		
	:rtype: boolean
		

.. function:: IJmdk.AchaHandlerJanOcultaPorNome nomePesq resp


	:comentário: Encontra handler de uma janela oculta passando o nome tem que ser exato
		
		
		

	:param  nomeJanela: 
		
		
	:rtype: boolean
		

.. function:: IJmdk.AchaHandlerJanOcultaPorNome nomePesq janelas resp


	:comentário: Encontra handler de uma janela oculta passando uma string de pesquisa e
		retorna todas as janelas que contem este nome, com os respectivos handlers
		
		
		

	:param  nomePesq: string para pesquisa. todos os nomes sem "^" sao inclusoes e com
		"^" sao exclusoes Exemplo "chrome,^mail" , devolve todos os
		handlers que contem chrome desde que nao contenham mail no titulo
		
		

	:param  pjanelas: lista das janelas <titulo>:<handler>
		
		

	:param  resp: lista dos handlers, separados por virgula
		
		
	:rtype: boolean
		

.. function:: IJmdk.estaJanMin nomeJanela


	:comentário: Verifica se uma janela está minimizada
		
		
		

	:param  nomeJanela: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: IJmdk.estaJanMin nomeJanela ehObj


	:comentário: 
	:param nomeJanela:
		
	:param ehObj:

.. function:: IJmdk.marcaEscala 


	:comentário: 

.. function:: IJmdk.esperaFocoJan nomeJanela


	:comentário: Aguarda um objeto ser criado
		
		
		

	:param  nomeJanela: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: IJmdk.esperaFocoJan nomeJanela milis


	:comentário: 
	:param nomeJanela:
		
	:param milis:

.. function:: IJmdk.esperaFimApp nomeJanela


	:comentário: 
	:param nomeJanela:

.. function:: IJmdk.esperaFimApp nomeJanela milis


	:comentário: 
	:param nomeJanela:
		
	:param milis:

.. function:: IJmdk.esperaObjeto nomeJanela ObjPos


	:comentário: 
	:param nomeJanela:
		
	:param ObjPos:

.. function:: IJmdk.esperaObjeto nomeJanela ObjPos milis


	:comentário: 
	:param nomeJanela:
		
	:param ObjPos:
		
	:param milis:

.. function:: IJmdk.monitorContent conteudo error milis


	:comentário: 
	:param conteudo:
		
	:param error:
		
	:param milis:

.. function:: IJmdk.capturaTela nomeJanela arquivo


	:comentário: tira print de uma janela
		
		
		

	:param  nomeJanela: Nome da Janela
		
		

	:param  arquivo: caminho da imagem a ser salva
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: IJmdk.capturaTela nomeJanela arquivo ObjPos


	:comentário: tira print de uma janela
		
		
		

	:param  nomeJanela: Nome da Janela
		
		

	:param  arquivo: caminho da imagem a ser salva
		
		

	:param  ObjPos: posição do objeto da tela a se tirar o print
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: IJmdk.capturaTela nomeJanela arquivo posx posy lwidth lheight ObjPos


	:comentário: 
	:param nomeJanela:
		
	:param arquivo:
		
	:param posx:
		
	:param posy:
		
	:param lwidth:
		
	:param lheight:
		
	:param ObjPos:

.. function:: IJmdk.habilitaSessao session


	:comentário: 
	:param session:

.. function:: IJmdk.habilitaSessao session resp


	:comentário: 
	:param session:
		
	:param resp:

.. function:: IJmdk.listarHandlers 


	:comentário: 

.. function:: IJmdk.listarHandlers resp


	:comentário: 
	:param resp:

.. function:: IJmdk.pegaDetalhesHandler hdl resp


	:comentário: pegaPosHandler retorna o detalhes de um handler independe de janela
		
	:param hdl:
		
	:param resp:

.. function:: IJmdk.pegaDetalhesHandler hdl pos classe janela


	:comentário: pegaPosHandler retorna o detalhes de um handler independe de janela pos -
		posicao do objeto classe - classe do objeto ( button, Edit, ComboBox, Etc...)
		janela - nome da janela do objeto
		
	:param hdl:
		
	:param pos:
		
	:param classe:
		
	:param janela:

.. function:: IJmdk.debugIJmdk args


	:comentário: 
	:param args:

