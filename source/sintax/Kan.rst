
Kan
===
.. function:: Kan.getUser email


	:comentário: consegue os dados de um usuario a partir do email seta os dados golobais do
		usuario e do email
		
		
		

	:param  email: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Kan.getUser email user org


	:comentário: 
		
		
		

	:param  email: email a ser informado
		
		

	:param  user: devolve o usuario
		
		

	:param  org: devolve o codigo da organizacao
		
		
	:rtype: boolean  true se encontrar o usuario
		
		
	:throws:  ErrBds
		

.. function:: Kan.impForms newIdForms idForms organization_id


	:comentário: importa um Forms e coloca na organizacao nova. concatena ao nome do
		formulario o id da organizacao antiga
		
		
		

	:param  newIdForms: 
		
		

	:param  idForms: 
		
		

	:param  organization_id: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Kan.impCkLists newIdCkLists id_CkLists organization_id


	:comentário: importa um CkLists e coloca na organizacao nova. concatena ao nome do
		formulario o id da organizacao antiga
		
		
		

	:param  newIdCkLists: 
		
		

	:param  idCkLists: 
		
		

	:param  organization_id: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Kan.impPFlows newIdPFlows idPFlows process_id


	:comentário: importa um PFlows e coloca na organizacao nova. concatena ao nome do
		formulario o id da organizacao antiga
		
		
		

	:param  newIdPFlows: 
		
		

	:param  idPFlows: 
		
		

	:param  organization_id: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Kan.impCkLField newIdCkLField id_CkLField ckList_id


	:comentário: importa um CkLField e coloca na organizacao nova. concatena ao nome do
		formulario o id da organizacao antiga
		
		
		

	:param  newIdCkLField: 
		
		

	:param  idCkLField: 
		
		

	:param  ckList_id: indentificacao do ckList "pai" deste campo
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		atencao: campos de formularios ou de check list devem apenas
		serem criados com o id do form novo que foi importado
		

.. function:: Kan.exportaProcesso id process_orig_id comentarios nomeBase


	:comentário: Inicializa a importação de um processo
		
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		
	:param id:
		
	:param process_orig_id:
		
	:param comentarios:
		
	:param nomeBase:

.. function:: Kan.exportaPFlows idProcesso


	:comentário: Gera uma string de um arquivo contendo objetos Json. monta mapa k.mapKan com
		todas as referencias
		
		
		

	:param  mapObjetos: 
		
		

	:param  str: 
		
		

	:param  process_id: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Kan.trataJson json


	:comentário: 
	:param json:

.. function:: Kan.geraDePara lista


	:comentário: 
	:param lista:

.. function:: Kan.geraForms resp idForms


	:comentário: gera a string de um forms e coloca na organizacao nova. concatena ao nome do
		formulario a string "_ACF"
		
		
		

	:param  resp: retorna a string a ser montada no documento de saida. Gera tambem
		os form_field correspondentes
		
		

	:param  idForms: id original do forms
		
		

	:param  arg: a ser usado nas trocas de de-para , exemplo no caso de form:
		"pky_b_nn1", onde "pky_" onde é uma string usada para localizar o
		"de-para" e "b" indica o tratamento de "form"
		
		
	:rtype: boolean  true se ok
		
		
	:throws:  ErrBds
		

.. function:: Kan.geraFField resp idField


	:comentário: gera a string que permite se associar o novo form_field ao form_field
		original indicado pela string de localizacao "arg"
		
		
		

	:param  resp: retorna a string a ser montada no documento de saida. Gera tambem
		os form_field correspondentes
		
		

	:param  idField: indica o nome do idField original
		
		

	:param  arg: a ser usado nas trocas de de-para , exemplo no caso de form:
		"pky_b_nn1", onde "pky_" onde é uma string usada para localizar o
		"de-para" e "b" indica o tratamento de "form"
		
		
	:rtype: boolean  true se ok
		
		
	:throws:  ErrBds
		

.. function:: Kan.geraCkList resp idCkLists


	:comentário: gera a string de um CkList e coloca na organizacao nova. concatena ao nome do
		formulario string "_ACF"
		
		
		

	:param  resp: retorna a string a ser montada no documento de saida. Gera tambem
		os form_field correspondentes
		
		

	:param  idForms: id original do forms
		
		

	:param  arg: a ser usado nas trocas de de-para , exemplo no caso de form:
		"pky_d_nn1", onde "pky_" onde é uma string usada para localizar o
		"de-para" e "b" indica o tratamento de "form"
		
		
	:rtype: boolean  true se ok
		
		
	:throws:  ErrBds
		

.. function:: Kan.geraGroup resp idGroups


	:comentário: gera a string de um Group e coloca na organizacao nova. concatena ao nome do
		Grupo string "_ACF"
		
		
		

	:param  resp: retorna a string a ser montada no documento de saida. Gera tambem
		os form_field correspondentes
		
		

	:param  idForms: id original do forms
		
		

	:param  arg: a ser usado nas trocas de de-para , exemplo no caso de form:
		"pky_d_nn1", onde "pky_" onde é uma string usada para localizar o
		"de-para" e "b" indica o tratamento de "form"
		
		
	:rtype: boolean  true se ok
		
		
	:throws:  ErrBds
		

.. function:: Kan.geraCol resp idWCols


	:comentário: gera a string de um Group e coloca na organizacao nova. concatena ao nome do
		Grupo string "_ACF"
		
		
		

	:param  resp: retorna a string a ser montada no documento de saida. Gera tambem
		os form_field correspondentes
		
		

	:param  idForms: id original do forms
		
		

	:param  arg: a ser usado nas trocas de de-para , exemplo no caso de form:
		"pky_d_nn1", onde "pky_" onde é uma string usada para localizar o
		"de-para" e "b" indica o tratamento de "form"
		
		
	:rtype: boolean  true se ok
		
		
	:throws:  ErrBds
		

.. function:: Kan.geraAutExec resp idAutExec


	:comentário: gera a string de um Group e coloca na organizacao nova. concatena ao nome do
		Grupo string "_ACF"
		
		
		

	:param  resp: retorna a string a ser montada no documento de saida. Gera tambem
		os form_field correspondentes
		
		

	:param  idForms: id original do forms
		
		

	:param  arg: a ser usado nas trocas de de-para , exemplo no caso de form:
		"pky_d_nn1", onde "pky_" onde é uma string usada para localizar o
		"de-para" e "b" indica o tratamento de "form"
		
		
	:rtype: boolean  true se ok
		
		
	:throws:  ErrBds
		

.. function:: Kan.geraAut resp idAut


	:comentário: gera a string de um Group e coloca na organizacao nova. concatena ao nome do
		Grupo string "_ACF"
		
		
		

	:param  resp: retorna a string a ser montada no documento de saida. Gera tambem
		os form_field correspondentes
		
		

	:param  idAut: id original do automatismo
		
		

	:param  pky: Numero do pky a ser montado no processo
		
		

	:param  arg: a ser usado nas trocas de de-para , exemplo no caso de form:
		"pky_d_nn1", onde "pky_" onde é uma string usada para localizar o
		"de-para" e "b" indica o tratamento de "form"
		
		
	:rtype: boolean  true se ok
		
		
	:throws:  ErrBds
		

.. function:: Kan.geraPRules resp idPRules


	:comentário: gera a string de um Rules e coloca na organizacao nova. concatena ao nome do
		Rules string "_ACF"
		
		
		

	:param  resp: retorna a string a ser montada no documento de saida. Gera tambem
		os form_field correspondentes
		
		

	:param  idForms: id original do forms
		
		

	:param  arg: a ser usado nas trocas de de-para , exemplo no caso de form:
		"pky_z_nn1", onde "pky_" onde é uma string usada para localizar o
		"de-para" e "b" indica o tratamento de "rules"
		
		
	:rtype: boolean  true se ok
		
		
	:throws:  ErrBds
		

.. function:: Kan.geraPPRules resp idPPRules


	:comentário: gera a string de um Rules e coloca na organizacao nova. concatena ao nome do
		Rules string "_ACF"
		
		
		

	:param  resp: retorna a string a ser montada no documento de saida. Gera tambem
		os form_field correspondentes
		
		

	:param  idForms: id original do forms
		
		

	:param  arg: a ser usado nas trocas de de-para , exemplo no caso de form:
		"pky_z_nn1", onde "pky_" onde é uma string usada para localizar o
		"de-para" e "b" indica o tratamento de "rules"
		
		
	:rtype: boolean  true se ok
		
		
	:throws:  ErrBds
		

.. function:: Kan.importaProc idExport user organization_id comentarios nomeBase


	:comentário: 
	:param idExport:
		
	:param user:
		
	:param organization_id:
		
	:param comentarios:
		
	:param nomeBase:

.. function:: Kan.SYNC 


	:comentário: public static Bds SYNC = new Bds();    Insere uma linha de insert no banco
		

	:param  id: 
		

	:param  plinha: 
		
	:rtype: boolean
		
	:throws:  ErrBds

.. function:: Kan.update id plinha


	:comentário: Insere uma linha de update no banco (usado no momento apenas para dar um
		update no processo
		
		
		

	:param  id: 
		
		

	:param  plinha: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Kan.select id plinha


	:comentário: faz o select indicado e cria uma entrada para o mapKan
		
		
		

	:param  id: 
		
		

	:param  plinha: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Kan.deParaImport linhas naoResolvidos


	:comentário: 
	:param linhas:
		
	:param naoResolvidos:

.. function:: Kan.deParaImport0 linha naoResolvidos


	:comentário: 
	:param linha:
		
	:param naoResolvidos:

.. function:: Kan.putKan chave dado


	:comentário: poe um put de um dado no mapKan
		
		
		

	:param  chave: 
		
		

	:param  dado: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Kan.getKan chave dado


	:comentário: recupera um dado no mapKan
		
		
		

	:param  chave: 
		
		

	:param  dado: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Kan.listKan 


	:comentário: lista os dados de um Kan
		
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Kan.wind table id


	:comentário: Acumula para um "rewind" posterior, inserts que se faz na importação de um
		processo. Considera o variavel k.processImport para se referenciar na
		montagem do wind
		
		
		

	:param  table: 
		
		

	:param  id: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Kan.rewind idRewind nomeBase


	:comentário: deleta todos os registros constantes de tabela rewind com id=idRewind
		
		
		

	:param  table: 
		
		

	:param  id: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Kan.atExport info pos tot


	:comentário: Atualiza a tabela de status de expor com o andamento do percentual de
		exportacao
		
		
		

	:param  info: informação basica do status
		
		

	:param  pos: posicao relativa em relacao ao total das coisas que tem que ser
		executadas
		
		

	:param  tot: Estimativa de um total que é atingido quando todos os itens são
		executados
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Kan.atImport info pos tot


	:comentário: Atualiza a tabela de status de importacao com o andamento do percentual de
		importacao
		
		
		

	:param  info: informação basica do status
		
		

	:param  pos: posicao relativa em relacao ao total das coisas que tem que ser
		executadas
		
		

	:param  tot: Estimativa de um total que é atingido quando todos os itens são
		executados
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Kan.getStatus 


	:comentário: 

.. function:: Kan.novoIdElemento elemento idElemento idCliDest idElemDest


	:comentário: registra um novo elemento de destino
		
	:param elemento:
		
	:param idElemento:
		
	:param idCliDest:
		
	:param idElemDest:

.. function:: Kan.erroKan arg


	:comentário: 
	:param arg:

.. function:: Kan.eerroKan arg


	:comentário: erro Kan enviando mensagem de erro
		
		
		

	:param  arg: 
		
		
	:throws:  ErrBds
		

.. function:: Kan.trazdecoded 


	:comentário: 

.. function:: Kan.copiaencoded 


	:comentário: 

.. function:: Kan.copiadecoded 


	:comentário: 

.. function:: Kan.decode 


	:comentário: 

.. function:: Kan.encode 


	:comentário: 

