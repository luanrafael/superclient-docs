
ServidorTcp
===========
.. function:: ServidorTcp.fecha 


	:comentário: 

.. function:: ServidorTcp.ativaBdjava porta


	:comentário: 
	:param porta:

.. function:: ServidorTcp.ativaBdjava porta ipsAutorizados abstrais


	:comentário: 
	:param porta:
		
	:param ipsAutorizados:
		
	:param abstrais:

.. function:: ServidorTcp.ativaBdjava ipsAutorizados abstrais


	:comentário: 
	:param ipsAutorizados:
		
	:param abstrais:

.. function:: ServidorTcp.setaParms porta ipsAutorizados abstrais


	:comentário: Seta os parametros (ips autorizados e abstrais) para o processamento de
		seguranca do servico de comandos remotos do servidorTcp
		
		
		

	:param  porta: 
		
		

	:param  ipsAutorizados: 
		
		

	:param  abstrais: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: ServidorTcp.fecha porta


	:comentário: Fecha o Servico Bdjava do Servidor TCP na porta
		
		
		

	:param  porta: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

