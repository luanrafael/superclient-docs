
Rx
==
.. function:: Rx.exec respRxCmd cmd sinc


	:comentário: Executa um comando procurando uma thread disponivel usando o sincronizador
		
		
		

	:param  respRxCmd: resposta do comando
		
		

	:param  cmd: comando
		
		

	:param  sinc: string do grupo de comandos de deve ser sicronizado (somente
		exec um de cada vez)
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Rx.exec resp cmd


	:comentário: Executa um comando procurando uma thread disponivel
		
		
		

	:param  respRxCmd: resposta do comando
		
		

	:param  cmd: comando
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Rx.execTh resp cmd sth


	:comentário: executa o comando em determinada thread
		
		
		

	:param  resp: string de resposta, se o tamanho de "respRxCmd" existir, o seu
		conteudo sera devolvido como resp, senao será devolvido como
		retorno o que tiver sido impresso na janela
		
		

	:param  cmd: string do comando
		
		

	:param  sth: thread
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

