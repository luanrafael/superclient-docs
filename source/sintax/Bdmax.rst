
Bdmax
=====
.. function:: Bdmax.libera args


	:comentário: public static boolean libera = false; public static boolean jaInicBdjava = false;  // main  public static void main(String[] args) throws IOException, NoSuchMethodException, java.lang.IllegalAccessException, java.lang.InstantiationException, java.lang.ClassNotFoundException    Main dos processos BDMAX - argumentos podem ser do seguinte tipo: procs=<qtde  de processos simultaneos> - indica a quantidade de threads (processos)  simultaneos podemos disparar com o bdmax-java - valor default - 200 processos  vars=<qtde maxima de variaveis bdmax em cada processo > Cada processo bdnmax  pode ter uma quantidade maxima de variaveis. O default neste caso eh de 2000  variaveis
	:param args:

.. function:: Bdmax.inicBdjava 


	:comentário: public static void inicBdjava() throws IOException, NoSuchMethodException, java.lang.IllegalAccessException, java.lang.InstantiationException, java.lang.ClassNotFoundException    Main dos processos BDMAX - argumentos podem ser do seguinte tipo: procs=<qtde  de processos simultaneos> - indica a quantidade de threads (processos)  simultaneos podemos disparar com o bdmax-java - valor default - 200 processos  vars=<qtde maxima de variaveis bdmax em cada processo > Cada processo bdnmax  pode ter uma quantidade maxima de variaveis. O default neste caso eh de 2000  variaveis

.. function:: Bdmax.inicBdjava th driveBdjava


	:comentário: public static void inicBdjava(Bds th, Bds driveBdjava) throws IOException, NoSuchMethodException, java.lang.IllegalAccessException, java.lang.InstantiationException, java.lang.ClassNotFoundException    Main dos processos BDMAX estipulando a thread e o drive onde esta o bdjava -  argumentos podem ser do seguinte tipo: procs=<qtde de processos simultaneos>  - indica a quantidade de threads (processos) simultaneos podemos disparar com  o bdmax-java - valor default - 200 processos vars=<qtde maxima de variaveis  bdmax em cada processo > Cada processo bdnmax pode ter uma quantidade maxima  de variaveis. O default neste caso eh de 2000 variaveis
	:param th:
		
	:param driveBdjava:

