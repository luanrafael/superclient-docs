
Clitelnet
=========
.. function:: Clitelnet.abre sock host porta


	:comentário: 
	:param sock:
		
	:param host:
		
	:param porta:

.. function:: Clitelnet.fecha sock


	:comentário: Esse método fecha a conexão com o Servidor especificado pela referência sock.
		
		
		

	:param  sock: especifica o nome da conexão a ser fechada. Ex: Clitcp.fecha sock
		Clitcp.fecha kombi Clitcp.fecha terra
		
		

.. function:: Clitelnet.env sock msg


	:comentário: Método responsável pelo envio de mensagem ou comando ao Servidor TelNet.
		
		
		

	:param  sock: especifica o contexto da comunicação referente ao respectivo
		Socket msg especifica o comando ou a mensagem a ser enviada.
		
		
	:author:  Eric
		

.. function:: Clitelnet.readTelnet sock resp stam mascara tout


	:comentário: Método responsável pela Leitura da mensagem enviada pelo Servidor conectado.
		
		
		

	:param  sock: especifica o contexto da comunicação referente ao respectivo
		Socket. resp armazena as informações enviadas pelo Servidor no
		formato Bds. stam especifica o tamanho a ser alocado para a
		mensagem recebida do Servidor mascara especifica o caracter de
		parada da mensagem enviada pelo Servidor. tout especifica o
		TimeOut de captura da informação.
		
		
	:author:  Eric
		

