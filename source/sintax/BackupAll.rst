
BackupAll
=========
.. function:: BackupAll.realizaBackups diretoriosAPesquisar diretorioBkpai


	:comentário: 
	:param diretoriosAPesquisar:
		
	:param diretorioBkpai:

.. function:: BackupAll.realizaBackup diretorioAPesquisar diretorioOndeSeraCopiado extensoes


	:comentário: 
	:param diretorioAPesquisar:
		
	:param diretorioOndeSeraCopiado:
		
	:param extensoes:

.. function:: BackupAll.arquivoFoiCopiado nome


	:comentário: 
		
		

	:param  nome: 
		
		
	:rtype: boolean
		

.. function:: BackupAll.gravaDadosArquivoProperties arquivo chave[] valor[]


	:comentário: 
		
		

	:param  arquivo: 
		
		

	:param  chave: 
		
		

	:param  valor: 
		
		
	:rtype: boolean
		

.. function:: BackupAll.arquivoExiste diretorio


	:comentário: 
		
		

	:param  diretorio: 
		
		
	:rtype: boolean
		

