
EdtTabT
=======
.. function:: EdtTabT.inic op


	:comentário: 
	:param op:

.. function:: EdtTabT.tela 


	:comentário: inicializa o EDTTAB chamando a tabela Assume
		
		
		
	:throws:  ErrBds
		

.. function:: EdtTabT.tela userScript


	:comentário: inicializa o EDTTAB chamando a tabela Assume
		
		
		
	:throws:  ErrBds
		
	:param userScript:

.. function:: EdtTabT.cscargaScript a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: EdtTabT.loadScript nomeScript


	:comentário: 
	:param nomeScript:

.. function:: EdtTabT.csnovoScript a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: EdtTabT.csedtScript a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: EdtTabT.cssalvaScript a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: EdtTabT.cslimpaDados a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: EdtTabT.csexReg a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: EdtTabT.csatDados a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: EdtTabT.csdelDados a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: EdtTabT.csedtDados a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: EdtTabT.csdspesqTab a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: EdtTabT.cspesqDados a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: EdtTabT.setaDados 


	:comentário: grava na tela os dados constantes de listaEdtDados
		
		
		
	:throws:  ErrBds
		

.. function:: EdtTabT.pegaScript nomeScript


	:comentário: 
	:param nomeScript:

.. function:: EdtTabT.alertLog str


	:comentário: 
	:param str:

.. function:: EdtTabT.criaListaXml lista0


	:comentário: 
	:param lista0:

