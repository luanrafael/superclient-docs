
Sel
===
.. function:: Sel.clona brw dest


	:comentário: 
	:param brw:
		
	:param dest:

.. function:: Sel.cria brw browser


	:comentário: Método responsável por criar um webdriver do selenium
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param  browser: navegador a ser utilizado 'chrome' 'firefox' - não implementado
		'ie' - não implementado
		
		
	:author:  Luan Rafael
		

.. function:: Sel.isReady brw


	:comentário: 
	:param brw:

.. function:: Sel.quit brw


	:comentário: Método responsável por finalizar uma instancia do webdriver do selenium
		diferente do Sel.close, esse metodo consegue finalizar o processo do
		webdriver
		
		
		

	:param  brw: handle do webdriver a ser criado
		
		
	:author:  Luan Rafael
		
		
	:rtype: boolean
		

.. function:: Sel.close brw


	:comentário: Método responsável por fechar a janela do browser webdriver
		
		
		
	:rtype: boolean  void
		
		

	:param  brw: handle do webdriver a ser criado
		
		
	:author:  Luan Rafael
		

.. function:: Sel.close brw window


	:comentário: Método responsável por fechar a janela ou uma aba do browser webdriver
		
		
		

	:param  brw: handle do webdriver a ser criado
		@parm window - nome ou indice da aba a ser fechada
		
		
	:author:  Luan Rafael
		
		
	:rtype: boolean
		

.. function:: Sel.kill browser


	:comentário: Método responsável por matar o processo webdriver do selenium Esse método
		destroi todos os processo abertos referentes ao webdriver
		
		
		
	:rtype: void  boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		
	:author:  Luan Rafael
		

.. function:: Sel.foca brw


	:comentário: Método responsável por focar um webdriver do selenium
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		
	:author:  Luan Rafael
		

.. function:: Sel.get brw url


	:comentário: Método responsável pela envio de GET's ao webdriver
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param  url: url a ser acessar pelo webdriver
		
		
	:author:  Luan Rafael
		

.. function:: Sel.initTestCase brw test


	:comentário: 
	:param brw:
		
	:param test:

.. function:: Sel.endTestCase brw


	:comentário: 
	:param brw:

.. function:: Sel.oculta brw


	:comentário: 
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		### NÃO IMPLEMENTADO ####
		
		

.. function:: Sel.getTitle brw title


	:comentário: Método responsável por pegar o titulo da pagina
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param  title: variavel de retorno
		
		
	:author:  Luan Rafael
		

.. function:: Sel.execJS brw cmd resp


	:comentário: Método responsável por executar um código JS no webdriver
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param  cmd: comando a ser executado
		
		

	:param  resp: variave de retorno a priori o retorno está sendo tratado como
		string, mas poderia ser tratado como um object qualquer ( em
		análise )
		
		
	:author:  Luan Rafael
		

.. function:: Sel.execJS brw cmd


	:comentário: 
	:param brw:
		
	:param cmd:

.. function:: Sel.forEach brw seletor function resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param function:
		
	:param resp:

.. function:: Sel.getScreenShot brw arq


	:comentário: Método responsável por tirar um print do webdriver em questão
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param  arq: local e nome do arquivo de saida ex: e:\d\img.jpg extensões:
		.jpg .png
		
		
	:author:  Luan Rafael
		

.. function:: Sel.getScreenShotToPdf brw arq


	:comentário: 
	:param brw:
		
	:param arq:

.. function:: Sel.scrollIntoView brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.click brw seletor


	:comentário: Método responsável por clicar em um elemento da página
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param  seletor: padrão do selenium
		
		
	:see:  Sel.getSelector
		
		
	:author:  Luan Rafael
		

.. function:: Sel.click brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.clickById brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.clickByCss brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.clickByName brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.clickByClass brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.clickByLink brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.clickByPartialLink brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.clickByTag brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.clickByXpath brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.clickAndWait brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.getValue brw seletor resp


	:comentário: Método responsável por retorno o valor do atributo "value" de um elemento
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param  seletor: padrão do selenium
		
		
	:see:  Sel.getSelector
		
		

	:param  resp: variavel de retorno
		
		
	:author:  Luan Rafael
		
		

.. function:: Sel.getValue brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getValueById brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getValueByCss brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getValueByName brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getValueByClass brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getValueByLink brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getValueByPartialLink brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getValueByTag brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getValueByXpath brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getText brw seletor resp


	:comentário: Método responsável por retorno o texto contido no elemento
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param  seletor: padrão do selenium
		
		
	:see:  Sel.getSelector
		
		

	:param  resp: variavel de retorno
		
		
	:author:  Luan Rafael
		
		
	:throws:  ErrBds
		
		

.. function:: Sel.getText brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getTextById brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getTextByCss brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getTextByName brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getTextByClass brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getTextByLink brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getTextByPartialLink brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getTextByTag brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getTextByXpath brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getElementWidth brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getElementWidth brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getElementWidthById brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getElementWidthByCss brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getElementWidthByName brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getElementWidthByClass brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getElementWidthByLink brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getElementWidthByPartialLink brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getElementWidthByTag brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getElementWidthByXpath brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getElementHeight brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getElementHeight brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getElementHeightById brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getElementHeightByCss brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getElementHeightByName brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getElementHeightByClass brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getElementHeightByLink brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getElementHeightByPartialLink brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getElementHeightByTag brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getElementHeightByXpath brw seletor resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param resp:

.. function:: Sel.getLocation brw resp


	:comentário: 
	:param brw:
		
	:param resp:

.. function:: Sel.waitForPageLoad brw tempo


	:comentário: Método responsável por aguardar a página carregar ou processar um requisição
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param : tempo: segundos
		
		
	:author:  Luan Rafael
		
		
	:throws:  ErrBds
		
		

.. function:: Sel.waitForPageLoad brw


	:comentário: 
	:param brw:

.. function:: Sel.waitForElementPresent brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.waitForLocation brw location


	:comentário: 
	:param brw:
		
	:param location:

.. function:: Sel.waitForLocation brw location tempo


	:comentário: 
	:param brw:
		
	:param location:
		
	:param tempo:

.. function:: Sel.getStatusCode brw resp


	:comentário: 
	:param brw:
		
	:param resp:

.. function:: Sel.isElementPresent brw seletor


	:comentário: Método responsável verificar se existe um determinado elemento na página
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param  seletor: padrão do selenium
		
		
	:see:  Sel.getSelector
		
		
	:author:  Luan Rafael
		
		

.. function:: Sel.isElementPresent brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementPresentById brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementPresentByCss brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementPresentByName brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementPresentByClass brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementPresentByLink brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementPresentByPartialLink brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementPresentByTag brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementPresentByXpath brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementEnable brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementVisible brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementVisible brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementVisibleById brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementVisibleByCss brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementVisibleByName brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementVisibleByClass brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementVisibleByLink brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementVisibleByPartialLink brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementVisibleByTag brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementVisibleByXpath brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementChecked brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementChecked brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementCheckedById brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementCheckedByCss brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementCheckedByName brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementCheckedByClass brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementCheckedByLink brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementCheckedByPartialLink brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementCheckedByTag brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.isElementCheckedByXpath brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.clear brw seletor


	:comentário: Método responsável por limpar o valor de um elemento
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param  seletor: padrão do selenium
		
		
	:see:  Sel.getSelector
		
		
	:author:  Luan Rafael
		
		
	:throws:  ErrBds
		
		

.. function:: Sel.clear brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.clearById brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.clearByCss brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.clearByName brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.clearByClass brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.clearByLink brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.clearByPartialLink brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.clearByTag brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.clearByXpath brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.focus brw seletor


	:comentário: Método responsável por focar um elemento
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param  seletor: padrão do selenium
		
		
	:see:  Sel.getSelector
		
		
	:author:  Luan Rafael
		
		
	:throws:  ErrBds
		
		

.. function:: Sel.focus brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.focusById brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.focusByCss brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.focusByName brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.focusByClass brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.focusByLink brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.focusByPartialLink brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.focusByTag brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.focusByXpath brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.doubleClick brw seletor


	:comentário: Método responsável por executar um duplo clique no elemento
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param  seletor: padrão do selenium
		
		
	:see:  Sel.getSelector
		
		
	:author:  Luan Rafael
		
		
	:throws:  ErrBds
		
		

.. function:: Sel.doubleClick brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.doubleClickById brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.doubleClickByCss brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.doubleClickByName brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.doubleClickByClass brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.doubleClickByLink brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.doubleClickByPartialLink brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.doubleClickByTag brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.doubleClickByXpath brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.rightClick brw seletor


	:comentário: Método responsável por executar um clique com o botão direito no elemento
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param  seletor: padrão do selenium
		
		
	:see:  Sel.getSelector
		
		
	:author:  Luan Rafael
		
		
	:throws:  ErrBds
		
		

.. function:: Sel.rightClick brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.rightClickById brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.rightClickByCss brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.rightClickByName brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.rightClickByClass brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.rightClickByLink brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.rightClickByPartialLink brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.rightClickByTag brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.rightClickByXpath brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.type brw seletor value


	:comentário: 
		
		Método responsável por escrever em um determinado elemento
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param  seletor: padrão do selenium
		
		
	:see:  Sel.getSelector
		
		

	:param  value: valor a ser escrito no elemento
		
		
	:author:  Luan Rafael
		
		
	:throws:  ErrBds
		
		

.. function:: Sel.typeById brw seletor value


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param value:

.. function:: Sel.typeByCss brw seletor value


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param value:

.. function:: Sel.typeByName brw seletor value


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param value:

.. function:: Sel.typeByClass brw seletor value


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param value:

.. function:: Sel.typeByLink brw seletor value


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param value:

.. function:: Sel.typeByPartialLink brw seletor value


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param value:

.. function:: Sel.typeByTag brw seletor value


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param value:

.. function:: Sel.typeByXpath brw seletor value


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param value:

.. function:: Sel.type args


	:comentário: 
		
		Método responsável por escrever em um determinado elemento
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param  seletor: padrão do selenium
		
		
	:see:  Sel.getSelector
		
		

	:param  value: array de valores a serem escritos no elemento
		
		
	:author:  Luan Rafael
		
		
	:throws:  ErrBds
		
		

.. function:: Sel.sendKeys args


	:comentário: 
		
		Método responsável por escrever em um determinado elemento
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param  seletor: padrão do selenium
		
		
	:see:  Sel.getSelector
		
		

	:param  value: array de valores a serem escritos no elemento
		
		
	:author:  Luan Rafael
		
		
	:throws:  ErrBds
		
		

.. function:: Sel.performCtrlS brw


	:comentário: 
	:param brw:

.. function:: Sel.select brw seletor selection


	:comentário: 
		
		Método responsável por selecionar um valor em um elemento do tipo SELECT
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param  seletor: padrão do selenium
		
		
	:see:  Sel.getSelector
		
		

	:param  selection: opção a ser selecionada opções de seleção: - index=1 - seleciona
		a opção de index 1 - value="ok" - seleciona a opção de valor "ok"
		text="texto 1" - seleciona opção que contém o texto "texto 1"
		
		
	:author:  Luan Rafael
		
		
	:throws:  ErrBds
		
		

.. function:: Sel.select brw seletor selection


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param selection:

.. function:: Sel.selectById brw seletor selection


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param selection:

.. function:: Sel.selectByCss brw seletor selection


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param selection:

.. function:: Sel.selectByName brw seletor selection


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param selection:

.. function:: Sel.selectByClass brw seletor selection


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param selection:

.. function:: Sel.selectByLink brw seletor selection


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param selection:

.. function:: Sel.selectByPartialLink brw seletor selection


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param selection:

.. function:: Sel.selectByTag brw seletor selection


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param selection:

.. function:: Sel.selectByXpath brw seletor selection


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param selection:

.. function:: Sel.setAttribute brw seletor attribute value


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param attribute:
		
	:param value:

.. function:: Sel.getAttribute brw seletor attribute resp


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param attribute:
		
	:param resp:

.. function:: Sel.setValue brw seletor value


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param value:

.. function:: Sel.elementScreenShot2 brw seletor file


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param file:

.. function:: Sel.elementScreenShot brw seletor file


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param file:

.. function:: Sel.elementScreenShot3 brw seletor file


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param file:

.. function:: Sel.elementScreenShot brw seletor file x y w h


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param file:
		
	:param x:
		
	:param y:
		
	:param w:
		
	:param h:

.. function:: Sel.captureEntirePageScreenshot brw file


	:comentário: 
	:param brw:
		
	:param file:

.. function:: Sel.captureEntirePageAsPdf brw file


	:comentário: 
	:param brw:
		
	:param file:

.. function:: Sel.selectAndWait brw seletor selection


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param selection:

.. function:: Sel.dragAndDrop brw seletor location


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param location:

.. function:: Sel.eval brw expressao resp


	:comentário: 
		
		Executa uma expressão JavaScript
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param  expressao: padrão do selenium
		
		

	:param  resp: variavel de retorno
		
		
	:author:  Luan Rafael
		
		

.. function:: Sel.montaExp resp args


	:comentário: 
		
		Monta uma expressão a ser executada pelo webdriver
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param  args: argumentos da expressao a ser montada
		
		
	:author:  Edgar
		
		

.. function:: Sel.acceptPopUp brw


	:comentário: 
	:param brw:

.. function:: Sel.dismissPopUp brw


	:comentário: 
	:param brw:

.. function:: Sel.isPopUpPresent brw


	:comentário: 
	:param brw:

.. function:: Sel.getPopUpText brw resp


	:comentário: 
	:param brw:
		
	:param resp:

.. function:: Sel.assertConfirmation brw arg


	:comentário: 
		
		Verifica o texto de um popup
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param  arg: argumentos da expressao a ser montada
		
		
	:author:  Luan Rafael
		
		

.. function:: Sel.assertTitle brw verifyTitle


	:comentário: 
	:param brw:
		
	:param verifyTitle:

.. function:: Sel.assertText brw todo


	:comentário: 
	:param brw:
		
	:param todo:

.. function:: Sel.assertText brw todo parte


	:comentário: 
	:param brw:
		
	:param todo:
		
	:param parte:

.. function:: Sel.assertTextPresent brw todo parte


	:comentário: 
	:param brw:
		
	:param todo:
		
	:param parte:

.. function:: Sel.storeText brw seletor var


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param var:

.. function:: Sel.storeLocation brw var


	:comentário: 
	:param brw:
		
	:param var:

.. function:: Sel.storeValue brw seletor var


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param var:

.. function:: Sel.storeEditable brw seletor var


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param var:

.. function:: Sel.storeElementPresent brw seletor var


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param var:

.. function:: Sel.storeVisible brw seletor var


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param var:

.. function:: Sel.storeElementWidth brw seletor var


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param var:

.. function:: Sel.storeElementHeigth brw seletor var


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param var:

.. function:: Sel.storeChecked brw seletor var


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param var:

.. function:: Sel.storeSelectedLabel brw seletor var


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param var:

.. function:: Sel.store brw value var


	:comentário: 
	:param brw:
		
	:param value:
		
	:param var:

.. function:: Sel.storeEval brw exp var


	:comentário: 
	:param brw:
		
	:param exp:
		
	:param var:

.. function:: Sel.echo brw value


	:comentário: 
	:param brw:
		
	:param value:

.. function:: Sel.verifyTrue brw expressao


	:comentário: 
		
		Verifica se uma expressão é verdadeira
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param  expressao: expressao a ser validada
		
		
	:author:  Luan Rafael
		
		

.. function:: Sel.verifyFalse brw expressao


	:comentário: 
		
		Verifica se uma expressão é falsa
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param  expressao: expressao a ser validada
		
		
	:author:  Luan Rafael
		
		

.. function:: Sel.verifyEquals brw arg1 arg2


	:comentário: 
		
		Verifica se duas expressões são equivalentes
		
		
		
	:rtype: boolean
		
		

	:param  brw: handle do webdriver a ser criado
		
		

	:param  arg1: 
		
		

	:param  arg2: 
		
		
	:author:  Luan Rafael
		
		

.. function:: Sel.verifyElementChecked brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.verifyElementNotChecked brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.verifyElementPresent brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.verifyElementNotPresent brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.verifyVisible brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.verifyNotVisible brw seletor


	:comentário: 
	:param brw:
		
	:param seletor:

.. function:: Sel.verifyLocation brw arg


	:comentário: 
	:param brw:
		
	:param arg:

.. function:: Sel.verifyElementWidth brw seletor arg


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param arg:

.. function:: Sel.verifyEval brw exp arg


	:comentário: 
	:param brw:
		
	:param exp:
		
	:param arg:

.. function:: Sel.verifySelectedLabel brw seletor arg


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param arg:

.. function:: Sel.verifyElementHeight brw seletor arg


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param arg:

.. function:: Sel.verifyValue brw seletor arg


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param arg:

.. function:: Sel.verifyText brw seletor arg


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param arg:

.. function:: Sel.verifyNotText brw seletor arg


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param arg:

.. function:: Sel.verifyConfirmation brw arg


	:comentário: 
	:param brw:
		
	:param arg:

.. function:: Sel.ativaAndamentoSel 


	:comentário: 

.. function:: Sel.desativaAndamentoSel 


	:comentário: 

.. function:: Sel.logSel msg


	:comentário: 
	:param msg:

.. function:: Sel.logSel level brw cmd


	:comentário: 
	:param level:
		
	:param brw:
		
	:param cmd:

.. function:: Sel.logSel level brw cmd target


	:comentário: 
	:param level:
		
	:param brw:
		
	:param cmd:
		
	:param target:

.. function:: Sel.logSel level brw cmd target


	:comentário: 
	:param level:
		
	:param brw:
		
	:param cmd:
		
	:param target:

.. function:: Sel.logSel level brw cmd target


	:comentário: 
	:param level:
		
	:param brw:
		
	:param cmd:
		
	:param target:

.. function:: Sel.logSel level brw cmd target value


	:comentário: 
	:param level:
		
	:param brw:
		
	:param cmd:
		
	:param target:
		
	:param value:

.. function:: Sel.logSel level brw cmd target value


	:comentário: 
	:param level:
		
	:param brw:
		
	:param cmd:
		
	:param target:
		
	:param value:

.. function:: Sel.logSel level brw cmd target value


	:comentário: 
	:param level:
		
	:param brw:
		
	:param cmd:
		
	:param target:
		
	:param value:

.. function:: Sel.criaSelAbs fileOrig fileDest


	:comentário: 
	:param fileOrig:
		
	:param fileDest:

.. function:: Sel.verifyExpression exp resp


	:comentário: 
	:param exp:
		
	:param resp:

.. function:: Sel.deleteAllCookies brw


	:comentário: 
	:param brw:

.. function:: Sel.selectFrame brw title


	:comentário: 
	:param brw:
		
	:param title:

.. function:: Sel.selectMainWindow brw


	:comentário: 
	:param brw:

.. function:: Sel.selectLastWindow brw


	:comentário: 
	:param brw:

.. function:: Sel.selectWindow brw title


	:comentário: 
	:param brw:
		
	:param title:

.. function:: Sel.pause brw tempo


	:comentário: 
	:param brw:
		
	:param tempo:

.. function:: Sel.getPosition brw seletor px py


	:comentário: 
	:param brw:
		
	:param seletor:
		
	:param px:
		
	:param py:

.. function:: Sel.gemLog brw file


	:comentário: 
	:param brw:
		
	:param file:

.. function:: Sel.criaPlayList file resp


	:comentário: 
	:param file:
		
	:param resp:

