
Utiivr
======
.. function:: Utiivr.abrefax pret pcanal


	:comentário: 
	:param pret:
		
	:param pcanal:

.. function:: Utiivr.iniccan pcnl


	:comentário: Testa se não houve algum erro operacional
		if (B.i(iret) < 0) {
		¨B.mensagem("Erro operacional...");
		¨return false;
		}
		.dbg 'Resp:[' irsp '] Ret=[' iret ']'
		B.parte(irsp, iargs, C.CHAR1, 5);
		B.parte(iargs, pret, C.CHAR3, 1);
		if (!B.ig(pret, pcanal)) {
		¨B.mensagem("Canal NOK");
		¨return false;
		¨} else
		¨return true;
		}
		// INICCAN
		//
		
		Este metodo inicia o canal indicado
		
		
		

	:param  pcnl: Indica qual canal que vai ser iniciado
		
		
		
	:author:  CAR
		@Since 040913
		
		

.. function:: Utiivr.discan pcnl pani pnum ptmp pret


	:comentário: 
	:param pcnl:
		
	:param pani:
		
	:param pnum:
		
	:param ptmp:
		
	:param pret:

.. function:: Utiivr.disca pret pnumero


	:comentário: 
	:param pret:
		
	:param pnumero:

.. function:: Utiivr.fechafax 


	:comentário: .dbg 'Resp:[' irsp '] Ret=[' iret ']'
		B.parte(irsp, iargs, C.CHAR1, 5);
		B.parte(iargs, pret, C.CHAR3, 1);
		testa se houve algum erro operacional
		if (B.i(iret) < 0) {
		¨B.mensagem("Erro operacional...");
		¨return false;
		}
		return true;
		}
		// FECHAFAX
		//
		static public boolean fechafax() throws ErrBds
		static public boolean fechafax() throws ErrBds

.. function:: Utiivr.atende pcnl ptout pani pdni pret


	:comentário: 
	:param pcnl:
		
	:param ptout:
		
	:param pani:
		
	:param pdni:
		
	:param pret:

.. function:: Utiivr.pegateclas pteclas ptam ptempo1 pdecay popcoes pteclasf


	:comentário: 
	:param pteclas:
		
	:param ptam:
		
	:param ptempo1:
		
	:param pdecay:
		
	:param popcoes:
		
	:param pteclasf:

.. function:: Utiivr.play parquivo


	:comentário: 
	:param parquivo:

.. function:: Utiivr.play parquivo popcoes


	:comentário: 
	:param parquivo:
		
	:param popcoes:

.. function:: Utiivr.play pret parquivo popcoes


	:comentário: 
	:param pret:
		
	:param parquivo:
		
	:param popcoes:

.. function:: Utiivr.hup pcnl ptout


	:comentário: 
	:param pcnl:
		
	:param ptout:

.. function:: Utiivr.rec pret parquivo popcoes ptimeout


	:comentário: testa se houve algum erro operacional
		if (B.i(iret) < 0) {
		¨B.mensagem("Erro operacional...");
		¨return false;
		}
		return true;
		}
		// REC
		//
		
		Este metodo grava um arquivo de voz
		
		
		

	:param  pcnl: Indica qual canal que vai gravar o arquivo parq O arquivo .wav a
		ser gravado ptout TimeOut do comando (o tempo maximo que a placa
		vai levar para gravar o arquivo)
		
		
		
	:author:  MAX
		@Since 040330
		

.. function:: Utiivr.beep ptom ptempo


	:comentário: testa se houve algum erro operacional
		if (B.i(iret) < 0) {
		¨B.mensagem("Erro operacional...");
		¨return false;
		}
		return true;
		}
		// BEEP
		//
		
		Este metodo digita tons da linha
		
		
		

	:param  pcnl: Indica qual canal que vai digitar os tons ptecs Tons a serem
		digitados (0123456789#) ptout TimeOut do comando (o tempo maximo
		que a placa vai levar para digitar os tons)
		
		
		
	:author:  MAX
		@Since 040330
		

.. function:: Utiivr.sendIVR pmsg pret ptmt prsp


	:comentário: testa se houve algum erro operacional
		if (B.i(iret) < 0) {
		¨B.mensagem("Erro operacional...");
		¨return false;
		}
		.dbg 'Resp:[' irsp '] Ret=[' iret ']'
		B.parte(irsp, iargs, C.CHAR1, 5);
		B.parte(iargs, irets, C.CHAR3, 1);
		return true;
		}
		// SENDIVR
		//
	:param pmsg:
		
	:param pret:
		
	:param ptmt:
		
	:param prsp:

.. function:: Utiivr.montapacote popr pcnl ptmt pcmd pargs ppac


	:comentário: 
	:param popr:
		
	:param pcnl:
		
	:param ptmt:
		
	:param pcmd:
		
	:param pargs:
		
	:param ppac:

.. function:: Utiivr.ouvewav parq


	:comentário: 
	:param parq:

.. function:: Utiivr.fazdemotxt arqtxt iarq


	:comentário: 
	:param arqtxt:
		
	:param iarq:

.. function:: Utiivr.playwav p_ret p_arquivo p_opcoes


	:comentário: 
	:param p_ret:
		
	:param p_arquivo:
		
	:param p_opcoes:

.. function:: Utiivr.limpateclas 


	:comentário: 

.. function:: Utiivr.msgdbg msg


	:comentário: 
	:param msg:

.. function:: Utiivr.pegaret pret


	:comentário: 
	:param pret:

.. function:: Utiivr.flash pret


	:comentário: 
	:param pret:

.. function:: Utiivr.transfere pnumero


	:comentário: 
	:param pnumero:

.. function:: Utiivr.playtxt string opt


	:comentário: testa se houve algum erro operacional
		if (B.i(iret) < 0) {
		¨msgdbg(B.s("retorno do transfere menor q zero!"));
		¨return false;
		}
		if (!hup(gcanal, B.s("5"))) {
		¨msgdbg(B.s("NAO FEZ O HUP NO TRANSFERE!"));
		¨return false;
		}
		// B.pausa(1);
		B.mensagem("Transferido para " + iarg + " OK!");
		return true;
		}
		// PLAYTXT
		//
	:param string:
		
	:param opt:

.. function:: Utiivr.fazarqTxtConv p_arq p_hash


	:comentário: 
	:param p_arq:
		
	:param p_hash:

.. function:: Utiivr.convTexto p_str p_arqvoz


	:comentário: 
	:param p_str:
		
	:param p_arqvoz:

.. function:: Utiivr.testatts pstring


	:comentário: 
	:param pstring:

.. function:: Utiivr.testaconv arqconv arqconv0


	:comentário: 
	:param arqconv:
		
	:param arqconv0:

.. function:: Utiivr.playantes arq_voz_antes


	:comentário: 
	:param arq_voz_antes:

