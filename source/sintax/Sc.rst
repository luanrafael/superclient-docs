
Sc
==
.. function:: Sc.aguarda idLock


	:comentário: 
	:param idLock:

.. function:: Sc.aguarda idLock beep


	:comentário: 
	:param idLock:
		
	:param beep:

.. function:: Sc.trava idLock


	:comentário: 
	:param idLock:

.. function:: Sc.inic 


	:comentário: 

.. function:: Sc.sc args janela


	:comentário: 
	:param args:
		
	:param janela:

.. function:: Sc.sc args janela resp


	:comentário: 
	:param args:
		
	:param janela:
		
	:param resp:

.. function:: Sc.sc str janela resp op teclado


	:comentário: 
	:param str:
		
	:param janela:
		
	:param resp:
		
	:param op:
		
	:param teclado:

.. function:: Sc.sc args janela resp op


	:comentário: 
	:param args:
		
	:param janela:
		
	:param resp:
		
	:param op:

.. function:: Sc.ehExe op


	:comentário: 
	:param op:

.. function:: Sc.convasc THbd THwindow resp teclado


	:comentário: 
	:param THbd:
		
	:param THwindow:
		
	:param resp:
		
	:param teclado:

.. function:: Sc.sComb args argSend


	:comentário: 
	:param args:
		
	:param argSend:

.. function:: Sc.cks serial


	:comentário: 
	:param serial:

.. function:: Sc.teclas []args


	:comentário: 
	:param []args:

.. function:: Sc.send args jan


	:comentário: 
	:param args:
		
	:param jan:

.. function:: Sc.teclasCtlv args janela


	:comentário: 
	:param args:
		
	:param janela:

.. function:: Sc.sendAcentos args janela


	:comentário: 
	:param args:
		
	:param janela:

.. function:: Sc.ctlvConfere arg janela


	:comentário: 
	:param arg:
		
	:param janela:

.. function:: Sc.jan janela atalho


	:comentário: 
	:param janela:
		
	:param atalho:

.. function:: Sc.jan janela 


	:comentário: 
	:param janela:
		
	:param :

.. function:: Sc.jan janela


	:comentário: 
	:param janela:

.. function:: Sc.jan janela


	:comentário: 
	:param janela:

.. function:: Sc.espera jan segs


	:comentário: 
	:param jan:
		
	:param segs:

.. function:: Sc.wait jan segs


	:comentário: 
	:param jan:
		
	:param segs:

.. function:: Sc.copiaCampo destino janela


	:comentário: 
	:param destino:
		
	:param janela:

.. function:: Sc.copiaCampo posX posY posX1 posY1 argSend destino nomeJanela


	:comentário: 
	:param posX:
		
	:param posY:
		
	:param posX1:
		
	:param posY1:
		
	:param argSend:
		
	:param destino:
		
	:param nomeJanela:

.. function:: Sc.copiaCampo posX posY argSend destino nomeJanela


	:comentário: 
	:param posX:
		
	:param posY:
		
	:param argSend:
		
	:param destino:
		
	:param nomeJanela:

.. function:: Sc.copiaCampo argSend destino janela


	:comentário: 
	:param argSend:
		
	:param destino:
		
	:param janela:

.. function:: Sc.poePass pass janela


	:comentário: 
	:param pass:
		
	:param janela:

.. function:: Sc.chamaMenu pilha niv


	:comentário: 
	:param pilha:
		
	:param niv:

.. function:: Sc.convasc THbd resp teclado


	:comentário: 
	:param THbd:
		
	:param resp:
		
	:param teclado:

.. function:: Sc.geraArvore pdirBase parqScl


	:comentário: 
	:param pdirBase:
		
	:param parqScl:

.. function:: Sc.geraComandos pcomandos parqsScl


	:comentário: 
	:param pcomandos:
		
	:param parqsScl:

.. function:: Sc.procDirScl pdirBase comandos


	:comentário: 
	:param pdirBase:
		
	:param comandos:

.. function:: Sc.exibeTit 


	:comentário: 

.. function:: Sc.titulo titulo


	:comentário: 
	:param titulo:

.. function:: Sc.clickMouse 


	:comentário: 

.. function:: Sc.rightClick 


	:comentário: 

.. function:: Sc.opcaoA 


	:comentário: 

.. function:: Sc.opcaoC 


	:comentário: 

.. function:: Sc.opcaoD 


	:comentário: 

.. function:: Sc.opcaoD0 


	:comentário: 

.. function:: Sc.opcaoE 


	:comentário: 

.. function:: Sc.opcaoH 


	:comentário: 

.. function:: Sc.opcaoJ 


	:comentário: 

.. function:: Sc.opcaoL 


	:comentário: 

.. function:: Sc.pegaArquivo 


	:comentário: 

.. function:: Sc.opcaoN 


	:comentário: 

.. function:: Sc.opcaoP 


	:comentário: 

.. function:: Sc.opcaoS 


	:comentário: 

.. function:: Sc.opcaoT 


	:comentário: 

.. function:: Sc.opcaoU 


	:comentário: 

.. function:: Sc.opcaoV 


	:comentário: 

.. function:: Sc.opcaoW 


	:comentário: 

.. function:: Sc.fechaCmd 


	:comentário: 

.. function:: Sc.criaTela nomeTela arqTela


	:comentário: 
	:param nomeTela:
		
	:param arqTela:

.. function:: Sc.montaJson json arqTela


	:comentário: 
	:param json:
		
	:param arqTela:

.. function:: Sc.montaJson0 json comandos


	:comentário: 
	:param json:
		
	:param comandos:

.. function:: Sc.getCursor tipoClick


	:comentário: 
	:param tipoClick:

.. function:: Sc.getCursor 


	:comentário: 

.. function:: Sc.getCursor x y


	:comentário: 
	:param x:
		
	:param y:

.. function:: Sc.click xx yy


	:comentário: 
	:param xx:
		
	:param yy:

.. function:: Sc.dirClick xx yy


	:comentário: 
	:param xx:
		
	:param yy:

.. function:: Sc.dClick xx yy


	:comentário: 
	:param xx:
		
	:param yy:

.. function:: Sc.rClick xx yy


	:comentário: 
	:param xx:
		
	:param yy:

.. function:: Sc.sendTab qt


	:comentário: 
	:param qt:

.. function:: Sc.sendEdt str


	:comentário: 
	:param str:

.. function:: Sc.sendEdt str


	:comentário: 
	:param str:

.. function:: Sc.sendV []args


	:comentário: 
	:param []args:

.. function:: Sc.sendV str janela


	:comentário: 
	:param str:
		
	:param janela:

.. function:: Sc.sendV str janela


	:comentário: 
	:param str:
		
	:param janela:

.. function:: Sc.setTxt cp tela


	:comentário: 
	:param cp:
		
	:param tela:

.. function:: Sc.iconifica icon


	:comentário: 
	:param icon:

.. function:: Sc.bloqueia bloq


	:comentário: 
	:param bloq:

.. function:: Sc.pegaDados dados args nomeJanela


	:comentário: 
	:param dados:
		
	:param args:
		
	:param nomeJanela:

.. function:: Sc.maximiza janela


	:comentário: 
	:param janela:

.. function:: Sc.minimiza janela


	:comentário: 
	:param janela:

.. function:: Sc.restaura janela


	:comentário: 
	:param janela:

.. function:: Sc.maximiza janela


	:comentário: 
	:param janela:

.. function:: Sc.minimiza janela


	:comentário: 
	:param janela:

.. function:: Sc.restaura janela


	:comentário: 
	:param janela:

.. function:: Sc.arrumaJson json


	:comentário: 
	:param json:

.. function:: Sc.arrumaJson json ident


	:comentário: 
	:param json:
		
	:param ident:

.. function:: Sc.configura 


	:comentário: 

