
DOM
===
.. function:: DOM.load htmlFile resp


	:comentário: 
	:param htmlFile:
		
	:param resp:

.. function:: DOM.load htmlFile resp encode


	:comentário: 
	:param htmlFile:
		
	:param resp:
		
	:param encode:

.. function:: DOM.loadHtml html resp


	:comentário: 
	:param html:
		
	:param resp:

.. function:: DOM.connect url doc


	:comentário: 
	:param url:
		
	:param doc:

.. function:: DOM.getUrl urlSource urlResp


	:comentário: 
	:param urlSource:
		
	:param urlResp:

.. function:: DOM.forEach doc seletor function resp


	:comentário: 
	:param doc:
		
	:param seletor:
		
	:param function:
		
	:param resp:

.. function:: DOM.getText doc seletor resp


	:comentário: 
	:param doc:
		
	:param seletor:
		
	:param resp:

.. function:: DOM.getHtml doc seletor resp


	:comentário: 
	:param doc:
		
	:param seletor:
		
	:param resp:

.. function:: DOM.getValue doc seletor resp


	:comentário: 
	:param doc:
		
	:param seletor:
		
	:param resp:

.. function:: DOM.getAttribute doc seletor attribute resp


	:comentário: 
	:param doc:
		
	:param seletor:
		
	:param attribute:
		
	:param resp:

