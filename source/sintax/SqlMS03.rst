
SqlMS03
=======
.. function:: SqlMS03.conecta con


	:comentário: 
	:param con:

.. function:: SqlMS03.conecta con bd user pass base


	:comentário: 
	:param con:
		
	:param bd:
		
	:param user:
		
	:param pass:
		
	:param base:

.. function:: SqlMS03.conecta con bd user pass base tembk


	:comentário: 
	:param con:
		
	:param bd:
		
	:param user:
		
	:param pass:
		
	:param base:
		
	:param tembk:

.. function:: SqlMS03.strcriatab lista0 nometab strtab


	:comentário: 
	:param lista0:
		
	:param nometab:
		
	:param strtab:

.. function:: SqlMS03.strcriatab lista0 nometab strtab tipo


	:comentário: 
	:param lista0:
		
	:param nometab:
		
	:param strtab:
		
	:param tipo:

.. function:: SqlMS03.strinstab lista0 nometab strtab


	:comentário: 
	:param lista0:
		
	:param nometab:
		
	:param strtab:

.. function:: SqlMS03.xsql con resp arg


	:comentário: static public boolean xsql(Bds con, Bds resp, Bds arg) throws ErrBds    executa o comando arg em cima do banco de dados bd o argumento do comando sql  esta em arg, retorna verdadeiro se o comando foi bem sucedido e se no caso de  update, delete, etc conseguir agir em pelo menos uma linha no caso de insert  update e delete, resp retorna a qt de linhas inseridas, atualizadas ou  deletadas   O commit eh automatico
	:param con:
		
	:param resp:
		
	:param arg:

.. function:: SqlMS03.xsql con resp arg commit


	:comentário: static public boolean xsql(Bds con, Bds resp, Bds arg, Bds commit) throws ErrBds    executa o comando arg em cima do banco de dados bd o argumento do comando sql  esta em arg, retorna verdadeiro se o comando foi bem sucedido e se no caso de  update, delete, etc conseguir agir em pelo menos uma linha no caso de insert  update e delete, resp retorna a qt de linhas inseridas, atualizadas ou  deletadas   commit="S" - da um commit automatico
	:param con:
		
	:param resp:
		
	:param arg:
		
	:param commit:

.. function:: SqlMS03.query con cursor arg


	:comentário: static public boolean query(Bds con, Bds cursor, Bds arg) throws ErrBds    executa o comando arg em cima do banco de dados bd o argumento do comando sql  esta em arg, retorna verdadeiro se o comando foi bem sucedido e se no caso de  update, delete, etc conseguir agir em pelo menos uma linha no caso de insert  update e delete, resp retorna a qt de linhas inseridas, atualizadas ou  deletadas   commit="S" - da um commit automatico
	:param con:
		
	:param cursor:
		
	:param arg:

.. function:: SqlMS03.query con arg


	:comentário: 
	:param con:
		
	:param arg:

.. function:: SqlMS03.update con resp tab lista select


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:

.. function:: SqlMS03.update con resp tab lista select tudo


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:
		
	:param tudo:

.. function:: SqlMS03.update con resp tab lista select tudo


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:
		
	:param tudo:

.. function:: SqlMS03.insert con resp tab lista


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:

.. function:: SqlMS03.delete con resp tab select


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param select:

.. function:: SqlMS03.select con resp tab lista select sop


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:
		
	:param sop:

.. function:: SqlMS03.select con resp tab lista select sop


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:
		
	:param sop:

.. function:: SqlMS03.fecha con


	:comentário: 
	:param con:

.. function:: SqlMS03.t sqt


	:comentário: 
	:param sqt:

.. function:: SqlMS03.Opersql con resp tab lista select oper


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:
		
	:param oper:

.. function:: SqlMS03.Opersql0 con resp tab campo select oper


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param campo:
		
	:param select:
		
	:param oper:

.. function:: SqlMS03.count con resp tab campo select


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param campo:
		
	:param select:

.. function:: SqlMS03.sum con resp tab lista select


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:

.. function:: SqlMS03.avg con resp tab lista select


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:

.. function:: SqlMS03.max con resp tab lista select


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:

.. function:: SqlMS03.min con resp tab lista select


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:

.. function:: SqlMS03.criaerro arg


	:comentário: 
	:param arg:

.. function:: SqlMS03.criaerro arg


	:comentário: 
	:param arg:

.. function:: SqlMS03.ledad chave dado


	:comentário: 
	:param chave:
		
	:param dado:

.. function:: SqlMS03.xx 


	:comentário: 
		

.. function:: SqlMS03.gravadad chave dado


	:comentário: 
	:param chave:
		
	:param dado:

.. function:: SqlMS03.trataerrosql con resp arg


	:comentário: 
	:param con:
		
	:param resp:
		
	:param arg:

.. function:: SqlMS03.trataerrosql con resp arg


	:comentário: 
	:param con:
		
	:param resp:
		
	:param arg:

.. function:: SqlMS03.acertalista lista


	:comentário: 
	:param lista:

.. function:: SqlMS03.serv 


	:comentário: 

.. function:: SqlMS03.rxsql blocrec resp


	:comentário: 
	:param blocrec:
		
	:param resp:

.. function:: SqlMS03.rselect con resp arg


	:comentário: 
	:param con:
		
	:param resp:
		
	:param arg:

.. function:: SqlMS03.pegaupd codupd


	:comentário: 
	:param codupd:

.. function:: SqlMS03.sendbk con cmd args


	:comentário: 
	:param con:
		
	:param cmd:
		
	:param args:

.. function:: SqlMS03.gravabk con


	:comentário: 
	:param con:

.. function:: SqlMS03.gravalog 


	:comentário: 

.. function:: SqlMS03.locultlog arqlog fp linha seq


	:comentário: 
	:param arqlog:
		
	:param fp:
		
	:param linha:
		
	:param seq:

