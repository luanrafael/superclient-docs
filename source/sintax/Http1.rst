
Http1
=====
.. function:: Http1.addProp props chave valor


	:comentário: 
	:param props:
		
	:param chave:
		
	:param valor:

.. function:: Http1.addProp props chave valor isList


	:comentário: 
	:param props:
		
	:param chave:
		
	:param valor:
		
	:param isList:

.. function:: Http1.limpa map


	:comentário: 
	:param map:

.. function:: Http1.auth name password respAuth


	:comentário: 
	:param name:
		
	:param password:
		
	:param respAuth:

.. function:: Http1.get url


	:comentário: 
	:param url:

.. function:: Http1.get url resp


	:comentário: 
	:param url:
		
	:param resp:

.. function:: Http1.get url params resp


	:comentário: 
	:param url:
		
	:param params:
		
	:param resp:

.. function:: Http1.get url params headers resp


	:comentário: 
	:param url:
		
	:param params:
		
	:param headers:
		
	:param resp:

.. function:: Http1.get url params headers auth resp


	:comentário: 
	:param url:
		
	:param params:
		
	:param headers:
		
	:param auth:
		
	:param resp:

.. function:: Http1.post url


	:comentário: 
	:param url:

.. function:: Http1.post url resp


	:comentário: 
	:param url:
		
	:param resp:

.. function:: Http1.post url params resp


	:comentário: 
	:param url:
		
	:param params:
		
	:param resp:

.. function:: Http1.post url params headers resp


	:comentário: 
	:param url:
		
	:param params:
		
	:param headers:
		
	:param resp:

.. function:: Http1.post url params headers auth resp


	:comentário: 
	:param url:
		
	:param params:
		
	:param headers:
		
	:param auth:
		
	:param resp:

.. function:: Http1.addJson form campo valor


	:comentário: 
	:param form:
		
	:param campo:
		
	:param valor:

.. function:: Http1.addJson form campo valor ehLista


	:comentário: 
	:param form:
		
	:param campo:
		
	:param valor:
		
	:param ehLista:

.. function:: Http1.postJson url data resp


	:comentário: 
	:param url:
		
	:param data:
		
	:param resp:

.. function:: Http1.postJson url data headers resp


	:comentário: 
	:param url:
		
	:param data:
		
	:param headers:
		
	:param resp:

.. function:: Http1.arqToBase64 arq resp


	:comentário: 
	:param arq:
		
	:param resp:

.. function:: Http1.base64ToArq base64 dest


	:comentário: 
	:param base64:
		
	:param dest:

