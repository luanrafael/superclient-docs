
BdjHook
=======
.. function:: BdjHook.soTeclas 


	:comentário: 

.. function:: BdjHook.InicializaWindowsHook 


	:comentário: Método inicializador do Hook uma vez que a variavel $$wListaAtalhos tenha
		sido configurada no bdjava
		
		
		
	:throws:  ErrBds
		

.. function:: BdjHook.FinalizaWindowsHook 


	:comentário: Método responsável por finalizar o hook do Windows
		

.. function:: BdjHook.InicializaMap 


	:comentário: Método que inicialzia as letras e seus respectivos hexa que são interpretados
		pelo o Windows
		

.. function:: BdjHook.desativaPrintScreen 


	:comentário: comando para desativar o acionamento da tecla PrintScreen
		
		se o bloqueio de print estiver ativo, pode copiar uma imagem default para a area de trabalho ou copiar a string "PRINT NAO AUTORIZADO"
		para definir uma imagem default sete o caminho da mesma na variavel $imgBlockPrtSc
		
		

.. function:: BdjHook.ativaPrintScreen 


	:comentário: comando para ativar o acionamento da tecla PrintScreen
		

