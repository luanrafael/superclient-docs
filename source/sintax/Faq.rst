
Faq
===
.. function:: Faq.start bdsdir


	:comentário: 
	:param bdsdir:

.. function:: Faq.estaSalvo 


	:comentário: Esse método irá verificar se o documento corrente está salvo.
		
		
		
	:rtype: boolean  Esse método retorna true se o documento estiver salvo.
		

.. function:: Faq.imprimir 


	:comentário: 

.. function:: Faq.setRamFaq 


	:comentário: Esse método atualiza o documento corrente referente à imagem do arquivo na
		memória.
		

.. function:: Faq.setHdFaq 


	:comentário: Esse método atualiza a imagem referente ao disco rígido.
		

.. function:: Faq.atimagem 


	:comentário: Esse método atualiza a imagem do documento na memória gerando uma string a
		partir do conjunto de variáveis titulo,resumo,palschave,textofaq Atualiza a
		imagem do hd. Atualiza a imagem da memória.
		

.. function:: Faq.abreTextPad 


	:comentário: Esse método abre o documento corrente do Gerenciador de FAQs no aplicativo
		Text Pad.
		

.. function:: Faq.configuraDir 


	:comentário: Esse método configura o diretório corrente referente à aplicação.
		

.. function:: Faq.limpa 


	:comentário: Esse método limpa todos os campos referentes ao formulário da FAQ verificando
		se o documento está salvo.
		

.. function:: Faq.limpatela 


	:comentário: Esse método limpa os campos do formulário sem verificar se o documento está
		salvo.
		

.. function:: Faq.click campo


	:comentário: Esse método trata o evento relacionado ao click sobre uma linha de consulta.
		A lógica principal é interpretar o conteúdo de uma linha da string buffer
		partindo da posição (postcaret) do click.
		
	:param campo:

.. function:: Faq.pesquisa 


	:comentário: pega a linha
		inic = poscaret;
		while (inic > 0) {
		¨if ((str.s.charAt(inic - 1)) == '\n')
		¨break;
		¨--inic;
		}
		fim = poscaret;
		// B.mensagem("tam:"+tam);
		while (fim <= tam) {
		¨if ((str.s.charAt(fim - 1)) == '\n')
		¨break;
		¨++fim;
		}
		B.meio(str, linha, inic + 1, fim - inic);
		B.compchar(linha, '\n');
		pega a palavra
		inic = poscaret;
		while (inic > 0) {
		¨if ((str.s.charAt(inic - 1)) == ' ' || (str.s.charAt(inic - 1)) == '\n')
		¨break;
		¨--inic;
		}
		fim = poscaret;
		while (fim <= tam) {
		¨if ((str.s.charAt(fim - 1)) == ' ' || (str.s.charAt(fim - 1)) == '\n')
		¨break;
		¨fim++;
		}
		B.meio(str, palavra, inic + 1, fim - inic);
		}
		}
		// PESQUISA
		
		Esse método executa uma consulta baseada no método pesqarq() da classe Io.
		Varre uma árvore de diretórios enraizada no diretório inicial especificado
		pelo usuário procurando por palavras chaves no interior de cada arquivo lido.
		
		
		
	:see:  Io
		

.. function:: Faq.fecha 


	:comentário: Esse método fecha o aplicativo.
		

.. function:: Faq.deleta 


	:comentário: Esse método deleta um arquivo especificado no formulário do aplicativo
		

.. function:: Faq.nova 


	:comentário: Esse método cria um novo documento.
		

.. function:: Faq.lefaq 


	:comentário: Esse método é responsável pela leitura dos arquivos especificados pelo
		usuário.
		

.. function:: Faq.gravafaq 


	:comentário: Esse método grava uma arquivo especificado no formulário do aplicativo.
		

.. function:: Faq.formataArquivo 


	:comentário: Esse método formata os arquivos lidos possibilitando a exibição no
		aplicativo.
		

