
Rel
===
.. function:: Rel.fazsdthrel psdthrel dt hh


	:comentário: 
	:param psdthrel:
		
	:param dt:
		
	:param hh:

.. function:: Rel.ajzero arg sqt


	:comentário: 
	:param arg:
		
	:param sqt:

.. function:: Rel.hhmmss hh millis1 millis2


	:comentário: 
	:param hh:
		
	:param millis1:
		
	:param millis2:

.. function:: Rel.hhmmss segs hh


	:comentário: 
	:param segs:
		
	:param hh:

.. function:: Rel.calcHHMMSS segs qt hh


	:comentário: 
	:param segs:
		
	:param qt:
		
	:param hh:

.. function:: Rel.perc resp sqt stot


	:comentário: 
	:param resp:
		
	:param sqt:
		
	:param stot:

.. function:: Rel.perc resp sqt stot scasas


	:comentário: 
	:param resp:
		
	:param sqt:
		
	:param stot:
		
	:param scasas:

.. function:: Rel.ajdec str decs


	:comentário: 
	:param str:
		
	:param decs:

.. function:: Rel.pxsdthrel sdthrel


	:comentário: 
	:param sdthrel:

.. function:: Rel.cksdthrel sdthrel


	:comentário: 
	:param sdthrel:

.. function:: Rel.antsdthrel sdthrel


	:comentário: 
	:param sdthrel:

.. function:: Rel.pxdata data


	:comentário: 
	:param data:

.. function:: Rel.pxdata data strdata


	:comentário: 
	:param data:
		
	:param strdata:

.. function:: Rel.pxdatames data


	:comentário: 
	:param data:

.. function:: Rel.pxdatames data op


	:comentário: 
	:param data:
		
	:param op:

.. function:: Rel.pxdataano data


	:comentário: 
	:param data:

.. function:: Rel.trataDt dt vaiInv


	:comentário: 
	:param dt:
		
	:param vaiInv:

.. function:: Rel.trataDt dt vaiInv


	:comentário: 
	:param dt:
		
	:param vaiInv:

.. function:: Rel.ckdata data


	:comentário: 
	:param data:

.. function:: Rel.ckdata data strdata


	:comentário: 
	:param data:
		
	:param strdata:

.. function:: Rel.antdata data


	:comentário: 
	:param data:

.. function:: Rel.antdata data strdata


	:comentário: 
	:param data:
		
	:param strdata:

.. function:: Rel.antdatames data


	:comentário: 
	:param data:

.. function:: Rel.antdataano data


	:comentário: 
	:param data:

.. function:: Rel.criastrdata ndias strdata


	:comentário: 
	:param ndias:
		
	:param strdata:

.. function:: Rel.segs hhmmss segs


	:comentário: 
	:param hhmmss:
		
	:param segs:

.. function:: Rel.compara rega regb sdel campos precisoes regperc


	:comentário: 
	:param rega:
		
	:param regb:
		
	:param sdel:
		
	:param campos:
		
	:param precisoes:
		
	:param regperc:

.. function:: Rel.compara a b precisao perc


	:comentário: 
	:param a:
		
	:param b:
		
	:param precisao:
		
	:param perc:

.. function:: Rel.div b c


	:comentário: 
	:param b:
		
	:param c:

.. function:: Rel.div a b c


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:

.. function:: Rel.dec numero decimais


	:comentário: 
	:param numero:
		
	:param decimais:

.. function:: Rel.totColunas vetDest vetOrig ignoraCol1 ignoraLin1


	:comentário: 
	:param vetDest:
		
	:param vetOrig:
		
	:param ignoraCol1:
		
	:param ignoraLin1:

.. function:: Rel.totColunas vetDest vetOrig ignoraCol1 ignoraLin1 qtCols


	:comentário: 
	:param vetDest:
		
	:param vetOrig:
		
	:param ignoraCol1:
		
	:param ignoraLin1:
		
	:param qtCols:

.. function:: Rel.totLinhas vetDest vetOrig ignoraCol1 ignoraLin1


	:comentário: 
	:param vetDest:
		
	:param vetOrig:
		
	:param ignoraCol1:
		
	:param ignoraLin1:

.. function:: Rel.totLinhas vetDest vetOrig ignoraCol1 ignoraLin1 qtCols


	:comentário: 
	:param vetDest:
		
	:param vetOrig:
		
	:param ignoraCol1:
		
	:param ignoraLin1:
		
	:param qtCols:

.. function:: Rel.tamCamposTable 


	:comentário: 

.. function:: Rel.montaTableTxt orig dest titulos tcampos


	:comentário: Monta uma tabela com os dados recebidos de uma pesquisa com os titulos e os
		tamanhos
		
		
		

	:param  orig: 
		
		

	:param  dest: 
		
		

	:param  tcampos: 
		
		
	:throws:  ErrBds
		

.. function:: Rel.montaLinhaTxt orig dest tcampos


	:comentário: 
	:param orig:
		
	:param dest:
		
	:param tcampos:

