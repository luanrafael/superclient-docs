
CliJson
=======
.. function:: CliJson.send ipCloud portaCloud servicoCloud prmte pconsumidor pdest penv presp pret ptimeout


	:comentário: 
	:param ipCloud:
		
	:param portaCloud:
		
	:param servicoCloud:
		
	:param prmte:
		
	:param pconsumidor:
		
	:param pdest:
		
	:param penv:
		
	:param presp:
		
	:param pret:
		
	:param ptimeout:

.. function:: CliJson.post ip pporta servico post resposta


	:comentário: 
	:param ip:
		
	:param pporta:
		
	:param servico:
		
	:param post:
		
	:param resposta:

.. function:: CliJson.decodeJson jsongResp prmte pconsumidor pdest penv presp pret


	:comentário: 
	:param jsongResp:
		
	:param prmte:
		
	:param pconsumidor:
		
	:param pdest:
		
	:param penv:
		
	:param presp:
		
	:param pret:

.. function:: CliJson.sendJ prmte pconsumidor pdest penv presp pret ptimeout


	:comentário: 
	:param prmte:
		
	:param pconsumidor:
		
	:param pdest:
		
	:param penv:
		
	:param presp:
		
	:param pret:
		
	:param ptimeout:

.. function:: CliJson.sendJ ipCloud portaCloud servicoCloud prmte pconsumidor pdest penv presp pret ptimeout


	:comentário: 
	:param ipCloud:
		
	:param portaCloud:
		
	:param servicoCloud:
		
	:param prmte:
		
	:param pconsumidor:
		
	:param pdest:
		
	:param penv:
		
	:param presp:
		
	:param pret:
		
	:param ptimeout:

.. function:: CliJson.postJ ip pporta servico post resposta


	:comentário: 
	:param ip:
		
	:param pporta:
		
	:param servico:
		
	:param post:
		
	:param resposta:

