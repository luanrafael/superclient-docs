
Tts
===
.. function:: Tts.jaInic 


	:comentário: 

.. function:: Tts.inicTts 


	:comentário: 

.. function:: Tts.inicTts forca


	:comentário: 
	:param forca:

.. function:: Tts.criaHash ptexto hash


	:comentário: cria uma string com o hash de um texto
		
		
		

	:param  str: 
		
		

.. function:: Tts.montaNomeArq texto arq


	:comentário: 
		
		

	:param  texto: 
		
		

	:param  arq: 
		
		

	:param  dir: 
		
		
	:throws:  ErrBds
		@epp 080919 monta um nome de arquivo a partir do hash de um texto
		

.. function:: Tts.ttx texto


	:comentário: 
	:param texto:

.. function:: Tts.convTts texto textoProc arqs


	:comentário: 
		
		

	:param  texto: 
		
		

	:param  arqs: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		@epp 080919 Converte um arquivo texto em um conjunto de arquivos wav,
		considerando como delimitadores pontos, virgulas, exclamacao e
		interrogacao
		

.. function:: Tts.procFraseOtimizado resp str tabela


	:comentário: Processa uma string contra uma determinada tabela
		<p/>
		faz um processamento reentrante ateh esgotar todas as possibilidades
		
	:param resp:
		
	:param str:
		
	:param tabela:

.. function:: Tts.procFrase resp str tabela


	:comentário: 
	:param resp:
		
	:param str:
		
	:param tabela:

.. function:: Tts.trataFrase tabela atalho componente


	:comentário: 
	:param tabela:
		
	:param atalho:
		
	:param componente:

.. function:: Tts.trataTrad tabela atalho componente


	:comentário: 
	:param tabela:
		
	:param atalho:
		
	:param componente:

.. function:: Tts.pqTrad traducao origem


	:comentário: 
	:param traducao:
		
	:param origem:

