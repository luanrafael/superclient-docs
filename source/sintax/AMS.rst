
AMS
===
.. function:: AMS.assume 


	:comentário: 

.. function:: AMS.inicBd 


	:comentário: 

.. function:: AMS.inicBd bd user pass base


	:comentário: 
	:param bd:
		
	:param user:
		
	:param pass:
		
	:param base:

.. function:: AMS.inicBd bd user pass base


	:comentário: 
	:param bd:
		
	:param user:
		
	:param pass:
		
	:param base:

.. function:: AMS.lpteste identidade servico tempo ip porta acesso


	:comentário: 
	:param identidade:
		
	:param servico:
		
	:param tempo:
		
	:param ip:
		
	:param porta:
		
	:param acesso:

.. function:: AMS.start servMysql userMysql senhaMysql schemaMysql identidade servico tempo ip porta acesso forca


	:comentário: 
	:param servMysql:
		
	:param userMysql:
		
	:param senhaMysql:
		
	:param schemaMysql:
		
	:param identidade:
		
	:param servico:
		
	:param tempo:
		
	:param ip:
		
	:param porta:
		
	:param acesso:
		
	:param forca:

.. function:: AMS.start servMysql userMysql senhaMysql schemaMysql identidade servico tempo ip porta acesso forca


	:comentário: 
	:param servMysql:
		
	:param userMysql:
		
	:param senhaMysql:
		
	:param schemaMysql:
		
	:param identidade:
		
	:param servico:
		
	:param tempo:
		
	:param ip:
		
	:param porta:
		
	:param acesso:
		
	:param forca:

.. function:: AMS.loop identidade servico tempo ip porta acesso sforca


	:comentário: 
	:param identidade:
		
	:param servico:
		
	:param tempo:
		
	:param ip:
		
	:param porta:
		
	:param acesso:
		
	:param sforca:

.. function:: AMS.souMestre identidade servico tempo ip porta acesso forca


	:comentário: 
	:param identidade:
		
	:param servico:
		
	:param tempo:
		
	:param ip:
		
	:param porta:
		
	:param acesso:
		
	:param forca:

.. function:: AMS.souMestre identidade servico tempo ip porta acesso status forca


	:comentário: 
	:param identidade:
		
	:param servico:
		
	:param tempo:
		
	:param ip:
		
	:param porta:
		
	:param acesso:
		
	:param status:
		
	:param forca:

.. function:: AMS.souMestre identidade servico tempo ip porta acesso forca


	:comentário: 
	:param identidade:
		
	:param servico:
		
	:param tempo:
		
	:param ip:
		
	:param porta:
		
	:param acesso:
		
	:param forca:

.. function:: AMS.souMestre identidade servico tempo ip porta acesso status forca


	:comentário: 
	:param identidade:
		
	:param servico:
		
	:param tempo:
		
	:param ip:
		
	:param porta:
		
	:param acesso:
		
	:param status:
		
	:param forca:

.. function:: AMS.owner servico


	:comentário: mostra os dados
		
		
		

	:param  servico: 
		
		
	:throws:  ErrBds
		

.. function:: AMS.query identidade tempo servico


	:comentário: retorna os dados de um servico na lista padrao. Retorna o dth separado em
		milis para fazer algum tipo de compracacao com a data hora atual
		
		
		

	:param  dth: 
		
		

	:param  tempo: 
		
		

	:param  servico: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: AMS.query identidade tempo servico


	:comentário: retorna os dados de um servico na lista padrao. Retorna o dth separado em
		milis para fazer algum tipo de compracacao com a data hora atual
		
		
		

	:param  dth: 
		
		

	:param  tempo: 
		
		

	:param  servico: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: AMS.getEndPoint resp servico forca


	:comentário: 
	:param resp:
		
	:param servico:
		
	:param forca:

.. function:: AMS.set identidade servico dth ip porta acesso status


	:comentário: 
	:param identidade:
		
	:param servico:
		
	:param dth:
		
	:param ip:
		
	:param porta:
		
	:param acesso:
		
	:param status:

.. function:: AMS.set identidade servico dth ip porta acesso status


	:comentário: 
	:param identidade:
		
	:param servico:
		
	:param dth:
		
	:param ip:
		
	:param porta:
		
	:param acesso:
		
	:param status:

.. function:: AMS.updStat identidade servico dth status status_sec status_rec msg_resp ident_rec


	:comentário: 
	:param identidade:
		
	:param servico:
		
	:param dth:
		
	:param status:
		
	:param status_sec:
		
	:param status_rec:
		
	:param msg_resp:
		
	:param ident_rec:

.. function:: AMS.updStatRec identidade status_rec identSolicitante


	:comentário: 
	:param identidade:
		
	:param status_rec:
		
	:param identSolicitante:

.. function:: AMS.updStatRec identidade status_rec identSolicitante


	:comentário: faz um update da tabela ams_stat_rec no que se refere ao status - somente
		atua se a identidade de solicitante for a mesma de quem esta querendo trocar
		o status
		
		
		

	:param  identidade: 
		
		

	:param  status_rec: 
		
		

	:param  identSolicitante: 
		
		

	:param  forca: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: AMS.limpaStatusCmd identidade identSolicitante


	:comentário: Limpa o status das tabelas de status de um servico
		
		
		

	:param  identidade: 
		
		
	:throws:  ErrBds
		

.. function:: AMS.sendCmd resp identidade timeout cmd status_rec msg_resp identSolicitante


	:comentário: 
	:param resp:
		
	:param identidade:
		
	:param timeout:
		
	:param cmd:
		
	:param status_rec:
		
	:param msg_resp:
		
	:param identSolicitante:

.. function:: AMS.checaExecCmd identidade servico maxPrazo


	:comentário: Dentro de um loop de execução de tarefas de um servico, o checaExec verifica
		se existe algum comando na tabela status_rec direcionado ao servico e que
		deve ser executado se existir um comando, se este tiver um "&" no final da
		string será executado em background. Se for o caso da execucao do comando o
		checaExec dispara o comando. Se nao for executado em background, este comando
		ficará locado até o final. Se nao, o comando retornará e apos isto, o proprio
		loop do servico ira´checar se o comando acabou se o status do comando for
		"END" ou entao se se esgotar o limite maxPrazo.
		
		
		

	:param  identidade: identidade do servico que esta verificando se existe comando
		
		
		

	:param  maxPrazo: maximo prazo em segundos para a execucao do comando
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: AMS.checaFimCmd identidade maxPrazo


	:comentário: processo a ser executado em background
		
		
		

	:param  identidade: 
		
		

	:param  maxPrazo: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: AMS.registraThread 


	:comentário: registra
		
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: AMS.pegaThread resp refThread


	:comentário: 
	:param resp:
		
	:param refThread:

.. function:: AMS.interrompeThreads stringBusca interrompidas


	:comentário: 
	:param stringBusca:
		
	:param interrompidas:

.. function:: AMS.removeThreads stringBusca removidas


	:comentário: remove as threads que atendam a uma determinada string de busca
		
		
		

	:param  stringBusca: mascara de busca desejada
		
		

	:param  removidas: lista as threads efetivamente removidas
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: AMS.limpaThreads timeout


	:comentário: pede a iterrupcao de todas as threads e retorna true se isto aconteceu dentro
		do timeout indicado
		
		
		

	:param  timeout: tempo limite em segundos
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: AMS.cmdThread resp refTh cmd timeout


	:comentário: 
	:param resp:
		
	:param refTh:
		
	:param cmd:
		
	:param timeout:

.. function:: AMS.logTh tipo msg


	:comentário: 
	:param tipo:
		
	:param msg:

.. function:: AMS.logThI msg strings


	:comentário: faz o log basico quando é uma ocorrencia dentro de uma thread
		
		
		

	:param  msg: 
		
		
	:throws:  ErrBds
		

.. function:: AMS.logThE msg strings


	:comentário: faz o log basico quando é uma ocorrencia dentro de uma thread
		
		
		

	:param  msg: 
		
		
	:throws:  ErrBds
		

.. function:: AMS.logTh tipo msg strings


	:comentário: faz o log basico quando é uma ocorrencia dentro de uma thread
		
		
		

	:param  msg: 
		
		
	:throws:  ErrBds
		

.. function:: AMS.getBdMillis resp con


	:comentário: 
	:param resp:
		
	:param con:

.. function:: AMS.lock servico identidade


	:comentário: faz o lock para poider gravar em gestao
		
		
		

	:param  con: 
		
		

	:param  servico: 
		
		

	:param  identidade: 
		
		

	:param  segundos: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: AMS.lock servico identidade


	:comentário: faz o lock para poider gravar em gestao
		
		
		

	:param  con: 
		
		

	:param  servico: 
		
		

	:param  identidade: 
		
		

	:param  segundos: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: AMS.unLock servico identidade


	:comentário: Faz o unlock se o servico estiver com o dominio do lock
		
		
		

	:param  con: 
		
		

	:param  servico: 
		
		

	:param  identidade: 
		
		

	:param  segundos: prazo em segundos para considerar o lock anterior vencido
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: AMS.unLock servico identidade


	:comentário: Faz o unlock se o servico estiver com o dominio do lock
		
		
		

	:param  con: 
		
		

	:param  servico: 
		
		

	:param  identidade: 
		
		

	:param  segundos: prazo em segundos para considerar o lock anterior vencido
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: AMS.limpa str


	:comentário: 
	:param str:

.. function:: AMS.statOper statOper


	:comentário: 
	:param statOper:

