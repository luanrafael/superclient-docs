
Thtml
=====
.. function:: Thtml.inic 


	:comentário: 

.. function:: Thtml.montaCampo strCps cp


	:comentário: 
	:param strCps:
		
	:param cp:

.. function:: Thtml.monta dadosTela arqTrabHtml nomeTela


	:comentário: 
	:param dadosTela:
		
	:param arqTrabHtml:
		
	:param nomeTela:

.. function:: Thtml.tela dadosTela prompt largurax larguray posx posy


	:comentário: 
	:param dadosTela:
		
	:param prompt:
		
	:param largurax:
		
	:param larguray:
		
	:param posx:
		
	:param posy:

.. function:: Thtml.limpaTela dadosTela


	:comentário: limpa uma tela , e na proxima invocacao, chama outra vez do zero
		
		
		

	:param  dadosTela: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Thtml.setTxt pcampos dadosTela


	:comentário: seta o valor de um campo ou uma lista de campos em uma tela
		
		
		

	:param  campos: 
		
		

	:param  dadosTela: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Thtml.getTxt pcampos dadosTela


	:comentário: pega o valor de um campo ou uma lista de campos em uma tela
		
		
		

	:param  campos: 
		
		

	:param  dadosTela: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Thtml.mostra dadosTela


	:comentário: 
	:param dadosTela:

.. function:: Thtml.oculta dadosTela


	:comentário: 
	:param dadosTela:

