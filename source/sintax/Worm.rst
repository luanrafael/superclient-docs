
Worm
====
.. function:: Worm.geraMundo qtWorms


	:comentário: 
	:param qtWorms:

.. function:: Worm.mostraMundo 


	:comentário: Mostra um mundo de worms
		
		
		
	:throws:  ErrBds
		

.. function:: Worm.criaWorm str


	:comentário: Cria uma worm padrao repetindo a string indicada ate' o seu tamanho maximo
		
		
		

	:param  str: 
		= string a ser repetida
		
		
		
	:throws:  ErrBds
		

.. function:: Worm.mapeia 


	:comentário: cria um map novo e mapeia todas as entradas, totalizando a nota de cada item
		
		
		
	:throws:  ErrBds
		

