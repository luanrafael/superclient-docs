
Kmin
====
.. function:: Kmin.cmdx ax bx cx dx


	:comentário: este metodo é acionado quando se escolhe uma opcao do combo cmdEscolhido
		
	:param ax:
		
	:param bx:
		
	:param cx:
		
	:param dx:

.. function:: Kmin.criaCmd cmd pdicas


	:comentário: coloca na area de trabalho uma string correspondente ao comando que se deseja
		criar
		
		
		

	:param  cmd: string do comando que sugere o que por
		
		

	:param  dicas: dicas para a montagem do comando
		
		
	:throws:  ErrBds
		

.. function:: Kmin.retanguloCimaEsq nomeJanela


	:comentário: 
		
		

	:param  nomeJanela: Nome da janela onde se quer mapear o retangulo. Se nula, acha a
		posicao absoluta do retangulo Acha a coordenada cima esquerda de
		um retangulo (pox0 e posy0)
		
		
	:throws:  ErrBds
		

.. function:: Kmin.retanguloBaixoDir nomeJanela


	:comentário: Acha a coordenada cima esquerda de um retangulo (pox0 e posy0)
		
		
		

	:param  nomeJanela: Nome da janela onde se quer mapear o retangulo. Se nula, acha a
		posicao absoluta do retangulo
		
		
	:throws:  ErrBds
		

.. function:: Kmin.ultCoordenadas 


	:comentário: marca na string ultCoord as ultimas coordenadas a saber: ultimas posicoes
		obtidas do cursor : posx (distancia horizontal em pixels do limite esquerdo
		superior da tela) posy (distancia vertical em pixels do limite esquerdo
		superior da tela) ultimas posicoes obtidas do retangulo dados do canto
		esquerdo superior: posx0 (distancia horizontal em pixels do limite esquerdo
		superior da tela) posy0 (distancia vertical em pixels do limite esquerdo
		superior da tela) dados do canto direito inferior : posx1 (distancia
		horizontal em pixels do limite esquerdo superior da tela) posy1 (distancia
		vertical em pixels do limite esquerdo superior da tela)
		
		
		
		
	:throws:  ErrBds
		

.. function:: Kmin.poeDica dica


	:comentário: 
	:param dica:

.. function:: Kmin.limpaDicas 


	:comentário: Salva o atual contexto do IDE é sempre chamado quando se aciona algum botao
		
		
		
	:throws:  ErrBds
		

.. function:: Kmin.salvaContexto 


	:comentário: Salva o atual contexto do IDE é sempre chamado quando se aciona algum botao
		
		
		
	:throws:  ErrBds
		

.. function:: Kmin.trazContexto 


	:comentário: 

.. function:: Kmin.execCmd cmd dicas


	:comentário: Se prepara para executar um comando com possibilidade de oferecer um help
		
		
		

	:param  cmd: string do comando que sugere o que por
		
		

	:param  dicas: dicas para a execucao do comando
		
		
	:throws:  ErrBds
		

.. function:: Kmin.pegaJan 


	:comentário: Pega a ultima janela clicada desde que nao seja a do SuperClient ou a do
		proprio IDE
		
		
		
	:throws:  ErrBds
		

.. function:: Kmin.seta campo


	:comentário: 
	:param campo:

.. function:: Kmin.traz campo


	:comentário: 
	:param campo:

.. function:: Kmin.seta campo tela


	:comentário: 
	:param campo:
		
	:param tela:

.. function:: Kmin.traz campo tela


	:comentário: 
	:param campo:
		
	:param tela:

.. function:: Kmin.setaTela campo tela titulo


	:comentário: 
	:param campo:
		
	:param tela:
		
	:param titulo:

.. function:: Kmin.sai tela op


	:comentário: sai de uma tela salvando o conteudo e ocultando a dita se op=1
		
		
		

	:param  tela: Tela
		
		

	:param  op: se = "1" salva o conteudo da tela
		
		
	:throws:  ErrBds
		

.. function:: Kmin.cstabPesqComandos evento conteudo ultTecla uldCodTecla


	:comentário: quando ocorre um evento de escolha na tabela, pega a linha respectiva e
		carrega os comandos correspondentes na tabela de volta
		
		
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.csabstral evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.cscomandoEscolhido evento conteudo ultTecla uldCodTecla


	:comentário: quando ocorre um evento de escolha na tabela, pega a linha respectiva e
		carrega os comandos correspondentes na tabela de volta
		
		
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmatualiza evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmchecar evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmclick evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmtestaAtalho evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmtrazAtalho evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmteclada evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmtestaTeclada evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmexecutar evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmgruposCmd evento conteudo ultTecla uldCodTecla


	:comentário: carrega na tabela de funcoes os grupos de comandos
		
		
		

	:param  evento: 
		
		

	:param  conteudo: 
		
		

	:param  ultTecla: 
		
		

	:param  uldCodTecla: 
		
		
	:throws:  ErrBds
		

.. function:: Kmin.kmpesqCmd evento conteudo ultTecla uldCodTecla


	:comentário: vai para o campo arq comando e o limpa para poder fazer uma pesquisa
		
		
		

	:param  evento: 
		
		

	:param  conteudo: 
		
		

	:param  ultTecla: 
		
		

	:param  uldCodTecla: 
		
		
	:throws:  ErrBds
		

.. function:: Kmin.kmmontaCmd evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmversoes evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmsalva evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmedtPacote evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmlePacote evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmcriaCampo evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmvaiJan evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmnovo evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmhelpCmd evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmabstral evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmdescAbs evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmdescPac evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.pegaAutoIt 


	:comentário: 

.. function:: Kmin.kmautoIt evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmresult evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmtesteAutoIt evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmusuario evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmpacote evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmjanAbstral evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmsalvaHelp evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmocultaHelp evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmsalvaAutoIt evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmocultaAutoIt evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmsalvaDescPac evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmocultaDescPac evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmsalvaDescAbs evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmocultaDescAbs evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmsalvaResult evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmocultaResult evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmtroca evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmexecSel evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmabsAntes evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.kmabsDepois evento conteudo ultTecla uldCodTecla


	:comentário: 
	:param evento:
		
	:param conteudo:
		
	:param ultTecla:
		
	:param uldCodTecla:

.. function:: Kmin.salvaDados 


	:comentário: salva o conteudo do campo de dados (pode ser o helo, o abstral, a doc do abs,
		a doc do pacote e a compilacao do autoit)
		
		
		

	:param  ultDado: 
		
		
	:throws:  ErrBds
		

.. function:: Kmin.lePacote pacote arqPacote user listaAbs


	:comentário: Le um pacote de trabalho (arquivo com abstrais) e monta uma lista com todos
		os abstrais ali existentes coloca na variavel global o conteudo da descricao
		do arquivo Este metodo sempre monta a descricao do pacote internamente
		(descPac)
		
		
		

	:param  pacote: 
		
		

	:param  arqPacote: 
		
		

	:param  user: 
		
		

	:param  listaAbs: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Kmin.pegaAbstral arqPacote nomeAbstral abstral argsAbstral descricao


	:comentário: 
	:param arqPacote:
		
	:param nomeAbstral:
		
	:param abstral:
		
	:param argsAbstral:
		
	:param descricao:

.. function:: Kmin.atAbstral 


	:comentário: 

.. function:: Kmin.montaStrAbs str


	:comentário: 
	:param str:

.. function:: Kmin.trataArgs 


	:comentário: 

.. function:: Kmin.transfCp 


	:comentário: transforma um criacampo do bdjava para java ou vice versa
		
		
		
	:throws:  ErrBds
		

.. function:: Kmin.transfCp arg


	:comentário: transforma um criacampo do bdjava para java ou vice versa
		
		
		

	:param  arg: contem a string ida e volta do criacampo
		
		
	:throws:  ErrBds
		

.. function:: Kmin.pegaLista 


	:comentário: Pega lista de campos a partir de um conjunto de criacampos copiados para a
		area de trabalho Coloca o resultado de volta na area de trabalho
		
		
		

	:param  arg: argumento que na ida contem os criacampos e recebe o conteudo da
		lista
		
		
	:throws:  ErrBds
		

.. function:: Kmin.pegaLista arg


	:comentário: Pega lista de campos a partir de um conjunto de criacampos copiados para arg
		Coloca o resultado de volta em arg
		
		
		
	:throws:  ErrBds
		
	:param arg:

.. function:: Kmin.criaTrabCp 


	:comentário: assistente para a criacao de campos usando o bdjava ou o proprio java traz
		para a area de trabalho o resultado
		
		
		
	:throws:  ErrBds
		

.. function:: Kmin.criaTrabCp arg


	:comentário: assistente para a criacao de campos usando o bdjava ou o proprio java
		
		
		

	:param  arg: recebe o resultado do criacampo
		
		
	:throws:  ErrBds
		

.. function:: Kmin.addOpCp cp


	:comentário: Adiciona nas opcoes de um campo Combo a ultima opcao. Verifica se esta opcao
		ja existe. Se existir, vem para a primeira opcao
		
		
		

	:param  cp: 
		
		
	:throws:  ErrBds
		

.. function:: Kmin.addOpCp cp opts


	:comentário: Adiciona nas opcoes de um campo Combo a ultima opcao. Verifica se esta opcao
		ja existe. Se existir, vem para a primeira opcao. atualiza a string opts
		
		
		

	:param  cp: 
		
		
	:throws:  ErrBds
		

.. function:: Kmin.addMapCp cp


	:comentário: Adiciona o ultimo conteudo de um Campo Combo a um mapa que está armazenado no
		campo cp
		
		
		

	:param  cp: 
		
		
	:throws:  ErrBds
		

.. function:: Kmin.addMapVars novaVar


	:comentário: adiciona no mapa de variaveis uma nova variavel
		
		
		

	:param  novaVar: nova variavel contendo o nome e sua descricao
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Kmin.addMapVars map novaVar


	:comentário: adiciona no mapa de variaveis uma nova variavel
		
		
		

	:param  map: mapa onde vai fazer a gerencia das variaveis
		
		

	:param  novaVar: nova variavel contendo o nome e sua descricao
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Kmin.pesqMapVars lista filtro


	:comentário: adiciona no mapa de variaveis uma nova variavel
		
		
		

	:param  novaVar: nova variavel contendo o nome e sua descricao
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Kmin.pesqMapVars map lista filtro


	:comentário: retorna uma lista com todas as opcoes que contem o filtro proposto
		
		
		

	:param  map: mapa onde estao as variaveis
		
		

	:param  lista: que vai receber as opcoes
		
		

	:param  filtro: filtro da pesquisa
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Kmin.pegaAtalho 


	:comentário: Pega um atalho no explorer e o coloca no campo atalho a acao tem que comecar
		com o atalho escolhico, mas com o cursor na barra do caminho do explorer
		
		
		
	:throws:  ErrBds
		

.. function:: Kmin.poePrefixo str pref


	:comentário: 
	:param str:
		
	:param pref:

.. function:: Kmin.retPrefixo str pref


	:comentário: 
	:param str:
		
	:param pref:

.. function:: Kmin.gravaPacote strPlanilha


	:comentário: B.parte (linhaPlanilha, ptipoLinha , '\t', 1 ); B.parte (linhaPlanilha,
		ppacTrab , '\t', 2 ); B.parte (linhaPlanilha, pdescPac , '\t', 3 ); B.parte
		(linhaPlanilha, patividade , '\t', 4 ); B.parte (linhaPlanilha, ptipoAtv ,
		'\t', 5 ); B.parte (linhaPlanilha, pnomeAbstral, '\t', 6 ); B.parte
		(linhaPlanilha, pdescAbs , '\t', 7 ); B.parte (linhaPlanilha, pdescAbs1 ,
		'\t', 8 ); B.parte (linhaPlanil ha, pdescAbs2 , '\t', 9 ); B.parte
		(linhaPlanilha, pfuncs , '\t', 10);
		
		
	:param strPlanilha:

.. function:: Kmin.montaResult result filtro dados


	:comentário: 
	:param result:
		
	:param filtro:
		
	:param dados:

.. function:: Kmin.emsg coment


	:comentário: Registra um comentario no log
		
		
		

	:param  coment: 
		
		
	:throws:  ErrBds
		

