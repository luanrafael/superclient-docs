
Comp
====
.. function:: Comp.criaVars th1 th2


	:comentário: 
	:param th1:
		
	:param th2:

.. function:: Comp.cpabs parqabs java vars


	:comentário: 
	:param parqabs:
		
	:param java:
		
	:param vars:

.. function:: Comp.java 


	:comentário: 

.. function:: Comp.abs2j pabs java vars


	:comentário: 
	:param pabs:
		
	:param java:
		
	:param vars:

.. function:: Comp.procLinha args


	:comentário: 
	:param args:

.. function:: Comp.procLinha linha args vars


	:comentário: 
	:param linha:
		
	:param args:
		
	:param vars:

.. function:: Comp.poevsem args pexcecoes


	:comentário: 
	:param args:
		
	:param pexcecoes:

.. function:: Comp.poev args pexcecoes


	:comentário: 
	:param args:
		
	:param pexcecoes:

.. function:: Comp.calcv args pexcecoes


	:comentário: 
	:param args:
		
	:param pexcecoes:

.. function:: Comp.montaIf pargs pexcecoes


	:comentário: 
	:param pargs:
		
	:param pexcecoes:

.. function:: Comp.montaIf0 pargs pexcecoes metodo


	:comentário: 
	:param pargs:
		
	:param pexcecoes:
		
	:param metodo:

.. function:: Comp.montaIf1 pargs pexcecoes metodo


	:comentário: 
	:param pargs:
		
	:param pexcecoes:
		
	:param metodo:

.. function:: Comp.montaIf2 pargs pexcecoes metodo


	:comentário: 
	:param pargs:
		
	:param pexcecoes:
		
	:param metodo:

.. function:: Comp.virg2ponto arg


	:comentário: troca as virgular de arg por ponto
		
		
		

	:param  arg: 
		
		
	:rtype: Bds
		
		
	:throws:  ErrBds
		

.. function:: Comp.ponto2virg arg


	:comentário: troca as virgular de arg por ponto
		
		
		

	:param  arg: 
		
		
	:rtype: Bds
		
		
	:throws:  ErrBds
		

.. function:: Comp.vs a


	:comentário: retorna um vetor de Bds
		
		
		
	:param a:

.. function:: Comp.vs a b


	:comentário: 
	:param a:
		
	:param b:

.. function:: Comp.vs a b c


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:

.. function:: Comp.vs a b c d


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: Comp.vs a b c d e


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:
		
	:param e:

.. function:: Comp.vs a b c d e f


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:
		
	:param e:
		
	:param f:

.. function:: Comp.vs a b c d e f g


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:
		
	:param e:
		
	:param f:
		
	:param g:

.. function:: Comp.vs a b c d e f g h


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:
		
	:param e:
		
	:param f:
		
	:param g:
		
	:param h:

.. function:: Comp.vs a b c d e f g h i


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:
		
	:param e:
		
	:param f:
		
	:param g:
		
	:param h:
		
	:param i:

.. function:: Comp.vs a b c d e f g h i j


	:comentário: 
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:
		
	:param e:
		
	:param f:
		
	:param g:
		
	:param h:
		
	:param i:
		
	:param j:

.. function:: Comp.tt args


	:comentário: 
	:param args:

.. function:: Comp.addVarPrep pvar


	:comentário: 
	:param pvar:

.. function:: Comp.addVar vars


	:comentário: 
	:param vars:

.. function:: Comp.achaClasse nomecla nomemet retorno qtargs tipomet


	:comentário: 
	:param nomecla:
		
	:param nomemet:
		
	:param retorno:
		
	:param qtargs:
		
	:param tipomet:

.. function:: Comp.checaMetodo metodo qtArgs tipomet


	:comentário: 
	:param metodo:
		
	:param qtArgs:
		
	:param tipomet:

.. function:: Comp.proc2abs parq


	:comentário: 
	:param parq:

.. function:: Comp.trataEscape arg


	:comentário: 
	:param arg:

.. function:: Comp.cpp pargsBds


	:comentário: 
	:param pargsBds:

.. function:: Comp.cpp pargsBds


	:comentário: 
	:param pargsBds:

.. function:: Comp.nomemetodo pargsBds


	:comentário: chama um metodo compilado a partir de outro abstral
		
		
		

	:param  pargsBds: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Comp.mountFor strfor arg1 arg2 arg3 arg4


	:comentário: monta uma string do for a partir de seus 4 argumentos
		
		
		

	:param  strfor: 
		
		

	:param  arg1: 
		
		

	:param  arg2: 
		
		

	:param  arg3: 
		
		

	:param  arg4: 
		
		
	:throws:  ErrBds
		

.. function:: Comp.criaAbstrais 


	:comentário: cria a partir de abstraisOk um metodo que cria estes abstrais a partir da
		lista de abstrais OK
		
		
		
	:throws:  ErrBds
		

.. function:: Comp.trataEscapeBds str


	:comentário: trata str para trocar os caracteres escape Bdjava para Java Exemplo : troca
		"Edgar com o numero \035 de coisas" ´por "Edgar com o numero"+(char)0x35+ "de
		coisas "
		
		
		

	:param  str: 
		
		
	:throws:  ErrBds
		

.. function:: Comp.criaAbsFig arqFig


	:comentário: 
	:param arqFig:

.. function:: Comp.batchTurbo 


	:comentário: 

.. function:: Comp.criaTurbo 


	:comentário: cria uma classe "turbo" que é usada ao inves dos abstrais
		
		
		
	:throws:  ErrBds
		

.. function:: Comp.trataErrosJava 


	:comentário: trata os erros de uma compilacao de Turbo
		
		
		
	:throws:  ErrBds
		

.. function:: Comp.eda 


	:comentário: edita um abstral que na linha onde o cursor está
		
		
		
	:throws:  ErrBds
		

.. function:: Comp.addEx 


	:comentário: adiciona na lista de excecoes um abstral
		
		
		
	:throws:  ErrBds
		

.. function:: Comp.excecoes 


	:comentário: edita a lista de abstrais que nao devem ser compilados no turbo
		
		
		
	:throws:  ErrBds
		

.. function:: Comp.limpaExcecoes 


	:comentário: adiciona na lista de excecoes um abstral
		
		
		
	:throws:  ErrBds
		

.. function:: Comp.pegaAbsJava linha strAbs


	:comentário: 
	:param linha:
		
	:param strAbs:

.. function:: Comp.cpj arqAbs


	:comentário: 
		
		compila um arquivo abs e cria a claase java correspondente cria uma classe
		"turbo" que é usada ao inves dos abstrais
		
		
		
	:throws:  ErrBds
		
	:param arqAbs:

.. function:: Comp.trataPlics arg


	:comentário: 
	:param arg:

.. function:: Comp.trataPlic arg


	:comentário: 
	:param arg:

.. function:: Comp.voltaPlics arg


	:comentário: 
	:param arg:

.. function:: Comp.voltaPlic arg


	:comentário: 
	:param arg:

.. function:: Comp.edErros 


	:comentário: edita o ultimo arqlog
		
		
		

	:param  strLog: 
		
		
	:throws:  ErrBds
		

.. function:: Comp.log strLog


	:comentário: monta o resultado no arquivo de log /j/<user>/logCompilacao<data hora-eps>
		
		
		

	:param  pargsBds: 
		
		
	:rtype: void
		
		
	:throws:  ErrBds
		

.. function:: Comp.log strLog


	:comentário: 
	:param strLog:

.. function:: Comp.contemParen str pedaco


	:comentário: 
	:param str:
		
	:param pedaco:

.. function:: Comp.montaParen comando0 excecoes


	:comentário: 
	:param comando0:
		
	:param excecoes:

.. function:: Comp.montaParen comando excecoes paren bkk pilha


	:comentário: 
	:param comando:
		
	:param excecoes:
		
	:param paren:
		
	:param bkk:
		
	:param pilha:

.. function:: Comp.montaRetBds arg excecoes


	:comentário: 
	:param arg:
		
	:param excecoes:

