
Jx
==
.. function:: Jx.checkHandler handler


	:comentário: Método responsável por verificar se o handler existe (utilizado somente no
		caso de Frames)
		
		
		

	:param  handler: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.checkHandler handler resp


	:comentário: 
	:param handler:
		
	:param resp:

.. function:: Jx.getBrowser handler


	:comentário: Método responsável por coletar um browser novo se ncessário
		
		
		

	:param  handler: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.getBrowser handler user password


	:comentário: 
	:param handler:
		
	:param user:
		
	:param password:

.. function:: Jx.wSetAbsX value


	:comentário: 
	:param value:

.. function:: Jx.wSetEscape value


	:comentário: Permite habilitar o Escape para fechar a Janela do Brw
		
		
		

	:param  value: 
		
		
	:throws:  ErrBds
		

.. function:: Jx.getBrowserTab handler


	:comentário: Método responsável por gerar um browser novo para utilizar no modo de abas
		
		
		

	:param  handler: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.getBrowserTab handler resp


	:comentário: 
	:param handler:
		
	:param resp:

.. function:: Jx.wDisablePopUp value


	:comentário: 
		
		Método responsável por desabilitar o pop up de sites web
		
		
		

	:param  value: 1 para desativar 0 para ativar
		
		
	:throws:  ErrBds
		

.. function:: Jx.wDisablePopUp handler PopUpName


	:comentário: 
	:param handler:
		
	:param PopUpName:

.. function:: Jx.ativaBrwListener ativa


	:comentário: Ativa os listners da página web, permite visualizar no frame do bdjava o
		andamento da página
		
		
		

	:param  ativa: 
		
		
	:throws:  ErrBds
		

.. function:: Jx.wSetBrwType type


	:comentário: método para setar o tipo de Browser 0 - para IE 1 para Mozilla
		
		
		

	:param  type: 
		
		
	:throws:  ErrBds
		

.. function:: Jx.wSetModoCompatibilidade arg


	:comentário: 
	:param arg:

.. function:: Jx.wDisableJavaScriptMsgs ativa


	:comentário: Método responsável por desabilitar msgs de erro javascript
		
		
		

	:param  ativa: 
		
		
	:throws:  ErrBds
		

.. function:: Jx.wOpenTab handler url


	:comentário: Método responsável por abrir um Browser no modo Aba, se não existe cria o
		frame e a primeira aba, senão cria só a aba
		
		
		

	:param  handler: 
		
		

	:param  url: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wOpenTab handler url user password


	:comentário: 
	:param handler:
		
	:param url:
		
	:param user:
		
	:param password:

.. function:: Jx.wOpenTab handler url posx posy tamx tamy


	:comentário: 
	:param handler:
		
	:param url:
		
	:param posx:
		
	:param posy:
		
	:param tamx:
		
	:param tamy:

.. function:: Jx.wOpenTab handler url user password posx posy tamx tamy


	:comentário: 
	:param handler:
		
	:param url:
		
	:param user:
		
	:param password:
		
	:param posx:
		
	:param posy:
		
	:param tamx:
		
	:param tamy:

.. function:: Jx.wShowTab handler


	:comentário: Método responsável por exibir um determinada aba de acordo com seu handler
		
		
		

	:param  handler: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wShowTab handler resp


	:comentário: 
	:param handler:
		
	:param resp:

.. function:: Jx.wCloseTab handler


	:comentário: Método responsável por fechar uma aba, caso seja a ultima do frame, fecha o
		frame
		
		
		

	:param  handler: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wCloseTab handler resp


	:comentário: 
	:param handler:
		
	:param resp:

.. function:: Jx.wCloseAllTabs 


	:comentário: Método responsável por fechar um frame e toda as suas aba
		
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wCloseAllTabs resp


	:comentário: 
	:param resp:

.. function:: Jx.wGetSourceTab handler resp


	:comentário: Método responsável por coletar o source de um hmtl em determinada aba
		
		
		

	:param  handler: 
		
		

	:param  resposta: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wGetSourceTab handler erro resp


	:comentário: 
	:param handler:
		
	:param erro:
		
	:param resp:

.. function:: Jx.wNavigateTab handler url post


	:comentário: Método que permite navegar por uma pagina passando um post data
		
		
		

	:param  handler: 
		
		

	:param  url: 
		
		

	:param  post: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wNavigateTab handler erro url post


	:comentário: 
	:param handler:
		
	:param erro:
		
	:param url:
		
	:param post:

.. function:: Jx.wSetFieldTab handler campo valor


	:comentário: 
		
		Método responsável por inserir um dado em um determinado campo passando seus
		parametros
		
		
		

	:param  handler: 
		
		

	:param  campo: 
		
		

	:param  valor: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wSetFieldTab handler resp campo valor


	:comentário: 
	:param handler:
		
	:param resp:
		
	:param campo:
		
	:param valor:

.. function:: Jx.wSetFieldTab handler resp atributo campo valor


	:comentário: 
	:param handler:
		
	:param resp:
		
	:param atributo:
		
	:param campo:
		
	:param valor:

.. function:: Jx.wGetFieldTab handler campo resp


	:comentário: Método responsável por coletar um valor de um campo passando seus parametros
		
		
		

	:param  handler: 
		
		

	:param  campo: 
		
		

	:param  resposta: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wGetFieldTab handler erro campo resp


	:comentário: 
	:param handler:
		
	:param erro:
		
	:param campo:
		
	:param resp:

.. function:: Jx.wClickTab handler campo


	:comentário: Método responsável por dar um click em um determinado campo da página Web via
		DOM
		
		
		

	:param  handler: 
		
		

	:param  campo: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wClickTab handler resp campo


	:comentário: 
	:param handler:
		
	:param resp:
		
	:param campo:

.. function:: Jx.wGetFieldFocusedAttrTab handler resp


	:comentário: Método responsavel por coletar os atributos de um campo
		
		
		

	:param  handler: 
		
		

	:param  resposta: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wGetFieldFocusedAttrTab handler erro resp


	:comentário: 
	:param handler:
		
	:param erro:
		
	:param resp:

.. function:: Jx.wGetURLTab handler resp


	:comentário: Método responsável por coletar uma URL da Aba especificada
		
		
		

	:param  handler: 
		
		

	:param  resp: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wGetURLTab handler erro resp


	:comentário: 
	:param handler:
		
	:param erro:
		
	:param resp:

.. function:: Jx.wGetTitleTab handler resp


	:comentário: Método responsável por pegar o titulo da página Web
		
		
		

	:param  handler: 
		
		

	:param  resp: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wGetTitleTab handler erro resp


	:comentário: 
	:param handler:
		
	:param erro:
		
	:param resp:

.. function:: Jx.wJScriptTab handler script


	:comentário: Método responsável por executar um script e receber um retorno
		
		
		

	:param  handler: 
		
		

	:param  script: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wJScriptTab handler resp script


	:comentário: 
	:param handler:
		
	:param resp:
		
	:param script:

.. function:: Jx.wJScriptTab handler erro resp script


	:comentário: 
	:param handler:
		
	:param erro:
		
	:param resp:
		
	:param script:

.. function:: Jx.wSetFocusTab handler campo


	:comentário: Método responsável por por em foco determinado campo
		
		
		

	:param  handler: 
		
		

	:param  campo: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wSetFocusTab handler resp campo


	:comentário: 
	:param handler:
		
	:param resp:
		
	:param campo:

.. function:: Jx.wPrintScreenTab handler diretorio arquivo


	:comentário: Método responsável por realizar um printscreen na aba selecionada e jogar em
		um arquivo
		
		
		

	:param  handler: 
		
		

	:param  arquivo: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wPrintScreenTab handler resp diretorio arquivo


	:comentário: 
	:param handler:
		
	:param resp:
		
	:param diretorio:
		
	:param arquivo:

.. function:: Jx.wOpen handler url


	:comentário: Método responsável por abrir uma página Web
		
		
		

	:param  handler: 
		
		

	:param  url: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wOpen handler url oculto


	:comentário: 
	:param handler:
		
	:param url:
		
	:param oculto:

.. function:: Jx.wOpen handler url user pwd


	:comentário: 
	:param handler:
		
	:param url:
		
	:param user:
		
	:param pwd:

.. function:: Jx.wOpen handler url user pwd oculto


	:comentário: 
	:param handler:
		
	:param url:
		
	:param user:
		
	:param pwd:
		
	:param oculto:

.. function:: Jx.wOpen handler url posx posy tamx tamy


	:comentário: 
	:param handler:
		
	:param url:
		
	:param posx:
		
	:param posy:
		
	:param tamx:
		
	:param tamy:

.. function:: Jx.wOpen handler url posx posy tamx tamy oculto


	:comentário: 
	:param handler:
		
	:param url:
		
	:param posx:
		
	:param posy:
		
	:param tamx:
		
	:param tamy:
		
	:param oculto:

.. function:: Jx.wOpen handler url user pwd posx posy tamx tamy


	:comentário: 
	:param handler:
		
	:param url:
		
	:param user:
		
	:param pwd:
		
	:param posx:
		
	:param posy:
		
	:param tamx:
		
	:param tamy:

.. function:: Jx.wOpen handler url user pwd posx posy tamx tamy oculto


	:comentário: 
	:param handler:
		
	:param url:
		
	:param user:
		
	:param pwd:
		
	:param posx:
		
	:param posy:
		
	:param tamx:
		
	:param tamy:
		
	:param oculto:

.. function:: Jx.wNavigate handler url post


	:comentário: Método que permite navegar por uma pagina passando um post data
		
		
		

	:param  handler: 
		
		

	:param  url: 
		
		

	:param  post: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wNavigate handler erro url post


	:comentário: 
	:param handler:
		
	:param erro:
		
	:param url:
		
	:param post:

.. function:: Jx.wClose handler


	:comentário: Método responsável por encerrar o browser remover os objetos do mapa
		
		
		

	:param  handler: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wClose handler resp


	:comentário: 
	:param handler:
		
	:param resp:

.. function:: Jx.wGetSource handler resp


	:comentário: Método responsável por coletar os dados Source
		
		
		

	:param  handler: 
		
		

	:param  resp: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wGetSource handler erro resp


	:comentário: 
	:param handler:
		
	:param erro:
		
	:param resp:

.. function:: Jx.wSetField handler campo valor


	:comentário: Método responsável por setar um valor em um campo
		
		
		

	:param  handler: 
		
		

	:param  campo: 
		
		

	:param  valor: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wSetField handler resp campo valor


	:comentário: 
	:param handler:
		
	:param resp:
		
	:param campo:
		
	:param valor:

.. function:: Jx.wSetField handler resp tipoCampo campo valor


	:comentário: 
	:param handler:
		
	:param resp:
		
	:param tipoCampo:
		
	:param campo:
		
	:param valor:

.. function:: Jx.wGetField handler campo resp


	:comentário: Método responsável por coletar o dado do campo escolhido
		
		
		

	:param  handler: 
		
		

	:param  campo: 
		
		

	:param  resposta: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wGetField handler erro campo resp


	:comentário: 
	:param handler:
		
	:param erro:
		
	:param campo:
		
	:param resp:

.. function:: Jx.wClick handler campo


	:comentário: Método responsá´vel por executar um click dentro da engine escolhida
		
		
		

	:param  handler: 
		
		

	:param  campo: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wClick handler resp campo


	:comentário: 
	:param handler:
		
	:param resp:
		
	:param campo:

.. function:: Jx.wGetFieldFocusedAttr handler resp


	:comentário: Método responsável por coletar os atributos (id ou name) do campo que tiver
		foco
		
		
		

	:param  handler: 
		
		

	:param  resp: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wGetFieldFocusedAttr handler erro resp


	:comentário: 
	:param handler:
		
	:param erro:
		
	:param resp:

.. function:: Jx.wJScript handler script


	:comentário: Método responsável por executar um script e receber um retorno
		
		
		

	:param  handler: 
		
		

	:param  script: 
		
		

	:param  resp: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wJScript handler resp script


	:comentário: 
	:param handler:
		
	:param resp:
		
	:param script:

.. function:: Jx.wJScript handler erro resp script


	:comentário: 
	:param handler:
		
	:param erro:
		
	:param resp:
		
	:param script:

.. function:: Jx.wHide handler


	:comentário: Método responsável por ocultar uma página Web
		
		
		

	:param  handler: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wHide handler resp


	:comentário: 
	:param handler:
		
	:param resp:

.. function:: Jx.wShow handler


	:comentário: Método responsável por mostrar página web caso o status seja oculto
		
		
		

	:param  handler: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wShow handler resp


	:comentário: 
	:param handler:
		
	:param resp:

.. function:: Jx.wIsVisible handler


	:comentário: Método responsável por checar se a página Web está visivel
		
		
		

	:param  handler: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wIsVisible handler resp


	:comentário: 
	:param handler:
		
	:param resp:

.. function:: Jx.wGetTitle handler resp


	:comentário: Método responsável por pegar o titulo da página Web
		
		
		

	:param  handler: 
		
		

	:param  resp: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wGetTitle handler erro resp


	:comentário: 
	:param handler:
		
	:param erro:
		
	:param resp:

.. function:: Jx.wGetURL handler resp


	:comentário: Método responsável por coletar o URL da atual página Web
		
		
		

	:param  handler: 
		
		

	:param  resposta: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wGetURL handler erro resp


	:comentário: 
	:param handler:
		
	:param erro:
		
	:param resp:

.. function:: Jx.wWaitOk handler


	:comentário: Método responsável por aguardar o carregamento da página web
		
		
		

	:param  handler: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wWaitOk handler resp


	:comentário: 
	:param handler:
		
	:param resp:

.. function:: Jx.wSetFocus handler campo


	:comentário: Método responsável por por em foco determinado campo
		
		
		

	:param  handler: 
		
		

	:param  campo: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wSetFocus handler resp campo


	:comentário: 
	:param handler:
		
	:param resp:
		
	:param campo:

.. function:: Jx.wPrintScreen handler arquivo


	:comentário: Método responsável por tirar um print screen da tela e armazenar em uma
		arquivo
		
		
		

	:param  handler: 
		
		

	:param  arquivo: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Jx.wPrintScreen handler resp arquivo


	:comentário: 
	:param handler:
		
	:param resp:
		
	:param arquivo:

.. function:: Jx.wPrintScreen handler arquivo wpx wpy wlx wly


	:comentário: 
	:param handler:
		
	:param arquivo:
		
	:param wpx:
		
	:param wpy:
		
	:param wlx:
		
	:param wly:

.. function:: Jx.wPrintScreen handler resp arquivo wpx wpy wlx wly


	:comentário: 
	:param handler:
		
	:param resp:
		
	:param arquivo:
		
	:param wpx:
		
	:param wpy:
		
	:param wlx:
		
	:param wly:

.. function:: Jx.wRegisterFunction handler


	:comentário: 
	:param handler:

.. function:: Jx.wRegisterFunction handler functionName


	:comentário: 
	:param handler:
		
	:param functionName:

.. function:: Jx.wMapper handler resp


	:comentário: 
	:param handler:
		
	:param resp:

.. function:: Jx.wMapperClick handler


	:comentário: 
	:param handler:

.. function:: Jx.wMapperClick handler arg


	:comentário: 
	:param handler:
		
	:param arg:

.. function:: Jx.wMapperClick handler arg absname


	:comentário: 
	:param handler:
		
	:param arg:
		
	:param absname:

