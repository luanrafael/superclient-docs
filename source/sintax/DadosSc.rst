
DadosSc
=======
.. function:: DadosSc.setaJan posx posy tamx tamy tela


	:comentário: seta as dimensoes de uma janela do DadosSc
		
		
		

	:param  posx: 
		
		

	:param  posy: 
		
		

	:param  tamx: 
		
		

	:param  tamy: 
		
		

	:param  tela: 
		"lista" que identifica a tela do dadosSC
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.undecorated flag


	:comentário: 
	:param flag:

.. function:: DadosSc.nome nome lista


	:comentário: seta o nome da tela, cria uma variavel de referencia com este nome e ainda
		poe o DadosSc no .o desta referencia
		
		
		

	:param  nome: nome que se deseja dar `a tela
		
		

	:param  lista: lista de campos que criou a tela
		
		

.. function:: DadosSc.limpaTela lista0


	:comentário: 
	:param lista0:

.. function:: DadosSc.limpaTela lista0


	:comentário: 
	:param lista0:

.. function:: DadosSc.atArquivo sii acao dados a b c d


	:comentário: 
	:param sii:
		
	:param acao:
		
	:param dados:
		
	:param a:
		
	:param b:
		
	:param c:
		
	:param d:

.. function:: DadosSc.atArquivo sii acao dados


	:comentário: 
	:param sii:
		
	:param acao:
		
	:param dados:

.. function:: DadosSc.pegaArquivo arquivo


	:comentário: 
	:param arquivo:

.. function:: DadosSc.pegaArquivo arquivo diretorio ext Prompt


	:comentário: 
	:param arquivo:
		
	:param diretorio:
		
	:param ext:
		
	:param Prompt:

.. function:: DadosSc.tela dadosx


	:comentário: 
	:param dadosx:

.. function:: DadosSc.tela dados prompt


	:comentário: 
	:param dados:
		
	:param prompt:

.. function:: DadosSc.tela dados prompt


	:comentário: 
	:param dados:
		
	:param prompt:

.. function:: DadosSc.tela dados prompt largurax larguray x y


	:comentário: 
	:param dados:
		
	:param prompt:
		
	:param largurax:
		
	:param larguray:
		
	:param x:
		
	:param y:

.. function:: DadosSc.tela dados prompt x y


	:comentário: 
	:param dados:
		
	:param prompt:
		
	:param x:
		
	:param y:

.. function:: DadosSc.pegaDados listaDados0


	:comentário: 
		
		Pega os dados e aguarda a resposta
		
		
		

	:param  listaDados: lista com os dados
		
		
		

.. function:: DadosSc.pegaDados listaDados0


	:comentário: 
		
		Pega os dados e aguarda a resposta
		
		
		

	:param  listaDados: lista com os dados
		
		
		

.. function:: DadosSc.pegaDados listaDados0 argJanTempo


	:comentário: 
		
		Pega os dados e aguarda a resposta
		
		
		

	:param  listaDados: lista com os dados
		
		
		

	:param  argJanTempo: 
	: se numero tempo em que a janela vai ficar aberta, se nao o
		titulo desejado para a janela
		
		

.. function:: DadosSc.naoPrimeiroPlano listaDados0 pargJanTempo x y


	:comentário: 
	:param listaDados0:
		
	:param pargJanTempo:
		
	:param x:
		
	:param y:

.. function:: DadosSc.pega dadosx


	:comentário: 
	:param dadosx:

.. function:: DadosSc.pega dados prompt


	:comentário: 
	:param dados:
		
	:param prompt:

.. function:: DadosSc.pega dados prompt


	:comentário: 
	:param dados:
		
	:param prompt:

.. function:: DadosSc.pega dados prompt x y


	:comentário: 
	:param dados:
		
	:param prompt:
		
	:param x:
		
	:param y:

.. function:: DadosSc.proc arg op dadosx


	:comentário: 
	:param arg:
		
	:param op:
		
	:param dadosx:

.. function:: DadosSc.listaTabs dados listaCampos


	:comentário: sintaxe do cs deve ser: "funcaoCs op cp sdados codigo"
		if (cp.cs.length() > 0 && (B.existe("$$" + cp.cs) || B.contem(cp.cs, "."))) { // se realmente existir ...
		argExec.s(cp.cs);
		// abaixo: se existe a indicacao de uma classe de tela filha, a chamada 'e feita
		// antes na "filha"
		if (B.contem(argExec, "TelaBd.") && B.tamanho(sc.nomeClasseTelaBdTrab)) {
		¨B.troca(argExec, "TelaBd.", sc.nomeClasseTelaBdTrab);
		¨cp.cs = argExec.s;
		}
		// B.mensagem ("Executando "+""+cp.cs+" '"+op+"' '"+cp+"' '"+sdados+"'
		// '"+code+"' ");
		B.retchar(cp, '\'', '`');
		varDadosScTrab.s(cp);
		if (cp.co == 1 || cp.co == 2 || cp.co == 4) { // se for campos de texto - limpa os caracteres
		B.retchar(varDadosScTrab, '\'', '`');
		B.retchar(sdados, '\'', '`');
		}
		B.mensagem(13, "EXEC CS:" + cp.cs + " '" + op + "' _varDadosScTrab '" + sdados + "' '" + code + "' ");
		Run.exec("" + cp.cs + " '" + op + "' _varDadosScTrab '" + sdados + "' '" + code + "' ", B.th(),
		Run.EHCS);
		}
		if (limpaKeys)
		BdjKey.limpaKeys();
		if (cp.co == 7 && code >= 48 && code <= (int) 'Z')// pausa para o combo
		ret = 'L';
		if (cp.co == 7) // marca o objeto combo para uso futuro
		comboEstatico = cp;
		// B.pausa(1,500);
		}
		if (B.tamanho(sc.retornoCs)) {
		¨cp.s(sc.retornoCs);
		¨// B.mensagem( "setando cp="+cp);
		¨sc.retornoCs.s("");
		¨DadosSc.setTxt(cp);
		¨ret = 'L'; // marcar para limpar o ultimo char!!
		}
		sc.dentroCs = false;
		if (enterConfirmando) {
		¨if (B.tamanho(sc.absEnter)) {
		¨¨Run.exec("pausa 1 50;" + sc.absEnter, B.th(), Run.EHCS);
		¨¨// Run.exec(sc.absEnter);
		¨¨} else if (toNoFoiOk && (true || Alerta.checaOk("Confirma ?"))) { // @epp 100721 - deixa de usar o confirma
		¨¨// Run.exec("pausa 1 100;teclas '{A}o' ''"); // se confirmar - da um alt-o!!!
		¨¨sc.foiOk = true;
		¨¨sc.tovivo = false;
		¨¨int qtCampos = sc.cps.length;
		¨¨for (int ii = 0; ii < qtCampos; ++ii) {
		¨¨¨try {
		¨¨¨¨cp = (Cp) sc.cps[ii];
		¨¨¨¨} catch (RuntimeException e) {
		¨¨¨¨¨// TODO Auto-generated catch block
		¨¨¨¨¨new Alerta("A variavel [" + ii + "] da lista nao eh campo ");
		¨¨¨¨¨B.emsg("A variavel [" + ii + "] da lista nao eh campo ", "R", "", "S");
		¨¨¨¨¨e.printStackTrace();
		¨¨¨¨}
		¨¨¨¨comp = sc.campos[ii];
		¨¨¨¨if (cp.co == 2)
		¨¨¨¨comp = sc.camposE[ii]; // no caso do texto eh o alternativo
		¨¨¨¨getTxt(cp, comp);
		¨¨¨¨// sc.sCampos[ii].s(""+sc.campos[ii].getText());
		¨¨¨}
		¨¨¨// sc.eu.setVisible(false);
		¨¨¨} else if (B.existe("$$cs_ENTER_" + sc.nomeTela)) {// primeiro vai ver se existe o pgup da tela
		¨¨¨B.mensagem("EXEC = cs_ENTER");
		¨¨¨Run.exec("pausa 1 100;cs_ENTER_" + sc.nomeTela, B.th(), Run.EHCS);
		¨¨}
		¨¨else if (B.existe("$$cs_ENTER"))
		¨¨Run.exec("pausa 1 100;cs_ENTER", B.th(), Run.EHCS);
		¨¨if (limpaKeys)
		¨¨BdjKey.limpaKeys();
		¨}
		¨sc.dentroCs = false;
		¨toNoCs = false;
		¨return ret;
		}
		public static boolean listaTabs(Bds dados, Bds listaCampos) throws ErrBds
		public static boolean listaTabs(Bds dados, Bds listaCampos) throws ErrBds
	:param dados:
		
	:param listaCampos:

.. function:: DadosSc.foca pos tela


	:comentário: 
		
		
		

	:param  pos: posicao do campo na lista da tela
		
		

	:param  tela: tela / lista de campos
		
		
	:rtype: boolean  true se deu certo
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.foca spos


	:comentário: 
		
		
		

	:param  pos: posicao do campo na lista da tela
		
		

	:param  tela: tela / lista de campos
		
		
	:rtype: boolean  true se deu certo
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.focaComp campo


	:comentário: 
		
		Foca em um componente a do registro de componente de um campo
		
		
		

	:param  pos: posicao do campo na lista da tela
		
		

	:param  tela: tela / lista de campos
		
		
	:rtype: boolean  true se deu certo
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.focaComp campo lista


	:comentário: 
		
		Foca em um componente a do registro de componente de um campo
		
		
		

	:param  pos: posicao do campo na lista da tela
		
		

	:param  tela: tela / lista de campos
		
		
	:rtype: boolean  true se deu certo
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.acertaRadio radioButton tipo


	:comentário: Ajusta um radio button para o tipo indicado
		
		
		

	:param  radioButton: 
		
		

	:param  tipo: 
		= se = N ou n deve mudar para 1 ou 0 , se = B, deve mudar para
		true ou false como for o caso - se = nulo para para 0 ou false
		conform for o caso
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.vai cpx tela


	:comentário: vai para determinado campo em determinada tela
		
		
		

	:param  cp: campo desejado
		
		

	:param  tela: tela desejada
		
		

.. function:: DadosSc.getTxtLista campos tela


	:comentário: da uma sequencia de gets txt dos campos listados em "campos"
		
		
		

	:param  campos: 
		
		

	:param  tela: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.getUltimoCp scp ultimo


	:comentário: Coloca o ultimo valor valido em cp - no caso de tabelas , despresa os valores
		correspondentes a selecoes
		
		
		

	:param  cp: campo de onde se quer saber o ultimo valor
		
		

	:param  ultimo: retorna o ultimo valor obtido
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.getUltimoCp scp ultimo pos


	:comentário: Coloca o ultimo valor valido em cp - no caso de tabelas , despresa os valores
		correspondentes a selecoes
		
		
		

	:param  cp: campo de onde se quer saber o ultimo valor
		
		

	:param  ultimo: retorna o ultimo valor obtido
		
		
		

	:param  pos: se pos=1 - é o ultimo valor, se pos =2, etc, retorna
		os valores inferiores
		
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.setTxtLista campos tela


	:comentário: da uma sequencia de sets txt dos campos listados em "campos"
		
		
		

	:param  campos: 
		
		

	:param  tela: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.aguarda 


	:comentário: 

.. function:: DadosSc.aguarda comando


	:comentário: 
	:param comando:

.. function:: DadosSc.trava op


	:comentário: trava o Superclide para o processo com a identficacao fornecida por idLock se
		op=1 Se ja estiver travado e for de idLock diferente, aguarda destravar pelo
		Idatual
		
		
		

	:param  idLock: 
		
		

	:param  op: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.pegaDadosSempreFocado visivel


	:comentário: 
	:param visivel:

.. function:: DadosSc.toNoFoiOk tela tempo


	:comentário: 
	:param tela:
		
	:param tempo:

.. function:: DadosSc.setaOk tela ok


	:comentário: se ok="1" , informa que a saida eh ok, se 0, informa que nao eh ok
		
	:param tela:
		
	:param ok:

.. function:: DadosSc.focaTela tela


	:comentário: foca na tela
		
		
		

	:param  tela: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.noTopo tela noTopo


	:comentário: se noTopo ="1", marca a janela para ficar sempre no topo
		
		
		

	:param  tela: lista de campos da tela
		

.. function:: DadosSc.oculta tela


	:comentário: oculta a tela
		
		
		

	:param  tela: lista de campos da tela
		

.. function:: DadosSc.minimiza tela


	:comentário: minimiza uma tela atencao sempre salva os dados de volta na lista
		
		
		

	:param  tela: lista de campos da tela
		

.. function:: DadosSc.locCampo cpx tela pos


	:comentário: Localiza o indice de um campo dentro de uma tela
		
		
		

	:param  cpx: Campo
		
		

	:param  tela: Bds lista (tela) correspondente
		
		

.. function:: DadosSc.delayFoca jan


	:comentário: Dispara uma thread auxiliar para dentro de 500mseg focar na janela desejada
		
		
		

	:param  jan: 
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.delayFoca jan


	:comentário: Dispara uma thread auxiliar para dentro de 500mseg focar na janela desejada
		
		
		

	:param  jan: 
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.delayFocaR jan


	:comentário: Dispara uma thread auxiliar para dentro de 500mseg focar na janela desejada
		
		
		

	:param  jan: 
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.mouseExec cpx tela abstral


	:comentário: mouseExec
		
		Marca um abstral para trabalhar sobre o ponto en que o mouse clica em um
		texto
		
		
		

	:param  cpx: campo texto da tela onde se clica o mouse
		
		
		

	:param  tela: tela
		
		

	:param  abstral: abstral que se deseja usar
		
		
		

.. function:: DadosSc.getScreen largura altura


	:comentário: getScreen
		
		geta o texto no campo de uma determiada tela
		
		
		

	:param  cp: campo do qual se deseja getar o texto na tela
		
		

	:param  tela: tela
		
		
		

.. function:: DadosSc.reexibTela 


	:comentário: 

.. function:: DadosSc.nomeTela tela nomeTela


	:comentário: seta o abstral que devera se executado se teclar enter
		
		
		

	:param  tela: 
		
		

	:param  abstral: 
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.setaAbsEnter tela abstral


	:comentário: seta o abstral que devera se executado se teclar enter
		
		
		

	:param  tela: 
		
		

	:param  abstral: 
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.setaAbsX tela abstral


	:comentário: seta o abstral que devera se executado apertar o X da janela
		
		
		

	:param  tela: 
		
		

	:param  abstral: 
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.setaAbsM tela abstral


	:comentário: seta o abstral que devera se executado se minimalizar a tela
		
		
		

	:param  tela: 
		
		

	:param  abstral: 
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.setaAbsN tela abstral


	:comentário: seta o abstral que devera se executado se a tela voltar para Normal
		
		
		

	:param  tela: 
		
		

	:param  abstral: 
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.setaAbsEsc tela abstral


	:comentário: seta o abstral que devera se executado se teclar ESC
		
		
		

	:param  tela: 
		
		

	:param  abstral: 
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.setaAbsFoco tela abstral


	:comentário: seta o abstral que devera se executado se a tela receber o foco
		
		
		

	:param  tela: 
		
		

	:param  abstral: 
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.setaAbsSaiFoco tela abstral


	:comentário: seta o abstral que devera se executado se a tela perder o foco
		
		
		

	:param  tela: 
		
		

	:param  abstral: 
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.setaAutoCombo scombo lista del


	:comentário: 
	:param scombo:
		
	:param lista:
		
	:param del:

.. function:: DadosSc.comboAutoOpcao cp op


	:comentário: salva automatica mente as opcoes de um combo de determinado nome e sempre se
		lembrando delas
		
		
		

	:param  cp: campo se desejado
		
		

	:param  op: se ="L" ou "l" - le as opcoes armazenadas, se "G" ou "g", grava
		as opcoes armazenadas
		
		
	:throws:  ErrBds
		

.. function:: DadosSc.gravaCombo combo


	:comentário: 
	:param combo:

