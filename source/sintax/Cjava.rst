
Cjava
=====
.. function:: Cjava.xproc sarq0


	:comentário: 
	:param sarq0:

.. function:: Cjava.proc sarq0


	:comentário: static public boolean proc(String[] sarq0) throws ErrBds  faz um proc igual ao do bdmax IMPORTANTE : Nao consegue compilar nem executar  for, while, if etc entende entretanto o ';' como separador no futuro pode ser  que tenhamos um interpretador bdmax embutido
	:param sarq0:

.. function:: Cjava.cpj arquivo


	:comentário: chama o Comp.cpj (Transforma em classe java um arquivo abs e todos os
		abstrais associados
		
		
		

	:param  arquivo: arquivo abs contendo um conjunto de abstrais
		
		

.. function:: Cjava.cp 


	:comentário: compila o ultimo arquivo abs (bdjava) compilado
		

.. function:: Cjava.cp arq0


	:comentário: compila os abstrais constantes de arq0 IMPORTANTE : Nao consegue compilar nem
		executar for, while, if etc
		
		
		

	:param  arq0: nome do arquivo que contem os abstrais Se o arquivo nao contiver
		o caminho, cp busta este arquivo nos caminhos constantes em
		v._pathclides atencao se abstrais contem '!' - compila sempre
		

.. function:: Cjava.cp arq0 abstrais


	:comentário: 
		
		compila os abstrais constantes de arq0 IMPORTANTE : Nao consegue compilar nem
		executar for, while, if etc
		
		
		

	:param  arq0: nome do arquivo que contem os abstrais
		
		

	:param  abstrais: lista dos abstrais que se quer compilar separados por branco Se
		o arquivo nao contiver o caminho, cp busta este arquivo nos
		caminhos constantes em v._pathclides atencao se abstrais contem
		'!' - compila sempre somente compila os abstrais que estiverem em
		abstrais
		
		

.. function:: Cjava.cp arq0 abstrais opcional


	:comentário: faz um proc igual ao do bdmax IMPORTANTE : Nao consegue compilar nem executar
		for, while, if etc entende entretanto o ';' como separador no futuro pode ser
		que tenhamos um interpretador bdmax embutido atencao se abstrais contem '!' -
		compila sempre
		
		
		

	:param  arq0: nome do arquivo que contem os abstrais
		
		

	:param  abstrais: lista dos abstrais que se quer compilar separados por branco
		
		

	:param  opcional: se =1,nao compila
		

.. function:: Cjava.cj classes


	:comentário: public static boolean cj(Bds classes) throws ErrBds  comando basico de compilacao usa o jikes preferencialmente   em classes devem estar separadas por brancos as classes que se deseja  compilar Se a opcao de compiacao (prod ou dbg) nao estiver definida,
	:param classes:

.. function:: Cjava.cj classes comSeq


	:comentário: public static boolean cj(Bds classes, Bds comSeq) throws ErrBds  comando basico de compilacao - usa o jikes em classes devem estar separadas  por brancos as classes que se deseja compilar Se a opcao de compiacao (prod  ou dbg) nao estiver definida,
	:param classes:
		
	:param comSeq:

.. function:: Cjava.ctu 


	:comentário: Cria a classe turbo
		
		
		

	:param  classes: 
		
		
	:rtype: void
		
		
	:throws:  ErrBds
		

.. function:: Cjava.cjj classes


	:comentário: public static boolean cjj(Bds classes) throws ErrBds  comando basico de compilacao usa o javac preferencialmente   em classes devem estar separadas por brancos as classes que se deseja  compilar Se a opcao de compiacao (prod ou dbg) nao estiver definida,
	:param classes:

.. function:: Cjava.cjj classes comSeq


	:comentário: public static boolean cjj(Bds classes, Bds comSeq) throws ErrBds  comando basico de compilacao - usa o javac em classes devem estar separadas  por brancos as classes que se deseja compilar Se a opcao de compiacao (prod  ou dbg) nao estiver definida,
	:param classes:
		
	:param comSeq:

.. function:: Cjava.cj classes opt comSeq


	:comentário: public static boolean cj(Bds classes, Bds opt, Bds comSeq) throws ErrBds  comando basico de compilacao em classes devem estar separadas por brancos as  classes que se deseja compilar em conSeq, se = "N", nao incrementa o  sequencial
	:param classes:
		
	:param opt:
		
	:param comSeq:

.. function:: Cjava.convclass classtrab pacote opcao classes_orig classes_em_vigor


	:comentário: public static boolean convclass(Bds classtrab, Bds pacote, Bds opcao, Bds classes_orig, Bds classes_em_vigor) throws ErrBds  Cj compila uma classe tomando as seguintes providencias: Dependendo de opcao,  compila usando o jikes ("jikes" ) ou ("javac" - sun) alem da compilacao  oficial, 'e gerado uma classe provisoria de debug que o Bdjava acessa  automaticamente enquanto o programa bdjava estiver ativo No caso de uma nova  carga, o bdjava comecar utilizando a .class oficial.
	:param classtrab:
		
	:param pacote:
		
	:param opcao:
		
	:param classes_orig:
		
	:param classes_em_vigor:

.. function:: Cjava.setpackage pacote


	:comentário: static public void setpackage(String pacote) throws ErrBds  marca no ambiente o pacote desejado se pacote for nulo, pega o pacote  (_package) e devolve se ainda for nulo, pega a propriedade epsoft.package
	:param pacote:

.. function:: Cjava.setpackage pacote


	:comentário: static public void setpackage(Bds pacote) throws ErrBds  marca no ambiente o pacote desejado se pacote for nulo, pega o pacote  (_package) e devolve se ainda for nulo, pega a propriedade epsoft.package
	:param pacote:

.. function:: Cjava.ambiente amb


	:comentário: static public void ambiente(Bds amb) throws ErrBds   O ambiente de trabalho pode ser apl, bdjava, tst ou uti a  partir dai que se monta o dircomp
	:param amb:

.. function:: Cjava.ambiente amb pacote


	:comentário: static public void ambiente(Bds amb, Bds pacote) throws ErrBds   O ambiente de trabalho pode ser apl, bdjava, tst  ou uti a partir dai que se monta o dircomp
	:param amb:
		
	:param pacote:

.. function:: Cjava.ambiente amb pacote dbg


	:comentário: static public void ambiente(Bds amb, Bds pacote, Bds dbg) throws ErrBds   O ambiente de trabalho pode ser apl, bdjava, tst ou uti a partir dai que se  monta o dircomp
	:param amb:
		
	:param pacote:
		
	:param dbg:

.. function:: Cjava.locdbg dbg


	:comentário: static public void locdbg(Bds dbg) throws ErrBds   O ambiente de trabalho pode ser apl, bdjava, tst ou uti a partir  dai que se monta o dircomp
	:param dbg:

.. function:: Cjava.tratajikes stdout


	:comentário: static public void tratajikes(Bds stdout) throws ErrBds  trata a saida do jikes para ter um padrao facil de ser lido
	:param stdout:

.. function:: Cjava.tratajavac stdout


	:comentário: static public void tratajavac(Bds stdout) throws ErrBds  trata a saida do jikes para ter um padrao facil de ser lido
	:param stdout:

.. function:: Cjava.trata_click0 trecho comando sth poscaret total posrel


	:comentário: static public void trata_click0(Bds trecho, Bds comando, Bds sth, Bds poscaret, Bds total, Bds posrel) throws ErrBds  vai tratar e localizar os comandos necessarios para cada trecho clicado
	:param trecho:
		
	:param comando:
		
	:param sth:
		
	:param poscaret:
		
	:param total:
		
	:param posrel:

.. function:: Cjava.edj nomeclasse


	:comentário: static public void edj(Bds nomeclasse) throws ErrBds   vai editar diretamente o nome de uma classe localiza a classe se eh no dbg ou  no src, no ambiente correto e no local correto do pacote
	:param nomeclasse:

.. function:: Cjava.edcmds nomeshell


	:comentário: static public void edcmds(Bds nomeshell) throws ErrBds   vai editar diretamente um shell cmds localiza a classe se eh no dbg ou no  src, no ambiente correto e no local correto do pacote
	:param nomeshell:

.. function:: Cjava.edcmds nomeshell tipo


	:comentário: static public void edcmds(Bds nomeshell, Bds tipo) throws ErrBds   vai editar diretamente um shell cmds localiza a classe se eh no dbg ou no  src, no ambiente correto e no local correto do pacote
	:param nomeshell:
		
	:param tipo:

.. function:: Cjava.ca abs


	:comentário: recompila um abstral a partir de seu arquivo abre este arquivo e localiza o
		abstral desejado
		
	:param abs:

.. function:: Cjava.eda abs


	:comentário: localiza um arquivo que pertence a um abstral, chama o editor, abre este
		arquivo e localiza o abstral desejado
		
	:param abs:

.. function:: Cjava.edb arq0


	:comentário: vai editar diretamente um arquivo ex bdmax .abs ou um fig se nao encontrar.
		busca as opcoes de caminhos em _pathclides
		
	:param arq0:

.. function:: Cjava.edtt arq0


	:comentário: vai editar diretamente um arquivo de tooltip com extensao txt
		
	:param arq0:

.. function:: Cjava.edtel arq0


	:comentário: vai editar diretamente um arquivo .tel . busca as opcoes de caminhos em
		_pathclides
		
	:param arq0:

.. function:: Cjava.edf arq0


	:comentário: vai editar diretamente um arquivo .fig do diretorio users. arq0 - nome do
		arquivo sem a extensao usa os paths que estao em _pathfigs para localizar o
		arquivo
		
	:param arq0:

.. function:: Cjava.edf arq0 ext0


	:comentário: vai editar diretamente um arquivo do diretorio users. arq0 - nome do arquivo
		sem a extensao ext - extensao desejada
		
	:param arq0:
		
	:param ext0:

.. function:: Cjava.edtem arq0


	:comentário: static public void edtem(Bds arq0) throws ErrBds   vai editar diretamente um template localiza a classe se eh no dbg  ou no src, no ambiente correto e no local correto do pacote
	:param arq0:

.. function:: Cjava.edf nomearq


	:comentário: 
		
		vai editar diretamente o nome de um arquivo.
		
		
		

	:param  nomearq: nome do arquivo
		
		

.. function:: Cjava.edb nomearq


	:comentário: 
		
		vai editar diretamente o nome de um arquivo.
		
		
		

	:param  nomearq: nome do arquivo
		
		

.. function:: Cjava.edt nomearq sop linha


	:comentário: static public void edt(Bds nomearq, Bds sop, Bds linha) throws ErrBds   vai editar diretamente o nome de um arquivo. Tem duas opcoes - se op=1 vai  localizar o arquivo no diretorio corrente se for op = 2, vai localizar o  arquivo no diretorio de desenvolvimento que estiver sendo usado
	:param nomearq:
		
	:param sop:
		
	:param linha:

.. function:: Cjava.edt nomearq


	:comentário: 
		
		vai editar diretamente o nome de um arquivo.
		
		
		

	:param  nomearq: nome do arquivo
		
		

.. function:: Cjava.edt nomearq


	:comentário: 
		
		vai editar diretamente o nome de um arquivo.
		
		
		

	:param  nomearq: nome do arquivo
		
		

.. function:: Cjava.edt nomearq sop


	:comentário: 
	:param nomearq:
		
	:param sop:

.. function:: Cjava.getarq nomearq arq


	:comentário: static public void getarq(Bds nomearq, Bds arq) throws ErrBds  monta o nome completo de um arquivo a partir do  nome completo ou o localiza no diretorio corrente
	:param nomearq:
		
	:param arq:

.. function:: Cjava.getarqfonte nomeclasse arq


	:comentário: 
	:param nomeclasse:
		
	:param arq:

.. function:: Cjava.getarqfonte nomeclasse


	:comentário: static public Bds getarqfonte(Bds nomeclasse) throws ErrBds  localiza o arquivo de fum fonte a partir no nome da  classe no diretorio correto
	:param nomeclasse:

.. function:: Cjava.nov nomeclasse


	:comentário: static public void nov(Bds nomeclasse) throws ErrBds  Cria uma nova classe com o nome indicado
	:param nomeclasse:

.. function:: Cjava.nov nomeclasse templatex


	:comentário: static public void nov(Bds nomeclasse, Bds templatex) throws ErrBds  Cria uma nova classe com o nome indicado e do template indicado
	:param nomeclasse:
		
	:param templatex:

.. function:: Cjava.novf nomefig


	:comentário: static public void novf(Bds nomefig) throws ErrBds  Cria uma novo arquivo de configuracao com o nome indicado
	:param nomefig:

.. function:: Cjava.novf nomefig ext


	:comentário: static public void novf(Bds nomefig, Bds ext) throws ErrBds  Cria umnovo arquivo de configuracao, abs, tela ou txt com o nome indicado e a  extensao desejada (fig para .fig, tel para .tel, prm para ,prm, txtpara txt)
	:param nomefig:
		
	:param ext:

.. function:: Cjava.novf nomefig ext0 templatex


	:comentário: static public void novf(Bds nomefig, Bds ext0, Bds templatex) throws ErrBds  Cria umnovo arquivo de configuracao, abs, tela ou txt com o nome indicado e a  extensao desejada (fig para .fig, tel para .tel, prm para ,prm, txtpara txt),  e ainda com o template indicado
	:param nomefig:
		
	:param ext0:
		
	:param templatex:

.. function:: Cjava.novb nomeabs


	:comentário: static public void novb(Bds nomeabs) throws ErrBds  Cria uma novo arquivo de abstrais com o nome indicado
	:param nomeabs:

.. function:: Cjava.atprod inicio fim


	:comentário: 
	:param inicio:
		
	:param fim:

.. function:: Cjava.atprod 


	:comentário: 

.. function:: Cjava.jexec args


	:comentário: static public Bds jexec(Bds args) throws ErrBds  executa o comando ar e devolve um bds com a resposta
	:param args:

.. function:: Cjava.jexec args resp


	:comentário: static public void jexec(Bds args, Bds resp) throws ErrBds  executa um comando em java e exib o resultado se houver
	:param args:
		
	:param resp:

.. function:: Cjava.criaclasse strclass arqclass


	:comentário: public static boolean criaclasse(Bds strclass, Bds arqclass) throws ErrBds  cria uma classe provisoria a partir de uma string
	:param strclass:
		
	:param arqclass:

.. function:: Cjava.montametodo arg nomemet arqclass strclass


	:comentário: public static void montametodo(Bds arg, Bds nomemet, Bds arqclass, Bds strclass) throws ErrBds   monta uma classe de trabalho com um determinado metodo
	:param arg:
		
	:param nomemet:
		
	:param arqclass:
		
	:param strclass:

.. function:: Cjava.bkj op


	:comentário: static public void bkj(Bds op) throws ErrBds  faz backups automaticos para os drives ou cpus backup se existirem do  ambiente que estiver em desenvolvimento Este backup eh feito dentro  das seguintes regras: se op="" - faz do ambiente de desenvolvimento  um determinado diretorio de todos os arquivos que foram modificados e  para o diretorio simetrico dos arquivos mais novos
	:param op:

.. function:: Cjava.bkdir sdir op


	:comentário: static public void bkdir(Bds sdir, Bds op) throws ErrBds  faz backup de um determinado diretorio ja especificado para os _dirs_backup  que estao especificados este backup, se op eh nulo, eh feito automaticamente  quando se sexecuta o uma compilacao ou um edt Este backup nunca 'e feito para  um diretorio de producao
	:param sdir:
		
	:param op:

.. function:: Cjava.tuser 


	:comentário: 

.. function:: Cjava.ckpathscentral 


	:comentário: static public void ckpathscentral() throws ErrBds  testa se o usuario esta cadastrado indica na tela qual o usuario

.. function:: Cjava.gcpx uu


	:comentário: 
	:param uu:

.. function:: Cjava.cuser user


	:comentário: Passa para o usuario indicado. Para criar de fato o usuário, usar "cuser
		'<user>' '1'"
		
		
		

	:param  user: Sigla do usuário indicado
		

.. function:: Cjava.cuser user spodecriar


	:comentário: static public void cuser(Bds user, Bds spodecriar) throws ErrBds   Cria um usuario <user> ou passa todo o sistema para o usuario <user>
	:param user:
		
	:param spodecriar:

.. function:: Cjava.criaUser user


	:comentário: cria um novo usuario, sem mudar a referencia principal de usuario
		
		
		

	:param  user: 
		
		
	:throws:  ErrBds
		

.. function:: Cjava.cprov user


	:comentário: static public void cprov(Bds user) throws ErrBds   a partir do nome de usuario indicado, cria um usuario provisorio  para esta thread
	:param user:

.. function:: Cjava.logcomp 


	:comentário: static public void logcomp() throws ErrBds   faz um log continuo de cada compilacao, quantos metodos sao, linhas, etc

.. function:: Cjava.cvars proj cps user


	:comentário: Esta funcao monta no local do projeto a classe <proj> que são usada para
		criar um objeto que contenha todas as variaveis de estado de modo que seja
		mais facil usar variaveis de estado dentro de funcoes
		
		
		

	:param  proj: nome do projeto (exemplo - c11) - a partir do nome do projeto
		monta o arquivo cpsc11.fig, le este arquivo e para todas as
		variaveis ali definidas, cria um campo correspondente na classe
		Vc11.java que estará sendo criada
		
		

.. function:: Cjava.copiaConf arqOrig arqDest mods


	:comentário: 
	:param arqOrig:
		
	:param arqDest:
		
	:param mods:

.. function:: Cjava.montapaths 


	:comentário: se os paths _pathfigs _pathclides nao existirem, os monta a partir do padrao
		anterior do bdjava (user, + cfg)
		
		
		
		

.. function:: Cjava.marcaarq arq prefix


	:comentário: cria nomearq trocando os tracos de arq por ":" cria uma variavel com nome se
		iniciando com prefix finalizando com nomearq prefix no caso de abs = $$#
		prefix no caso de proc =###
		
		
		
	:param arq:
		
	:param prefix:

.. function:: Cjava.marcaarq arq prefix


	:comentário: cria nomearq trocando os tracos de arq por ":" cria uma variavel com nome se
		iniciando com prefix finalizando com nomearq prefix no caso de abs = $$#
		prefix no caso de proc =###
		
		
		
	:param arq:
		
	:param prefix:

.. function:: Cjava.maisnovo arq prefix


	:comentário: 
		
		
		cria nomearq trocando os tracos de arq por ":" cria uma variavel com nome se
		iniciando com prefix finalizando com nomearq retorna true se o arquivo eh
		maisnovo do que o long que esta na variavel prefix no caso de abs = $$#
		prefix no caso de proc =###
		
		
		
	:param arq:
		
	:param prefix:

.. function:: Cjava.maisnovo arq prefix


	:comentário: cria nomearq trocando os tracos de arq por ":" cria uma variavel com nome se
		iniciando com prefix finalizando com nomearq retorna true se o arquivo eh
		maisnovo do que o long que esta na variavel prefix no caso de abs = $$#
		prefix no caso de proc =###
		
		
		
	:param arq:
		
	:param prefix:

.. function:: Cjava.b2j abs arqJava


	:comentário: Metodo que converte um determinado abstral Bdjava para incorporacao a uma
		classe Java
		
		
		
		
	:param abs:
		
	:param arqJava:

.. function:: Cjava.pacotesAmigos pacotes


	:comentário: 
	:param pacotes:

