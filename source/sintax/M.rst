
M
=
.. function:: M.dely del


	:comentário: seta dely
		
		
		

	:param  del: contem o dely desejado
		
		
	:throws:  ErrBds
		

.. function:: M.prepara tabela


	:comentário: 
		
		1) Verifica se o campo "o" do Bds onde está tabela existe, se não existe,
		cria um vetor "oo[10]" onde estarão os seguintes dados: a. oo[0]=cksum da
		string referente à tabela; b. oo [1]=delx desta tabela; c. oo [2]=dely desta
		tabela; d. oo [3]=qtx : quantidade de campos por linha e. oo [4]=qty: quantidade de linhas; f. oo [5]=1 se houver operação pendente de numeros ou 2
		se houver apenas operações pendentes nos dados g. oo [6]=vetor Bds [][]
		contendo todos os campos da tabela em forma de vetor h. oo [7]=vetor double[]
		contendo o valor numerico de cada campo da tabela i. Oo[8]=vetor com os
		decimais de cada coluna j. oo [9]=vetor com as larguras de cada coluna k. oo
		[10]=vetor com as opções de ajuste (centro, esq, dir) 2) Se o vetor oo
		existe, calcula o cksum da string da tabela e compara com o constante de
		oo[0] para ver se a string foi mexida, se foi, remonta todos os dados de oo.
		3) Se o cksum está ok: a. verifica se oo[6]=1. Se for remonta todos os dados
		de oo[6] a partir dos dados que foram processados em oo[7]. Após isto, a
		tabela é remontada a partir de cada campo constante de oo[6]. b. Se oo[6]=2,
		apenas os dados foram mexidos, e a tabela é remontada a partir dos dados de
		oo[6]. como delx é usado o default '^' como dely é usado o default '\n'
		
		
		

	:param  tabela: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: M.prepara tabela pdelx pdely tipoTab


	:comentário: 
		
		1) Verifica se o campo "o" do Bds onde está tabela existe, se não existe,
		cria um vetor "oo[10]" onde estarão os seguintes dados: a. oo[0]=cksum da
		string referente à tabela; b. oo [1]=delx desta tabela; c. oo [2]=dely desta
		tabela; d. oo [3]=qtx : quantidade de campos por linha e. oo [4]=qty: quantidade de linhas; f. oo [5]=1 se houver operação pendente de numeros ou 2
		se houver apenas operações pendentes nos dados g. oo [6]=vetor Bds [][]
		contendo todos os campos da tabela em forma de vetor h. oo [7]=vetor double[]
		contendo o valor numerico de cada campo da tabela i. Oo[8]=vetor com os
		decimais de cada coluna j. oo [9]=vetor com as larguras de cada coluna k. oo
		[10]=vetor com as opções de ajuste (centro, esq, dir) 2) Se o vetor oo
		existe, calcula o cksum da string da tabela e compara com o constante de
		oo[0] para ver se a string foi mexida, se foi, remonta todos os dados de oo.
		3) Se o cksum está ok: a. verifica se oo[6]=1. Se for remonta todos os dados
		de oo[6] a partir dos dados que foram processados em oo[7]. Após isto, a
		tabela é remontada a partir de cada campo constante de oo[6]. b. Se oo[6]=2,
		apenas os dados foram mexidos, e a tabela é remontada a partir dos dados de
		oo[6].
		
		
		

	:param  tabela: 
		
		

	:param  delx: 
		
		

	:param  dely: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: M.atDouble tabela


	:comentário: atualiza em uma tabela já formatada os dados double a partir dos campos
		
		
		

	:param  tabela: 
		
		
	:throws:  ErrBds
		

.. function:: M.mostra tabela


	:comentário: 
	:param tabela:

.. function:: M.reMontaTab tabela


	:comentário: remonta uma tabela, se precisar
		
		
		

	:param  tabela: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: M.campo 


	:comentário: 

.. function:: M.xmescla tabelaDest tabela1 tabela2 posx


	:comentário: A partir da coluna posx, inclusive, troca o conteudo da tabela pela
		subtabela. Se for no final, insere
		
		
		

	:param  tabelaDest: tabela que vai receber a mesclagem de tab1 e tab2
		
		

	:param  tab1: tab1 que vai se mesclar com tab2
		
		

	:param  tab2: tab2 - quem vai se mesclar com tab1
		
		

	:param  posx: coluna a partir da qual, inclusive , tab2 vai se mesclar com
		tab1. se posx for maior que a qtde de colunas de tab1, tab2 se
		junta à direita de tab1
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: M.xpega tabelaDest tabela1 posx qtcols


	:comentário: 
	:param tabelaDest:
		
	:param tabela1:
		
	:param posx:
		
	:param qtcols:

.. function:: M.xdel tabelaDest posx qtdelcols


	:comentário: Deleta um conjunto de qtdelcols colunas da tabela tabelaDest a partir da
		coluna posx
		
		
		

	:param  tabelaDest: tabela de onde vao ser deletadas colunas
		
		

	:param  posx: posicao da primeira coluna a ser deletada
		
		

	:param  qtdelcols: quantidade de colunas a serem deletadas
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: M.expHtml tabela str argCampo argLinha


	:comentário: 
		
		expHtml tabela str argCampo argLinha
		
		Exporta a tabela para um arquivo 1) str: variavel para onde deve ser
		exportada a tabela 2) argCampo - indica a string que deve ficar entre campos
		3) argLinha - Indica a String que deve ficar entre linhas. Se arLinha
		contiver duas strings separadas por virgula, vai alternar para cada linha uma
		opcao de argLinha (para fazer composicoes do tipo TR1, TR2, por exemplo)
		
		
		

	:param  tabela: tabela a ser exportada
		
		

	:param  str: string que vai receber a exportacao
		
		

	:param  argCampo: string que representa a separacao de cada campo
		
		

	:param  argLinha: string que representa um ou mais separadores de linha separados
		por linha
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: M.criaCor cor percV


	:comentário: 
	:param cor:
		
	:param percV:

.. function:: M.expCsv tabela str delCampo


	:comentário: 
		
		expCsv tabela str argCampo argLinha
		
		Exporta a tabela criando uma string pronta para se montar um arquivoCSV 1)
		str: variavel para onde deve ser exportada a tabela 2) argCampo - indica a
		string que deve ficar entre campos 3) argLinha - Indica a String que deve
		ficar entre linhas. Se arLinha contiver duas strings separadas por virgula,
		vai alternar para cada linha uma opcao de argLinha (para fazer composicoes do
		tipo TR1, TR2, por exemplo)
		
		
		

	:param  tabela: tabela a ser exportada
		
		

	:param  str: string que vai receber a exportacao em csv
		
		

	:param  delCampo: delimitador entre cada campo - o delimitador entre cda linha é o
		"\n"
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: M.configura tabela orientH linhasCorFixa colunasCorFixa ordemCor logica linInic linFim colInic colFim


	:comentário: 
	:param tabela:
		
	:param orientH:
		
	:param linhasCorFixa:
		
	:param colunasCorFixa:
		
	:param ordemCor:
		
	:param logica:
		
	:param linInic:
		
	:param linFim:
		
	:param colInic:
		
	:param colFim:

.. function:: M.sort tabela coluna


	:comentário: 
	:param tabela:
		
	:param coluna:

.. function:: M.sort tabela coluna delColuna delLinha


	:comentário: 
	:param tabela:
		
	:param coluna:
		
	:param delColuna:
		
	:param delLinha:

.. function:: M.tab2Map tab delLin delCol colChave mapResp


	:comentário: O tab2Map é capaza de criar uma tabela em um mapa, utilizando uma coluna ou
		mais como chaves
		
		
		

	:param  tab: tabela de dados
		
		

	:param  delLin: delimitador de linhas
		
		

	:param  delCol: delimitador de colunas
		
		

	:param  colChave: colunas que serão utilizadas como chave separadas por "^"
		
		

	:param  mapResp: variavel de resposta
		
		Exemplo: faz tabela 'cor,amarelo,verde\nfruta,banana,abacate\n'
		
		tab2Map tabela '\n' ',' '1' mapa mapa >>
		{fruta=fruta,banana,abacate, cor=cor,amarelo,verde}
		
		Exemplo: faz tbl
		'carro\tverde\tgol\ncarro\tvermelho\tgol\ncarro\tamarelho\tgol\ncarro\tamarelo\tfiat\ncarro\tverde\tfiat\ncarro\tazul\tfiat\nmoto\tvermelha\thonda\nmoto\tamarela\thonda\nmoto\tpreta\thonda\ncarro\tbranco\thonda\ncarro\tpreto\thonda'
		
		M.tab2Map tbl '\n' '\t' '1,3' mapa_resp {moto^honda=moto preta
		honda|moto amarela honda|moto vermelha honda, carro^honda=carro
		preto honda|carro branco honda, carro^fiat=carro azul fiat|carro
		verde fiat|carro amarelo fiat, carro^gol=carro amarelho gol|carro
		vermelho gol|carro verde gol}
		
		
		
	:rtype: boolean
		

.. function:: M.criaLinhas colunas resp qtd


	:comentário: 
	:param colunas:
		
	:param resp:
		
	:param qtd:

.. function:: M.qualificaTexto tabela del quali resp


	:comentário: 
	:param tabela:
		
	:param del:
		
	:param quali:
		
	:param resp:

.. function:: M.qualificaTexto2 tabela resp


	:comentário: 
	:param tabela:
		
	:param resp:

.. function:: M.trocaPonto tabela resp


	:comentário: 
	:param tabela:
		
	:param resp:

.. function:: M.desqualificaTexto tabela resp


	:comentário: 
	:param tabela:
		
	:param resp:

