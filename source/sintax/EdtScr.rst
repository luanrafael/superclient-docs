
EdtScr
======
.. function:: EdtScr.inic op


	:comentário: 
	:param op:

.. function:: EdtScr.criaDefault nomeCp lista op cp dadx code


	:comentário: cria um cs default em um campo a partir de um nome
		
	:param nomeCp:
		
	:param lista:
		
	:param op:
		
	:param cp:
		
	:param dadx:
		
	:param code:

.. function:: EdtScr.criaCsDefault nomeCp


	:comentário: 
	:param nomeCp:

.. function:: EdtScr.csCriaCampo op cp dadx code


	:comentário: cria um cs default em um campo a partir de um nome
		
	:param op:
		
	:param cp:
		
	:param dadx:
		
	:param code:

.. function:: EdtScr.telaScript 


	:comentário: 

.. function:: EdtScr.telaDefs 


	:comentário: 

.. function:: EdtScr.telaPesq 


	:comentário: 

.. function:: EdtScr.telaCampos 


	:comentário: 

.. function:: EdtScr.telaBotoes 


	:comentário: 

.. function:: EdtScr.popularCs dest lista


	:comentário: 
		
		

	:param  dest: arquivo texto que tem que todos os Cs da Lista
		
		

	:param  lista: tela correspondente onde os cs e pr devem ser lidos e populados
		em dest
		

.. function:: EdtScr.metodosCs dest lista


	:comentário: Atencao - este metodo deve ser deslocado para o DadosSc
		
		
		

	:param  dest: arquivo texto que tem que todos os Cs da Lista
		
		

	:param  lista: tela correspondente onde os cs e pr devem ser lidos e populados
		em dest
		

.. function:: EdtScr.tt cp


	:comentário: 
	:param cp:

.. function:: EdtScr.setaCps lista args


	:comentário: seta os parametros internos dos campos constantes da lista
		
		
		

	:param  lista: lista de campos a serem setados em conjunto
		
		

	:param  args: string com os argumentos desejados
		
		

.. function:: EdtScr.setaCps lista args


	:comentário: seta os parametros internos dos campos constantes da lista
		
		
		

	:param  lista: lista de campos a serem setados em conjunto
		
		

	:param  args: string com os argumentos desejados
		
		

.. function:: EdtScr.modoPesq 


	:comentário: No modo pesq, a edicao dos itens do Scrip está bloqueada e o modo de pesquisa
		liberado
		
		

.. function:: EdtScr.modoEdt 


	:comentário: No modo edt, a edicao dos itens do Scrip está liberada e o modo de pesquisa
		bloqueado
		
		

.. function:: EdtScr.modoRenomeia 


	:comentário: renomear. Apenas se pode mudar os nomes e salvar
		
		

.. function:: EdtScr.csScript op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.csVaiDefs op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.csVoltaScripts op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.csNovoCampo 


	:comentário: 

.. function:: EdtScr.csNovoCampo op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.cscarregaCampo nomeCampo


	:comentário: 
	:param nomeCampo:

.. function:: EdtScr.csCarregaCampo op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.csSalva op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.csSalva 


	:comentário: 

.. function:: EdtScr.bkSalva 


	:comentário: 

.. function:: EdtScr.bkSalva argBK


	:comentário: 
	:param argBK:

.. function:: EdtScr.csPesq op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.csPesq1 op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.csPesqBks op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.pesqScrs ehBks


	:comentário: monta a pesquisa de scripts ou a pesquisa de bks conforme for desejado
		
		
		

	:param  ehBks: se =1 , pesquisa apenas dos registros de backup temporario
		
		

.. function:: EdtScr.csGeraHtml op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.csRemove op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.csRenomear op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.csEditarScript op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.csExecSelecao op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.csExecScript op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.csNovoScript op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.csTrazEdicao op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.csEditarDefs op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.csModoPesq op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.csTemplateLinha op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.csExComp op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.csExMacro op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.csTable op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.csEdtSelecao op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.csPegaScript op cp dado cod


	:comentário: 
	:param op:
		
	:param cp:
		
	:param dado:
		
	:param cod:

.. function:: EdtScr.pegaScript 


	:comentário: pega o script selecionado e o transforma no comando EdtScr.leScript
		nomeScriptTable campanhaTable clienteTable alem disto, chama a tela
		EdtScr.telaCampos
		

.. function:: EdtScr.leScript nomeScript campanha cliente


	:comentário: 
	:param nomeScript:
		
	:param campanha:
		
	:param cliente:

.. function:: EdtScr.limpaBks 


	:comentário: 

.. function:: EdtScr.gravaArq radical cliente campanha nomeScr


	:comentário: 
	:param radical:
		
	:param cliente:
		
	:param campanha:
		
	:param nomeScr:

.. function:: EdtScr.gravaBk 


	:comentário: 

.. function:: EdtScr.gravaBk arqBk cliente campanha nomeScr


	:comentário: Salva um conjunto de registros de Script
		
		
		

	:param  arqBk: nome do arquivo onde vao ser salvos
		
		

	:param  cliente: nome do cliente a que se refere a campanha (colocar % antes ou
		depois se desejar pegar um grupo de registros que contenha a chave
		indicada)
		
		

	:param  campanha: nome da campanha a qual se refere o cliente (colocar % antes ou
		depois se desejar pegar um grupo de registros que contenha a chave
		indicada)
		
		

	:param  nomeScr: nome do script (colocar % antes ou depois se desejar pegar um
		grupo de registros que contenha a chave indicada)
		
		

.. function:: EdtScr.trazBk arqBk cliente campanha nomeScr evitaBkp


	:comentário: traz de um arquivo de log para o banco de dados pode-se usar o conceito de
		para. os argumentos cliente, campanha e nomeScr podem ser usados para isto se
		forem nulos, os registros do arquivo nao sao renomeados.
		
		se existirem , ocorre a renomeacao. cada um deve ser informado do seguinte
		jeito" str1=str2 . Deste modo, todo campos que tiver str1, vai ter str1
		trocado por str2
		
		
		

	:param  arqBk: arquivo de backup de onde pegar o backup
		
		

	:param  cliente: filtro do cliente desejado
		
		

	:param  campanha: filtro da campanha desejado
		
		

	:param  nomeScr: filtro do nome do Script desejado
		
		

	:param  evitaBkp: se nulo ou = 1, nao traz os backups
		
		
		

.. function:: EdtScr.poeBuf cliente campanha nome


	:comentário: coloca mais uma string no buffer verifica se ela esta em algum lugar. se
		estiver, a retira e poe no inicio
		
		
		

	:param  cliente: cliente do script
		
		

	:param  campanha: nome da campanha
		
		

	:param  nome: nome do script
		
		

.. function:: EdtScr.poeBuf cliente campanha nome


	:comentário: coloca mais uma string no buffer verifica se ela esta em algum lugar. se
		estiver, a retira e poe no inicio
		
		
		

	:param  cliente: cliente do script
		
		

	:param  campanha: nome da campanha
		
		

	:param  nome: nome do script
		
		

.. function:: EdtScr.poeCondBuf cliente campanha nome


	:comentário: coloca mais uma string no buffer, condicional, se nao existir antes verifica
		se ela esta em algum lugar. se estiver, a retira e poe no inicio
		
		
		

	:param  cliente: cliente do script
		
		

	:param  campanha: nome da campanha
		
		

	:param  nome: nome do script
		
		

.. function:: EdtScr.poeCondBuf cliente campanha nome


	:comentário: coloca mais uma string no buffer verifica se ela esta em algum lugar. se
		estiver, a retira e poe no inicio
		
		
		

	:param  cliente: cliente do script
		
		

	:param  campanha: nome da campanha
		
		

	:param  nome: nome do script
		
		

.. function:: EdtScr.ctlPgUp cliente campanha nome


	:comentário: 
		
		Volta uma tela para a penultima editada (lembra-se de todas)
		
	:param cliente:
		
	:param campanha:
		
	:param nome:

.. function:: EdtScr.ctlPgDn cliente campanha nome


	:comentário: 
		
		Volta uma tela para a penultima editada (lembra-se de todas)
		
	:param cliente:
		
	:param campanha:
		
	:param nome:

.. function:: EdtScr.pgDn 


	:comentário: 
		
		EVOlui uma tela na sequencia do table resposta da pesquisa
		

.. function:: EdtScr.pgUp 


	:comentário: 
		
		volta uma tela na sequencia do table resposta da pesquisa
		

.. function:: EdtScr.csCtlE 


	:comentário: Chama a edicao com os ultimos campos da tela de Script
		
		
		

.. function:: EdtScr.carregaCampo 


	:comentário: 

.. function:: EdtScr.carregaCampo str


	:comentário: pega o comando de criacao de um campo e monta os dados para sua formatacao
		
		
		

	:param  str: string a ser analisada
		

.. function:: EdtScr.pegaNomesCampos 


	:comentário: pega a string correspondente as definicoes dos campos da definicoes Bds defs
		texto onde estao das definicoes
		
		

.. function:: EdtScr.pegaNomesCampos defs


	:comentário: pega a string correspondente as definicoes dos campos da definicoes Bds defs
		texto onde estao das definicoes
		
		
	:param defs:

.. function:: EdtScr.pegaDefCampo nomeCampo def


	:comentário: pega a definicao de definicoes correspondente a um campo
		
		
		

	:param  nomeCampo: nome do campo do qual se quer a definicao
		
		

	:param  def: definicao que sera obtida
		
		

.. function:: EdtScr.pegaDefCampo defs nomeCampo def


	:comentário: pega a string correspondente a um campo
		
		
		

	:param  defs: definicoes onde se vai pegar o campo
		
		

	:param  nomeCampo: nome do campo do qual se quer a definicao
		
		

	:param  def: definicao que sera obtida
		

.. function:: EdtScr.poeDefCampo nomeCampo def


	:comentário: poe em defs as a definicao do campo
		
		
		

	:param  defs: string que ira receber a definicao do campo
		
		

	:param  nomeCampo: nome do campo
		
		

	:param  def: definicao do campo
		
		

.. function:: EdtScr.poeDefCampo defs nomeCampo def


	:comentário: poe em defs as a definicao do campo
		
		
		

	:param  defs: string que ira receber a definicao do campo
		
		

	:param  nomeCampo: nome do campo
		
		

	:param  def: definicao do campo
		
		

.. function:: EdtScr.montaStrCampo 


	:comentário: pega o comando de criacao de um campo da area de trabalho e monta os dados
		para sua formatacao
		

.. function:: EdtScr.montaStrCampo str


	:comentário: pega o comando de criacao de um campo e monta os dados para sua formatacao
		
		
		

	:param  str: string a ser analisada
		

.. function:: EdtScr.atStrCampo 


	:comentário: Atualiza a string de um campo na str def
		

.. function:: EdtScr.atStrCampo defs


	:comentário: Atualiza a string de um campo na str def
		
	:param defs:

.. function:: EdtScr.limpaDefCampo listaCampos


	:comentário: limpa a definicao de um campo de caracteres que nao podem existir
		
	:param listaCampos:

.. function:: EdtScr.troca 


	:comentário: processa a troca de strings no tela em edicao ou entao em todo um resultado
		das pesquisas
		
		

.. function:: EdtScr.deleta 


	:comentário: deleta as strings da selecao
		
		

.. function:: EdtScr.inicCriaCmd 


	:comentário: 

.. function:: EdtScr.criaCmd cmd dicas


	:comentário: coloca na area de trabalho uma string correspondente ao comando que se deseja
		criar
		
		
		

	:param  cmd: string do comando que sugere o que por
		
		

	:param  dicas: dicas para a montagem do comando
		
		
	:throws:  ErrBds
		

.. function:: EdtScr.execCmd cmd dicas


	:comentário: Se prepara para executar um comando com possibilidade de oferecer um help
		
		
		

	:param  cmd: string do comando que sugere o que por
		
		

	:param  dicas: dicas para a execucao do comando
		
		
	:throws:  ErrBds
		

.. function:: EdtScr.trazLinha 


	:comentário: 

.. function:: EdtScr.getCmdxxx cmd parq


	:comentário: Localiza um determinado comando em um arquivo de documentacao
		
		
		

	:param  cmd: string referencia por onde se deseja recuperar o comando
		
		
	:throws:  ErrBds
		

.. function:: EdtScr.getCmdxx cmd parq


	:comentário: Localiza um determinado comando em um arquivo de documentacao
		
		
		

	:param  cmd: string referencia por onde se deseja recuperar o comando
		
		
	:throws:  ErrBds
		

.. function:: EdtScr.prepTextoLinha 


	:comentário: 

