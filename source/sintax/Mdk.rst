
Mdk
===
.. function:: Mdk.env resp arg


	:comentário: 
	:param resp:
		
	:param arg:

.. function:: Mdk.env resp arg timeout


	:comentário: 
	:param resp:
		
	:param arg:
		
	:param timeout:

.. function:: Mdk.teclas args[]


	:comentário: 
	:param args[]:

.. function:: Mdk.jan janela


	:comentário: busca uma janela e dá foco nela retorna true se existe
		
		
		

	:param  janela: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Mdk.jan janela


	:comentário: busca uma janela e dá foco nela retorna true se existe
		
		
		

	:param  janela: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Mdk.copiaTrab str


	:comentário: copia para a area de trabalho o conteudo de str
		
		
		

	:param  str: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Mdk.copiaTrab str


	:comentário: copia para a area de trabalho o conteudo de str
		
		
		

	:param  str: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Mdk.trazTrab str


	:comentário: traz para a area de trabalho o conteudo de str
		
		
		

	:param  str: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Mdk.dimensiona lx ly jan


	:comentário: Dimensiona a janela de nome jan com a largura lx e altura ly
		
		
		

	:param  lx: largura
		
		

	:param  ly: altura
		
		

	:param  jan: nome da janela
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Mdk.posiciona px py jan


	:comentário: posiciona a janela de nome jan com a largura px e altura py
		
		
		

	:param  px: largura
		
		

	:param  py: altura
		
		

	:param  jan: nome da janela
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Mdk.posDim posx posy largura altura pnomeJanela


	:comentário: posiciona a janela de nome jan com a largura px e altura py
		
		
		

	:param  px: largura
		
		

	:param  py: altura
		
		

	:param  jan: nome da janela
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Mdk.cursor px py jan


	:comentário: devolve em px e py a posicao do cursor dentro da janela jan
		
		
		

	:param  px: 
		
		

	:param  py: 
		
		

	:param  jan: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Mdk.cursor px py


	:comentário: devolve em px e py a posicao do cursor em relacao à tela do computador
		
		
		

	:param  px: 
		
		

	:param  py: 
		
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Mdk.click px py jan


	:comentário: da um click de mouse na posicao indicada
		
		
		

	:param  px: 
		
		

	:param  py: 
		
		

	:param  jan: nome da janela
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Mdk.click px py


	:comentário: da um click de mouse na posicao absoluta da tela
		
		
		

	:param  px: 
		
		

	:param  py: 
		
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Mdk.dirClick px py jan


	:comentário: da um click com o botao direito do mouse na posicao indicada
		
		
		

	:param  px: 
		
		

	:param  py: 
		
		

	:param  jan: nome da janela
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Mdk.dirClick px py


	:comentário: da um click com o botao direito do mouse na posicao absoluta da tela
		
		
		

	:param  px: 
		
		

	:param  py: 
		
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Mdk.dClick px py jan


	:comentário: da um duplo Click de mouse na posicao indicada
		
		
		

	:param  px: 
		
		

	:param  py: 
		
		

	:param  jan: nome da janela
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Mdk.dClick px py


	:comentário: da um duplo Click de mouse na posicao absoluta da tela
		
		
		

	:param  px: 
		
		

	:param  py: 
		
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Mdk.pegaHandlers listaHandlers jan


	:comentário: pega os handlers de janelas que contem a string jan
		
		
		

	:param  listaHandlers: devolve a lista de handlers
		
		

	:param  jan: string informada para a pesquisa
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Mdk.focaHandler handler


	:comentário: faz aparecer uma janela oculta que corresponda ao handler informado
		
		
		

	:param  handler: handler correspondente à janela oculta
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Mdk.ocultaJan handler


	:comentário: faz oculta uma janela que corresponda ao handler informado
		
		
		

	:param  handler: handler correspondente à janela
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

