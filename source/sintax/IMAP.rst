
IMAP
====
.. function:: IMAP.setProp propriedades nome valor


	:comentário: 
		
		Método responsável por setar propriedades
		
		Ex: IMAP.setProp propriedades 'mail.imap.socketFactory.port' '993'
		
		
		

	:param  propriedades: variável que armazenará as propriedades
		
		

	:param  nome: nome da propriedade
		
		

	:param  valor: valor da propriedade
		
		
		
	:rtype: void
		
		
		
	:author:  Luan Rafael
		
		

.. function:: IMAP.imap propriedades host usuario senha imapStore


	:comentário: 
		
		Método responsável por criar uma conexão IMAP utilizando storeType "imap"
		
		Esse método usa como padrão a porta 143, default para conexão "imap"
		
		Ex: IMAP.imap propriedades 'imap.gmail.com' 'email@gmail.com' '1234'
		imap_store
		
		
		

	:param  propriedades: propriedades da conexão
		
		

	:param  host: endereço de conexão Ex: imap.google.com
		
		

	:param  usuario: nome do usuário
		
		

	:param  senha: senha do usuário
		
		

	:param  imapStore: variável de resposta, retorna a conexão
		
		
		
	:rtype: boolean
		
		
		
	:author:  Luan Rafael
		
		

.. function:: IMAP.imaps propriedades host usuario senha imapStore


	:comentário: 
		
		Método responsável por criar uma conexão IMAP utilizando storeType "imaps",
		utiliza ssl
		
		Esse método usa como padrão a porta 993, default para conexão "imap"
		
		Ex: IMAP.imaps propriedades 'imap.gmail.com' 'email@gmail.com' '1234'
		imap_store
		
		
		

	:param  propriedades: propriedades da conexão
		
		

	:param  host: endereço de conexão Ex: imap.google.com
		
		

	:param  usuario: nome do usuário
		
		

	:param  senha: senha do usuário
		
		

	:param  imapStore: variável de resposta, retorna a conexão
		
		
		
	:rtype: boolean
		
		
		
	:author:  Luan Rafael
		
		

.. function:: IMAP.conectar propriedades storeType host porta usuario senha imapStore


	:comentário: 
	:param propriedades:
		
	:param storeType:
		
	:param host:
		
	:param porta:
		
	:param usuario:
		
	:param senha:
		
	:param imapStore:

.. function:: IMAP.listarPastas imapStore pastasResp


	:comentário: Método responsável por listar as pastas de uma conexão IMAP
		
		Ex: IMAP.listarPastas imap_store pastas
		
		
		

	:param  imapStore: variável de controle contendo a conexão IMAP
		
		

	:param  pastasResp: variável de resposta, contém lista de pastas encontradas no
		servidor
		
		
		
	:rtype: boolean
		
		
		
	:author:  Luan Rafael
		
		

.. function:: IMAP.abrirPasta imapStore nomePasta pasta


	:comentário: 
		
		Método responsável por abrir um pasta do servidor IMAP
		
		Ex: IMAP.abrirPasta imap_store 'inbox' pasta
		
		
		

	:param  imapStore: variável de controle contendo a conexão IMAP
		
		

	:param  nomePasta: pasta a ser aberta no servidor
		
		

	:param  pasta: variável de resposta, contendo a pasta aberta no servidor
		
		
		
	:rtype: boolean
		
		
		
	:author:  Luan Rafael
		
		

.. function:: IMAP.addFiltro filtro tipo valor


	:comentário: 
	:param filtro:
		
	:param tipo:
		
	:param valor:

.. function:: IMAP.listarNovasMensagens pasta filtro resp respQtd


	:comentário: 
	:param pasta:
		
	:param filtro:
		
	:param resp:
		
	:param respQtd:

.. function:: IMAP.pegarMensagem mensagens mensagem indice


	:comentário: 
		
		Método responsável por capturar uma mensagem da lista de mensagens
		
		Ex: IMAP.pegarMensagem mensagens mensagem 1
		
		
		

	:param  mensagens: lista de mensagens
		
		

	:param  mensagem: variável de resposta
		
		

	:param  indice: indice da mensagem que deseja pegar
		
		
		
	:rtype: boolean
		
		
		
	:author:  Luan Rafael
		
		

.. function:: IMAP.marcarComoLido mensagem


	:comentário: 
		
		Método responsável por marcar uma mensagem como lida.
		
		Ex: IMAP.marcarComoLido mensagem
		
		
		

	:param  mensagem: variável de retorno do método "pegarMensagem"
		
		
		
	:rtype: boolean
		
		
		
	:author:  Luan Rafael
		
		

.. function:: IMAP.marcarNaoComoLido mensagem


	:comentário: 
		
		Método responsável por marcar uma mensagem como NAO lida.
		
		Ex: IMAP.marcarComoLido mensagem
		
		
		

	:param  mensagem: variável de retorno do método "pegarMensagem"
		
		
		
	:rtype: boolean
		
		
		
	:author:  Luan Rafael
		
		

.. function:: IMAP.pegarConteudo mensagem resp


	:comentário: 
		
		Método responsável por marcar uma mensagem como lida.
		
		Ex: IMAP.pegarConteudo mensagem resposta
		
		
		

	:param  mensagem: variável de retorno do método "pegarMensagem"
		
		

	:param  resp: variável de resposta, contendo o conteudo do email
		
		
		
	:rtype: boolean
		
		
		
	:author:  Luan Rafael
		
		

.. function:: IMAP.pegarConteudo mensagem resp opHtml


	:comentário: 
		
		Método responsável por marcar uma mensagem como lida.
		
		Ex: IMAP.pegarConteudo mensagem resposta '1'
		
		
		

	:param  mensagem: variável de retorno do método "pegarMensagem"
		
		

	:param  resp: variável de resposta, contendo o conteudo do email
		
		

	:param  opHtml: se 1, retorna Html se MimeType for text/html
		
		
	:rtype: boolean
		
		
		
	:author:  Luan Rafael
		
		

