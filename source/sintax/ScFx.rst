
ScFx
====
.. function:: ScFx.aguarda idLock


	:comentário: aguarda o idLOck anterior destravar ou entao, destrava por timeout
		
		
		

	:param  idLock: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: ScFx.aguarda idLock beep


	:comentário: aguarda o idLOck anterior destravar ou entao, destrava por timeout
		
		
		

	:param  idLock: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: ScFx.trava idLock op timeout


	:comentário: trava o Superclide para o processo com a identficacao fornecida por idLock se
		op=1 Se ja estiver travado e for de idLock diferente, aguarda destravar pelo
		Idatual
		
		
		

	:param  idLock: 
		
		

	:param  op: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: ScFx.trava idLock


	:comentário: mostra os dados do travamento
		
		
		
	:throws:  ErrBds
		
	:param idLock:

.. function:: ScFx.inic 


	:comentário: 

.. function:: ScFx.chamaMenu pilha niv


	:comentário: 
	:param pilha:
		
	:param niv:

.. function:: ScFx.geraArvore pdirBase parqScl


	:comentário: 
	:param pdirBase:
		
	:param parqScl:

.. function:: ScFx.geraComandos pcomandos parqsScl


	:comentário: 
	:param pcomandos:
		
	:param parqsScl:

.. function:: ScFx.procDirScl pdirBase comandos


	:comentário: 
	:param pdirBase:
		
	:param comandos:

.. function:: ScFx.ScStart 


	:comentário: 

.. function:: ScFx.ScTest 


	:comentário: 

.. function:: ScFx.focaMenu 


	:comentário: 

.. function:: ScFx.exibeTit 


	:comentário: seta um novo titulo para a janela
		

.. function:: ScFx.titulo titulo


	:comentário: customiza o titulo do SuperClient
		
	:param titulo:

.. function:: ScFx.clickMouse 


	:comentário: Opcao "." (ponto)
		
		Click do mouse
		

.. function:: ScFx.rightClick 


	:comentário: Opcao "." (ponto)
		
		Click do mouse
		

.. function:: ScFx.opcaoA 


	:comentário: atualiza o campo do abstral (scrip) na tela do projeto a partir dos comandos
		acumulados
		
		
		
	:throws:  ErrBds
		

.. function:: ScFx.opcaoC 


	:comentário: copia de um campo em uma determinada janela e o processa como string
		
		
		

	:param: 
		
		
	:throws:  ErrBds
		

.. function:: ScFx.opcaoD 


	:comentário: Pega pega uma string e a coloca no buffer, se for o caso
		
		
		
	:throws:  ErrBds
		

.. function:: ScFx.opcaoD0 


	:comentário: Pega pega uma string e a coloca no buffer, se for o caso
		
		
		
	:throws:  ErrBds
		

.. function:: ScFx.opcaoE 


	:comentário: opcaoE Simula tecla de Escape
		

.. function:: ScFx.opcaoF 


	:comentário: opcao F fecha um comando, bufferSc para o absAcc, se estiver em modo de
		gravacao, transfere para o aplicativo limpa o bufferSc
		
		ele eh chamado sempre que se quizer fechar, se queira usar o mouse ou entap
		transfere o
		

.. function:: ScFx.opcaoG 


	:comentário: 

.. function:: ScFx.opcaoH 


	:comentário: opcao H
		
		mostra e apresenta o help
		
		

.. function:: ScFx.opcaoJ 


	:comentário: opcao J
		
		Permite mudar / editar a janela atual do aplicativo onde se estiver
		trabalhando
		
		

.. function:: ScFx.opcaoL 


	:comentário: opcao L
		
		localiza um arquivo e o processa para gravacao ou para edicao
		

.. function:: ScFx.pegaArquivo 


	:comentário: pega um determinado arquivo e o envia para o buffer e o grava eventualmente
		
		
		
	:throws:  ErrBds
		

.. function:: ScFx.opcaoN 


	:comentário: opcao N volta para o modo normal sem edicao
		
		
		

.. function:: ScFx.opcaoP 


	:comentário: Abre a tela de gerencia de projeto
		
		
		
	:throws:  ErrBds
		

.. function:: ScFx.opcaoS 


	:comentário: opcao S volta para o modo sem gravacao
		
		
		

.. function:: ScFx.opcaoT 


	:comentário: opcao T
		
		simula o envio do TAB
		
		

.. function:: ScFx.opcaoU 


	:comentário: Cria uma pausa
		
		
		
	:throws:  ErrBds
		

.. function:: ScFx.opcaoV 


	:comentário: Traz da area de trabalho uma string para o processamento
		
		
		
	:throws:  ErrBds
		

.. function:: ScFx.opcaoW 


	:comentário: Espera por determinada janela
		
		
		
	:throws:  ErrBds
		

.. function:: ScFx.fechaCmd 


	:comentário: fecha um comando, bufferSc para o absAcc, se estiver em modo de gravacao,
		transfere para o aplicativo limpa o bufferSc
		
		ele eh chamado sempre que se quizer fechar, se queira usar o mouse ou entap
		transfere o
		

.. function:: ScFx.criaTela nomeTela arqTela


	:comentário: 
	:param nomeTela:
		
	:param arqTela:

.. function:: ScFx.montaJson json arqTela


	:comentário: 
	:param json:
		
	:param arqTela:

.. function:: ScFx.montaJson0 json comandos


	:comentário: 
	:param json:
		
	:param comandos:

.. function:: ScFx.getCursor tipoClick


	:comentário: 
	:param tipoClick:

.. function:: ScFx.getCursor 


	:comentário: 

.. function:: ScFx.getCursor x y


	:comentário: 
	:param x:
		
	:param y:

.. function:: ScFx.click xx yy


	:comentário: 
	:param xx:
		
	:param yy:

.. function:: ScFx.dirClick xx yy


	:comentário: 
	:param xx:
		
	:param yy:

.. function:: ScFx.dClick xx yy


	:comentário: 
	:param xx:
		
	:param yy:

.. function:: ScFx.rClick xx yy


	:comentário: 
	:param xx:
		
	:param yy:

.. function:: ScFx.sendTab qt


	:comentário: Envia uma quantidade de tabs para o sendEdt
		
		
		

	:param  qt: quantidade de tabs desejados
		

.. function:: ScFx.sendEdt str


	:comentário: 
	:param str:

.. function:: ScFx.sendEdt str


	:comentário: 
	:param str:

.. function:: ScFx.sendV args


	:comentário: 
	:param args:

.. function:: ScFx.sendV str janela


	:comentário: 
		
		envia uma string sem conversao no journaling, via CTL-V no para a janela
		indicada r Para ser usado no sentido de fugir de um pretatamento do
		journaling
		
		
		

	:param  str: string que se deseja escrever ipssis literis na janela desejada
		
		

	:param  janela: nome da janela
		

.. function:: ScFx.sendV str janela


	:comentário: 
		
		envia uma string sem conversao no journaling, via CTL-V no para a janela
		indicada r Para ser usado no sentido de fugir de um pretatamento do
		journaling
		
		
		

	:param  str: string que se deseja escrever ipssis literis na janela desejada
		
		

	:param  janela: nome da janela
		

.. function:: ScFx.setTxt cp tela


	:comentário: seta um texto em um campo - normalmente chama o DadosSc.setTxt Pode estar
		desabilitado se janHabilitada estiver falsa
		
		
		

	:param  cp: 
		
		

	:param  tela: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: ScFx.iconifica icon


	:comentário: 
	:param icon:

.. function:: ScFx.isShowing; bloq


	:comentário: public static boolean isShowing;  public static int ind;    Se bloq = 1, liga o flag flagBloqueia que impede o anstral que compila a  arvore chamar a si mesmo
		

	:param  bloq: 
		
	:throws:  ErrBds

.. function:: ScFx.dentro 


	:comentário: 
		
		
		
	:rtype: boolean  verdadeiro se a arvore estiver dentro de um processo de compilacao
		
		
	:throws:  ErrBds
		

.. function:: ScFx.pegaDados dados args nomeJanela


	:comentário: Pega os dados que estao em um determinado campo onde está o cursor e os
		coloca em dados, isto para uma determinada janela
		
		
		

	:param  dados: recebe os dados desejados
		
		

	:param  args: argumentos a serem usados para se "copiar os dados " (em alguns
		casos somente o CTL-C, e em outros uma sequencia mais complicada
		de teclas
		
		

	:param  nomeJanela: nome da janela onde esta o campo de onde se deseja copiar
		
		
	:rtype: boolean  verdadeiro de for recuperado pelo menos um caractere
		
		
	:throws:  ErrBds
		

.. function:: ScFx.maximiza janela


	:comentário: Maximiza uma janela
		
		
		

	:param  janela: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: ScFx.minimiza janela


	:comentário: Minimiza uma janela
		
		
		

	:param  janela: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: ScFx.restaura janela


	:comentário: restaura uma janela para o seu tamanho normal
		
		
		

	:param  janela: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: ScFx.maximiza janela


	:comentário: Maximiza uma janela
		
		
		

	:param  janela: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: ScFx.minimiza janela


	:comentário: Minimiza uma janela
		
		
		

	:param  janela: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: ScFx.restaura janela


	:comentário: restaura uma janela para o seu tamanho normal
		
		
		

	:param  janela: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: ScFx.arrumaJson json


	:comentário: 
	:param json:

.. function:: ScFx.arrumaJson json ident


	:comentário: 
	:param json:
		
	:param ident:

