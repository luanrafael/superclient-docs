
Sql
===
.. function:: Sql.conecta pcon


	:comentário: 
	:param pcon:

.. function:: Sql.conecta con bd user pass base


	:comentário: 
	:param con:
		
	:param bd:
		
	:param user:
		
	:param pass:
		
	:param base:

.. function:: Sql.conecta con bd user pass base tembk


	:comentário: 
	:param con:
		
	:param bd:
		
	:param user:
		
	:param pass:
		
	:param base:
		
	:param tembk:

.. function:: Sql.conectaBdv strCon


	:comentário: Gerencia a alocacao dinamica de conectores para uma base de dados
		
		
		

	:param  strCon: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		
		Composição atual do con - ver definicao
		

.. function:: Sql.conectaBdv strCon forca


	:comentário: Gerencia a alocacao dinamica de conectores para uma base de dados
		
		
		

	:param  strCon: 
		
		

	:param  forca,: se = 1, forca uma nova conexao
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		
		Composição atual do con - ver definicao
		

.. function:: Sql.setaParamBdv nomeBase qtPerm qtMax


	:comentário: Seta as qtdes de conexoes permanentes para uma base de dados e tambem a qtde
		maxima de conexoes
		
		
		

	:param  nomeConexao: Nome da conexao usada (Base), por exemplo "LogV", para a base de
		dados com acesso virtual
		
		

	:param  qtPerm: Quantidade maxima de conexoes permanentes que pode ter
		(default:1)
		
		

	:param  qtMax: Qtde maxima de conexoes possiveis. (default 3 )
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Sql.mostraConv nomeBase


	:comentário: Mostra todos os conectores de uma base de dados
		
		
		

	:param  nome: referencia base
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		
		Composição atual do con - ver definicao no metodo conecta
		
		
		

.. function:: Sql.limpaBdv nomeBase tout


	:comentário: Fecha os conectores existentes na pilha virtual que estejam com um timeout
		acima de tout segundos
		
		
		

	:param  nome: referencia da base que deverá ser "limpa" dos conectores que
		estejam abertos sem atividade
		
		

	:param  tout: tempo limite em segundos em que o conector poderá estar aberto
		sem atividade
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		
		Composição atual do con - ver definicao no metodo conecta
		
		
		

.. function:: Sql.strcriatab lista0 nometab strtab


	:comentário: 
	:param lista0:
		
	:param nometab:
		
	:param strtab:

.. function:: Sql.strcriatab lista0 nometab strtab tipo


	:comentário: 
	:param lista0:
		
	:param nometab:
		
	:param strtab:
		
	:param tipo:

.. function:: Sql.strinstab lista0 nometab strtab tipo


	:comentário: 
	:param lista0:
		
	:param nometab:
		
	:param strtab:
		
	:param tipo:

.. function:: Sql.criaWhere lista0 strWhere funcao conteudo logica


	:comentário: 
	:param lista0:
		
	:param strWhere:
		
	:param funcao:
		
	:param conteudo:
		
	:param logica:

.. function:: Sql.xsql con resp arg


	:comentário: static public boolean xsql(Bds con, Bds resp, Bds arg) throws ErrBds    executa o comando arg em cima do banco de dados bd o argumento do comando sql  esta em arg, retorna verdadeiro se o comando foi bem sucedido e se no caso de  update, delete, etc conseguir agir em pelo menos uma linha no caso de insert  update e delete, resp retorna a qt de linhas inseridas, atualizadas ou  deletadas   O commit eh automatico
	:param con:
		
	:param resp:
		
	:param arg:

.. function:: Sql.txsql con


	:comentário: 
	:param con:

.. function:: Sql.xsql pcon resp arg commit


	:comentário: static public boolean xsql(Bds pcon, Bds resp, Bds arg, Bds commit) throws ErrBds    executa o comando arg em cima do banco de dados bd o argumento do comando sql  esta em arg, retorna verdadeiro se o comando foi bem sucedido e se no caso de  update, delete, etc conseguir agir em pelo menos uma linha no caso de insert  update e delete, resp retorna a qt de linhas inseridas, atualizadas ou  deletadas   commit="S" - da um commit automatico
	:param pcon:
		
	:param resp:
		
	:param arg:
		
	:param commit:

.. function:: Sql.query pcon cursor arg


	:comentário: static public boolean query(Bds pcon, Bds cursor, Bds arg) throws ErrBds    executa o comando arg em cima do banco de dados bd o argumento do comando sql  esta em arg, retorna verdadeiro se o comando foi bem sucedido e se no caso de  update, delete, etc conseguir agir em pelo menos uma linha no caso de insert  update e delete, resp retorna a qt de linhas inseridas, atualizadas ou  deletadas   commit="S" - da um commit automatico
	:param pcon:
		
	:param cursor:
		
	:param arg:

.. function:: Sql.query pcon arg


	:comentário: 
	:param pcon:
		
	:param arg:

.. function:: Sql.update con resp tab lista select


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:

.. function:: Sql.update con resp tab lista select tudo


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:
		
	:param tudo:

.. function:: Sql.update pcon resp tab lista select tudo


	:comentário: 
	:param pcon:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:
		
	:param tudo:

.. function:: Sql.insert pcon resp tab lista


	:comentário: 
	:param pcon:
		
	:param resp:
		
	:param tab:
		
	:param lista:

.. function:: Sql.insUpd pcon resp tab lista pselect


	:comentário: 
	:param pcon:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param pselect:

.. function:: Sql.insUpd pcon resp tab lista select tudo


	:comentário: 
	:param pcon:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:
		
	:param tudo:

.. function:: Sql.insUpd pcon resp tab lista select tudo


	:comentário: 
	:param pcon:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:
		
	:param tudo:

.. function:: Sql.add pcon resp tab lista select


	:comentário: 
	:param pcon:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:

.. function:: Sql.delete pcon resp tab select


	:comentário: 
	:param pcon:
		
	:param resp:
		
	:param tab:
		
	:param select:

.. function:: Sql.select con resp select qtMax


	:comentário: 
	:param con:
		
	:param resp:
		
	:param select:
		
	:param qtMax:

.. function:: Sql.select con resp select primeiro qt


	:comentário: 
	:param con:
		
	:param resp:
		
	:param select:
		
	:param primeiro:
		
	:param qt:

.. function:: Sql.select con resp tab lista select sop


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:
		
	:param sop:

.. function:: Sql.select pcon resp tab lista select sop


	:comentário: 
	:param pcon:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:
		
	:param sop:

.. function:: Sql.pegaTitulos pcon titulos


	:comentário: Pega os titulos previstos para a saida da query
		
		
		

	:param  con: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Sql.setaResp cp sqlResp


	:comentário: 
	:param cp:
		
	:param sqlResp:

.. function:: Sql.teste con select qt


	:comentário: 
	:param con:
		
	:param select:
		
	:param qt:

.. function:: Sql.fechaBdv pcon


	:comentário: Fecha virtualmente o conector con (ele passa para fechado, e o conector real
		é posto como disponivel no mapa correspondente) usa o conector para localizar
		
		
		

	:param  con: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Sql.fechaBdv pcon nomeCon th


	:comentário: Fecha virtualmente o conector con (ele passa para fechado, e o conector real
		é posto como disponivel no mapa correspondente) usa o nome da variavel
		conector e a thread IMportante - este comando fecha sempre e se o conector
		nao estiver valido, o libera para outro uso
		
		
		

	:param  nomeCom: nome da variavel conector - exemplo: "con"
		
		

	:param  th: thread onde está o conector
		
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Sql.existeBaseVirtual nomeBase


	:comentário: informa se a base de dados nomeBase tem uma base virtual
		
		
		

	:param  nomeBase: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Sql.bdsBaseVirtual nomeBase


	:comentário: devolve o Bds de uma base de dados virtual
		
		
		

	:param  nomeBase: 
		
		
	:rtype: Bds
		
		
	:throws:  ErrBds
		

.. function:: Sql.fechaBd con


	:comentário: fecha a conexao de banco de dados correspondente a con
		
		
		

	:param  con: conector da conexao a ser fechado
		

.. function:: Sql.fecha pcon


	:comentário: 
	:param pcon:

.. function:: Sql.commit pcon


	:comentário: 
	:param pcon:

.. function:: Sql.rollback pcon


	:comentário: 
	:param pcon:

.. function:: Sql.setaCommit pcon temCommit


	:comentário: 
	:param pcon:
		
	:param temCommit:

.. function:: Sql.conectado pcon


	:comentário: 
	:param pcon:

.. function:: Sql.t sqt


	:comentário: 
	:param sqt:

.. function:: Sql.tcon pcon


	:comentário: 
	:param pcon:

.. function:: Sql.Opersql con resp tab lista select oper


	:comentário: 
	:param con:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:
		
	:param oper:

.. function:: Sql.Opersql0 pcon resp tab campo select oper


	:comentário: 
	:param pcon:
		
	:param resp:
		
	:param tab:
		
	:param campo:
		
	:param select:
		
	:param oper:

.. function:: Sql.existe pcon tab select


	:comentário: 
	:param pcon:
		
	:param tab:
		
	:param select:

.. function:: Sql.count pcon resp tab campo select


	:comentário: 
	:param pcon:
		
	:param resp:
		
	:param tab:
		
	:param campo:
		
	:param select:

.. function:: Sql.sum pcon resp tab lista select


	:comentário: 
	:param pcon:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:

.. function:: Sql.avg pcon resp tab lista select


	:comentário: 
	:param pcon:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:

.. function:: Sql.max pcon resp tab lista select


	:comentário: 
	:param pcon:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:

.. function:: Sql.min pcon resp tab lista select


	:comentário: 
	:param pcon:
		
	:param resp:
		
	:param tab:
		
	:param lista:
		
	:param select:

.. function:: Sql.criaerro arg


	:comentário: 
	:param arg:

.. function:: Sql.criaerro arg


	:comentário: 
	:param arg:

.. function:: Sql.ledad chave dado


	:comentário: 
	:param chave:
		
	:param dado:

.. function:: Sql.gravadad chave dado


	:comentário: 
	:param chave:
		
	:param dado:

.. function:: Sql.trataerrosql con resp arg


	:comentário: 
	:param con:
		
	:param resp:
		
	:param arg:

.. function:: Sql.trataerrosql con resp arg


	:comentário: 
	:param con:
		
	:param resp:
		
	:param arg:

.. function:: Sql.acertalista lista


	:comentário: 
	:param lista:

.. function:: Sql.serv 


	:comentário: 

.. function:: Sql.rxsql blocrec resp


	:comentário: 
	:param blocrec:
		
	:param resp:

.. function:: Sql.rselect con resp arg


	:comentário: 
	:param con:
		
	:param resp:
		
	:param arg:

.. function:: Sql.pegaupd codupd


	:comentário: 
	:param codupd:

.. function:: Sql.sendbk con cmd args


	:comentário: 
	:param con:
		
	:param cmd:
		
	:param args:

.. function:: Sql.gravabk con


	:comentário: 
	:param con:

.. function:: Sql.gravalog 


	:comentário: 

.. function:: Sql.locultlog arqlog fp linha seq


	:comentário: 
	:param arqlog:
		
	:param fp:
		
	:param linha:
		
	:param seq:

.. function:: Sql.ckCon diff


	:comentário: 
	:param diff:

.. function:: Sql.limpaCon 


	:comentário: 

.. function:: Sql.emsg arg


	:comentário: 
	:param arg:

.. function:: Sql.emsg arg


	:comentário: 
	:param arg:

.. function:: Sql.emsg arg gravidade


	:comentário: 
	:param arg:
		
	:param gravidade:

.. function:: Sql.emsg arg tipo help gravidade


	:comentário: 
	:param arg:
		
	:param tipo:
		
	:param help:
		
	:param gravidade:

.. function:: Sql.emsg arg tipo help gravidade


	:comentário: 
	:param arg:
		
	:param tipo:
		
	:param help:
		
	:param gravidade:

.. function:: Sql.pqTab con resp varMap arg


	:comentário: 
	:param con:
		
	:param resp:
		
	:param varMap:
		
	:param arg:

.. function:: Sql.pqTab con resp varMap arg tempo


	:comentário: 
	:param con:
		
	:param resp:
		
	:param varMap:
		
	:param arg:
		
	:param tempo:

.. function:: Sql.bd2arq con nomeTab listaTab select parquivo


	:comentário: carrega uma tabela para persistir em um arquivo. Este arquivo será lido
		depois para atualizar um google map
		
		
		

	:param  con: conector do banco de dados ja inicializado
		
		

	:param  nomeTab: nome da tabela
		
		

	:param  listaTab: 
		
		

	:param  select: 
		
		

	:param  chaves: 
		
		

	:param  arquivo: 
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Sql.acumula con acc


	:comentário: marca se acumula ou nao comandos
		
		
		

	:param  con: conector do banco de dados
		
		

	:param  acc: se 1 - indica para acumular, 0 ou outro valor que nao 1 - para
		nao acumular
		

.. function:: Sql.batch con


	:comentário: a partir deste comando, os comandos especificos para o conector constante
		dele sao acumulados Executa todos os comandos de uma vez so. Se retornar
		falso, um ou mais comandos derao errado. Resp retorna em sequencia a lista de
		comandos que deram certo
		
		
		

	:param  con: conectoro do banco de dados
		
		

	:param  resp: string de resposta que retorna a quantidade de linhas de tabelas
		inseridas ou updated em cada comando que deu certo
		
		
	:rtype: boolean  - falso, se o conector con nao for valido
		

.. function:: Sql.flush con resp


	:comentário: Faz o flush de comandos acumulados no batch Executa todos os comandos de uma
		vez so. Se retornar falso, um ou mais comandos derao errado. Resp retorna em
		sequencia a lista de comandos que deram certo
		
		
		

	:param  con: conectoro do banco de dados
		
		

	:param  resp: string de resposta que retorna a quantidade de linhas de tabelas
		inseridas ou updated em cada comando que deu certo
		
		
	:rtype: boolean  - falso, se o conector con nao for valido, ou entao se um ou mais
		comandos acumulados no batch derem errado
		

.. function:: Sql.ligaRegCmd liga


	:comentário: liga o registro dos comandos do sql se liga=1 para quaisquer outros valores,
		faz o flag ficar falso
		
		
		

	:param  liga: 
		
		
	:throws:  ErrBds
		

.. function:: Sql.stats selMedioOracle selMaxOracle sqlMedioOracle sqlMaxOracle selMedioMySql selMaxMySql sqlMedioMySql sqlMaxMySql


	:comentário: 
	:param selMedioOracle:
		
	:param selMaxOracle:
		
	:param sqlMedioOracle:
		
	:param sqlMaxOracle:
		
	:param selMedioMySql:
		
	:param selMaxMySql:
		
	:param sqlMedioMySql:
		
	:param sqlMaxMySql:

.. function:: Sql.codArg arg


	:comentário: Codifica arg, trocando virgulas que estejam entre parentesis por C.DEL1
		
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		
	:param arg:

.. function:: Sql.decodArg arg


	:comentário: decidufuca arg, trocando C.DEL1 de volta para "," (virgulas)
		
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		
	:param arg:

.. function:: Sql.getConBd con


	:comentário: localiza um conector de Banco de dados Disponivel
		
		
		

	:param  nomeBase: Nome da base (schema) desejado
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		

.. function:: Sql.libConBd con


	:comentário: libera um conector do vetor de conectores
		
		
		
	:rtype: boolean
		
		
	:throws:  ErrBds
		
	:param con:

.. function:: Sql.manutConBd 


	:comentário: 

