
Tray
====
.. function:: Tray.init 


	:comentário: Inicializa o icone do SuperClient no system tray utilizando a imagem scl.png
		presente no projeto
		
		
		
	:rtype: boolean
		

.. function:: Tray.init trayName trayImg


	:comentário: Inicializa o icone do SuperClient no system tray
		
		
		
	:rtype: boolean
		
		

	:param  trayName: 
		= Nome do Icone
		
		

	:param  trayImg: 
		= Imagem do Icone
		

.. function:: Tray.init trayName trayImg trayMessage


	:comentário: Inicializa o icone do SuperClient no system tray
		
		
		
	:rtype: boolean
		
		

	:param  trayName: 
		= Nome do Icone
		
		

	:param  trayImg: 
		= Imagem do Icone
		
		

	:param  trayMessage: 
		= Mensagem de popup ao inicializar o Tray
		

.. function:: Tray.msg msg


	:comentário: Exibe popup de mensagens
		
		
		
	:rtype: void
		
		

	:param  msg: 
		= Mensagem a ser exibida
		

.. function:: Tray.msg msg


	:comentário: 
	:param msg:

.. function:: Tray.msg msg type


	:comentário: Exibe popup de mensagens possibilitando tipagem
		
		Tipos: "I" - Informação "E" - Erro "A" - Atenção
		
		
		
	:rtype: void
		
		

	:param  msg: 
		= Mensagem a ser exibida
		
		

	:param  type: 
		= Tipo da mensagem
		

.. function:: Tray.msg msg type


	:comentário: 
	:param msg:
		
	:param type:

.. function:: Tray.msg titulo msg type


	:comentário: Exibe popup de mensagens possibilitando tipagem
		
		Tipos: "I" - Informação "E" - Erro "A" - Atenção
		
		
		
	:rtype: void
		
		

	:param  título: 
		= título da mensagem - obs. Titulo padrão: "SuperClient"
		
		

	:param  msg: 
		= Mensagem a ser exibida
		
		

	:param  type: 
		= Tipo da mensagem
		

.. function:: Tray.msg titulo msg type


	:comentário: 
	:param titulo:
		
	:param msg:
		
	:param type:

.. function:: Tray.criar trayName trayImg


	:comentário: método resposável por criar um novo icone do SuperClient
		
		
		
	:rtype: void
		
		

	:param  trayName: 
		= nome do tray
		
		

	:param  trayImg: 
		= icone do tray
		

.. function:: Tray.fechar 


	:comentário: método resposável por fechar o icone do SuperClient no tray
		
		
		
	:rtype: void
		

