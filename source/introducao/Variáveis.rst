
Variáveis
=========

As variáveis são estruturas que representam posições de memória, armazenando dados e os associando a um nome.

Declarando variáveis no BDJava
------------------------------

Declarar variáveis no BDJava é possível por meio do comando **faz**

Existem algumas regras para nomea-lás. Toda variável tem um nome e um valor, sendo que a declaração sempre deve ser feita da mesma maneira, inserindo o comando *faz*, seguido do nome da variável e do seu valor. Nomes podem conter números, mas não podem começar com um. Também são *case-sensitive*, diferenciando letras minúsculas e maiúsculas (*Variavel* é diferente de *variavel*).

.. code-block:: python

    faz variavel 1
    
O comando acima declar uma variável chamada *variavel* com um número inteiro de valor 1.


Também é possível armazenar cadeias de caracteres (textos), também conhecidos como strings. Strings devem ser declaradas dentro de aspas simples.

.. code-block:: python

    faz nome 'João'
    
Números decimais (ponto flutuante) são declarados utilizando um ponto para separar a parte inteira e fracionária.

.. code-block:: python

    faz valor 1.5
    
Exibindo o conteúdo de uma variável
-----------------------------------

É possível exibir o conteúdo de uma variável com o comando *exib*

.. code-block:: python

    faz foo 'Olá'
    exib foo

Operações com variáveis
-----------------------

A maior parte das operações com variáveis no BDJava envolvem operações aritméticas e concatenação (junção) de strings.

Aritmética deve ser feita dentro de parênteses. As operações de adição, subtração, divisão, multiplicação são permitidas.

.. code-block:: python

    faz dez 10
    faz vinte 20
    
    faz trinta (dez + vinte)
    faz dez (vinte - dez)
    faz duzentos (vinte * dez)
    faz dois (vinte / dez)

A concatenação de strings é feita simplesmente separando as strings a se concatenar com um espaço

.. code-block:: python

    faz foobar 'Concatenando' 'Strings'
    
Também funciona com variáveis

.. code-block:: python
    
    faz foo 'Concatenando'
    faz bar 'Strings'
    faz foobar foo bar

Isso resulta em uma variável contendo a string 'Concatenando Strings'
    
Escopo
------

Qualquer variável declarada dentro do BDJava tem escopo global, ficando acessível de qualquer script dentro da instância do SuperClient. Isso requer certa atenção por parte do desenvolvedor, pois a possibilidade de uma variável ser sobreescrita acidentalmente é muito grande, o que pode causar certos errors difíceis de serem detectados. Por exemplo, suponha um caso onde existem dois scripts BDJava, e em ambos é necessário guardar o nome de usuário em uma variável. 

Então, você declara uma variável chamada *nome* no primeiro script:

.. code-block:: python
    
    faz nome 'José'
    
Já no segundo script, é declarada uma variável também chamada *nome*:

.. code-block:: python
    
    faz nome 'João'
    
Já que ambas possuem o mesmo nome, o BDJava entende que essa é uma operação de atribuição a uma variável já existente, não a declaração de uma nova, gerando apenas uma variável chamada *nome* com o valor 'João'. Em alguns casos, isso poderia causar comportamentos estranhos nos scripts e seria difícil notar o erro.

