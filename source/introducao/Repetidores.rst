
Repetidores
===========
 
Os repetidores (ou laços de repetição) são estruturas que permitem a execução de instruções em número X de vezes.

while
-----

O *while* executa os comandos dentro das chaves enquanto a condição passada for verdadeira. O comando *while* só pode ser usado dentro de métodos do BDJava. Consulte a seção 'Métodos' dessa documentação para mais informações.

.. code-block:: python

    ()enquanto
        faz i 1
        
        while (i < 10) {
            exib i '\n'
            + i 1
        }

Esse código exibe todos os números entre 1 e 10. Começa com uma variável *i* com valor 1. Enquanto essa variável tem valor menor que 10, as instruções dentro das chaves são repetidas, exibindo o valor de *i* e incrementando-a. Repare que o *while* está contido em um método chamado *enquanto*.

O *while* também pode receber o retorno de um método.

.. code-block:: python

    while checaOk 'Clique no CANCELAR para sair do while' {
        alerta 'Executado while'
    }

O método checaOk abre uma janela de confirmação, com um botão 'OK' e outro 'CANCELAR', retornando VERDADEIRO quando o usuário clica em 'OK' e falso quando seleciona 'CANCELAR'. Esse *while* executa o método *alerta*, exibindo uma janela com a mensagem 'Executando while' enquanto o usuário clicar em 'OK'.
