
Condicionais
============

Os condicionais são um recurso da linguagem que permitem que o fluxo de execução do script mude diante de uma condição verdadeira ou falsa. 

if
--

Considere, por exemplo, que você deseja escrever um script BDJava que, com base na temperatura, diga se está calor. 

Tal funcionalidade pode ser implementada utilizando uma estrutura chamada *if*, que é o comando de decisão mais utilizado. O *if* do BDJava segue a seguinte sintaxe:

.. code-block:: python
    
    if CONDIÇÃO {
        COMANDO1
        COMANDO2
    }
    
Caso a condição do *if* seja VERDADEIRA, os comandos dentro das chaves são executados.

Sendo assim, poderíamos escrever o script da seguinte maneira:

.. code-block:: python
    
    faz temperatura 30
    
    if temperatura > 25 {
        alerta 'Está calor'
    }
    
Esse exemplo checa se o valor contido na variável *temperatura* é maior que 25. Essa é uma condição verdadeira, pois 30 é maior que 25, portanto o comando dentro das chaves é executado.

É possível executar comandos quando uma condição **não** for verdadeira, isso é feito com o *else*. O *else* sempre deve ser colocado no final de um *if*, também com os comandos delimitados por chaves. Por exemplo, se for necessário mostrar uma mensagem quando a temperatura for maior que 25 e outra quando ela não for maior, poderíamos usar um *else*.

.. code-block:: python
    
    faz temperatura 30
    
    if temperatura > 25 {
        alerta 'Está calor'
    } else {
        alerta 'Não está calor'
    }

ifnot
-----

Assim como o *if*, o condicional *ifnot* verifica uma condição, porém checando se essa é FALSA.

.. code-block:: python
    
    faz temperatura 20
    
    ifnot temperatura > 25 {
        alerta 'Não está calor'
    }
    
Esse exemplo executa o comando caso a temperatura não seja maior que 25, exibindo a mensagem 'Não está calor'.

Como o *if*, o *ifnot* aceita um *else*.

.. code-block:: python
    
    faz temperatura 20
    
    ifnot temperatura > 25 {
        alerta 'Não está calor'
    } else {
        alerta 'Está calor'
    }
    
Que, nesse caso, executa quando a temperatura for maior que 25.

switch
------

O *switch* permite que várias condições sejam testadas em apenas uma estrutura. 

Segue a sintaxe:

.. code-block:: python
    
    switch variavel {
        case CONDIÇÃO {
            COMANDO1
            COMANDO2
        }
    }

Recebendo uma variável e verificando se ela está dentro das condições nos comandos *case*. Caso exista apenas um comando para ser executado em um *case*, as chaves podem ser omitidas.

No exemplo abaixo, o *switch* verifica a variável *nome*. Os dois *case*'s verificam se ela possui valor 'José' e 'João', respectivamente, exibindo uma mensagem dependendo do nome. O *default* é acionado quando nenhuma condição do *switch* é satisfeita.

.. code-block:: python
    
    switch nome {
        case 'José' {
            exib 'O nome é José'
        }
        
        case 'João' {
            exib 'O nome é João'
        }
        
        default {
            exib 'Não sei o nome'
        }
    }

Um *case* pode receber várias opções:

.. code-block:: python
    
    switch nome {
        case 'José' 'João' {
            exib 'O nome é José ou João'
        }
        
        case 'Maria' {
            exib 'O nome é Maria'
        }
        
        default {
            exib 'Não sei o nome'
        }
    }

Assim, o primeiro *case* executaria quando o nome fosse 'José' ou 'João'. 


Operadores lógicos
------------------

O BDJava conta com os operadores lógicos E (&&) e OU (||). Podem ser usados em qualquer condicional, desde que entre parênteses.

E (&&)
^^^^^^

O operador E (AND) verifica se as duas condições são verdadeiras, retornando VERDADEIRO caso positivo. 

.. code-block:: python
    
    faz temperatura 27
    
    if (temperatura > 25 && temperatura < 30) {
        alerta 'Temperatura é maior que 25Cº E menor que 30Cº'
    }

Esse *if* verifica se variável *temperatura* está entre 25 E 30.

OU (||)
^^^^^^^

O operador OU (OR) verifica se pelomenos uma das condições é verdadeira, retornando VERDADEIRO caso positivo.

.. code-block:: python

    faz temperatura 25
    faz umidade 70
    
    if (temperatura > 20 || umidade > 80) {
        alerta 'Temperatura é maior que 20Cº OU umidade é maior que 80%"
    }


Nesse exemplo, apenas a primeira condição (temperatura maior que 20) é verdadeira, a segunda (umidade é maior que 80) é falsa, mesmo assim, o retorno é VERDADEIRO, pois a primeira condição é verdadeira.
